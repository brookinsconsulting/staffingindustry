<?php /* #?ini charset="utf-8"?

[AliasSettings]
AliasList[]
AliasList[]=small
AliasList[]=medium
AliasList[]=biomedium
AliasList[]=listitem
AliasList[]=articleimage
AliasList[]=articlethumbnail
AliasList[]=gallerythumbnail
AliasList[]=galleryline
AliasList[]=imagelarge
AliasList[]=large
AliasList[]=rss
AliasList[]=logo
AliasList[]=infoboximage
AliasList[]=billboard
AliasList[]=reference
AliasList[]=overlay_large
AliasList[]=newsletter
AliasList[]=newsletter_main
AliasList[]=newsletter_image_article


[small]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=100;160

[medium]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=200;290

[biomedium]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=100;150

[large]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=360;440

[rss]
Reference=
Filters[]
Filters[]=geometry/scale=88;31

[logo]
Reference=
Filters[]
Filters[]=geometry/scaleheight=36

[listitem]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=130;190

[articleimage]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=170;350

[articlethumbnail]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=70;150

[gallerythumbnail]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=105;100

[galleryline]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=70;150

[imagelarge]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=550;730

[infoboximage]
Reference=
Filters[]
Filters[]=geometry/scalewidth=75

[billboard]
Reference=
Filters[]
Filters[]=geometry/scalewidth=764

[overlay_large]
Reference=
Filters[]
Filters[]=geometry/scalewidthdownonly=980

[newsletter]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=50;50

[newsletter_main]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=90;90


[newsletter_image_article]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=180;180



*/ ?>
