<?php /* #?ini charset="utf-8"?

[full_splash_page]
Source=node/view/full.tpl
MatchFile=full/splash_page.tpl
Subdir=templates
Match[class_identifier]=splash_page

[splash_pagelayout]
Source=pagelayout.tpl
MatchFile=splash_pagelayout.tpl
Subdir=templates
Match[class_identifier]=splash_page

[line_topic]
Source=node/view/line.tpl
MatchFile=line/topic.tpl
Subdir=templates
Match[class_identifier]=topic

[full_topic]
Source=node/view/full.tpl
MatchFile=full/topic.tpl
Subdir=templates
Match[class_identifier]=topic

[embed_conference]
Source=content/view/embed.tpl
MatchFile=embed/conference.tpl
Subdir=templates
Match[class_identifier]=conference

[embed_large_file]
Source=content/view/embed.tpl
MatchFile=embed/large_file.tpl
Subdir=templates
Match[class_identifier]=large_file

[full_conference]
Source=node/view/full.tpl
MatchFile=full/conference.tpl
Subdir=templates
Match[class_identifier]=conference

[embed_topic_summary]
Source=content/view/embed.tpl
MatchFile=embed/topic.tpl
Subdir=templates
Match[class_identifier]=topic

[line_conference]
Source=node/view/line.tpl
MatchFile=line/conference.tpl
Subdir=templates
Match[class_identifier]=conference

[embed_blog_post]
Source=node/view/embed.tpl
MatchFile=embed/blog_post.tpl
Subdir=templates
Match[class_identifier]=blog_post

[embedline_blog_post]
Source=node/view/embedline.tpl
MatchFile=embedline/blog_post.tpl
Subdir=templates
Match[class_identifier]=blog_post

[full_enhanced_frontpage]
Source=node/view/full.tpl
MatchFile=full/enhanced_frontpage.tpl
Subdir=templates
Match[class_identifier]=enhanced_frontpage

[news_item_embed]
Source=content/view/embed.tpl
MatchFile=embed/news_item.tpl
Subdir=templates
Match[class_identifier]=news_item

[cws_daily_news_full]
Source=node/view/full.tpl
MatchFile=full/cws_daily_news.tpl
Subdir=template
Match[node]=40775

[store_full]
Source=node/view/full.tpl
MatchFile=full/store.tpl
Subdir=template
Match[node]=189

[botr_full]
Source=node/view/full.tpl
MatchFile=full/botr.tpl
Subdir=template
Match[class_identifier]=botr

[member_blogs_full]
Source=node/view/full.tpl
MatchFile=full/member_blogs.tpl
Subdir=template
Match[node]=340

[staffing_user_full]
Source=node/view/full.tpl
MatchFile=full/staffing_user.tpl
Subdir=templates
Match[class_identifier]=staffing_user

[staffing_user_line]
Source=node/view/line.tpl
MatchFile=line/staffing_user.tpl
Subdir=templates
Match[class_identifier]=staffing_user

[user_full]
Source=node/view/full.tpl
MatchFile=full/user.tpl
Subdir=templates
Match[class_identifier]=user

[user_line]
Source=node/view/line.tpl
MatchFile=line/user.tpl
Subdir=templates
Match[class_identifier]=user

[xrow_client_edit]
Source=content/edit.tpl
MatchFile=edit/user.tpl
Subdir=templates
Match[class_identifier]=client

[xrow_manufacturer_full]
Source=node/view/full.tpl
MatchFile=full/xrow_manufacturer.tpl
Subdir=templates
Match[class_identifier]=manufacturer

[xrow_manufacturer_line]
Source=node/view/line.tpl
MatchFile=line/xrow_manufacturer.tpl
Subdir=templates
Match[class_identifier]=manufacturer

[edit_xrow_product_review]
Source=content/edit.tpl
MatchFile=edit/xrow_product_review.tpl
Subdir=templates
Match[class_identifier]=xrow_product_review

[line_xrow_product_category]
Source=node/view/line.tpl
MatchFile=line/xrow_product_category.tpl
Subdir=templates
Match[class_identifier]=xrow_product_category

[embed_botr]
Source=content/view/embed.tpl
MatchFile=embed/botr.tpl
Subdir=templates
Match[class_identifier]=botr

[content_full_botr]
Source=content/view/full.tpl
MatchFile=embed/botr.tpl
Subdir=templates
Match[class_identifier]=botr

[embed_xrow_product_category]
Source=content/view/embed.tpl
MatchFile=embed/xrow_product_category.tpl
Subdir=templates
Match[class_identifier]=xrow_product_category

[full_xrow_product_category]
Source=node/view/full.tpl
MatchFile=full/xrow_product_category.tpl
Subdir=templates
Match[class_identifier]=xrow_product_category

[full_xrow_product]
Source=node/view/full.tpl
MatchFile=full/xrow_product.tpl
Subdir=templates
Match[class_identifier]=xrow_product

[embed_xrow_product]
Source=content/view/embed.tpl
MatchFile=embed/xrow_product.tpl
Subdir=templates
Match[class_identifier]=xrow_product

[line_xrow_product]
Source=node/view/line.tpl
MatchFile=line/xrow_product.tpl
Subdir=templates
Match[class_identifier]=xrow_product

[listitem_blog_post]
Source=node/view/listitem.tpl
MatchFile=listitem/blog_post.tpl
Subdir=templates
Match[class_identifier]=blog_post

[full_botr]
Source=node/view/full.tpl
MatchFile=full/botr.tpl
Subdir=templates
Match[class_identifier]=botr

[listitem_event]
Source=node/view/listitem.tpl
MatchFile=listitem/event.tpl
Subdir=templates
Match[class_identifier]=event

[listitem_news_item]
Source=node/view/listitem.tpl
MatchFile=listitem/news_item.tpl
Subdir=templates
Match[class_identifier]=news_item

[embed_alert]
Source=content/view/embed.tpl
MatchFile=embed/alert.tpl
Subdir=templates
Match[class_identifier]=alert

[embed_issue]
Source=node/view/embed.tpl
MatchFile=embed/issue.tpl
Subdir=templates
Match[class_identifier]=issue

[node_embed_article]
Source=node/view/embed.tpl
MatchFile=embed/article.tpl
Subdir=templates
Match[class_identifier]=article


[full_article]
Source=node/view/full.tpl
MatchFile=full/article.tpl
Subdir=templates
Match[class_identifier]=article

[full_geo_article]
Source=node/view/full.tpl
MatchFile=full/geo_article.tpl
Subdir=templates
Match[class_identifier]=geo_article

[full_article_mainpage]
Source=node/view/full.tpl
MatchFile=full/article_mainpage.tpl
Subdir=templates
Match[class_identifier]=article_mainpage

[full_article_subpage]
Source=node/view/full.tpl
MatchFile=full/article_subpage.tpl
Subdir=templates
Match[class_identifier]=article_subpage

[full_banner]
Source=node/view/full.tpl
MatchFile=full/banner.tpl
Subdir=templates
Match[class_identifier]=banner

[full_blog]
Source=node/view/full.tpl
MatchFile=full/blog.tpl
Subdir=templates
Match[class_identifier]=blog

[full_blog_post]
Source=node/view/full.tpl
MatchFile=full/blog_post.tpl
Subdir=templates
Match[class_identifier]=blog_post

[full_comment]
Source=node/view/full.tpl
MatchFile=full/comment.tpl
Subdir=templates
Match[class_identifier]=comment

[full_documentation_page]
Source=node/view/full.tpl
MatchFile=full/documentation_page.tpl
Subdir=templates
Match[class_identifier]=documentation_page

[full_event_calendar]
Source=node/view/full.tpl
MatchFile=full/event_calendar.tpl
Subdir=templates
Match[class_identifier]=event_calendar

[full_event]
Source=node/view/full.tpl
MatchFile=full/event.tpl
Subdir=templates
Match[class_identifier]=event

[full_feedback_form]
Source=node/view/full.tpl
MatchFile=full/feedback_form.tpl
Subdir=templates
Match[class_identifier]=feedback_form

[full_file]
Source=node/view/full.tpl
MatchFile=full/file.tpl
Subdir=templates
Match[class_identifier]=file

[full_flash]
Source=node/view/full.tpl
MatchFile=full/flash.tpl
Subdir=templates
Match[class_identifier]=flash

[full_folder]
Source=node/view/full.tpl
MatchFile=full/folder.tpl
Subdir=templates
Match[class_identifier]=folder

[full_forum]
Source=node/view/full.tpl
MatchFile=full/forum.tpl
Subdir=templates
Match[class_identifier]=forum

[full_forum_reply]
Source=node/view/full.tpl
MatchFile=full/forum_reply.tpl
Subdir=templates
Match[class_identifier]=forum_reply

[full_forum_topic]
Source=node/view/full.tpl
MatchFile=full/forum_topic.tpl
Subdir=templates
Match[class_identifier]=forum_topic

[full_forums]
Source=node/view/full.tpl
MatchFile=full/forums.tpl
Subdir=templates
Match[class_identifier]=forums

[full_frontpage]
Source=node/view/full.tpl
MatchFile=full/frontpage.tpl
Subdir=templates
Match[class_identifier]=frontpage

[full_gallery]
Source=node/view/full.tpl
MatchFile=full/gallery.tpl
Subdir=templates
Match[class_identifier]=gallery

[full_image]
Source=node/view/full.tpl
MatchFile=full/image.tpl
Subdir=templates
Match[class_identifier]=image

[full_infobox]
Source=node/view/full.tpl
MatchFile=full/infobox.tpl
Subdir=templates
Match[class_identifier]=infobox

[full_issue]
Source=node/view/full.tpl
MatchFile=full/issue.tpl
Subdir=templates
Match[class_identifier]=issue

[full_link]
Source=node/view/full.tpl
MatchFile=full/link.tpl
Subdir=templates
Match[class_identifier]=link

[full_multicalendar]
Source=node/view/full.tpl
MatchFile=full/multicalendar.tpl
Subdir=templates
Match[class_identifier]=multicalendar

[full_news_item]
Source=node/view/full.tpl
MatchFile=full/news_item.tpl
Subdir=templates
Match[class_identifier]=news_item

[full_poll]
Source=node/view/full.tpl
MatchFile=full/poll.tpl
Subdir=templates
Match[class_identifier]=poll

[full_product]
Source=node/view/full.tpl
MatchFile=full/product.tpl
Subdir=templates
Match[class_identifier]=product

[full_quicktime]
Source=node/view/full.tpl
MatchFile=full/quicktime.tpl
Subdir=templates
Match[class_identifier]=quicktime

[full_real_video]
Source=node/view/full.tpl
MatchFile=full/real_video.tpl
Subdir=templates
Match[class_identifier]=real_video

[full_silverlight]
Source=node/view/full.tpl
MatchFile=full/silverlight.tpl
Subdir=templates
Match[class_identifier]=silverlight

[full_windows_media]
Source=node/view/full.tpl
MatchFile=full/windows_media.tpl
Subdir=templates
Match[class_identifier]=windows_media

[line_article]
Source=node/view/line.tpl
MatchFile=line/article.tpl
Subdir=templates
Match[class_identifier]=article

[line_geo_article]
Source=node/view/line.tpl
MatchFile=line/geo_article.tpl
Subdir=templates
Match[class_identifier]=geo_article

[line_article_mainpage]
Source=node/view/line.tpl
MatchFile=line/article_mainpage.tpl
Subdir=templates
Match[class_identifier]=article_mainpage

[line_article_subpage]
Source=node/view/line.tpl
MatchFile=line/article_subpage.tpl
Subdir=templates
Match[class_identifier]=article_subpage

[line_banner]
Source=node/view/line.tpl
MatchFile=line/banner.tpl
Subdir=templates
Match[class_identifier]=banner

[line_blog]
Source=node/view/line.tpl
MatchFile=line/blog.tpl
Subdir=templates
Match[class_identifier]=blog

[line_blog_post]
Source=node/view/line.tpl
MatchFile=line/blog_post.tpl
Subdir=templates
Match[class_identifier]=blog_post

[line_comment]
Source=node/view/line.tpl
MatchFile=line/comment.tpl
Subdir=templates
Match[class_identifier]=comment

[line_documentation_page]
Source=node/view/line.tpl
MatchFile=line/documentation_page.tpl
Subdir=templates
Match[class_identifier]=documentation_page

[line_event_calendar]
Source=node/view/line.tpl
MatchFile=line/event_calendar.tpl
Subdir=templates
Match[class_identifier]=event_calendar

[line_event]
Source=node/view/line.tpl
MatchFile=line/event.tpl
Subdir=templates
Match[class_identifier]=event

[line_feedback_form]
Source=node/view/line.tpl
MatchFile=line/feedback_form.tpl
Subdir=templates
Match[class_identifier]=feedback_form

[line_file]
Source=node/view/line.tpl
MatchFile=line/file.tpl
Subdir=templates
Match[class_identifier]=file

[line_flash]
Source=node/view/line.tpl
MatchFile=line/flash.tpl
Subdir=templates
Match[class_identifier]=flash

[line_folder]
Source=node/view/line.tpl
MatchFile=line/folder.tpl
Subdir=templates
Match[class_identifier]=folder

[line_forum]
Source=node/view/line.tpl
MatchFile=line/forum.tpl
Subdir=templates
Match[class_identifier]=forum

[line_forum_reply]
Source=node/view/line.tpl
MatchFile=line/forum_reply.tpl
Subdir=templates
Match[class_identifier]=forum_reply

[line_forum_topic]
Source=node/view/line.tpl
MatchFile=line/forum_topic.tpl
Subdir=templates
Match[class_identifier]=forum_topic

[line_forums]
Source=node/view/line.tpl
MatchFile=line/forums.tpl
Subdir=templates
Match[class_identifier]=forums

[line_gallery]
Source=node/view/line.tpl
MatchFile=line/gallery.tpl
Subdir=templates
Match[class_identifier]=gallery

[line_image]
Source=node/view/line.tpl
MatchFile=line/image.tpl
Subdir=templates
Match[class_identifier]=image

[line_infobox]
Source=node/view/line.tpl
MatchFile=line/infobox.tpl
Subdir=templates
Match[class_identifier]=infobox

[line_issue]
Source=node/view/line.tpl
MatchFile=line/issue.tpl
Subdir=templates
Match[class_identifier]=issue

[line_link]
Source=node/view/line.tpl
MatchFile=line/link.tpl
Subdir=templates
Match[class_identifier]=link

[line_multicalendar]
Source=node/view/line.tpl
MatchFile=line/multicalendar.tpl
Subdir=templates
Match[class_identifier]=multicalendar

[line_news_item]
Source=node/view/line.tpl
MatchFile=line/news_item.tpl
Subdir=templates
Match[class_identifier]=news_item

[line_poll]
Source=node/view/line.tpl
MatchFile=line/poll.tpl
Subdir=templates
Match[class_identifier]=poll

[line_product]
Source=node/view/line.tpl
MatchFile=line/product.tpl
Subdir=templates
Match[class_identifier]=product

[line_silverlight]
Source=node/view/line.tpl
MatchFile=line/silverlight.tpl
Subdir=templates
Match[class_identifier]=silverlight

[line_quicktime]
Source=node/view/line.tpl
MatchFile=line/quicktime.tpl
Subdir=templates
Match[class_identifier]=quicktime

[line_real_video]
Source=node/view/line.tpl
MatchFile=line/real_video.tpl
Subdir=templates
Match[class_identifier]=real_video

[line_windows_media]
Source=node/view/line.tpl
MatchFile=line/windows_media.tpl
Subdir=templates
Match[class_identifier]=windows_media

[edit_comment]
Source=content/edit.tpl
MatchFile=edit/comment.tpl
Subdir=templates
Match[class_identifier]=comment

[edit_file]
Source=content/edit.tpl
MatchFile=edit/file.tpl
Subdir=templates
Match[class_identifier]=file

[edit_forum_topic]
Source=content/edit.tpl
MatchFile=edit/forum_topic.tpl
Subdir=templates
Match[class_identifier]=forum_topic

[edit_ezsubtreesubscription_forum_topic]
Source=content/datatype/edit/ezsubtreesubscription.tpl
MatchFile=datatype/edit/ezsubtreesubscription/forum_topic.tpl
Subdir=templates
Match[class_identifier]=forum_topic

[edit_forum_reply]
Source=content/edit.tpl
MatchFile=edit/forum_reply.tpl
Subdir=templates
Match[class_identifier]=forum_reply

[highlighted_object]
Source=content/view/embed.tpl
MatchFile=embed/highlighted_object.tpl
Subdir=templates
Match[classification]=highlighted_object

[embed_article]
Source=content/view/embed.tpl
MatchFile=content/article.tpl
Subdir=templates
Match[class_identifier]=article

[embed_banner]
Source=content/view/embed.tpl
MatchFile=embed/banner.tpl
Subdir=templates
Match[class_identifier]=banner

[embed_file]
Source=content/view/embed.tpl
MatchFile=embed/file.tpl
Subdir=templates
Match[class_identifier]=file

[embed_flash]
Source=content/view/embed.tpl
MatchFile=embed/flash.tpl
Subdir=templates
Match[class_identifier]=flash

[itemized_sub_items]
Source=content/view/embed.tpl
MatchFile=embed/itemized_sub_items.tpl
Subdir=templates
Match[classification]=itemized_sub_items

[vertically_listed_sub_items]
Source=content/view/embed.tpl
MatchFile=embed/vertically_listed_sub_items.tpl
Subdir=templates
Match[classification]=vertically_listed_sub_items

[horizontally_listed_sub_items]
Source=content/view/embed.tpl
MatchFile=embed/horizontally_listed_sub_items.tpl
Subdir=templates
Match[classification]=horizontally_listed_sub_items

[itemized_subtree_items]
Source=content/view/embed.tpl
MatchFile=embed/itemized_subtree_items.tpl
Subdir=templates
Match[classification]=itemized_subtree_items

[embed_folder]
Source=content/view/embed.tpl
MatchFile=embed/folder.tpl
Subdir=templates
Match[class_identifier]=folder

[embed_forum]
Source=content/view/embed.tpl
MatchFile=embed/forum.tpl
Subdir=templates
Match[class_identifier]=forum

[embed_gallery]
Source=content/view/embed.tpl
MatchFile=embed/gallery.tpl
Subdir=templates
Match[class_identifier]=gallery

[embed_image]
Source=content/view/embed.tpl
MatchFile=embed/image.tpl
Subdir=templates
Match[class_identifier]=image

[embed_poll]
Source=content/view/embed.tpl
MatchFile=embed/poll.tpl
Subdir=templates
Match[class_identifier]=poll

[embed_product]
Source=content/view/embed.tpl
MatchFile=embed/product.tpl
Subdir=templates
Match[class_identifier]=product

[embed_quicktime]
Source=content/view/embed.tpl
MatchFile=embed/quicktime.tpl
Subdir=templates
Match[class_identifier]=quicktime

[embed_real_video]
Source=content/view/embed.tpl
MatchFile=embed/real_video.tpl
Subdir=templates
Match[class_identifier]=real_video

[embed_windows_media]
Source=content/view/embed.tpl
MatchFile=embed/windows_media.tpl
Subdir=templates
Match[class_identifier]=windows_media

[embed_inline_image]
Source=content/view/embed-inline.tpl
MatchFile=embed-inline/image.tpl
Subdir=templates
Match[class_identifier]=image

[embed_itemizedsubitems_gallery]
Source=content/view/itemizedsubitems.tpl
MatchFile=itemizedsubitems/gallery.tpl
Subdir=templates
Match[class_identifier]=gallery

[embed_itemizedsubitems_forum]
Source=content/view/itemizedsubitems.tpl
MatchFile=itemizedsubitems/forum.tpl
Subdir=templates
Match[class_identifier]=forum

[embed_itemizedsubitems_folder]
Source=content/view/itemizedsubitems.tpl
MatchFile=itemizedsubitems/folder.tpl
Subdir=templates
Match[class_identifier]=folder

[embed_itemizedsubitems_event_calendar]
Source=content/view/itemizedsubitems.tpl
MatchFile=itemizedsubitems/event_calendar.tpl
Subdir=templates
Match[class_identifier]=event_calendar

[embed_itemizedsubitems_documentation_page]
Source=content/view/itemizedsubitems.tpl
MatchFile=itemizedsubitems/documentation_page.tpl
Subdir=templates
Match[class_identifier]=documentation_page

[embed_itemizedsubitems_itemized_sub_items]
Source=content/view/itemizedsubitems.tpl
MatchFile=itemizedsubitems/itemized_sub_items.tpl
Subdir=templates

[embed_event_calendar]
Source=content/view/embed.tpl
MatchFile=embed/event_calendar.tpl
Subdir=templates
Match[class_identifier]=event_calendar

[embed_horizontallylistedsubitems_article]
Source=node/view/horizontallylistedsubitems.tpl
MatchFile=horizontallylistedsubitems/article.tpl
Subdir=templates
Match[class_identifier]=article

[embed_horizontallylistedsubitems_event]
Source=node/view/horizontallylistedsubitems.tpl
MatchFile=horizontallylistedsubitems/event.tpl
Subdir=templates
Match[class_identifier]=event

[embed_horizontallylistedsubitems_image]
Source=node/view/horizontallylistedsubitems.tpl
MatchFile=horizontallylistedsubitems/image.tpl
Subdir=templates
Match[class_identifier]=image

[embed_horizontallylistedsubitems_product]
Source=node/view/horizontallylistedsubitems.tpl
MatchFile=horizontallylistedsubitems/product.tpl
Subdir=templates
Match[class_identifier]=product

[factbox]
Source=content/datatype/view/ezxmltags/factbox.tpl
MatchFile=datatype/ezxmltext/factbox.tpl
Subdir=templates

[quote]
Source=content/datatype/view/ezxmltags/quote.tpl
MatchFile=datatype/ezxmltext/quote.tpl
Subdir=templates

[table_cols]
Source=content/datatype/view/ezxmltags/table.tpl
MatchFile=datatype/ezxmltext/table_cols.tpl
Subdir=templates
Match[classification]=cols

[table_comparison]
Source=content/datatype/view/ezxmltags/table.tpl
MatchFile=datatype/ezxmltext/table_comparison.tpl
Subdir=templates
Match[classification]=comparison

[image_galleryline]
Source=node/view/galleryline.tpl
MatchFile=galleryline/image.tpl
Subdir=templates
Match[class_identifier]=image

[image_galleryslide]
Source=node/view/galleryslide.tpl
MatchFile=galleryslide/image.tpl
Subdir=templates
Match[class_identifier]=image

[article_listitem]
Source=node/view/listitem.tpl
MatchFile=listitem/article.tpl
Subdir=templates
Match[class_identifier]=article

[image_listitem]
Source=node/view/listitem.tpl
MatchFile=listitem/image.tpl
Subdir=templates
Match[class_identifier]=image

[billboard_banner]
Source=content/view/billboard.tpl
MatchFile=billboard/banner.tpl
Subdir=templates
Match[class_identifier]=banner

[billboard_flash]
Source=content/view/billboard.tpl
MatchFile=billboard/flash.tpl
Subdir=templates
Match[class_identifier]=flash

[tiny_image]
Source=content/view/tiny.tpl
MatchFile=tiny_image.tpl
Subdir=templates
Match[class_identifier]=image

[embed_user]
Source=content/view/embed.tpl
MatchFile=embed/user.tpl
Subdir=templates
Match[class_identifier]=user

*/ ?>