<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=Staffing Industry Analysts
SiteURL=siadev.thinkcreative.com/index.php/eng
LoginPage=embedded
AdditionalLoginFormActionURL=http://siadev.thinkcreative.com/index.php/site_admin/user/login

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=true
RelatedSiteAccessList[]
RelatedSiteAccessList[]=site
RelatedSiteAccessList[]=eng
RelatedSiteAccessList[]=row
RelatedSiteAccessList[]=site_member
RelatedSiteAccessList[]=eng_member
RelatedSiteAccessList[]=row_member
RelatedSiteAccessList[]=site_admin
ShowHiddenNodes=false

[DesignSettings]
SiteDesign=member
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=site
AdditionalSiteDesignList[]=xrowecommerce
AdditionalSiteDesignList[]=xrowforum
AdditionalSiteDesignList[]=framework
AdditionalSiteDesignList[]=ezwebin
AdditionalSiteDesignList[]=base
AdditionalSiteDesignList[]=admin2

[RegionalSettings]
Locale=eng-GB@euro
ContentObjectLocale=eng-GB@euro
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=eng-GB@euro
TextTranslation=disabled

[FileSettings]
VarDir=var/ezwebin_site

[ContentSettings]
TranslationList=eng-GB@euro;eng-RW;eng-US

[MailSettings]
AdminEmail=mark@thinkcreative.com
EmailSender=
ContentType=text/html

[OutputSettings]
OutputFilterName=xrowCDNFilter
*/ ?>
