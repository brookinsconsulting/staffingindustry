<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=Staffing Industry Analysts
SiteURL=siadev.thinkcreative.com/index.php
DefaultPage=content/dashboard
LoginPage=custom

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=true
RelatedSiteAccessList[]=site
RelatedSiteAccessList[]=eng
RelatedSiteAccessList[]=row
RelatedSiteAccessList[]=site_member
RelatedSiteAccessList[]=eng_member
RelatedSiteAccessList[]=row_member
RelatedSiteAccessList[]=site_admin
ShowHiddenNodes=true

[DesignSettings]
SiteDesign=site_admin
AdditionalSiteDesignList[]=xrowecommerce_site_admin
AdditionalSiteDesignList[]=admin2
AdditionalSiteDesignList[]=xrowforum
AdditionalSiteDesignList[]=admin


[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=enabled
SiteLanguageList[]=eng-US
SiteLanguageList[]=eng-GB@euro
SiteLanguageList[]=eng-RW
TextTranslation=enabled

[FileSettings]
VarDir=var/ezwebin_site

[ContentSettings]
CachedViewPreferences[full]=admin_navigation_content=1;admin_children_viewmode=list;admin_list_limit=1
TranslationList=eng-GB@euro;eng-RW;eng-US

[MailSettings]
AdminEmail=mark@thinkcreative.com
EmailSender=
ContentType=text/html
*/ ?>
