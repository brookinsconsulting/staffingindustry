<?php /* #?ini charset="utf-8"?

[ClassIDs]
ForumTopic=38
ForumReply=39
User=4

[IDs]
ForumIndexPageNodeID=440
ModeratorGroupObjectID=12

[GeneralSettings]
Rankings=enabled
WordToImage=enabled
SignatureImage=disabled
TopicsPerPage=20
PostsPerPage=20
PostHistoryLimit=20
HotTopicNumber=50
StatisticLimit=5

[BB-Codes]
BBCodeList[]
BBCodeList[big]=enabled
BBCodeList[italic]=enabled
BBCodeList[underline]=enabled
BBCodeList[quote]=enabled
BBCodeList[code]=enabled
BBCodeList[list]=enabled
BBCodeList[strike]=enabled
BBCodeList[img]=enabled
BBCodeList[url]=enabled
BBCodeList[fontcolor]=enabled
BBCodeList[fontsize]=enabled
BBCodeList[smilies]=enabled

[MostUserOn]
Amount=0
Date=
*/ ?>