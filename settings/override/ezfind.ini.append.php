<?php /* #?ini charset="utf-8"?

[LanguageSearch]
SearchMainLanguageOnly=disabled
MultiCore=enabled
DefaultCore=sia
LanguagesCoresMap[]
LanguagesCoresMap[eng-US]=sia
LanguagesCoresMap[eng-RW]=sia_row
LanguagesCoresMap[eng-GB@euro]=sia_gb


[IndexOptions]
OptimizeOnCommit=disabled

[IndexBoost]
Class[]
Class[article]=3.0
Class[xrow_product]=2.5
Class[issue]=2.0
Class[blog]=1.5
Class[blog_post]=1.5
Class[news_item]=0.8
Class[forum_topic]=0.8
Class[forum_reply]=0.8

Attribute[]
Attribute[name]=2
Attribute[title]=2
Attribute[publish_date]=var

Datatype[]
Datatype[ezkeyword]=2.0

ReverseRelatedScale=0.5

[IndexExclude]
ClassIdentifierList[]
ClassIdentifierList[]=ad_zone
ClassIdentifierList[]=banner
ClassIdentifierList[]=coupon
ClassIdentifierList[]=footer
ClassIdentifierList[]=nav_divider
ClassIdentifierList[]=site_sidebar

*/ ?>