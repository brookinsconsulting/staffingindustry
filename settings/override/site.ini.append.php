<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezmysql
Server=ccaurora-cluster.cluster-cxdweddzhatd.us-east-1.rds.amazonaws.com
; Port=33102
User=sia_ezp
Password=thinksia
Database=staffingindustry
Charset=
; Socket=/var/run/mysqld/mysqld-david.sock
SQLOutput=disabled
; do not enabled UseSlaveServer.
; http://project.issues.ez.no/IssueView.php?Id=9282&ProjectId=2640&Anchor=Comment44994
;UseSlaveServer=enabled
SlaveServerArray[]=dbslave.thinkcreativeinternal.net
SlaveServerPort[]=
SlaverServerUser[]=sia_ezp
SlaverServerPassword[]=thinksia
SlaverServerDatabase[]=sia_ezp

[ExtensionSettings]
ActiveExtensions[]=site
#ActiveExtensions[]=acceptcookies
ActiveExtensions[]=googlecharts
ActiveExtensions[]=xrowecommerce
ActiveExtensions[]=tcnewsletter
ActiveExtensions[]=framework
ActiveExtensions[]=admin2pp
ActiveExtensions[]=xrowforum
ActiveExtensions[]=oworfilter
ActiveExtensions[]=enhancedselection
ActiveExtensions[]=ezmultiupload
ActiveExtensions[]=ezjscore
ActiveExtensions[]=captcha
ActiveExtensions[]=ezwt
ActiveExtensions[]=ezstarrating
ActiveExtensions[]=ezgmaplocation
ActiveExtensions[]=ezwebin
ActiveExtensions[]=ezie
ActiveExtensions[]=ezoe
ActiveExtensions[]=ez_network
ActiveExtensions[]=ezcomments
ActiveExtensions[]=ezfarchive
ActiveExtensions[]=ezfind
ActiveExtensions[]=ezsurvey
ActiveExtensions[]=ezscriptmonitor
ActiveExtensions[]=moduletools
ActiveExtensions[]=sitelink
ActiveExtensions[]=sitemap
ActiveExtensions[]=multimedia
ActiveExtensions[]=moveuserafterreg
ActiveExtensions[]=addnewgrouproles
ActiveExtensions[]=sia
ActiveExtensions[]=ezauthorize
ActiveExtensions[]=machform
ActiveExtensions[]=twitter
ActiveExtensions[]=cybersource
ActiveExtensions[]=tchosting
ActiveExtensions[]=ggsysinfo
ActiveExtensions[]=ezphttprequest
ActiveExtensions[]=akismet
ActiveExtensions[]=searchindexrelatedbinaries
ActiveExtensions[]=extract
ActiveExtensions[]=xrowcdn
ActiveExtensions[]=ezs3upload


[Session]
SessionNameHandler=custom
SessionNamePerSiteAccess=disabled
Handler=ezpSessionHandlerDB
SlowQueriesOutput=500
SessionTimeout=2592000

[SiteSettings]
DefaultAccess=site
SiteList[]=site
SiteList[]=eng
SiteList[]=row
SiteList[]=site_member
SiteList[]=eng_member
SiteList[]=row_member
SiteList[]=site_admin
SiteList[]=news
RootNodeDepth=1
SiteURL=www2.staffingindustry.com/
AdditionalLoginFormActionURL=http://www2.staffingindustry.com/site_admin/user/login
DefaultPage=/content/dashboard

[UserSettings]
RedirectOnLogoutWithLastAccessURI=disabled
LogoutRedirect=/wp/logout_redirect
UserClassID=56
LoginRedirectionUriAttribute[group]=start_page

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]=site
AvailableSiteAccessList[]=eng
AvailableSiteAccessList[]=row
AvailableSiteAccessList[]=site_member
AvailableSiteAccessList[]=eng_member
AvailableSiteAccessList[]=row_member
AvailableSiteAccessList[]=site_admin
AvailableSiteAccessList[]=news
MatchOrder=uri
HostMatchMapItems[]

[DesignSettings]
DesignLocationCache=enabled

[FileSettings]
VarDir=var/ezwebin_site

[MailSettings]
Transport=SMTP
TransportServer=smtp.postmarkapp.com
TransportConnectionType=
TransportPort=25
TransportUser=6b5cc2ae-9f51-471d-a5d7-e386c4088b22
TransportPassword=6b5cc2ae-9f51-471d-a5d7-e386c4088b22

;AdminEmail=root@thinkcreative.com
AdminEmail=memberservices@staffingindustry.com
EmailSender=memberservices@staffingindustry.com

[EmbedViewModeSettings]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]=embed-inline

[SearchSettings]
LogSearchStats=disabled

[DebugSettings]
DebugOutput=enabled
DebugByUser=enabled
DebugRedirection=disabled

[TemplateSettings]
Debug=disabled
ShowXHTMLCode=enabled
ShowUsedTemplates=enabled

[RegionalSettings]
TranslationCache=disabled

[HTTPHeaderSettings]
CustomHeader=enabled
HeaderList[]=X-Robots-Tag
X-Robots-Tag[]
X-Robots-Tag[/Miscellaneous]=noindex;3

[OutputSettings]
;OutputFilterName=xrowCDNFilter

[TipAFriend]
FromEmail=memberservices@staffingindustry.com

[MachFormCaptcha]
CaptchaPublicKey=6LcbTeYSAAAAAHlhlYVmvR7LvpDjs_jpnpmaywtc
CaptchaPrivateKey=6LcbTeYSAAAAAFbLy6Vfbh926BAGoQTYnsVxL3py

*/ ?>
