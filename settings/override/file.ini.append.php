<?php /* #?ini charset="utf-8"?

[BinaryFileSettings]
Handler=eZFilePassthroughHandler

[ArchiveSettings]
Handlers[tar]=eztararchivehandler

[FileSettings]
Handlers[gzip]=ezgzipcompressionhandler
Handlers[gzipzlib]=ezgzipzlibcompressionhandler
Handlers[gzipshell]=ezgzipshellcompressionhandler

[ClusteringSettings]
FileHandler=eZDFSFileHandler
DBBackend=eZDBFileHandlerMysqlBackend
DBHost=db
DBPort=3306
DBSocket=
DBName=cluster
DBUser=root
DBPassword=
DBChunkSize=65535
DBConnectRetries=3
DBExecuteRetries=20
NonExistantStaleCacheHandling[viewcache]=wait
NonExistantStaleCacheHandling[cacheblock]=wait
NonExistantStaleCacheHandling[misc]=wait

[eZDFSClusteringSettings]
MountPointPath=mount
DBBackend=eZDFSFileHandlerMySQLBackend
DBHost=ccaurora-cluster.cluster-cxdweddzhatd.us-east-1.rds.amazonaws.com
DBPort=3306
DBSocket=
DBName=staffingindustry_cluster
DBUser=sia_ezp
DBPassword=thinksia
DBConnectRetries=3
DBExecuteRetries=20

[PassThroughSettings]
ContentDisposition[application/x-shockwave-flash]=inline
ContentDisposition[video/ogg]=inline
ContentDisposition[video/mp4]=inline
ContentDisposition[video/webm]=inline
*/ ?>