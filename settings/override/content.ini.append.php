<?php /* #?ini charset="utf-8"?

[ChildrenNodeList]
ExcludedClasses[]=ad_zone

[CustomTagSettings]
AvailableCustomTags[]=underline
IsInline[underline]=true

[factbox]
CustomAttributes[]=align
CustomAttributes[]=title
CustomAttributesDefaults[align]=right
CustomAttributesDefaults[title]=factbox

[table]
AvailableClasses[]=list
AvailableClasses[]=cols
AvailableClasses[]=comparison
AvailableClasses[]=default
AvailableClasses[]=plain
CustomAttributes[]=summary
CustomAttributes[]=caption
ClassDescription[list]=List
ClassDescription[cols]=Timetable
ClassDescription[comparison]=Comparison Table
ClassDescription[default]=Default
ClassDescription[plain]=Plain
Defaults[rows]=2
Defaults[cols]=2
Defaults[width]=100%
Defaults[border]=0
Defaults[class]=default

[td]
CustomAttributes[]=valign

[th]
CustomAttributes[]=scope
CustomAttributes[]=abbr
CustomAttributes[]=valign

[object]
AvailableClasses[]=itemized_sub_items
AvailableClasses[]=itemized_subtree_items
AvailableClasses[]=highlighted_object
AvailableClasses[]=vertically_listed_sub_items
AvailableClasses[]=horizontally_listed_sub_items
CustomAttributes[]=offset
CustomAttributes[]=limit
ClassDescription[itemized_sub_items]=Itemized Sub Items
ClassDescription[itemized_subtree_items]=Itemized Subtree Items
ClassDescription[highlighted_object]=Highlighted Object
ClassDescription[vertically_listed_sub_items]=Vertically Listed Sub Items
ClassDescription[horizontally_listed_sub_items]=Horizontally Listed Sub Items
CustomAttributesDefaults[offset]=0
CustomAttributesDefaults[limit]=5

[embed]
AvailableClasses[]=itemized_sub_items
AvailableClasses[]=itemized_subtree_items
AvailableClasses[]=highlighted_object
AvailableClasses[]=vertically_listed_sub_items
AvailableClasses[]=horizontally_listed_sub_items
AvailableClasses[]=image_no_border
AvailableClasses[]=image_no_overlay
AvailableClasses[]=image_no_overlay_no_border
AvailableClasses[]=direct_link_eng_us
AvailableClasses[]=direct_link_eng_gb
AvailableClasses[]=direct_link_eng_rw
CustomAttributes[]=offset
CustomAttributes[]=limit
ClassDescription[itemized_sub_items]=Itemized Sub Items
ClassDescription[itemized_subtree_items]=Itemized Subtree Items
ClassDescription[highlighted_object]=Highlighted Object
ClassDescription[vertically_listed_sub_items]=Vertically Listed Sub Items
ClassDescription[horizontally_listed_sub_items]=Horizontally Listed Sub Items
ClassDescription[image_no_border]=Image with no border
ClassDescription[image_no_overlay]=Image with no overlay
ClassDescription[image_no_overlay_no_border]=Image with no overlay and no border
ClassDescription[direct_link_eng_us]=Direct Link eng-US
ClassDescription[direct_link_eng_gb]=Direct Link eng-GB
ClassDescription[direct_link_eng_rw]=Direct Link eng-RW
CustomAttributesDefaults[offset]=0
CustomAttributesDefaults[limit]=5

[quote]
CustomAttributes[]=align
CustomAttributes[]=author
CustomAttributesDefaults[align]=right
CustomAttributesDefaults[autor]=Quote author

[embed-type_images]
AvailableClasses[]
AvailableClasses[]=image_no_border
AvailableClasses[]=image_no_overlay
AvailableClasses[]=image_no_overlay_no_border


[ClassAttributeSettings]
CategoryList[legacy]=Legacy Imported Content Attributes
CategoryList[shipping]=Shipping Information Attributes

*/ ?>
