#!/usr/bin/env php
<?php

error_reporting( E_ALL | E_NOTICE );

require_once 'autoload.php';

$cli = eZCLI::instance();
$script = eZScript::instance( array( 'description' => ( "eZ Publish pre-clusterize\n" .
                                                        "Script for setting db connection settings " .
                                                        "etc\n" .
                                                        "\n" .
                                                        "./bin/php/pre-clusterize.php" ),
                                     'use-session'    => false,
                                     'use-modules'    => false,
                                     'use-extensions' => true ) );

$script->startup();


$ini = new eZINI();
$file_ini = new eZINI('file.ini');

$dbname = $ini->variable('DatabaseSettings', 'Database');
$User = $ini->variable('DatabaseSettings', 'User');
$Password = $ini->variable('DatabaseSettings', 'Password');

if (!isset($argv[1])) $argv[1] = 'tcdbinstance.cxdweddzhatd.us-east-1.rds.amazonaws.com'; 

if (!$oldlink = @mysql_connect('tcdbinstance.cxdweddzhatd.us-east-1.rds.amazonaws.com', 'think', 'tcaccess95')) {
	echo 'Could not connect to mysql';
	exit;
}
$sql = "use $dbname;";
$result = mysql_query($sql, $oldlink);
$sql = "delete from ezsession;";
$result = mysql_query($sql, $oldlink);
mysql_close($oldlink);

foreach (array($dbname, $dbname."_cluster") as $Original_db) {

    exec("mysqldump --user=$User --password=$Password --host=".$argv[1]." $Original_db > mydump.sql");

    if (!$link = @mysql_connect('ccaurora-cluster.cluster-cxdweddzhatd.us-east-1.rds.amazonaws.com', 'think', 'tcaccess95')) {
    	echo 'Could not connect to mysql';
    	exit;
    }

    $sql = "CREATE DATABASE IF NOT EXISTS $Original_db CHARACTER SET utf8 COLLATE utf8_general_ci;";
    $result = mysql_query($sql, $link);
    $sql = "GRANT ALL ON $Original_db.* TO $User IDENTIFIED BY '$Password';";
    $result = mysql_query($sql, $link);

    if (file_exists('mydump.sql')) {
    	exec("mysql --user=think --password=tcaccess95 --host=ccaurora-cluster.cluster-cxdweddzhatd.us-east-1.rds.amazonaws.com $Original_db < mydump.sql", $output);
    	if (!isset($output) || !is_array($output) || (is_array($output) && isset($output[0]) && $output[0] != '')) {
    		echo "Could not import old database - ".serialize($output);
    	    exit;
    	}
	
    	unlink('mydump.sql');
    }

}

$script->initialize();

$script->shutdown();

?>
