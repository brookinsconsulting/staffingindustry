<!DOCTYPE html>
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">

<head>
<title>GeoTech Center Cutups</title>
<meta charset="utf-8" />
<meta name="Content-Type" content="text/html" />
<meta name="Content-Language" content="en-US" />
<link rel="stylesheet" type="text/css" href="http://files.thinkcreative.com/reset.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://ww.thinkcreative.com/extension/site/design/site/stylesheets/core.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://ww.thinkcreative.com/extension/site/design/site/stylesheets/pagecore.css" media="all" />
<style type="text/css">
body{
	margin:10px;
}
.menu .delimiter{
	display:none;
}
.menu li{
	line-height:1.25em;
}
</style>
</head>

<body>

<header role="banner"><h1>GeoTech Center</h1></header>
<section id="site-columns">
<nav>
	<ul class="menu vertical">
		<li><a href="home.html">Home</a><span class="delimiter">|</span></li>
		<li><a href="subpage.html">Subpage</a><span class="delimiter">|</span></li>

	</ul>
</nav>
</section>

</body>

</html>