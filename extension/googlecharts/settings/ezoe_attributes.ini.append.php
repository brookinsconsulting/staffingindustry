<?php /* #?ini charset="utf8"?

[CustomAttribute_googlecharts_title]
Name=Title
Type=text

[CustomAttribute_googlecharts_key]
Name=Key
Type=text

[CustomAttribute_googlecharts_type]
Type=select
Selection[]
Selection[column]=Column Chart
Selection[line]=Line Chart
Default=column

[CustomAttribute_googlecharts_size]
Name=Chart Size
Type=text

[CustomAttribute_googlecharts_sheet]
Name=Sheet Name
Type=text

[CustomAttribute_googlecharts_query]
Name=Query
Type=text

[CustomAttribute_googlecharts_range]
Name=Sheet Range
Type=text

*/ ?>