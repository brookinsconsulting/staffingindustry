{run-once}
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
{/run-once}
{def $chart_key=concat('key=', $key, '&sheet=', $sheet, '&range=', $range, '&tq=', $query)|md5()}


<div id="chart_{$chart_key}" style="{$size}"></div><br />
{*<div id="table_{$chart_key}"></div>*}

{literal}
<script type="text/javascript">
	// Load the Visualization API and the piechart package.
	google.load('visualization', '1.0', {'packages':['table', 'corechart']});

	// Set a callback to run when the Google Visualization API is loaded.
	google.setOnLoadCallback(function(){
		var 
			{/literal}DataSourceURL='http://spreadsheets.google.com/tq?pub=1&key={$key}&sheet={$sheet}&range={$range}&tq={$query}&headers=1'{literal}
			query = new google.visualization.Query(DataSourceURL)
		;

		// Send the query with a callback function.
		query.send(function(response){
			if (response.isError()) {
				alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
				return;
			}
			var data = response.getDataTable();
	{/literal}
			visualization = new google.visualization.{$type|upfirst()}Chart(document.getElementById('{concat('chart_', $chart_key)}'));
	{literal}
//	console.log('2010', data.getColumnRange(1));
//	console.log('2011', data.getColumnRange(2));
			visualization.draw(data, {
				title:'{/literal}{$title}{literal}',
				chartArea:{
					width:'88%',
//					height:'80%',
//					top:50,
					left:40
				},
//				titleTextStyle:{
//					fontSize:18
//				},
				vAxis:{
					gridlines:{
						count:10
					},
					minValue:180
				},
				legend:'top'
			});
//			var table = new google.visualization.Table(document.getElementById('table_div_ds'));
//			table.draw(data);
		});
	});
</script>
{/literal}
{*
<pre style="width:450px;white-space:pre-wrap;">
http://spreadsheets.google.com/tq?pub=1&key={$key}&sheet={$sheet}&range={$range}&tq={$query}&headers=1

http://spreadsheets.google.com/tq?pub=1&key=0Aofs_TmbXaFLdDBjSXRQOU1YOHJMRlBWNmNEc1NoTlE&sheet=Monthly Morning Average&range=A1:D13&tq=select A,B,C&headers=1
</pre>
*}