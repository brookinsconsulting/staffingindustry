<?php

	class Twitter
{

	function __construct($username, $limit=5){
		$this->username=$username;
		$this->limit=$limit;
//		$this->rss=self::rss($username);
		$this->isRSS=false;
		$this->isAtom=false;
	}

	function tweets($limit=false, $json=true){
//		$xml=simplexml_load_string($this->rss);
//		$tweets=array();
// Atom Parse
//		$count=0;
		return file_get_contents("http://twitter.com/status/user_timeline/$this->username.json?count=$limit");
/*
		foreach($xml->entry as $k=>$v){
			if($limit!==false && $count>=$limit) {
				break;
			}
			$published=strtotime($v->published);
			$tweet=new stdClass();
			$tweet->content=preg_replace('/'.$this->username.':\s+/','',(string)$v->content);
			$tweet->content=preg_replace("/(http:\/\/[^\s]+)/", "<a href=\"$1\">$1</a>", $tweet->content);
			$tweet->published=date('M j',$published);
			$tweet->image=str_replace('normal','bigger',$v->link[1]['href']);
			array_push($tweets, $tweet);
			if($limit!==false){$count++;}
		}
*/
// RSS Parse
//		foreach($xml->channel->item as $item){
//			array_push($tweets, preg_replace('/'.$this->username.':\s+/','',$item->description));
//		}
		return $json?json_encode($tweets):$tweets;
	}
/*
	function feedType($type){
		switch($type){
			case 'RSS':{
				$this->isRSS=true;
				break;
			}
			case 'Atom':{
				$this->isAtom=true
				break;
			}
		}
	}
*/
	static function instance($username, $limit=10){
		$instance = new self($username, $limit);
//		$instance->feedType('RSS');
		return $instance;
	}

	private static function rss($username){
		$feed = curl_init("http://twitter.com/statuses/user_timeline/$username.atom");
		curl_setopt($feed, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($feed, CURLOPT_HEADER, 0);
		$xml = curl_exec($feed);
		curl_close($feed);
		return $xml;
	}

}

?>