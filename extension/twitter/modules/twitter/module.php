<?php

$Module = array(
	'name' => 'Twitter Integration',
	'variable_params' => true
);

$ViewList = array();

$ViewList['tweets'] = array(
	'name' => 'Twitter Tweets Feed',
	'script' => 'tweets.php',
	'params' => array('UserName', 'Limit')
);

?>