<?php
/**
 * service_portal/ View
 *
 * @copyright Copyright (C) 1999-2010 eZ Systems AS. All rights reserved.
 * @license http://ez.no/licenses/gnu_gpl GNU GPLv2
 *
 */

include_once( 'extension/ez_network/lib/oauth/OAuth.php' );
include_once( 'extension/ez_network/classes/eznetoauthclientconsumeruser.php' );

$tpl         = eZTemplate::factory();
$currentUser = eZUser::currentUser();
$http        = eZHTTPTool::instance();
$hasAccessToken      = false;
$currentUserRemoteID = $currentUser->attribute('contentobject')->attribute('remote_id');
$clientConsumer      = eznetOAuthClientConsumerUser::fetchByRemoteId( $currentUserRemoteID );

if ( $clientConsumer instanceof eznetOAuthClientConsumerUser )
{
    $Result['content'] = $tpl->fetch( 'design:network/service_portal.tpl' );// iframe
    $Result['path'] = array( array( 'text' => ezi18n( 'kernel/content', 'Network' ),
                                    'url' => false ),
                             array( 'text' => ezi18n( 'kernel/content', 'Service Portal' ),
                                    'url' => false ) );
    $Result['content_info'] = array('persistent_variable' => array( 'extra_menu' => false,
                                                                    'left_menu'  => false ));
    return $Result;
}

$tpl->setVariable( 'user', $currentUser );

$Result = array();
$Result['content'] = $tpl->fetch( 'design:network/login.tpl' );
$Result['path'] = array( array( 'text' => ezi18n( 'kernel/content', 'Network' ),
                                'url' => false ) );
$Result['content_info'] = array('persistent_variable' => array( 'extra_menu' => false,
                                                                'left_menu'  => false ));

?>
