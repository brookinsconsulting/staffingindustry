<?php
/**
 * Network module
 *
 * @copyright Copyright (C) 1999-2010 eZ Systems AS. All rights reserved.
 * @license http://ez.no/licenses/gnu_gpl GNU GPLv2
 *
 */


$Module = array( 'name' => 'Network',
                 'variable_params' => true
);

$ViewList = array();

// CLIENT (CONSUMER) //

$ViewList['oauth'] = array(
    'functions' => array( 'service_portal' ),
    'script' => 'oauth.php',
    'default_navigation_part' => 'eznetworknavigationpart',
    'params' => array( ), // Params use GET parameters instead here as oauth lib is tweaked for that
    'unordered_params' => array( ) );


$ViewList['service_portal'] = array(
    'functions' => array( 'service_portal' ),
    'script' => 'service_portal.php',
    'default_navigation_part' => 'eznetworknavigationpart',
    'params' => array( ),
    'unordered_params' => array( ) );

$ViewList['service_portal_iframe'] = array(
    'functions' => array( 'service_portal' ),
    'script' => 'service_portal_iframe.php',
    'default_navigation_part' => 'eznetworknavigationpart',
    'params' => array( ),
    'unordered_params' => array( ) );

$FunctionList= array();
$FunctionList['service_portal'] = array();



?>
