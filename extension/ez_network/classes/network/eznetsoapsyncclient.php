<?php
//
// Definition of eZNetSOAPSyncClient class
//
// Created on: <27-Feb-2006 16:57:33 hovik>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZ Network
// SOFTWARE RELEASE: 4.4.0
// COPYRIGHT NOTICE: Copyright (C) 1999-2010 eZ Systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
//
//
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//

/*! \file eznetsoapsyncclient.php
*/

/*!
  \class eZNetSOAPSyncClient eznetsoapsyncclient.php
  \brief The class eZNetSOAPSyncClient does

*/


class eZNetSOAPSyncClient extends eZNetSOAPSync
{
    /*!
     Constructor

     \param eZPersistenceObject definition
    */
    function eZNetSOAPSyncClient( $definition = false , $remoteHost = false )
    {
        $this->eZNetSOAPSync( $definition, $remoteHost );
    }

    /*!
     Syncronize - push. Push client data to server.
    */
    function syncronizePushClient( $soapClient )
    {
        $sendCount = 0;

        // 1. Get remote eZ Publish hostID
        $request = new eZSOAPRequest( 'hostID', eZNetSOAPSync::SYNC_NAMESPACE );
        $response = $soapClient->send( $request );

        if( !$request ||
            $response->isFault() )
        {
            eZDebug::writeError( 'Did not get valid result running SOAP method : hostID, on class : ' . $this->ClassName );
            return false;
        }

        $this->RemoteHost = $response->value(); // Missing message IDs

        if ( !$this->RemoteHost )
        {
            eZDebug::writeError( 'RemoteHost not set: ' . var_export( $this->RemoteHost, 1 ),
                                 'eZNetSOAPSync::syncronize()' );
            return false;
        }

        $sendList = $this->createSendArrayList( $soapClient );

        while( $sendList &&
               count( $sendList  ) > 0 )
        {
            $request = new eZSOAPRequest( 'importElements', eZNetSOAPSync::SYNC_NAMESPACE );
            $request->addParameter( 'className', $this->ClassName );
            $request->addParameter( 'hostID', eZNetUtils::hostID() );
            $request->addParameter( 'data', $sendList );
            $response = $soapClient->send( $request );

            if( $response->isFault() )
            {
                eZDebug::writeNotice( 'Did not get valid result running SOAP method : importElements, on class : ' . $this->ClassName );
                return false;
            }

            $sendCount += count( $sendList );

            $sendList = $this->createSendArrayList( $soapClient );
        }

        return array( 'class_name' => $this->ClassName,
                      'export_count' => $sendCount );
    }

    /*!
     Get list of objects to send.

     \param soap client.

     \return list of objects to send.
    */
    function createSendArrayList( $soapClient )
    {
        $useModified = isset( $this->Fields['modified'] );

        if ( $useModified )
        {
            $request = new eZSOAPRequest( 'getLatestModified', eZNetSOAPSync::SYNC_NAMESPACE );
            $request->addParameter( 'className', $this->ClassName );
            $request->addParameter( 'hostID', eZNetUtils::hostID() );
            $response = $soapClient->send( $request );

            if( $response->isFault() )
            {
                eZDebug::writeNotice( 'Did not get valid result running SOAP method : getLatestModified, on class : ' . $this->ClassName );
                return false;
            }

            $latestModified = $response->value();

            $latestList = $this->fetchListByLatestModified( $latestModified );
        }
        else
        {
            $request = new eZSOAPRequest( 'getLatestID', eZNetSOAPSync::SYNC_NAMESPACE );
            $request->addParameter( 'className', $this->ClassName );
            $request->addParameter( 'hostID', eZNetUtils::hostID() );
            $response = $soapClient->send( $request );

            if( $response->isFault() )
            {
                eZDebug::writeNotice( 'Did not get valid result running SOAP method : getLatestID, on class : ' . $this->ClassName );
                return false;
            }

            $latestID = $response->value(); // Missing message IDs

            $latestList = $this->fetchListByLatestID( $latestID );
        }

        return $latestList;
    }

    /*!
     Create standard soap request for "Fetch by latest"

     \param optional number of objects to be fetched at one time, default 100

     \return Soap request
    */
    function createFetchListSoapRequest( $limit = 100 )
    {
        $useModified = isset( $this->Fields['modified'] );
        $cli = eZCLI::instance();

        if ( $useModified )
        {
            $request = new eZSOAPRequest( 'fetchListByHostIDLatestModified', eZNetSOAPSync::SYNC_NAMESPACE );
            $request->addParameter( 'className', $this->ClassName );
            $request->addParameter( 'hostID', eZNetUtils::hostID() );
            $request->addParameter( 'latestModified', $this->getLatestModified() );
            $cli->output( 'Synchronizing: ' . $this->ClassName . ', latest updated: ' . $this->getLatestModified() );
        }
        else
        {
            $request = new eZSOAPRequest( 'fetchListByHostIDLatestID', eZNetSOAPSync::SYNC_NAMESPACE );
            $request->addParameter( 'className', $this->ClassName );
            $request->addParameter( 'hostID', eZNetUtils::hostID() );
            $request->addParameter( 'latestID', $this->getLatestID() );
            $cli->output( 'Synchronizing: ' . $this->ClassName . ', id: ' . $this->getLatestID() );
        }

        return $request;
    }

}

?>
