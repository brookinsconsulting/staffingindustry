<?php
//
// Definition of eZNetSOAPObject class
//
// Created on: <15-Jul-2005 00:38:44 hovik>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZ Network
// SOFTWARE RELEASE: 4.4.0
// COPYRIGHT NOTICE: Copyright (C) 1999-2010 eZ Systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
//
//
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//

/*! \file eznetsoapobject.php
*/

/*!
  \class eZNetSOAPObject eznetsoapobject.php
  \brief The class eZNetSOAPObject does

*/

class eZNetSOAPObject extends eZPersistentObject
{
    /*!
     Constructor
    */
    function eZNetSOAPObject( $row )
    {
        $this->eZPersistentObject( $row );
    }

    /*!
     Fetch missing object list specified by eZNetSOAP object ( using the eZNetSOAPLog )
    */
    static function fetchMissingObjectList( $eZNetSOAP )
    {
        $latestLogEntry = $eZNetSOAP->latestLogEntry();
        $lastEventTS = 0;

        if ( $latestLogEntry )
        {
            $lastEventTS = $latestLogEntry->attribute( 'timestamp' );
        }

        $className = $eZNetSOAP->attribute( 'local_class' );
        $classDefinition = call_user_func( array( $className, 'definition' ) );

        return eZPersistentObject::fetchObjectList( $classDefinition,
                                                    null,
                                                    array( 'timestamp' => array( '>', $lastEventTS ) ) );
    }

    /*!
     Create string representation of current object
    */
    function soapString()
    {
        $definition = $this->definition();

        $objectArray = array();
        foreach( array_keys( $definition['fields'] ) as $attributeName )
        {
            $objectArray[$attributeName] = $this->attribute( $attributeName );
        }

        return serialize( $objectArray );
    }

    /*!
      Get local ID
    */
    function soapLocalID()
    {
        return $this->attribute( 'id' );
    }
}

?>
