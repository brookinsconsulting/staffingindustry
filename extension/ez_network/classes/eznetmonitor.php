<?php
//
// Definition of eZNetMonitor class
//
// Created on: <18-Oct-2005 14:52:49 hovik>
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME: eZ Network
// SOFTWARE RELEASE: 4.4.0
// COPYRIGHT NOTICE: Copyright (C) 1999-2010 eZ Systems AS
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
//
//
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//

/*! \file eznetmonitor.php
*/

/*!
  \class eZNetMonitor eznetmonitor.php
  \brief The class eZNetMonitor does

*/

/**
 *
 * @deprecated Is not used anymore
 *
 */

class eZNetMonitor extends eZPersistentObject
{
    /*!
     Constructor
    */
    function eZNetMonitor( $rows = array() )
    {
        $this->eZPersistentObject( $rows );
    }

    static function definition()
    {
        return array( "fields" => array( "id" => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => 0,
                                                        'required' => true ),
                                         "timestamp" => array( 'name' => 'Timestamp',
                                                         'datatype' => 'integer',
                                                         'default' => 0,
                                                         'required' => true ),
                                         "text" => array( 'name' => 'Text',
                                                           'datatype' => 'string',
                                                           'default' => '',
                                                           'required' => true ),
                                         "started" => array( 'name' => 'Started',
                                                             'datatype' => 'integer',
                                                             'default' => 0,
                                                             'required' => true ),
                                         "finnished" => array( 'name' => 'Finnished',
                                                               'datatype' => 'integer',
                                                               'default' => 0,
                                                               'required' => true ),
                                         'synced' => array( 'name' => 'Synced',
                                                            'datatype' => 'integer',
                                                            'default' => 0,
                                                            'required' => true ) ),
                      "keys" => array( "id" ),
                      "function_attributes" => array(),
                      "increment_key" => "id",
                      "class_name" => "eZNetMonitor",
                      "sort" => array( "id" => "asc" ),
                      "name" => "ezx_ezpnet_monitor" );
    }

    /*!
     Create new monitor object
    */
    function create()
    {
        return new eZNetMonitor( array( 'timestamp' => time() ) );
    }

    /*!
     Run and store monitor.

     \param CLI object ( optional )
    */
    function run( $cli = false )
    {
        $this->setAttribute( 'started', time() );

        $this->DomDocument = new eZDomDocument();
        $this->DomDocument->setName( 'Monitor report' );
        $this->XMLRoot = $this->DomDocument->createElementNode( 'MonitorReport' );
        $this->DomDocument->setRoot( $this->XMLRoot );

        $ini = eZINI::instance( 'network.ini' );
        foreach( $ini->variable( 'MonitorSettings', 'FunctionList' ) as $function )
        {
            $functionResult = serialize( $this->$function() );
            if ( $cli )
            {
                $cli->output( 'Monitor test starting  : ' . $function );
            }
            $this->XMLRoot->appendChild( $this->DomDocument->createElementTextNode( $function,
                                                                                    $functionResult,
                                                                                    array( 'timestamp' => time() ) ) );
            if ( $cli )
            {
                $cli->output( 'Monitor test finished : ' . $function );
            }
        }

        $this->setAttribute( 'text', $this->DomDocument->toString() );
        $this->setAttribute( 'finnished', time() );

        $this->store();
    }

    /*!
     Return Role setup log
     // TODO
    */
    function eZRoleLog()
    {
        $originalRoleDefinition = eZNetStorage::get( eZNetUtils::ROLE_KEY );
        $currentRoleDefinition = eZNetUtils::getRoleIDList();
        return eZNetUtils::arrayDiffRecursive( $originalRoleDefinition, $currentRoleDefinition );
    }

    /*!
     Add Content object log  //TODO - add, remove, count

    */
    function eZObjectLog()
    {
        $objectCountSQL = 'SELECT COUNT( id ) as count FROM ezcontentobject AND is_published=\'1\'';

        $db = eZDb::instance();
        $resArray = $db->arrayQuery( $objectCountSQL );
        if ( count( $resArray ) > 0 )
        {
            $maxObjectCount = $resArray[0]['count'];
        }

        $originalCreationCount = eZNetStorage::get( eZNetUtils::OBJECT_CREATION_COUNT );
        $currentCreationCount = eZNetUtils::objectsCreated();

        return array( 'object_count' => $maxObjectCount,
                      'creation_count' => $currentCreationCount - $originalCreationCount );
    }

    function eZFilePermissions()
    {
        return ''; // TODO
    }

    /*!
     Check eZ Publish settings

     \return serialized string with all setting files and change timestamps.
    */
    function eZSettings()
    {
        $ini = eZINI::instance();

        $currentAccess = $GLOBALS['eZCurrentAccess']['name'];
        $availableSiteAccesses = $ini->variable( 'SiteAccessSettings', 'AvailableSiteAccessList' );

        $iniFileNameList = eZNetUtils::iniFileNameList();

        $originalSettings = eZNetStorage::get( eZNetUtils::SETTINGS_KEY );

        $detectedFileArray = array();
        $removedSettingsArray = array();
        $newSettingArray = array();
        $changedSettingArray = array();

        foreach( $availableSiteAccesses as $siteAccess )
        {
            $GLOBALS['eZCurrentAccess']['name'] = $siteAccess;
            changeAccess( array( 'name' => $siteAccess ) );

            foreach( $iniFileNameList as $iniFile )
            {
                $file = '';
                $ini = eZINI::instance( $iniFile );
                $ini->findInputFiles( $inputFiles, $file );

                foreach( $inputFiles as $inputFile )
                {
                    $fileDescription = array( 'Filename' => $inputFile,
                                              'IniFile' => $iniFile,
                                              'SiteAccess' => $siteAccess,
                                              'StoredTimestamp' => filemtime( $inputFile ) );

                    $storedFile = eZNetStorage::get( array( 'Filename' => $inputFile ),
                                                     array( 'SiteAccess' => $siteAccess ) );

                    if ( !$storedFile )
                    {
                        $newSettingArray[] = $fileDescription;
                        eZNetUtils::addSettingsFile( $inputFile, $siteAccess );
                        $originalSettings[$siteAccess][$iniFile][$inputFile] = filemtime( $inputFile );
                    }
                    else
                    {
                        if ( md5_file( $inputFile ) != $storedFile['MD5'] )
                        {
                            $changedSettingArray[] = $fileDescription;
                        }
                    }
                    $detectedFileArray[] = $fileDescription;
                }
            }
        }

        eZNetStorage::set( eZNetUtils::SETTINGS_KEY, $originalSettings, eZNetUtils::nodeID() );

        foreach( $detectedFileArray as $fileDescription )
        {
            if ( isset( $originalSettings[$fileDescription['SiteAccess']][$fileDescription['IniFile']][$fileDescription['Filename']] ) )
            {
                unset( $originalSettings[$fileDescription['SiteAccess']][$fileDescription['IniFile']][$fileDescription['Filename']] );
            }
        }

        foreach( $originalSettings as $siteAccess => $siteAccessSettings )
        {
            foreach( $siteAccessSettings as $iniFile => $iniFileSettings )
            {
                foreach( $iniFileSettings as $filename => $timestamp )
                {
                    $fileDescription = array( 'Filename' => $filename,
                                              'IniFile' => $iniFile,
                                              'SiteAccess' => $siteAccess,
                                              'StoredTimestamp' => $timestamp );
                    $removedSettingsArray[] = $fileDescription;
                }
            }
        }

        return array( 'Added' => $newSettingArray,
                      'Changed' => $changedSettingArray,
                      'Removed' => $removedSettingsArray );
    }

    /*!
     Check eZ Publish core file status // TODO - update list for patched files.

     \return empty array if list OK, serialized string of filenames if not.
    */
    function eZCoreFiles()
    {
        return eZNetUtils::checkMD5Sums();
    }

    /*!
     Get number of object created since start.

     \get objects created
    */
    function objectsCreated()
    {
    }

    // Dom Document
    var $DomDocument = false;
    var $XMLRoot = false;
}

?>
