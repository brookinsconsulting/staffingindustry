<div class="float-break">

<div class="context-block">
<div class="box-header"><div class="box-ml">
<h1 class="context-title">{'Service Portal'|i18n( 'design/admin/network' )}</h1>
<div class="header-mainline"></div>
</div></div>
<div class="box-ml"><div class="box-content">

  {"You are not logged in as Service Portal user!"|i18n( 'design/admin/network' )}<br />
  {"Click <a href=%link>here</a> to authenticate your user!"|i18n( 'design/admin/network', , hash( '%link', "network/oauth"|ezurl ) )}

</div></div>
</div>

</div>