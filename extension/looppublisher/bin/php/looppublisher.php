#!/usr/bin/env php
<?php
/**
 * Looppublisher main entry point
 *
 * @package LoopPublisher
 * @version //autogen//
 * @copyright Copyright 2010 (C) 39Web. All rights reserved.
 * @author Jérôme Renard <jr@39web.fr>
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 */

require 'autoload.php';

set_time_limit( 0 );

$script = eZScript::instance( array( 'description'    => ( "eZ Publish content object publisher in an loop\n" ),
                                     'use-session'    => false,
                                     'use-modules'    => true,
                                     'use-extensions' => false ) );

$script->startup();

$optionList = $script->getOptions(
    "[iterations:]",
    "",
    array(
        'iterations' => 'The number of iterations to loop over',
    )
);

$sys = eZSys::instance();

$script->initialize();

$loopPublisher = new LoopPublisher( $optionList, new ezcConsoleOutput() );
$loopPublisher->run();

$script->shutdown();
?>