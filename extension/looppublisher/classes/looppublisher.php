<?php
/**
 * LoopPublisher main class
 *
 * @package  LoopPublisher
 * @version //autogen//
 * @copyright Copyright 2010 (C) 39Web. All rights reserved.
 * @author Jérôme Renard <jr@39web.fr>
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 */

class LoopPublisher
{
    /**
     * Admin user
     */
    const CREATOR_ID = 14;

    /**
     * Folder
     */
    const CLASS_IDENTIFIER = 'folder';

    /**
     * Root node
     */
    const PARENT_NODE_ID = 2;

    /**
     * Holds the option list
     *
     * @var array
     */
    public $optionList = array();

    /**
     * Holds the ezcConsoleOutput object
     *
     * @var object
     */
    private $consoleOutput;

    /**
     * Flag that shows wether or not the dummy object is initialized
     *
     * @var bool
     */
    private $objectIsInitialized = false;


    /**
     * Holds the contentobjectID of the newly published
     * content object.
     *
     * @var int
     */
    private $contentObjectID;

    /**
     * Builds a new LoopPublisher object
     *
     * @param array $optionList
     * @param ezcConsoleOutput $consoleOutput
     * @return void
     */
    public function __construct( array $optionList, ezcConsoleOutput $consoleOutput )
    {
        $this->optionList    = $optionList;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * Runs the publishing process in a loop
     *
     * @return void
     */
    public function run()
    {
        $iterationNumber = $this->optionList['iterations'];

        $progress = new ezcConsoleProgressbar(
            $this->consoleOutput, (int)$iterationNumber, array( 'fractionFormat' => "%01d" )
        );

        for( $i = 0; $i <= $iterationNumber; $i++ )
        {
            if( !$this->publishDummyFolderContentObject() )
            {
                throw new Exception( "Error while publishing a content object in a loop" );
            }

            $progress->advance();

        }

        $progress->finish();
        $this->consoleOutput->outputLine();

        $this->removeDummyFolderContentObject();
    }

    /**
     * Publish a dummy folder object
     *
     * By default the maximum version number for a folder
     * is 5, this is perfect four our needs.
     *
     * @return void
     */
    protected function publishDummyFolderContentObject()
    {
        $xmlDeclaration = '<?xml version="1.0" encoding="utf-8"?> <section xmlns:image="http://ez.no/namespaces/ezpublish3/image/" xmlns:xhtml="http://ez.no/namespaces/ezpublish3/xhtml/" xmlns:custom="http://ez.no/namespaces/ezpublish3/custom/">';

        $folderAttributes = array(
            'name'              => __CLASS__,
            'short_name'        => __CLASS__,
            'short_description' => "${xmlDeclaration}<paragraph>This is the short description</paragraph></section>",
            'description'       => "${xmlDeclaration}<section><section><header>Some header</header><paragraph>Some paragraph with a <link target=\"_blank\" url_id=\"1\">link</link></paragraph> </section></section></section>",
            'show_children'     => false,
        );

        $params                     = array();
        $params['creator_id']       = self::CREATOR_ID;
        $params['class_identifier'] = self::CLASS_IDENTIFIER;
        $params['parent_node_id']   = self::PARENT_NODE_ID;
        $params['attributes']       = $folderAttributes;

        // publish the object once first
        if( !$this->objectIsInitialized )
        {
            $contentObject = eZContentFunctions::createAndPublishObject( $params );

            if( !$contentObject instanceof eZContentObject )
            {
                return false;
            }

            // the node must stay hidden
            $curNode = eZContentObjectTreeNode::fetch( $contentObject->attribute( 'main_node_id' ) );
            eZContentObjectTreeNode::hideSubTree( $curNode );

            $this->objectIsInitialized = true;
            $this->contentObjectID     = $contentObject->attribute( 'id' );
        }

        // now update it again and again
        return self::updateAndPublishObject(
            eZContentObject::fetch( $this->contentObjectID ), $params
        );
    }

    /**
     * Removes the created content object
     *
     * @return void
     */
    protected function removeDummyFolderContentObject()
    {
        $contentObject = eZContentObject::fetch( $this->contentObjectID );
        $contentObject->removeThis();
    }

    /**
     * Copy and paste from kernel/classes/ezcontentfunctions.php
     *
     * @param eZContentObject $object
     * @param array $params
     * @return void
     */
    protected static function updateAndPublishObject( eZContentObject $object, array $params )
    {
        if ( !array_key_exists( 'attributes', $params ) and !is_array( $params['attributes'] ) and count( $params['attributes'] ) > 0 )
        {
            eZDebug::writeError( 'No attributes specified for object' . $object->attribute( 'id' ),
                                 'eZContentFunctions::updateAndPublishObject' );
            return false;
        }

        $storageDir   = '';
        $languageCode = false;
        $mustStore    = false;

        if ( array_key_exists( 'remote_id', $params ) )
        {
            $object->setAttribute( 'remote_id', $params['remote_id'] );
            $mustStore = true;
        }

        if ( array_key_exists( 'section_id', $params ) )
        {
            $object->setAttribute( 'section_id', $params['section_id'] );
            $mustStore = true;
        }

        if ( $mustStore )
            $object->store();

        if ( array_key_exists( 'storage_dir', $params ) )
            $storageDir = $params['storage_dir'];

        if ( array_key_exists( 'language', $params ) and $params['language'] != false )
        {
            $languageCode = $params['language'];
        }
        else
        {
            $initialLanguageID = $object->attribute( 'initial_language_id' );
            $language = eZContentLanguage::fetch( $initialLanguageID );
            $languageCode = $language->attribute( 'locale' );
        }

        $db = eZDB::instance();
        $db->begin();

        $newVersion = $object->createNewVersion( false, true, $languageCode );

        if ( !$newVersion instanceof eZContentObjectVersion )
        {
            eZDebug::writeError( 'Unable to create a new version for object ' . $object->attribute( 'id' ),
                                 'eZContentFunctions::updateAndPublishObject' );

            $db->rollback();

            return false;
        }

        $newVersion->setAttribute( 'modified', time() );
        $newVersion->store();

        $attributeList = $newVersion->attribute( 'contentobject_attributes' );

        $attributesData = $params['attributes'];

        foreach( $attributeList as $attribute )
        {
            $attributeIdentifier = $attribute->attribute( 'contentclass_attribute_identifier' );
            if ( array_key_exists( $attributeIdentifier, $attributesData ) )
            {
                $dataString = $attributesData[$attributeIdentifier];
                switch ( $datatypeString = $attribute->attribute( 'data_type_string' ) )
                {
                    case 'ezimage':
                    case 'ezbinaryfile':
                    case 'ezmedia':
                    {
                        $dataString = $storageDir . $dataString;
                        break;
                    }
                    default:
                }

                $attribute->fromString( $dataString );
                $attribute->store();
            }
        }

        $db->commit();

        $operationResult = eZOperationHandler::execute( 'content', 'publish', array( 'object_id' => $newVersion->attribute( 'contentobject_id' ),
                                                                                     'version'   => $newVersion->attribute( 'version' ) ) );

        if( $operationResult['status'] == eZModuleOperationInfo::STATUS_CONTINUE )
            return true;

        return false;
    }
}
?>