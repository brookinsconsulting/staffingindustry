/********************************************************************************
*
********************************************************************************/
var AcceptCookies = (function($){if(!$){return;}

	var _private = {
			applyNotification:function(){
				this.notification.prependTo('body').hide().slideDown(1000);
				if(this.configuration.useMask){
					this.notification.expose(this.configuration.mask);
				}
			},
			configuration:{
				cookie:{
					expires:30,
					path:'/'
				},
				link:false,
				mask:{
					closeOnClick:false,
					closeOnEsc:false
				},
				useMask:typeof($.fn.expose) !== 'undefined'
			},
			createNotification:function(notification){
				if(!this.notification){
					var _self = this;
					this.notification = notification = $(notification).delegate('a.action`', 'click', function(){
						_self.handler.set($(this).hasClass('allow')).store();
						notification.slideUp('normal', function(){
							notification.detach();
							if(_self.configuration.useMask){
								$.mask.close();
							}
						});
						return false;
					});
				}
			},
			displayNotification:function(){
				var _self = this;
				if(!this.handler.has()){
					$(window).bind('load', function(e){
						_self.applyNotification();
					});
				}
				if(this.link = this.notification.data('notification-link')){
					this.link.call().bind('click', function(e){
						_self.applyNotification();
						return false;
					})
				}
			},
			handler:false,
			link:false,
			notification:false,
			settings:false
		}
	;

	var _cookie_handler = {
			construct:function(name, data, options){
				this.name = name;
				this.options = options;
				this.allow = function(){
					var handler = _private.handler;
					if(!handler.has() || (handler.has() && !handler.get())){
						return $.inArray(this.name, _private.settings.CookieExclusions) !== false;
					}
					return true;
				}
				var cookie = $.cookie(name);
				this.set(!!cookie ? $.evalJSON(cookie) : data);
			},
			get:function(name){
				if(name){
					if($.isPlainObject(this.cookie)){
						return this.cookie[name];
					}
					return null;
				}
				return this.cookie;
			},
			has:function(){
				return !!$.cookie(this.name);
			},
			set:function(data, value){
				if(this.cookie && value){
					this.cookie[data] = value;
					return this;
				}
				this.cookie = data;
				return this;
			},
			store:function(){
				if(this.allow()){
					$.cookie(this.name, $.toJSON(this.cookie), this.options);
					return true;
				}
				return false;
			}
		}
	;

	function __constructor(constructor, options){
		if(constructor && $.isFunction(constructor)){
			constructor.apply(this, options);
		}
	};

	var _this = {
			createHandler:function(){
				var f = __constructor,
					object = $.extend(true, {}, _cookie_handler),
					constructor = object.construct,
					options = []
				;
				delete object.construct;
				f.prototype = object;
				if(arguments.length > 1){
					for(var i = 0; i < arguments.length; i++){
						options.push(arguments[i]);
					}
				}
				return new f(constructor, options);
			},
			initialize:function(options){
				var configuration = _private.configuration = $.extend(true, _private.configuration, options);
				if(_private.notification && configuration.link && $.isFunction(configuration.link)){
					_private.notification.data('notification-link', configuration.link);
				}
			}
		}
	;

	$.ajax({
		async:false,
		url:'/ezjscore/call/acceptcookies::ClientConfiguration?ContentType=json',
		success:function(data){
			_private.settings = data.content.configuration;
			_private.createNotification(data.content.notification);
		}
	});

	_private.handler = _this.createHandler(_private.settings.CookieName, false,  _private.configuration.cookie);
	$(function(){
		if (location.href.indexOf('cookies=disabled') == -1) _private.displayNotification();
	});

	return _this;

})(typeof(jQuery)!=='undefined' ? jQuery : null);
