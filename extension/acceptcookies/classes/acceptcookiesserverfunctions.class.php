<?php

class AcceptCookiesServerFunctions extends ezjscServerFunctions
{

	public static function ClientConfiguration(){
		return array(
			'notification' => eZTemplate::factory()->fetch('design:acceptcookies/notice.tpl'),
			'configuration' => eZINI::instance('acceptcookies.ini')->group('GeneralSettings')
		);
	}

}

?>