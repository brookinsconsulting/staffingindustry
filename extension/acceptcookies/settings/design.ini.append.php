<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
DesignExtensions[]=acceptcookies

[StylesheetSettings]
FrontendCSSFileList[]=acceptcookies.css

[JavaScriptSettings]
FrontendJavaScriptList[]=jquery.json-2.3.min.js
FrontendJavaScriptList[]=jquery.acceptcookies.js

*/ ?>