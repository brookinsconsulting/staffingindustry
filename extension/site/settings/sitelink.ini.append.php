<?php /* #?ini charset="utf-8"?

[DataTypeSettings]
;ClassList[ezbinaryfile]=BinaryFile

[OperatorSettings]
;RoleOverride=enabled
;RoleList[23]=site
;RoleList[25]=eng

[OperatorSettings]
SiteLinkClassList[]=large_file

[DataTypeSettings]
ClassList[ezs3upload]=SiteLinkLargeBinaryFile

[large_file]
SelfLinking=disabled
DefaultLinkType=download
LinkTypeList[external]=aws_s3_upload_client
LinkTypeList[internal]=aws_s3_upload_client


*/ ?>
