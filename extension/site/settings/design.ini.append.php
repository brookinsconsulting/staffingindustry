<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
DesignExtensions[]=site

[StylesheetSettings]
CSSFileList[]=main.css
CSSFileList[]=matt.css
CSSFileList[]=css3effects.css
FrontendCSSFileList[]=xrowecommerce.css

[JavaScriptSettings]
; from framework, overriding here so as not to load from non-https cdn and throw browser warnings
LibraryScripts[]=ezjsc::jquery
LibraryScripts[]=jquery.tools_1.2.5_all.min.js

FrontendJavaScriptList[]=xrowproductvariation.js
FrontendJavaScriptList[]=main.js
BackendJavaScriptList[]=admin_main.js

*/ ?>
