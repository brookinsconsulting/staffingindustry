<?php /* #?ini charset="utf-8"?

[Fields]
address1[enabled]=true
NoPartialDelivery[enabled]=false

[MailSettings]
EmailBCCReceiver[]
EmailBCCReceiver[]=memberservices@staffingindustry.com
EmailBCCReceiver[]=kpeterson@staffingindustry.com
EmailBCCReceiver[]=kpostrero@staffingindustry.com
EmailBCCReceiver[]=dhenderson@staffingindustry.com
EmailBCCReceiver[]=siabilling@crain.com
EmailBCCReceiver[]=awan@staffingindustry.com
ReplyToMail=memberservices@staffingindustry.com

[BasketInformation]
DisplayTax=disabled
DisplayLogin=enabled
DisplayShipping=disabled
DisplayPaymentmethod=enabled
HazardousItems=disabled

[StatusSettings]
# Show payment status
ShowPaymentStatus=disabled

[InvoiceSettings]
ShowFooter=enabled
CompanyName=Staffing Industry Analysts
#CompanyAddress[]
CompanyWebsite=http://www2.staffingindustry.com
CompanyPhone=1 (800) 950-9496

[EPaymentSettings]
# Payments should be capture right away or later
# values are AUTH_ONLY or AUTH_AND_CAPTURE
PaymentRequestType=AUTH_AND_CAPTURE
# Wheater the gateways should store or not store payment information in the order
StorePaymentInformation=enabled
#List of active creditcards
ActiveCreditcards[]
ActiveCreditcards[2]=Visa
ActiveCreditcards[1]=MasterCard
#ActiveCreditcards[4]=American Express
#ActiveCreditcards[3]=Discover

[Settings]
ShowPriceAs=discount_price_ex_vat
ShopUserClassList[]=user

[AutomaticDiscounts]
CWS_Summit_Registration=80682
CWS_Summit_CW_Risk_Forum_Combo_Registration=82241
CW_Risk_Forum_Registration=82237
CWS_London_Summit_Registration=79168

*/ ?>
