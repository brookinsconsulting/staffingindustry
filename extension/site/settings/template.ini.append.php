<?php /* #?ini charset="utf-8"?

[PHP]
PHPOperatorList[html_entity_decode]=html_entity_decode
PHPOperatorList[base64_encode]=base64_encode
PHPOperatorList[base64_decode]=base64_decode
PHPOperatorList[header]=header
PHPOperatorList[json_encode]=json_encode
PHPOperatorList[addslashes]=addslashes
PHPOperatorList[htmlspecialchars]=htmlspecialchars
PHPOperatorList[strip_tags]=strip_tags
*/ ?>
