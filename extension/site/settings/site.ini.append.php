<?php /* #?ini charset="utf-8"?

[DebugSettings]
DebugOutput=enabled
DebugByUser=enabled
DebugUserIDList[]
DebugUserIDList[]=14
DebugUserIDList[]=172
DebugUserIDList[]=173
DebugUserIDList[]=174
DebugUserIDList[]=175
DebugUserIDList[]=176
DebugUserIDList[]=177
DebugUserIDList[]=178
DebugUserIDList[]=179
DebugUserIDList[]=40622

[GoogleSettings]
TrackingCode=

[RegionalSettings]
TranslationSA[site]=North America
TranslationSA[eng]=Europe
TranslationSA[row]=Rest Of World
LanguageSwitcherClass=siaLanguageSwitcher

[RoleSettings] 
PolicyOmitList[]=login
PolicyOmitList[]=services
PolicyOmitList[]=webex
PolicyOmitList[]=googlenews
PolicyOmitList[]=site
PolicyOmitList[]=wp
PolicyOmitList[]=ccerror

[SearchSettings]
AllowEmptySearch=enabled

[Session]
SessionNamePerSiteAccess=disabled

MetaDataArray[]
MetaDataArray[keywords]=
MetaDataArray[description]=
LaunchYear=2010

[TemplateSettings]
ExtensionAutoloadPath[]=site

[WP]
LoginCookieDomain=.staffingindustry.com
LoginRedirectURI=http://dev.staffingindustry.com/
*/ ?>
