<?php /*
[NewsletterSettings]
ParentNodeId=463

[TemplateSettings]
HTMLTemplateList[]
HTMLTemplateList[]=cws30_dailynews
;HTMLTemplateList[]=north_american_dailynews
;HTMLTemplateList[]=western_european_dailynews
HTMLTemplateList[]=global_dailynews
HTMLTemplateList[]=cws_global_dailynews
HTMLTemplateList[]=cws_global_dailynews_na
HTMLTemplateList[]=global_news_digest
HTMLTemplateList[]=cws_western_european_dailynews
HTMLTemplateList[]=north_american_research_bulletin
HTMLTemplateList[]=western_european_research_bulletin
HTMLTemplateList[]=row_research_bulletin
HTMLTemplateList[]=it_newsletter
HTMLTemplateList[]=it_newsletter_new_format
HTMLTemplateList[]=engineering_report_newsletter
HTMLTemplateList[]=industrial_report_newsletter
HTMLTemplateList[]=healthcare_newsletter
TextTemplateList[]
TextTemplateList[]=default

[CampaignSettings]
CampaignTemplateList[]
;CampaignTemplateList[]=na-dailynews
;CampaignTemplateList[]=we-dailynews
CampaignTemplateList[]=global-dailynews
CampaignTemplateList[]=cws-global-dailynews
CampaignTemplateList[]=cws-global-dailynews-na
CampaignTemplateList[]=cws30-dailynews
CampaignTemplateList[]=na-researchbulletin
CampaignTemplateList[]=we-researchbulletin
CampaignTemplateList[]=row-researchbulletin
CampaignTemplateList[]=global-news-digest
CampaignTemplateList[]=it-newsletter
CampaignTemplateList[]=engineering-report-newsletter
CampaignTemplateList[]=industrial-report-newsletter
CampaignTemplateList[]=healthcare-newsletter

[CampaignSettings_global-news-digest]
CampaignDefaults[Name]=Global News Digest
CampaignDefaults[Subject]=Global News Digest - Staffing Industry Analysts
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[ContentObject]=61307
CampaignDefaults[HTMLTemplate]=global_news_digest

[CampaignSettings_global-dailynews]
CampaignDefaults[Name]=Daily News
CampaignDefaults[Subject]=Daily News - Staffing Industry Analysts
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[ContentObject]=98133
CampaignDefaults[HTMLTemplate]=global_dailynews

[CampaignSettings_na-dailynews]
CampaignDefaults[Name]=NA Daily News
CampaignDefaults[Subject]=Daily News - North America - Staffing Industry Analysts
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=north_american_dailynews

[CampaignSettings_we-dailynews]
CampaignDefaults[Name]=WE Daily News
CampaignDefaults[Subject]=Daily News - Western Europe - Staffing Industry Analysts
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=western_european_dailynews

[CampaignSettings_cws-global-dailynews]
CampaignDefaults[Name]=CWS Global Daily News
CampaignDefaults[Subject]=CWS Global Daily News - Staffing Industry Analysts
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=cws_global_dailynews
CampaignDefaults[ContentObject]=100264

[CampaignSettings_cws-global-dailynews-na]
CampaignDefaults[Name]=CWS Global Daily News - North America
CampaignDefaults[Subject]=CWS Global Daily News - Staffing Industry Analysts
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=cws_global_dailynews_na
CampaignDefaults[ContentObject]=144123

[CampaignSettings_cws30-dailynews]
CampaignDefaults[Name]=CWS 3.0
CampaignDefaults[Subject]=CWS 3.0 - (Issue, Date)
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=cws30
CampaignDefaultLists[]=0aa18bf024c22e07853f8928ecd4d2c0
CampaignDefaultSegments[]=950fa94f780af63919b72726a66f8dd9

[CampaignSettings_na-researchbulletin]
CampaignDefaults[Name]=NA Research Bulletin
CampaignDefaults[Subject]=Staffing Industry Research Bulletin - North America
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=north_american_research_bulletin

[CampaignSettings_we-researchbulletin]
CampaignDefaults[Name]=WE Research Bulletin
CampaignDefaults[Subject]=Staffing Industry Research Bulletin - Western Europe
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=western_european_research_bulletin

[CampaignSettings_row-researchbulletin]
CampaignDefaults[Name]=ROW Research Bulletin
CampaignDefaults[Subject]=Staffing Industry Research Bulletin - ROW
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=row_research_bulletin
CampaignDefaults[ContentObject]=99479

[CampaignSettings_it-newsletter]
CampaignDefaults[Name]=IT Newsletter
CampaignDefaults[Subject]=Staffing Industry IT Newsletter
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=it_newsletter
CampaignDefaults[ContentObject]=102926

[CampaignSettings_engineering-report-newsletter]
CampaignDefaults[Name]=Engineering Report Newsletter
CampaignDefaults[Subject]=Staffing Industry Engineering Report Newsletter
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=engineering_report_newsletter
CampaignDefaults[ContentObject]=152193

[CampaignSettings_industrial-report-newsletter]
CampaignDefaults[Name]=Industrial Report Newsletter
CampaignDefaults[Subject]=Staffing Industry Industrial Report Newsletter
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=industrial_report_newsletter
CampaignDefaults[ContentObject]=152192

[CampaignSettings_healthcare-newsletter]
CampaignDefaults[Name]=Healthcare Newsletter
CampaignDefaults[Subject]=Staffing Industry Healthcare Newsletter
CampaignDefaults[FromName]=Staffing Industry Analysts
CampaignDefaults[FromEmail]=e-news@staffingindustry.com
CampaignDefaults[ReplyTo]=e-news@staffingindustry.com
CampaignDefaults[HTMLTemplate]=healthcare_newsletter
CampaignDefaults[ContentObject]=105606

*/ ?>
