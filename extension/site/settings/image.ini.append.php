<?php /* #?ini charset="utf-8"?

[AliasSettings]
AliasList[]=message
AliasList[]=related_content
AliasList[]=search_result
AliasList[]=article_small
AliasList[]=small_embed

[message]
Reference=
Filters[]
Filters[]=geometry/scalewidth=70
Filters[]=centerimg=70;70
Filters[]=strip=

[articlethumbnail]
Reference=
Filters[]
Filters[]=geometry/scalewidth=170
Filters[]=centerimg=170;125
Filters[]=strip=

[newsletter]
Reference=
Filters[]
Filters[]=geometry/scalewidth=80
Filters[]=centerimg=80;80
Filters[]=strip=

[small]
Filters[]
Filters[]=geometry/scalewidth=100
Filters[]=centerimg=100;100
Filters[]=strip=

[small_embed]
Filters[]
Filters[]=geometry/scalewidth=100

[related_content]
Filters[]
Filters[]=geometry/scalewidth=140
Filters[]=centerimg=140;100
Filters[]=strip=

[search_result]
Filters[]
Filter[]=geometry/scalewidth=100
#Filter[]=geometry/crop=100;72;0;0
Filters[]=centerimg=100;72
Filters[]=strip=

[article_small]
Reference=
Filters[]
Filters[]=geometry/scalewidthdownonly=370

*/ ?>