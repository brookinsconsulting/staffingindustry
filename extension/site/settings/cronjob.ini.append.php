<?php

[CronjobSettings]
ExtensionDirectories[]=site
;Scripts[]=importuserpages.php
Scripts[]=exportorders.php
;Scripts[]=edituserpages.php
;Scripts[]=updateusersubs.php
Scripts[]=expiredsubscribers.php
Scripts[]=exportactiveusers.php
Scripts[]=section_change.php
Scripts[]=expire_premium_access.php
; ezupdatelog requires read access to apache access log, run as root in web0:/etc/crontab
;Scripts[]=ezupdatelog.php
Scripts[]=expire_certification_access.php

[CronjobPart-importuserpages]
Scripts[]=importuserpages.php

[CronjobPart-edituserpages]
Scripts[]=edituserpages.php

[CronjobPart-exportorders]
Scripts[]=exportorders.php

[CronjobPart-frequent]
Scripts[]=hide.php
Scripts[]=runscheduledscripts.php

[CronjobPart-hide]
Scripts[]=hide.php

[CronjobPart-exportactiveusers]
Scripts[]=exportactiveusers.php

[CronjobPart-expiredsubscribers]
Scripts[]=expiredsubscribers.php

[CronjobPart-updateusersubs]
Scripts[]=updateusersubs.php

[CronjobPart-fixrels]
Scripts[]=fixrels.php

[CronjobPart-section_change]
Scripts[]=section_change.php

[CronjobPart-expire_premium_access]
Scripts[]=expire_premium_access.php

[CronjobPart-expire_certification_access]
Scripts[]=expire_certification_access.php

?>
