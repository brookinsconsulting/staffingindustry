<?php /* #?ini charset="utf-8"?

[CustomTagSettings]
AvailableCustomTags[]=article
AvailableCustomTags[]=featured_articles
AvailableCustomTags[]=recent_blogposts
AvailableCustomTags[]=recent_blogpostlist
AvailableCustomTags[]=gridview
AvailableCustomTags[]=realmedia
AvailableCustomTags[]=topicview
AvailableCustomTags[]=newslettersubscribe
AvailableCustomTags[]=facet_results
AvailableCustomTags[]=facet_results_ticker
AvailableCustomTags[]=facet_results_dailynews
AvailableCustomTags[]=daily_news
AvailableCustomTags[]=latestresearch
AvailableCustomTags[]=featured_blog_post
AvailableCustomTags[]=twitterfeed
AvailableCustomTags[]=http_header
AvailableCustomTags[]=analysts
AvailableCustomTags[]=the_staffing_stream_blog_posts
AvailableCustomTags[]=newtab

[column]
CustomAttributes[]=column_width

[UnpublishSettings]
#RootNodeList[]=2
#ClassList[]=53

[HideSettings]
RootNodeList[]=2
HideDateAttributeList[xrow_product]=end_date
HideDateAttributeList[article]=unpublish_date
HideDateAttributeList[alert]=unpublish_date

[realmedia]
CustomAttributes[]=position

[article]
CustomAttributes[]=page
CustomAttributes[]=header
CustomAttributes[]=depth
CustomAttributes[]=children_view
CustomAttributes[]=limit
CustomAttributes[]=stylize

[recent_blogposts]
CustomAttributes[]=header
CustomAttributes[]=sub_header
CustomAttributes[]=limit

[recent_blogpostlist]
CustomAttributes[]=header
CustomAttributes[]=sub_header
CustomAttributes[]=limit

[gridview]
CustomAttributes[]=width

[contentbox]
CustomAttributes[]=header
CustomAttributes[]=sub_header

[listsubitems]
CustomAttributes[]=page
CustomAttributes[]=header
CustomAttributes[]=sub_header
CustomAttributes[]=children_view
CustomAttributes[]=limit
CustomAttributes[]=stylize
CustomAttributes[]=alltext

[facet_results]
CustomAttributes[]=facet

[facet_results_ticker]
CustomAttributes[]=facet
CustomAttributes[]=header
CustomAttributes[]=sub_header

[facet_results_dailynews]
CustomAttributes[]=facet

[latestresearch]
CustomAttributes[]=articlenode
CustomAttributes[]=researchnode
CustomAttributes[]=limit

[daily_news]
CustomAttributes[]=limit

[embed]
;intended for products on the homepage (redesign 2012)
AvailableClasses[]=featured
AvailableClasses[]=newpage

[twitterfeed]
CustomAttributes[]=userlist

[ul]
CustomAttributes[]=column_limit
CustomAttributes[]=column_width

[separator]
CustomAttributes[]=class

[header]
AvailableClasses[]=gold

[link]
AvailableClasses[]=redBold
AvailableClasses[]=black
AvailableClasses[]=blackBold
AvailableClasses[]=lightGrey

[http_header]
CustomAttributes[]=header


[the_staffing_stream_blog_posts]
CustomAttributes[]=node_id
CustomAttributes[]=title
CustomAttributes[]=limit
CustomAttributes[]=userlist
CustomAttributes[]=banner_object_id
CustomAttributes[]=text_limit
CustomAttributes[]=sub_text_limit

[newtab]
CustomAttributes[]=title
CustomAttributes[]=link_to

*/ ?>
