<?php /* #?ini charset="utf8"?

;[ParamSettings]
;appendSiteAccess=1
;appendLanguage=0

[AccessLogSettings]
Path=/var/log/apache2/staffingindustry_access.log

;[PrivacySettings]
;IPAddressBlurring=disabled
;
;[TrackingSettings]
;TrackSiteAccess=enabled
;
;# List of user defined JavaScript events
;# Array key is a event parameter name, and value is a JavaScript method name
;[EventSettings]
;AvailableEvents[AddToBasket]=AddToBasket
;AvailableEvents[StartDownload]=StartDownload
;AvailableEvents[AddShoppingSum]=AddShoppingSum
;AvailableEvents[SetPayment]=SetPayment
;AvailableEvents[BoughtProduct]=BoughtProduct
;AvailableEvents[SentForm]=SentForm
;
;EventParamPrefix=osc


*/ ?>
