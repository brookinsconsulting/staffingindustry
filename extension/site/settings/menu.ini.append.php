<?php /* #?ini charset="utf-8"?

[MenuContentSettings]
# Default setting as found in settings/menu.ini is commented out
# TopIdentifierList[]
# TopIdentifierList[]=folder

# Default setting as found in settings/menu.ini is commented out
# LeftIdentifierList[]
# LeftIdentifierList[]=article
# LeftIdentifierList[]=article_mainpage
LeftIdentifierList[]=issue
LeftIdentifierList[]=nav_divider
LeftIdentifierList[]=topic

# Classes to use in extra menu (infobox)
# ExtraIdentifierList[]
# ExtraIdentifierList[]=infobox

[MenuSettings]
# HideSideMenuClasses[]=issue
#SideMenuPosition=none

[NavigationPart]
# Part[ezcontentnavigationpart]=Content Structure
# Part[ezmedianavigationpart]=Media Library
# Part[ezusernavigationpart]=User Accounts
# Part[ezmynavigationpart]=My Account

[Leftmenu_shop]
LinkNames[exportorders]=Order exports
Links[exportorders]=exportorders/view

*/ ?>
