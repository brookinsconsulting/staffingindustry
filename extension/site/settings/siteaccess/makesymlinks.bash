#!/bin/bash -e

cp -d site_member/* dut/
cp -d site_member/* dut_member/
cp -d site_member/* eng/
cp -d site_member/* eng_member/
cp -d site_member/* esl/
cp -d site_member/* esl_member/
cp -d site_member/* fre/
cp -d site_member/* fre_member/
cp -d site_member/* ger/
cp -d site_member/* ger_member/
cp -d site_member/* ita/
cp -d site_member/* ita_member/
