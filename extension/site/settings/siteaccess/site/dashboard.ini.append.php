<?php /*

[DashboardSettings]
DashboardBlocks[]
DashboardBlocks[]=my_top_articles
DashboardBlocks[]=topics
DashboardBlocks[]=upcoming_events
DashboardBlocks[]=my_recent_posts
DashboardBlocks[]=saved_searches
DashboardBlocks[]=notifications
DashboardBlocks[]=webinars

AdditionnalDashboardBlocks[]
;AdditionnalDashboardBlocks[]=

[DashboardBlock_my_top_articles]
Priority=10
Permanent=0
Customizeable=1

[DashboardBlock_topics]
Priority=20
Permanent=1
Customizeable=0

[DashboardBlock_upcoming_events]
Priority=30
Permanent=0
Customizeable=0

[DashboardBlock_my_recent_posts]
Priority=40
Permanent=0
Customizeable=0

[DashboardBlock_saved_searches]
Priority=50
Permanent=0
Customizeable=0

[DashboardBlock_notifications]
Priority=60
Permanent=1
Customizeable=0

[DashboardBlock_webinars]
Priority=70
Permanent=0
Customizeable=0

*/ ?>
