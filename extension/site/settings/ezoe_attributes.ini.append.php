<?php /* #?ini charset="utf8"?

[CustomAttribute_realmedia_position]
Title=Select ad size code
Name=Size Code
Type=select
Selection[]
Selection[Right]=Right
Selection[Right1]=Right1
Selection[Right2]=Right2
Selection[Right3]=Right3
Selection[Top1]=Top1
Default=Right

[CustomAttribute_article_page]
Name=Source Page
Type=link
LinkType[]
LinkType[eznode://]=Node ID

[CustomAttribute_article_header]
Title=Lets you define the text to be displayed at the top of the box.
Name=Header Text
Type=text

[CustomAttribute_article_depth]
Title=The view to use to render the children
Type=select
Selection[]
Selection[list]=Subitems
Selection[tree]=Subtree
Default=list

[CustomAttribute_article_children_view]
Title=The view to use to render the children
Type=select
Selection[]
Selection[line]=Line
Selection[embed]=Embed
Selection[listitem]=List Item
Selection[itemized]=Itemized Subitems
Default=listitem

[CustomAttribute_article_limit]
Title=Lets you define the maximum number of items to display in the box.
Name=Article Limit
Type=int
Default=5

[CustomAttribute_recent_blogposts_limit]
Title=Lets you define the maximum number of items to display in the box.
Name=Post Limit
Type=int
Default=3

[CustomAttribute_article_stylize]
Title=Gives the custom tag a stylized appearance
Name=Stylize Box
Type=select
Selection[]
Selection[basic]=Basic
Selection[modular]=Basic w/ Content Box
Selection[ads]=Advertisement
Default=modular

[CustomAttribute_recent_blogposts_header]
Title=Lets you define the text to be displayed at the top of the box.
Name=Header Text
Type=text

[CustomAttribute_recent_blogposts_sub_header]
Title=Lets you define the sub header text to be displayed at the top of the box.
Name=Sub Header Text
Type=text

[CustomAttribute_recent_blogpostlist_header]
Title=Lets you define top-level header text to be displayed at the top of the page.
Name=Header Text
Type=text

[CustomAttribute_recent_blogpostlist_sub_header]
Title=Lets you define the secondary header text to be displayed at the top of the page.
Name=Sub Header Text
Type=text

[CustomAttribute_listsubitems_sub_header]
Title=Lets you define the sub header text to be displayed at the top of the box.
Name=Sub Header Text
Type=text

[CustomAttribute_listsubitems_sub_header]
Title=Lets you define the sub header text to be displayed at the top of the box.
Name=Sub Header Text
Type=text

[CustomAttribute_gridview_width]
Title=Select the size of the grid
Name=Width
Type=select
Selection[]
Selection[normal]=Normal
Selection[full]=Full Width
Default=normal

[CustomAttribute_latestresearch_articlenode]
Name=Article Node
Type=link
LinkType[]
LinkType[eznode://]=Node ID

[CustomAttribute_twitterfeed_userlist]
Title=Enter one or more twitter usernames, separated by commas.
Name=User List
Type=text

[CustomAttribute_latestresearch_researchnode]
Name=Research Node
Type=link
LinkType[]
LinkType[eznode://]=Node ID

[CustomAttribute_latestresearch_limit]
Title=Lets you define the maximum number of items to display in the box.
Name=Limit
Type=int
Default=10

[CustomAttribute_separator_class]
Title=Select CSS Class
Name=Class
Type=select
Selection[]
Selection[normal]=Normal
Selection[gold]=Gold
Default=normal

[CustomAttribute_newtab_link_to]
Name=URL to open in new tab
Type=link
LinkType[]
LinkType[eznode://]=Node ID
LinkType[http://]=External Link

*/ ?>
