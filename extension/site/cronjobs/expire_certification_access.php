<?php

/**
 * @package SIA
 * @author  Serhey Dolgushev <serhey@thinkcreative.net>
 * @date    28 Sep 2015
 * */
$debugStep        = 50;
$rootNodeID       = 5;
$containerNodeIDs = array(
    // http://issuetrack.thinkcreative.com/view.php?id=20840
    array(
        'active'  => 142228,
        'archive' => 142229
    )
);

$ini           = eZINI::instance();
$userCreatorID = $ini->variable( 'UserSettings', 'UserCreatorID' );
$user          = eZUser::fetch( $userCreatorID );
if( ( $user instanceof eZUser ) === false ) {
    $cli->error( 'Cannot get user object by userID = "' . $userCreatorID . '". ( See site.ini [UserSettings].UserCreatorID )' );
    $script->shutdown( 1 );
}
eZUser::setCurrentlyLoggedInUser( $user, $userCreatorID );


$date                 = new DateTime( 'today+00:00' );
$timestamp            = $date->format( 'U' );
$params               = array(
    'ClassFilterType'  => 'include',
    'ClassFilterArray' => array( 'certification_subscription' ),
    'AsObject'         => false,
    'LoadDataMap'      => false,
    'AttributeFilter'  => array(
        array( 'certification_subscription/end_date', '<=', $timestamp )
    ),
    'MainNodeOnly'     => true,
    'Limitation'       => array()
);
$expiredSubscriptions = eZContentObjectTreeNode::subTreeByNodeID( $params, $rootNodeID );
$total                = count( $expiredSubscriptions );

foreach( $expiredSubscriptions as $i => $node ) {
    if( $i % $debugStep === 0 || $i == $total - 1 ) {
        $memoryUsage = number_format( memory_get_usage( true ) / ( 1024 * 1024 ), 2 );
        $output      = number_format( $i / $total * 100, 2 ) . '% (' . ( $i + 1 ) . '/' . $total . ')';
        $output .= ', Memory usage: ' . $memoryUsage . ' Mb';
        $cli->output( $output );
        $cli->output( str_repeat( '-', 80 ) );
    }

    $certNode = eZContentObjectTreeNode::fetch( $node[ 'node_id' ] );
    if( $certNode instanceof eZContentObjectTreeNode === false ) {
        continue;
    }

    $userNode = $certNode->attribute( 'parent' );
    if( $userNode->attribute( 'class_identifier' ) !== 'client' ) {
        continue;
    }

    $userObject    = $userNode->attribute( 'object' );
    $userLocations = $userObject->assignedNodes( false );
    foreach( $containerNodeIDs as $containerNodeID ) {
        foreach( $userLocations as $userLocation ) {
            if( (int) $userLocation[ 'parent_node_id' ] === $containerNodeID[ 'active' ] ) {
                $location = eZContentObjectTreeNode::fetch( $userLocation[ 'node_id' ] );

                eZContentOperationCollection::removeAssignment( $userNode->attribute( 'node_id' ), $userNode->attribute( 'contentobject_id' ), array( $location ), false );
                eZContentOperationCollection::addAssignment( $userNode->attribute( 'node_id' ), $userNode->attribute( 'contentobject_id' ), array( $containerNodeID[ 'archive' ] ) );
                break;
            }
        }
    }
}
