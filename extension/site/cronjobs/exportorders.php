<?php

$db = eZDB::instance();

$order_complete = array(
	"siteaccess" => "",
	"company_name" => "",
	"company_additional" => "",
	"tax_id" => "",
	"tax_id_valid" => "",
	"first_name" => "",
	"mi" => "",
	"last_name" => "",
	"address1" => "",
	"address2" => "",
	"city" => "",
	"state" => "",
	"zip" => "",
	"country" => "",
	"phone" => "",
	"fax" => "",
	"email" => "",
	"shipping" => "",
	"shippingtype" => "",
	"captcha" => "",
	"coupon_code" => "",
	"reference" => "",
	"message" => "",
	"s_company_name" => "",
	"s_company_additional" => "",
	"s_first_name" => "",
	"s_mi" => "",
	"s_last_name" => "",
	"s_address1" => "",
	"s_address2" => "",
	"s_city" => "",
	"s_state" => "",
	"s_zip" => "",
	"s_country" => "",
	"s_phone" => "",
	"s_fax" => "",
	"s_email" => "",
	"packages" => "",
	"paymentmethod" => "",
	"name" => "",
	"item_count" => "",
	"price" => "",
	"currency_code" => ""
);

// since when do we want orders
$timestamp = strtotime('last monday');

$query = "SELECT * FROM `ezorder` WHERE `created` > $timestamp";
$orders = $db->arrayQuery($query);

// first row, headers
$out .= '"' . implode('","', array_keys($order_complete)) . '"' . "\n\r";

// subsequent rows, data
foreach ($orders as $order) {
	$data_text = new SimpleXMLElement(str_replace("<state> </state>", "<state></state>", $order['data_text_1']));
	$product_id = $order['productcollection_id'];

	$query = "SELECT * FROM `ezproductcollection_item` WHERE `productcollection_id` = $product_id";
	$product = $db->arrayQuery($query);

	$query = "SELECT * FROM `ezproductcollection` WHERE `id` = $product_id";
	$product_extra = $db->arrayQuery($query);

	// not sure what happens if multiple products are purchased....
	foreach (array_merge((array) $data_text, $product[0], $product_extra[0]) as $key => $value) {
		if (isset($order_complete[$key]))
			$order_complete[$key] = $value;
	}

	$out .= '"' . implode('","', $order_complete) . '"' . "\n\r";
}

//echo $out;

$filename = 'var/orderexports/' .date('Y-m-d') . ".csv";
$export = fopen($filename, 'w');
fwrite($export, $out);
fclose($export);

?>
