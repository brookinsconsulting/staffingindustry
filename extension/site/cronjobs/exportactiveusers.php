<?php

$file = 'var/storage/active_users.csv';

// Administrator User, to ensure we have permission to read all users
$user = eZUser::fetch(14);
$user->loginCurrent();

$allUsersCount = eZContentObjectTreeNode::subTreeCountByNodeID(array(
	'ClassFilterType' => 'include',
	'ClassFilterArray' => array('client')
), 5);

var_dump($allUsersCount);

$limit = 1000;
$offset = 0;

$fp = fopen($file, 'w');

while ($offset < $allUsersCount) {
	$thisLimit = $offset + $limit;
	$thisOffset = $offset + 1;
	if ($thisLimit > $allUsersCount) $thisLimit = $allUsersCount;
	$memoryUsed = round(memory_get_usage() / 1024 / 1024) . ' MB';
	echo "processing $thisOffset - $thisLimit / $allUsersCount (memory usage: $memoryUsed)\n";

	$allUsers = eZContentObjectTreeNode::subTreeByNodeID(array(
		'ClassFilterType' => 'include',
		'ClassFilterArray' => array('client'),
		'Limit' => $limit,
		'Offset' => $offset
	), 5);

	foreach ($allUsers as $key => $userNode) {
		$row = array();

		// the database is a mess, filter out corruption etc.
		if (
			$userNode->attribute('contentobject_id') < 1 ||
			strpos($userNode->PathIdentificationString, 'looppublisher') !== false ||
			!is_a($userNode, 'eZContentObjectTreeNode')
		) {
			continue;
		}

		$eZUser = eZUser::fetch($userNode->attribute('contentobject_id'));

		if (!is_a($eZUser, 'eZUser')) {
			continue;
		}

		$row['email'] = $eZUser->Email;
		$row['enabled'] = $eZUser->isEnabled();
		
		if ($row['enabled'] == 0) continue;

		foreach ($userNode->dataMap() as $attribute) {
			$row[$attribute->ContentClassAttributeName()] = (string) $attribute->toString();
		}

		$roles = array();
		foreach ($eZUser->roles() as $role) {
			$roles[] = $role->Name;
		}
		$row['roles'] = implode(',', $roles);


		//var_dump($row);
		
		if (strpos($row['roles'], 'Access') !== false) {
			fputcsv($fp, $row);
		}

		foreach (array_keys($row) as $k) {
			if (!isset($headers[$k])) {
				$headers[$k] = true;
			}
		}
		//if ($key > 500) break;
	}
	$offset += $limit;

	// stop memory leak with dataMap not being emptied. http://share.ez.no/forums/developer/how-to-build-an-update-cli-script-without-memory-problem#comment51106
	unset($GLOBALS['eZContentObjectContentObjectCache']);
	unset($GLOBALS['eZContentObjectDataMapCache']);
	unset($GLOBALS['eZContentObjectVersionCache']);
}

var_dump(memory_get_peak_usage());

// back to the top to write headers on the first line
rewind($fp);
fwrite($fp, implode(',', array_keys($headers)) . "\n" . file_get_contents($file));
fclose($fp);

?>
