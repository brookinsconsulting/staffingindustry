#!/usr/bin/env php
<?php

require 'autoload.php';

if ( !function_exists( 'readline' ) )
{
    function readline( $prompt = '' )
        {
            echo $prompt . ' ';
            return trim( fgets( STDIN ) );
        }
}

function microtime_float()
{
    return microtime( true );
}

set_time_limit( 0 );

$cli = eZCLI::instance();
$endl = $cli->endlineString();

$script = eZScript::instance( array( 'description' => ( "eZ publish user object dump.\n\n" .
                                                        "Goes trough all user objects and dumps to csv" .
                                                        "\n" .
                                                        "exportusernames.php"),
                                     'use-session' => true,
                                     'use-modules' => true,
                                     'use-extensions' => true ) );


$unames = new ezExportUserNames( $script, $cli );
$unames->run();

$script->shutdown();

/**
 * Class containing controlling functions for updating the search index.
 */
class ezExportUserNames
{
    /**
     * Constructor
     *
     * @param eZScript Script instance
     * @param eZCLI CLI instance
     */
    function ezExportUserNames( eZScript $script, eZCLI $cli )
    {
	    $this->myfilename = "SIA_Unames.csv";
		$this->myfilepath = "/mnt/ebs/staffingindustry/var/storage/".$this->myfilename;
        $this->Script = $script;
        $this->CLI = $cli;
        $this->Options = null;
		$this->user_atts = array('is_enabled','id','username','email','published','tax_id','company_additional','company_name','first_name','mi','last_name','address1','address2','zip_code','city','phone','fax','state','shippingaddress','s_company_name','s_company_additional','s_first_name','s_last_name','s_address1','s_address2','s_zip_code','s_city','s_phone','s_fax','s_email','newsletter','s_state','type','country','s_country','max_subs','member_id','modulesid');
    }

    /**
     * Startup and run script.
     */
    public function run()
    {
        $this->Script->startup();

        $this->Options = $this->Script->getOptions( "[user-class-id:][php-exec:][offset:][limit:][topNodeID:][php-exec:][email:][scriptid:]",
                                                    "",
                                                    array( 'user-class-id' => "User object class ID",
                                                           'conc' => 'Parallelization, number of concurent processes to use',
                                                           'php-exec' => 'Full path to PHP executable',
                                                           'offset' => '*For internal use only*',
                                                           'limit' => '*For internal use only*',
                                                           'topNodeID' => '*For internal use only*',
                                                           ) );
        $this->Script->initialize();

        // Fix siteaccess
        $siteAccess = $this->Options['siteaccess'] ? $this->Options['siteaccess'] : false;
        if ( $siteAccess )
        {
            $this->changeSiteAccessSetting( $siteAccess );
        }
        else
        {
          
            
            $this->CLI->output( 'You did not specify a siteaccess. The admin siteaccess is a required option in most cases.' );
            $input = readline( 'Are you sure the default siteaccess has all available languages defined? ([y] or [q] to quit )' );
            if ( $input === 'q' )
            {
                $this->Script->shutdown();
                exit();
            }
        }

        $this->initializeDB();

		$scheduledScript = false;
		if (
			isset($this->Options['scriptid']) and
			in_array('ezscriptmonitor', eZExtension::activeExtensions()) and
			class_exists('eZScheduledScript')
		) {
			$scriptID = $this->Options['scriptid'];
			$scheduledScript = eZScheduledScript::fetch($scriptID);
		}

        // Check if current instance is main or sub process.
        // Main process can not have offset or limit set.
        // sub process MUST have offset and limit set.
        $offset = $this->Options['offset'];
        $limit = $this->Options['limit'];
        $topNodeID = $this->Options['topNodeID'];

		if (isset($topNodeID) && $topNodeID != 5) { 
			$this->myfilename = $topNodeID . "_" . $this->myfilename;
			$this->myfilepath = "/mnt/ebs/staffingindustry/var/storage/".$this->myfilename;
		}
		
        if ( !is_numeric( $offset ) &&
             !is_numeric( $limit ) &&
             !is_numeric( $topNodeID ) )
        {
            $this->CLI->output( 'Starting user object dump' );

            // Get PHP executable from user.
            $this->getPHPExecutable();

            $this->runMain();
        }
        elseif ( is_numeric( $offset ) &&
                 is_numeric( $limit ) &&
                 is_numeric( $topNodeID ) )
        {
					if ($offset == 0) {
						file_put_contents($this->myfilepath, '"' . implode('","', $this->user_atts) . '"' . "\n");
					}
					$this->runSubProcess( $topNodeID, $offset, $limit, $scheduledScript );
        }
        else
        {
            //OBS !!, invalid.
            $this->CLI->output( 'Invalid parameters provided.' );
            $this->Script->shutdown();
            exit();
        }
    }

    /**
     * Run sub process.
     *
     * @param int $topNodeID
     * @param int Offset
     * @param int Limit
     */
    protected function runSubProcess( $nodeID, $offset, $limit, $scheduledScript = false )
    {

        $count = 0;
        $node = eZContentObjectTreeNode::fetch( $nodeID );


        if ( !is_object($node) )
        {
            $this->Script->shutdown();
            exit();
        }

        if ( $subTree = $node->subTree( array( 'Offset' => $offset, 'Limit' => $limit, 'ClassFilterType' => 'include', 'ClassFilterArray' => array($this->Options['user-class-id']),
                                                  'Limitation' => array(),
                                                  'MainNodeOnly' => true ) ) )
        {

            foreach ( $subTree as $innerNode )
            {
                $ob = $innerNode->attribute( 'object' );

                if ( !$ob )
                {
                    continue;
                }
                //eZSearch::removeObject( $object );
                //pass false as we are going to do a commit at the end
                //
				$ob_id = $ob->ID;
				$user = eZUser::fetch($ob_id);
			    $addme = array();
				$data = $ob->dataMap();
				
				$addme[] = (string) $user->attribute('is_enabled');
				$addme[] = $ob_id;
				$addme[] = $user->Login;
				$addme[] = $user->Email;
				$addme[] = $published;
				
				while(list($at_k, $at) = each($this->user_atts)){
					if ($at_k > 4) {
						$out = $data[$at]->content();
						$addme[] = is_array($out) ? $data[$at]->metaData() : $out;
					}
				}
				
				reset($this->user_atts);
				
				$out = '';
				$unames = implode('","', $addme);
				$out .= '"' . $unames . '"' . "\n";

				$result = file_put_contents($this->myfilepath, $out, FILE_APPEND);


                if (! $result)
                {
                    $this->CLI->output(' Failed indexing object with ID ' . $ob->attribute('id'));
                }
                ++$count;

				if ( $scheduledScript ) {
					$percentComplete = (int) floor(($count+1)/count($subTree)*100);
					$scheduledScript->updateProgress($percentComplete);
				}
				eZContentObject::clearCache( $ob->attribute( 'id' ) );
				$ob->resetDataMap();
            }
        }

		if ( $scheduledScript ) {
			$scheduledScript->updateProgress(100);
			mail($this->Options['email'], 'User sub-user export complete', "Please download your results at http://www2.staffingindustry.com/var/storage/".$nodeID."_SIA_Unames.csv");
		}
        $this->CLI->output( $count );
        $this->Script->shutdown();
        exit();
    }

    /**
     * Get PHP executable from user input. Exit if no executable is entered.
     */
    protected function getPHPExecutable()
    {
        $validExecutable = false;
        $output = array();
        if ( !empty( $this->Options['php-exec'] ) )
        {
            $exec = $this->Options['php-exec'];
            exec( $exec . ' -v', $output );

            if ( count( $output ) &&
                 strpos( $output[0], 'PHP' ) !== false )
            {
                $validExecutable = true;
                $this->Executable = $exec;
            }

        }

        while( !$validExecutable )
        {
            $input = readline( 'Enter path to PHP-CLI executable ( or [q] to quit )' );
            if ( $input === 'q' )
            {
                $this->Script->shutdown();
                exit();
            }

            exec( $input . ' -v', $output );

            if ( count( $output ) &&
                 strpos( $output[0], 'PHP' ) !== false )
            {
                $validExecutable = true;
                $this->Executable = $input;
            }
        }
    }

    /**
     * Run main process
     */
    protected function runMain()
    {
        $startTS = microtime_float();

		file_put_contents($this->myfilepath, '"' . implode('","', $this->user_atts) . '"' . "\n");

        $processLimit = min( $this->Options['conc'] ? $this->Options['conc'] : 2,
                             10 ); // Maximum 10 processes
        $useFork = ( function_exists( 'pcntl_fork' ) &&
                     function_exists( 'posix_kill' ) );
        if ( $useFork )
        {
            $this->CLI->output( 'Using fork.' );
        }
        else
        {
            $processLimit = 1;
        }

        $this->CLI->output( 'Using ' . $processLimit . ' concurent process(es)' );

        $processList = array();
        for( $i = 0; $i < $processLimit; $i++ )
        {
            $processList[$i] = -1;
        }
		$topnode = eZContentObjectTreeNode::fetch(5);
		$topNodeArray = array($topnode);
        $this->ObjectCount = $topnode->subTreeCount( array( 'Limitation' => array(), 'MainNodeOnly' => true, 'ClassFilterType' => 'include', 'ClassFilterArray' => array($this->Options['user-class-id']) ) );
        $this->CLI->output( 'Number of objects to index: ' . $this->ObjectCount );
        $this->Script->resetIteration( $this->ObjectCount, 0 );

        // Loop through top nodes.
        foreach ( $topNodeArray as $node )
        {
            $nodeID = $node->attribute( 'node_id' );
            $offset = 0;

            $subtreeCount = $node->subTreeCount( array( 'Limitation' => array(), 'MainNodeOnly' => true, 'ClassFilterType' => 'include', 'ClassFilterArray' => array($this->Options['user-class-id']) ) );
            // While $offset < subtree count, loop through the nodes.
            while( $offset < $subtreeCount )
            {
                // Loop trough the available processes, and see if any has finished.
                for ( $i = 0; $i < $processLimit; $i++ )
                {
                    $pid = $processList[$i];
                    if ( $useFork )
                    {
                        if ( $pid === -1 ||
                             !posix_kill( $pid, 0 ) )
                        {
                            $newPid = $this->forkAndExecute( $nodeID, $offset, $this->Limit );
                            $this->CLI->output( "\n" . 'Creating a new thread: ' . $newPid );
                            if ( $newPid > 0 )
                            {
                                $offset += $this->Limit;
                                $this->iterate();
                                $processList[$i] = $newPid;
                            }
                            else
                            {
                                $this->CLI->output( "\n" . 'Returned invalid PID: ' . $newPid );
                            }
                        }
                    }
                    else
                    {
                        // Executre in same process
                        $count = $this->execute( $nodeID, $offset, $this->Limit );
                        $this->iterate( $count );
                        $offset += $this->Limit;
                    }

                    if ( $offset >= $subtreeCount )
                    {
                        break;
                    }
                }

                // If using fork, the main process must sleep a bit to avoid
                // 100% cpu usage. Sleep for 100 millisec.
                if ( $useFork )
                {
                    $status = 0;
                    pcntl_wait( $status, WNOHANG );
                    usleep( 100000 );
                }
            }
        }

        // Wait for all processes to finish.
        $break = false;
        while ( $useFork &&
                !$break )
        {
            $break = true;
            for ( $i = 0; $i < $processLimit; $i++ )
            {
                $pid = $processList[$i];
                if ( $pid !== -1 )
                {
                    // Check if process is still alive.
                    if ( posix_kill( $pid, 0 ) )
                    {
                        $break = false;
                    }
                    else
                    {
                        $this->CLI->output( 'Process finished: ' . $pid );
                        $processList[$i] = -1;
                    }
                }
            }
            // Sleep for 500 msec.
            $status = 0;
            pcntl_wait( $status, WNOHANG );

            usleep( 500000 );
        }

        $endTS = microtime_float();

        $this->CLI->output( 'Dumping took ' . ( $endTS - $startTS ) . ' secs ' .
                            '( average: ' . ( $this->ObjectCount / ( $endTS - $startTS ) ) . ' objects/sec )' );

        $this->CLI->output( 'Finished dumping user objects.' );
    }

    /**
     * Iterate index counter
     *
     * @param int Iterate count ( optional )
     */
    protected function iterate( $count = false )
    {
        if ( !$count )
        {
            $count = $this->Limit;
        }

        for( $iterateCount = 0; $iterateCount < $count; ++$iterateCount )
        {
            if ( ++$this->IterateCount > $this->ObjectCount )
            {
                break;
            }
            $this->Script->iterate( $this->CLI, true );

            if ( $this->IterateCount % 1000 === 0 )
            {

            }
        }
    }

    /**
     * Fork and executre
     *
     * @param int Top node ID
     * @param int Offset
     * @param int Limit
     */
    protected function forkAndExecute( $nodeID, $offset, $limit )
    {
        $pid = pcntl_fork();
        $db = eZDB::instance();
        $db->IsConnected = false;
        $db = null;
        eZDB::setInstance( $db );
        $this->initializeDB();
        if ($pid == -1)
        {
            die('could not fork');
        }
        else if ( $pid )
        {
            // Main process
            return $pid;
        }
        else
        {
            // We are the child process
            $this->execute( $nodeID, $offset, $limit, true );
            $this->Script->shutdown();
            exit();
        }
    }

    /**
     * Execute indexing of subtree
     *
     * @param int Top node ID
     * @param int Offset
     * @param int Limit
     * @param boolean Is sub process.
     *
     * @return int Number of objects indexed.
     */
    protected function execute( $nodeID, $offset, $limit, $isSubProcess = false )
    {
        global $argv;
        // Create options string.
        $paramString = '';
        $paramList = array( 'user-class-id' );
        foreach( $paramList as $param )
        {
            if ( !empty( $this->Options[$param] ) )
            {
                $paramString .= ' --' . $param . '=' . escapeshellarg( $this->Options[$param] );
            }
        }

        if ( $this->Options['siteaccess'] )
        {
            $paramString .= ' -s ' . escapeshellarg( $this->Options['siteaccess'] );
        }

        $paramString .= ' --limit=' . $limit .
            ' --offset=' . $offset .
            ' --topNodeID=' . $nodeID;

        $output = array();
        $command = $this->Executable . ' ' . $argv[0] . $paramString;

        exec( $command, $output );
//        wtf code follows, but leave it here commented for future enhancements
//        if ( $isSubProcess )
//        {
//            exec( $command, $output );
//        }
//        else
//        {
//            exec( $command, $output );
//        }

        if ( !empty( $output ) )
        {
            $num = array_pop( $output );
            if ( is_numeric( $num ) )
            {
                return $num;
            }
        }

        $this->CLI->output( "\n" . 'Did not index content correctly: ' . "\n" . var_export( $output, 1 ) );

        return 0;
    }


    /**
     * Ccreate custom DB connection if DB options provided
     *
     * @param array Options
     */
    protected function initializeDB()
    {
        $db = eZDB::instance();
        $db->setIsSQLOutputEnabled( $showSQL );
    }


    /**
     * Change siteaccess
     *
     * @param string siteacceee name
     */
    protected function changeSiteAccessSetting( $siteaccess )
    {
        global $isQuiet;
        $cli = eZCLI::instance();
        if ( !file_exists( 'settings/siteaccess/' . $siteaccess ) )
        {
            if ( !$isQuiet )
                $cli->notice( "Siteaccess $optionData does not exist, using default siteaccess" );
        }
    }


    /// Vars

    var $CLI;
    var $Script;
    var $Options;
    var $OffsetList;
    var $Executable;
    var $IterateCount = 0;
    var $Limit = 200;
    var $ObjectCount;
    var $user_atts;
    var $myfilename;
    var $myfilepath;
}

?>
