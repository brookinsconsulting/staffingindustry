<?php
/**
* File containing the expire_premium_access cronjob part
*
* @copyright Copyright (C) 1999 - 2015 Brookins Consulting. All rights reserved.
* @copyright Copyright (C) 2013 - 2015 Think Creative. All rights reserved.
* @license http://www.gnu.org/licenses/gpl-2.0.txt GNU General Public License v2 (or any later version)
* @version 0.0.2
* @package site
*/

// Fetch current timestamp
$currrentDate = time();

// Default offset and limit
$offset = 0;
$limit = 20;

// Default subtrees to remove
$subscriptionGroupNodeIDs = array( 65134,41490,65135 );

// Iterate over root nodeID
foreach( array(5) as $nodeID )
{
    // Fetch root node
    $rootNode = eZContentObjectTreeNode::fetch( $nodeID );

    while( true )
    {
        // Fetch only expired subscription objects
        $attribute_filter = array();
        $attribute_filter[] = array( "premium_expiration/end_date", "<=", $currrentDate - 86400 );

        $nodeArray = $rootNode->subTree( array( 'ClassFilterType' => 'include',
                                                'ClassFilterArray' => array('premium_expiration'),
						'AttributeFilter' => $attribute_filter,
						'Limitation' => array(),
                                                'Offset' => $offset,
                                                'Limit' => $limit ) );

        // Exit if no expired subscriptions are found
        if ( !$nodeArray || count( $nodeArray ) == 0 )
        {
            print_r("Could not find any more expired subscriptions!\n");
            break;
        }

        // Iterate offset
        $offset += $limit;

        // Iterate over expired subscription objects
        foreach ( $nodeArray as $node )
        {
                // Fetch user object
        	$parentObject = $node->fetchParent();

                // Fetch user assigned nodes
                $parentObjectAssignedNodes = $parentObject->object()->attribute( 'assigned_nodes' );

                // Iterate over assigned nodes
                foreach ( $parentObjectAssignedNodes as $parentObjectAssignedNode )
                {
                        // Only remove nodes with parent of subscriptionGroupNodeIDs
                        if( in_array( $parentObjectAssignedNode->attribute( 'parent_node_id' ), $subscriptionGroupNodeIDs ) )
                        {

                                        // Warn cronjob user of expiration location removal
                                        print_r("Expire: " . $parentObjectAssignedNode->attribute( 'name' ) . ". From: " . $parentObjectAssignedNode->fetchParent()->attribute( 'url' ) . ". NodeID: " . $parentObjectAssignedNode->attribute( 'node_id' ) . "\n" );

                                        // Remove user object from subscription content tree
                                        $parentObjectAssignedNode->removeNodeFromTree();
                                
                        }
                }
        }
    }
}

// Clear all related caches
eZContentCacheManager::clearAllContentCache();
eZUser::cleanupCache();

?>