<?php 

$arts = eZContentObjectTreeNode::subTreeByNodeID(array(
	'ClassFilterType' => 'include',
	'ClassFilterArray' => array('article'),
	'IgnoreVisibility' => true
), 213);

//$arts = array(eZContentObjectTreeNode::fetch(66784));


$map = array(
	'old_id_629' => 134400,
	'old_id_126263' => 133948,
	'old_id_252' => 130180,
	'old_id_260' => 130181,
	'old_id_632' => 130182,  
	'old_id_347' => 133945,
	'old_id_339' => 133931, 
	'old_id_589' => 133934,
	'old_id_617' => 133937,
	'old_id_616' => 133940, 
	'old_id_92326' => 133941,
	'old_id_633' => 133941,  
	'old_id_253' => 133942,  
	'old_id_612' => 133933,
	'old_id_614' => 130183,
	'old_id_613' => 133936,
	'old_id_615' => 133939,
	'old_id_635' => 133943,
	'old_id_126261' => 133934,   
	'old_id_331' => 133944, 
	'old_id_120149' => 133944,
	'old_id_111945' => 133944,
	'old_id_623' => 133944,
	'old_id_623' => 133944,   
	'old_id_621' => 133944, 
	'old_id_620' => 133944,  
	'old_id_637' => 133935,
	'old_id_126091' => 133935,
	'fallback' => 134233,
	'fallback2' => 133948
);

$default_topic_id = 133948;

$user = eZUser::fetch(172);

$user->loginCurrent();

$tot = count($arts);

foreach ($arts as $k_art => $art) {
	
	print_r("$k_art of $tot -  processing ".$art->Name."\r\n");
	
	$contentObjectID = $art->object()->ID;

	$ex_object = $art->object();
	$skipme = false;
	
	//if (count($art->object()->parentNodes()) > 1) continue; 
	
	$dm = $ex_object->dataMap();
	$orig_rels =  $dm['topics']->content();
	$done_ids = array($new_location_object_id);
	
	$pnodes = $art->object()->parentNodes();
	
	if (count($pnodes) > 1) continue;
	
	$old_parent_node_id = $ex_object->mainParentNodeID();
	$old_parent_node = eZContentObjectTreeNode::fetch($old_parent_node_id);
	if (strpos($old_parent_node->attribute('path_string'), "/213/") === false) {
		$old_parent_node = $pnodes[0];
	}
	$matchme_id = $old_parent_node->object()->ID;
	$matchme = 'old_id_'.$matchme_id;
	
	foreach($pnodes as $p) {
		if (in_array($p->object()->ID, array_values($map))) {
			$skipme = true;
		} else {
			if ($p->object()->ID != $matchme_id && strpos($p->attribute('path_string'), "/213/") !== false) {
				$orig_rels['relation_list'][] = array('contentobject_id' => $p->object()->ID); //add relation for multiple valid orig locations other than main.
			}
		}
	}  

	$new_location_object_id = (array_key_exists($matchme, $map) ) ? (($map[$matchme] == 134400) ? 134400 : 134233): $default_topic_id;
	
	if (!$skipme) {
		
		$new_location_node_r =  eZContentObjectTreeNode::findMainNodeArray(array($new_location_object_id));
		$contentobjectnode_r =  eZContentObjectTreeNode::findMainNodeArray(array($contentObjectID));

		$operationResult = eZOperationHandler::execute( 'content',
		                                                'addlocation', array( 'node_id'              => $contentobjectnode_r[0]->attribute('node_id'),
		                                                                      'object_id'            => $contentObjectID,
		                                                                      'select_node_id_array' => array($new_location_node_r[0]->attribute('node_id')) ),
		                                                null,
		                                                true );
	}
	
	
	
	$orig_rels['relation_list'][] = array('contentobject_id' => $old_parent_node->object()->ID);

	if (count($orig_rels['relation_list']) > 0) {  
	
		$contentObject = $ex_object;
	
		$current_version = $ex_object->nextVersion();

		$version = $ex_object->createNewVersion();  

		$dataMap = $version->dataMap(); 

		$objectAttribute = $dataMap['new_topics'];      

		//$value = $objectAttribute->content(); 
		
		$value = array('relation_list' => array()); 
	
		$new_priority = 1;

		//foreach ($value['relation_list'] as $rel)  {
		//	if ($new_priority <= $rel['priority']) $new_priority =  $rel['priority'] +1; 
		//	$done_ids[] = $rel['contentobject_id'];
		//}

		foreach ($orig_rels['relation_list'] as $rel) {    
		
			$matchme = 'old_id_'.$rel['contentobject_id'];
		
	        $relateme_object_id = (array_key_exists($matchme, $map) ) ? $map[$matchme]: $default_topic_id;
			$relateme_object = eZContentObject::fetch($relateme_object_id);
		
			if (!in_array($relateme_object_id, $done_ids)) {  
			
				$done_ids[] = $relateme_object_id; 

				$version_data = $relateme_object->currentVersion(false);   

				$value['relation_list'][] = array(
					"identifier" => '',
			        "priority" => $new_priority,
			        "in_trash" => '',
			        "contentobject_id" => $relateme_object_id,
			        "contentobject_version" => $version_data['version'],
			        "node_id" => $relateme_object->mainNodeID(),
			        "parent_node_id" => $relateme_object->mainParentNodeID(),
			        "contentclass_id" => $relateme_object->contentClass()->ID,
			        "contentclass_identifier" => $relateme_object->contentClassIdentifier(),
			        "is_modified" => '',
			        "contentobject_remote_id" => $relateme_object->remoteID() 
				);
				
				$new_priority++;


			}
		
		}   
	
		$objectAttribute->setContent( $value );  

		$objectAttribute->store();

		$operationResult = eZOperationHandler::execute( 'content', 'publish', array( 'object_id' => $contentObjectID,
		                                                                             'version' => $current_version ) );
	
	}

}


?>