<?php

$db = eZDB::instance();

$user = eZUser::fetch(172);

$user->loginCurrent();

$NA_dids = array();
$WE_dids = array();
$WE_only = array();
$NA_only = array();
$convert = array();


$file_array = file('var/storage/THINKCREATIVE_OXCYON_UPDATED_RECORDS_20110131.csv');
$key_map = array();
$parent_id = 174;

foreach ($file_array as $line_number =>$line)
{
	
	preg_match('/"[^"]*"/', $line, $matches);
	
	foreach ($matches as $m) {
		$line = str_replace($m, str_replace(",", "qqq", $m), $line);
	}
	
	$line = str_replace(",", '[tab]', $line);
	$line = str_replace("qqq", ',', $line);
	$line = str_replace('"', '', $line);
	
	$user_r = explode("[tab]", trim($line));
	

	if ($line_number == 0) {
		$key_map = $user_r;
		$map_key = array_flip($key_map);
		continue;
	}
	
	//print_r($user_r);
	
	if ($user_r[$map_key['PRIMARY_CONTACT']] != "TRUE" && $user_r[$map_key['PRIMARY_CONTACT']] != "1") continue;
	
	$mid = $user_r[$map_key['Oxcyon_ModuleSID']];

	$query = "select main_node_id from ezcontentobject_attribute a, ezcontentobject_tree o where o.contentobject_id = a.contentobject_id AND a.contentclassattribute_id = 517 AND depth = 3 AND data_text = '$mid'";
	$results = $db->arrayQuery($query);
	if (count($results) == 0) {
		$error = $user_r[$map_key['Oxcyon_ModuleSID']];
		print_r("Cound not find parent for $error!\r\n");
		continue;
	}
	
	$par_node = eZContentObjectTreeNode::fetch($results[0]['main_node_id']);
	$par_ob_id = $par_node->object()->ID;
	
//print_r(" par = $par_ob_id ");	
	
	$pnid = $results[0]['main_node_id'];
	
	$query = "select main_node_id, contentobject_id from ezcontentobject_tree t, ezcontentobject o where o.id = t.contentobject_id and t.parent_node_id = $pnid and o.contentclass_id = 59";
	
	$results = $db->arrayQuery($query); 
	
	foreach ($results as $r) {
		$contentobject_id = $r['contentobject_id'];
		$sub = eZContentObjectTreeNode::fetch($r['main_node_id']);
		$sub_data = $sub->dataMap();
		$end = $sub_data['end_date']->content()->timestamp();
		$name = $sub_data['name']->content();
		if (strpos($name, 'Western') > -1) {
			
			$ed = ($user_r[$map_key['WE_CM']] == "TRUE" || $user_r[$map_key['WE_CM']] == "1") ? $user_r[$map_key['WE_CM_ENDDATE']] : $user_r[$map_key['WE_CWSC_ENDDATE']];
			//print_r("\r\nend is $end");
			//print_r("\r\ned is " . strtotime($ed));
			if (strtotime($ed) <= (int)$end || $ed == '') continue;
			
			$role = eZRole::fetch( 25 );

		    $role->assignToUser( $par_ob_id );
		
			$new_daata = array('name' => $name, 'end_date' => $ed);
//print_r($new_daata);
			make_node($new_daata, $db, $contentobject_id);
			
		}
		
		if (strpos($name, 'North') > -1) {
			
			$ed = ($user_r[$map_key['NA_CM']] == "TRUE" || $user_r[$map_key['NA_CM']] == "1") ? $user_r[$map_key['NA_CM_ENDDATE']] : $user_r[$map_key['NA_CWSC_ENDDATE']];
			//print_r("\r\nend is $end");
			//print_r("\r\ned is " . strtotime($ed));
			if (strtotime($ed) > (int)$end || $ed == '') continue;
			
			$role = eZRole::fetch( 23 );

		    $role->assignToUser( $par_ob_id );
		
			$new_daata = array('name' => $name, 'end_date' => $ed);
//print_r($new_daata);
			make_node($new_daata, $db, $contentobject_id, $class_id, $page_name);
			
		}
	}
	
} 




function make_node($new_daata, $db, $contentobject_id) {

	//EZ SETTINGS

	$user_id = 172;
	
	//print_r($new_daata);
	//print_r($parent_id);

	// CREATE EZ OBJECT

	$ex_object = eZContentObject::fetch($contentobject_id);
	
	$page_name = $ex_object->attribute('name');

	print_r("Saving $page_name\n");
	
	$contentObject = $ex_object;

	$current_version = $ex_object->nextVersion();

	$version = $ex_object->createNewVersion();

	$contentObjectID = $ex_object->attribute( 'id' );

	$dataMap = $version->dataMap();

	foreach ($dataMap as $setme => $objectAttribute) {
		
		if (!array_key_exists($setme, $new_daata)) continue;
		
		$value = $new_daata[$setme];
		
		if ($value) {

			$dataType = $objectAttribute->attribute( 'data_type_string' );

			switch( $dataType )
			{
				
		  	 case 'ezboolean':
			  {
			      $objectAttribute->setAttribute( 'data_int', $value ? 1 : 0 );
			  } break;
			
			  case 'ezxmltext':
			  {
			      setEZXMLAttribute( $objectAttribute, $value );
			  } break;

			  case 'ezurl':
			  {
			      $objectAttribute->setContent( $value );
			  } break;

			  case 'ezkeyword':
			  {
			      $keyword = new eZKeyword();
			      $keyword->initializeKeyword( $value );
			      $objectAttribute->setContent( $keyword );
			  } break;

			  case 'ezdate':
			  {
			      $timestamp = strtotime( $value );
			      if ( $timestamp )
			          $objectAttribute->setAttribute( 'data_int', $timestamp );
			  } break;

			  case 'ezdatetime':
			  {
			      $objectAttribute->setAttribute( 'data_int', strtotime($value) );
			  } break;
			
			  case 'ezobjectrelation':
			  {
			      $objectAttribute->setAttribute( 'data_int', strtotime($value) );
			  } break;
			
			  case 'ezbinaryfile':
			  {

			      $status = $objectAttribute->insertRegularFile( $contentObject, $contentObject->attribute( 'current_version' ), eZContentObject::defaultLanguage(), $value, $storeResult );
					
				  if ( $storeResult['require_storage'] ) $dataMap[$fileAttribute]->store();
			  } break;

			  default:
			  {
			      $objectAttribute->setAttribute( 'data_text', $value );
			  } break;
			}

			$objectAttribute->store();
		
		}

	}
	
			//print_r('time2');
	
	if (array_key_exists('password', $new_daata)) {
		
		$username = $new_daata['username'];
		$password = $new_daata['password'];
		$email = $new_daata['email'];
	
		$user = new eZUser( $contentObjectID );

		$user->setAttribute('login', $username );
		$user->setAttribute('email', $email);

		$hashType = eZUser::hashType() . "";
		$newHash = $user->createHash( $username, $password, eZUser::site(), $hashType );

		$user->setAttribute( "password_hash_type", $hashType );
		$user->setAttribute( "password_hash", $newHash );

		$user->store();
	
	}
	
	//print_r('time3');

	$operationResult = eZOperationHandler::execute( 'content', 'publish', array( 'object_id' => $contentObjectID,
	                                                                             'version' => $current_version ) );

	// END EZ OBJECT CREATION
	
	$assignedNodes = $contentObject->assignedNodes();
	
		//print_r('time4');
	
	return $assignedNodes[0]->attribute('node_id');
	
}

function setEZXMLAttribute( $attribute, $attributeValue, $link = false )
{
    $contentObjectID = $attribute->attribute( "contentobject_id" );
    $parser = new eZSimplifiedXMLInputParser( $contentObjectID, false, 0, false );

    $attributeValue = str_replace( "\r", '', $attributeValue );
    $attributeValue = str_replace( "\n", '', $attributeValue );
    $attributeValue = str_replace( "\t", ' ', $attributeValue );

    $document = $parser->process( $attributeValue );
    if ( !is_object( $document ) )
    {
        $cli = eZCLI::instance();
        $cli->output( 'Error in xml parsing' );
        return;
    }
    $domString = eZXMLTextType::domString( $document );

    $attribute->setAttribute( 'data_text', $domString );
    $attribute->store();
}

?>
