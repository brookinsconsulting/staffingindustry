<?php

$Path       = array();
$Result     = array();
$tpl = eZTemplate::factory();

$orders = array();

if ($handle = opendir('var/orderexports/')) {
	while (false !== ($file = readdir($handle))) {
		if ($file[0] != '.')
			$orders[] = $file;
	}
	closedir($handle);
}

$tpl->setVariable( "orders", $orders );

$Path[]             = array( 'url' => false,
                             'text' => ezpI18n::tr( 'kernel/shop', 'Order Exports' ) );

$Result['content']  = $tpl->fetch( "design:shop/exportorders.tpl" );
$Result['path']     = $Path;

?>
