<?php

$ModuleTools = ModuleTools::instance($Module);
// aHR0cDovL3N0YWZmaW5naW5kdXN0cnkudGhpbmtjcmVhdGl2ZWludGVybmFsLm5ldC9SZXNlYXJjaC1QdWJsaWNhdGlvbnMvUmVzZWFyY2gvSW5kdXN0cnktR3Jvd3RoLUFuYWx5c2VzLUZvcmVjYXN0cy9OZXctYXJ0aWNsZS1FbmdsaXNo

$CurrentUser=eZUser::currentUser();
$Roles=$CurrentUser->roleIDList();


if(in_array(23,$Roles) || in_array(192,$Roles) || in_array(197,$Roles)){
		setcookie('american_access', 'empty', time()+60*60, '/', '.' . $_SERVER["HTTP_HOST"] ); 
}
if(in_array(25,$Roles) || in_array(195,$Roles) || in_array(199,$Roles)) {
		setcookie('european_access', 'empty', time()+60*60, '/', '.' . $_SERVER["HTTP_HOST"] ); 	
}
if(in_array(245,$Roles) || in_array(249,$Roles) || in_array(247,$Roles)) {
		setcookie('row_access', 'empty', time()+60*60, '/', '.' . $_SERVER["HTTP_HOST"] ); 	
}

if((in_array(23,$Roles) || in_array(192,$Roles) || in_array(197,$Roles)) && (in_array(25,$Roles) || in_array(195,$Roles) || in_array(199,$Roles)) && (in_array(245,$Roles) || in_array(247,$Roles) || in_array(249,$Roles))){
	// don't change language preference
} elseif(in_array(23,$Roles) || in_array(192,$Roles) || in_array(197,$Roles)){
	setcookie('lang', 'site', time()+60*60, '/', '.' . $_SERVER["HTTP_HOST"] ); 
} elseif(in_array(25,$Roles) || in_array(195,$Roles) || in_array(199,$Roles)) {
	setcookie('lang', 'eng', time()+60*60, '/', '.' . $_SERVER["HTTP_HOST"] ); 
} elseif(in_array(245,$Roles) || in_array(247,$Roles) || in_array(249,$Roles)) {
	setcookie('lang', 'row', time()+60*60, '/', '.' . $_SERVER["HTTP_HOST"] ); 
}

if (!$URL || $URL == "") $URL = base64_encode("/");

$togo = base64_decode($URL);
// There is a new extra slash after encoding "?" sign. Thats why we are using "|" instead of "?"
$togo = str_replace( '|', '?', $togo );

if ((substr($togo, 0, 4) != 'http') && (substr($togo, 0, 1) != '/')) $togo = '/' . $togo;

if (in_array(96, $Roles) && strstr($togo, '/content/dashboard')) {
	// Universal Staffing [Role] - these users do not have a dashboard
	header('Location: /');
} else {
	header('Location: '.$togo);
}

eZExecution::cleanExit();
//return $ModuleTools->result();
?>
