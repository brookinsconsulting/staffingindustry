<?php

$ini = eZINI::instance('webex.ini');

$WebExID = $ini->variable('WebExSettings', 'WebExID');
$Password = $ini->variable('WebExSettings', 'Password');
$SiteID = $ini->variable('WebExSettings', 'SiteID');
$SiteName = $ini->variable('WebExSettings', 'SiteName');
$SiteURL = $ini->variable('WebExSettings', 'SiteURL');

$tc = new TCWebEx($WebExID, $Password, $SiteID, $SiteName, $SiteURL);

$attendee = $tc->removeAttendee($_POST['attendee_id']);

if ($attendee->header->response->result == 'FAILURE') {
	header('HTTP/1.1 500 Internal Server Error');
	echo $attendee->header->response->reason;
} else {
	echo $attendee;
}

eZExecution::cleanExit();

?>
