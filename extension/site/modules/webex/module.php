<?php

$Module = array(
	'name' => 'webex',
	'variable_params' => true
);

$ViewList = array();

$ViewList['addattendee'] = array(
	'name' => 'addattendee',
	'functions' => array(),
	'script' => 'addattendee.php',
	'params' => array(),
	'unordered_params' => array()
);

$ViewList['removeattendee'] = array(
	'name' => 'removeattendee',
	'functions' => array(),
	'script' => 'removeattendee.php',
	'params' => array(),
	'unordered_params' => array()
);

$ViewList['getattendees'] = array(
	'name' => 'getattendees',
	'functions' => array(),
	'script' => 'getattendees.php',
	'params' => array('ID'),
	'unordered_params' => array()
);

$FunctionList = array();

?>
