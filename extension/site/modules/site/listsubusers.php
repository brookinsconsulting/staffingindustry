<?php

$UserID = $Params['UserID'];
$Limit = $Params['Limit'];
$Offset = $Params['Offset'];

$UserObject = eZContentObject::fetch($UserID);
$NodeID = $UserObject->mainNodeID();

$params = array(
	'SortBy' => array('published', false),
	'Limit' => $Limit,
	'Offset' => $Offset,
	'ClassFilterType' => 'include',
	'ClassFilterArray' => array('client')
);

$result = eZContentObjectTreeNode::subTreeByNodeID($params, $NodeID);

/*

header('Content-type: text/plain');
var_dump($result);

eZExecution::cleanExit();

*/

require_once('kernel/common/template.php');
$tpl = templateInit();
$tpl->setVariable('subusers', $result);
$content = $tpl->fetch("design:site/listsubusers.tpl");
$results = array(
	'content'=>$content,
	'pagelayout'=>"design:newsletter/pagelayout.tpl"
);

return $results;

?>
