<?php
 
$FunctionList = array();

$FunctionList['research'] = array(
	'name' => 'research',
	'call_method' => array(
		'class' => 'siteFunctionCollection',
		'method' => 'fetchResearch'
	),
	'parameters' => array(
		array(
			'name' => 'type',
			'type' => 'array',
			'required' => false
		),
		array(
			'name' => 'analyst',
			'type' => 'mixed',
			'required' => false
		),
		array(
			'name' => 'limit',
			'type' => 'integer',
			'required' => false,
			'default' => 10
		),
		array(
			'name' => 'offset',
			'type' => 'integer',
			'required' => false,
			'default' => 0
		),
		array(
			'name' => 'researchNodeID',
			'type' => 'integer',
			'required' => false,
			'default' => 0
		)
	)
);

$FunctionList['research_count'] = array(
	'name' => 'research_count',
	'call_method' => array(
		'class' => 'siteFunctionCollection',
		'method' => 'fetchResearchCount'
	),
	'parameters' => array(
		array(
			'name' => 'type',
			'type' => 'array',
			'required' => false
		),
		array(
			'name' => 'analyst',
			'type' => 'mixed',
			'required' => false
		),
		array(
			'name' => 'limit',
			'type' => 'integer',
			'required' => false,
			'default' => 0
		),
		array(
			'name' => 'offset',
			'type' => 'integer',
			'required' => false,
			'default' => 0
		),
		array(
			'name' => 'researchNodeID',
			'type' => 'integer',
			'required' => false,
			'default' => 0
		)
	)
);

$FunctionList['daily_news'] = array(
	'name' => 'featured_articles',
	'call_method' => array(
		'class' => 'siteFunctionCollection',
		'method' => 'fetchDailyNews'
	),
	'parameters' => array(
		array(
			'name' => 'type',
			'type' => 'array',
			'required' => false
		),
		array(
			'name' => 'language',
			'type' => 'mixed',
			'required' => false
		),
		array(
			'name' => 'limit',
			'type' => 'integer',
			'required' => false,
			'default' => 10
		),
		array(
			'name' => 'offset',
			'type' => 'integer',
			'required' => false,
			'default' => 0
		),
		array(
			'name' => 'researchNodeID',
			'type' => 'integer',
			'required' => false,
			'default' => 0
		)
	)
);

$FunctionList['fetch_node_by_url'] = array(
	'name' => 'fetch_node_by_url',
	'call_method' => array(
		'class'  => 'siteFunctionCollection',
		'method' => 'fetchNodeByURL'
	),
	'parameters' => array(
		array(
			'name' => 'url',
			'type' => 'string',
			'required' => true
		)
	)
);

?>
