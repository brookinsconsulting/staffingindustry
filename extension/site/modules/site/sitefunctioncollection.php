<?php

class siteFunctionCollection {
	private $researchNodeID = 2;
	private $buyerSectionID = 7;
	private $staffingSectionID = 8;
	private $buyerStaffingSectionID = 9;

	private function buildParams($type, $analyst, $limit, $offset, $language, $sortPublishDate = false) {
		$params = array(
			'SortBy' => array("published", false),
			'Limit' => $limit,
			'Offset' => $offset,
			'ClassFilterType' => 'include',
			'ClassFilterArray' => array('article')
		);
		if ($sortPublishDate)
			$params['SortBy'] = array("attribute", false, "article/publish_date");

		if (count($type) > 0) {
			$attributeFilterSectionIn = array($this->buyerStaffingSectionID);

			if (in_array('buyer', $type))
				$attributeFilterSectionIn[] = $this->buyerSectionID;

			if (in_array('staffing', $type))
				$attributeFilterSectionIn[] = $this->staffingSectionID;

			$params['AttributeFilter'] = array(
				array('section', 'in', $attributeFilterSectionIn)
			);
		}

		if (isset($analyst) && empty($langauge)) {
			$params['MainNodeOnly'] = true;
			$params['ExtendedAttributeFilter'] = array(
				'id' => 'orfilter',
				'params' => array(
					array('article/analyst', $analyst->id())
				)
			);
		}

		if (isset($language)) {
			$params['Language'] = $language;
		}

		return $params;
	}

	public function fetchResearch($type, $analyst, $limit, $offset, $researchNodeID = false) {
		if ($researchNodeID) $this->researchNodeID = $researchNodeID;
		eZDebug::accumulatorStart('fetchResearch', false, 'fetchResearch');

		$params = $this->buildParams($type, $analyst, $limit, $offset, NULL, $researchNodeID == 70472 ? true : false);
		$result = eZContentObjectTreeNode::subTreeByNodeID($params, $this->researchNodeID);

		/*
		eZDebug::writeNotice($params, 'fetchResearch $params');
		eZDebug::writeNotice($result, 'fetchResearch $params');
		*/

		eZDebug::accumulatorStop('fetchResearch');
		return array('result' => $result);
	}

	public function fetchResearchCount($type, $analyst, $limit, $offset, $researchNodeID = false) {
		if ($researchNodeID) $this->researchNodeID = $researchNodeID;
		eZDebug::accumulatorStart('fetchResearchCount', false, 'fetchResearchCount');
		
		$params = $this->buildParams($type, $analyst, $limit, $offset);
		$result = eZContentObjectTreeNode::subTreeCountByNodeID($params, $this->researchNodeID);

		eZDebug::accumulatorStop('fetchResearchCount');
		return array('result' => $result);
	}
	
	public function fetchDailyNews($type, $language, $limit, $offset, $researchNodeID= false) {
		if ($researchNodeID) $this->researchNodeID = $researchNodeID;

		$params = $this->buildParams($type, null, $limit, $offset, $language);
		
		$params['ClassFilterArray'] = array('news_item');
		
		$params['AttributeFilter'][] = array('news_item/hide_from_homepage', '!=', 1);
		eZDebug::writeDebug($params);
		
		$result = eZContentObjectTreeNode::subTreeByNodeID($params, $this->researchNodeID);

		$site = $GLOBALS['eZCurrentAccess']['name'];

		return array('result' => array($result, $site));
	}

	public function fetchNodeByURL( $url ) {
		$url = trim( $url, '/' );
		// Remove SA from the URL
		$tmp = explode( '/', $url );
		$SAs = eZINI::instance( 'site.ini' )->variable( 'SiteAccessSettings', 'AvailableSiteAccessList' );
		if( in_array( $tmp[0], $SAs ) ) {
			$url = implode( '/', array_slice( $tmp, 1 ) );
		}

		$node   = null;
		$nodeID = (int) eZURLAliasML::fetchNodeIDByPath( $url );
		if( $nodeID > 0 ) {
			$node = eZContentObjectTreeNode::fetch( $nodeID );
		}

		return array( 'result' => $node );
	}
}

?>
