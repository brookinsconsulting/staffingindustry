<?php header('Content-Type: text/xml');
$s_params = array(
	'Limit' => 1,
	'Language' => 'eng-US',
	'ClassFilterType' => 'include',
	'ClassFilterArray' => array('article', 'news_item', 'blog_post'),
	'AttributeFilter' => array(array('published', '>', (int) date('U') - 172800)),
	'SortBy' => array('published', false)
);
$s_nodes = eZContentObjectTreeNode::subTreeByNodeID($s_params, 2);

$e_params = array(
	'Limit' => 1,
	'Language' => 'eng-GB@euro',
	'ClassFilterType' => 'include',
	'ClassFilterArray' => array('article', 'news_item', 'blog_post'),
	'AttributeFilter' => array(array('published', '>', (int) date('U') - 172800)),
	'SortBy' => array('published', false)
);
$e_nodes = eZContentObjectTreeNode::subTreeByNodeID($e_params, 2);

$main_mod = filemtime('var/sitemap.xml');
$s_mod = (count($s_nodes)) ? $s_nodes[0]->object()->attribute('modified') : 0;
$e_mod = (count($e_nodes)) ? $e_nodes[0]->object()->attribute('modified') : 0;

$out = <<<EOF
<?xml version="1.0" encoding="UTF-8"?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

   <sitemap>

      <loc>http://www2.staffingindustry.com/sitemap.xml</loc>

      <lastmod>$main_mod</lastmod>

   </sitemap>

   <sitemap>

      <loc>http://www2.staffingindustry.com/eng/sitemap.xml</loc>

      <lastmod>$main_mod</lastmod>

   </sitemap>

   <sitemap>

      <loc>http://www2.staffingindustry.com/row/sitemap.xml</loc>

      <lastmod>$main_mod</lastmod>

   </sitemap>

   <sitemap>

      <loc>http://www2.staffingindustry.com/site/googlenews/sitemap</loc>

      <lastmod>$s_mod</lastmod>

   </sitemap>

   <sitemap>

      <loc>http://www2.staffingindustry.com/eng/googlenews/sitemap</loc>

      <lastmod>$e_mod</lastmod>

   </sitemap>

</sitemapindex>
EOF;

echo $out;

eZExecution::cleanExit();?>