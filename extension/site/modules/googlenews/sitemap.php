<?php

header('Content-Type: text/xml');

$params = array(
	'ClassFilterType' => 'include',
	'ClassFilterArray' => array('article', 'news_item', 'blog_post'),
	'AttributeFilter' => array(array('published', '>', (int) date('U') - 172800)),
	'SortBy' => array('published', false)
);

$nodes = eZContentObjectTreeNode::subTreeByNodeID($params, 2);

$out .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:news=\"http://www.google.com/schemas/sitemap-news/0.9\">\n";

$publicationName = "Staffing Industry Analysts";
$publicationLanguage = "eng";

// for use with sitelink inside the loop
$slParams = array('parameters'=>false,'absolute'=>true);

foreach ($nodes as $node) {
	$dataMap = $node->dataMap();
	$link = $node;
	SiteLinkOperator::sitelink($link, $slParams);

	$publicationDate = date('c', $node->object()->Published);
	$title = $node->Name;
	if (array_key_exists('tags', $dataMap)) {
		$keywords = implode(',', $dataMap['tags']->content()->KeywordArray);
	}
	if (!isset($keywords) || $keywords == "") {
		$keywords = "staffing industry,workforce";
	}

	if ($node->ClassIdentifier == "blog_post") {
		$genres = "Blog";
	}

	if ($node->object()->attribute("section_id") != 1) {
		$access = "Subscription";
	}

	$out .= "\t<url>\n";

	$out .= "\t\t<loc>";
	$out .= "$link?cookies=disabled";
	$out .= "</loc>\n";

	$out .= "\t\t<news:news>\n";
	
	$out .= "\t\t\t<news:publication>\n";

	$out .= "\t\t\t\t<news:name>";
	$out .= $publicationName;
	$out .= "</news:name>\n";

	$out .= "\t\t\t\t<news:language>";
	$out .= $publicationLanguage;
	$out .= "</news:language>\n";
	
	$out .= "\t\t\t</news:publication>\n";

	$out .= "\t\t\t<news:publication_date>";
	$out .= $publicationDate;
	$out .= "</news:publication_date>\n";

	$out .= "\t\t\t<news:title>";
	$out .= $title;
	$out .= "</news:title>\n";

	$out .= "\t\t\t<news:keywords>";
	$out .= $keywords;
	$out .= "</news:keywords>\n"; 

	if (isset($genres)) {
		$out .= "\t\t\t<news:genres>";
		$out .= $genres;
		$out .= "</news:genres>\n"; 
	}

	if (isset($access)) {
		$out .= "\t\t\t<news:access>";
		$out .= $access;
		$out .= "</news:access>\n"; 
	}

	$out .= "\t\t</news:news>\n";

	$out .= "\t</url>\n";

	unset($publicationDate, $keywords, $genres, $access);
}

$out .= "</urlset>\n";

$out = preg_replace('/&/', '&amp;', $out);

echo $out;

eZExecution::cleanExit();

?>