<?php

$Module = array(
	'name' => 'googlenews',
	'variable_params' => true
);

$ViewList = array();

$ViewList['sitemap'] = array(
	'name' => 'sitemap',
	'functions' => array(),
	'script' => 'sitemap.php',
	'params' => array(),
	'unordered_params' => array()
);

$ViewList['sitemaps'] = array(
	'name' => 'sitemaps',
	'functions' => array(),
	'script' => 'sitemaps.php',
	'params' => array(),
	'unordered_params' => array()
);

$FunctionList = array();

?>
