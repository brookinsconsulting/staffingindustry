<?php

$Path       = array();
$Result     = array();
$tpl = eZTemplate::factory();
$db = eZDB::instance();

if ($_FILES) {

	$currentUser = eZUser::currentUser();
	$currentUserID = $currentUser->id();
	$email = $currentUser->Email;
	$timestamp = time();
	$NodeID = $Params['NodeID'];

	$tmpFile = $_FILES['uifile']['tmp_name'];

	$fileHandler = eZClusterFileHandler::instance( 'var/userimport/' . $timestamp );
	$fileHandler->storeContents( file_get_contents($tmpFile) );

	fwrite($fh, file_get_contents($tmpFile));
	fclose($fh);

	$script = eZScheduledScript::create(
		'userexport.php',
		"extension/site/bin/php/userimport.php --timestamp=$timestamp --nodeid=$NodeID --email='$email' --userid=$currentUserID"
	);
	$script->store();

	$result = $db->arrayQuery("SELECT id FROM `ezscheduled_script` WHERE user_id = $currentUserID ORDER BY id DESC");
	$scriptid = $result[0]['id'];

}


$tpl->setVariable('script_id', $scriptid);
$tpl->setVariable('email', $email);


$Path[]             = array( 'url' => false,
                             'text' => 'Import Users' );

$Result['content']  = $tpl->fetch( "design:ezuiupload/upload.tpl" );
$Result['path']     = $Path;

?>
