<?php

$db = eZDB::instance();
$currentUser = eZUser::currentUser();
$currentUserID = $currentUser->id();
$email = $currentUser->Email;
$NodeID = $Params['NodeID'];

$script = eZScheduledScript::create(
	'userexport.php',
	"extension/site/cronjobs/exportusernames.php --php-exec=php --user-class-id=56 -s site_admin --topNodeID=$NodeID --offset=0 --limit=9999999999 --email=$email"
);
$script->store();

$result = $db->arrayQuery("SELECT id FROM `ezscheduled_script` WHERE user_id = $currentUserID ORDER BY id DESC");
$scriptid = $result[0]['id'];

header("Location: /site_admin/scriptmonitor/view/$scriptid");

?>
