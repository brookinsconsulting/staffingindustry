<?php

$Module = array(
	'name' => 'ezuiupload',
	'variable_params' => false
);

$ViewList = array();

$ViewList['upload'] = array(
	'name' => 'view',
	'functions' => array(),
	'script' => 'upload.php',
	'params' => array('NodeID'),
	'unordered_params' => array(),
	"default_navigation_part" => 'ezusernavigationpart'
);

$ViewList['download'] = array(
	'name' => 'view',
	'functions' => array(),
	'script' => 'download.php',
	'params' => array('NodeID'),
	'unordered_params' => array(),
	"default_navigation_part" => 'ezusernavigationpart'
);

$FunctionList = array();

?>
