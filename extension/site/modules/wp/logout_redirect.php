<?php

/**
 * @package SIA
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    25 Feb 2016
 * */

$ini = eZINI::instance();
$domain = $ini->hasVariable( 'WP', 'LoginCookieDomain' ) ? $ini->variable( 'WP', 'LoginCookieDomain' ) : '.staffingindustry.com';
setcookie( 'sia_user_name', false, time() - 24 * 3600, '/', $domain );

$URL = $ini->hasVariable( 'WP', 'LoginRedirectURI' ) ? $ini->variable( 'WP', 'LoginRedirectURI' ) : '/';
eZHTTPTool::headerVariable( 'Location', $URL );

eZExecution::cleanExit();
