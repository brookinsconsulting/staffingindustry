<?php
/**
 * @package SIA
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    09 Dec 2015
 * */
$Module = array(
    'name'      => 'WordPress Integration',
    'functions' => array()
);
$ViewList = array(
    'login_redirect' => array(
        'script'                  => 'login_redirect.php',
        'functions'               => array(),
        'params'                  => array()
    ),
    'logout_redirect' => array(
        'script'                  => 'logout_redirect.php',
        'functions'               => array(),
        'params'                  => array()
    )
);
$FunctionList = array();