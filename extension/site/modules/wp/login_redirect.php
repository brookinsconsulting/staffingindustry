<?php

/**
 * @package SIA
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    09 Dec 2015
 * */
$name   = '';
$user   = eZUser::currentUser();
$object = $user->attribute( 'contentobject' );
if( $object instanceof eZContentObject ) {
    $name = $object->attribute( 'name' );
}

$ini = eZINI::instance();
$domain = $ini->hasVariable( 'WP', 'LoginCookieDomain' ) ? $ini->variable( 'WP', 'LoginCookieDomain' ) : '.staffingindustry.com';
setcookie( 'sia_user_name', $name, 0, '/', $domain );

if( isset( $_GET['redirect_url'] ) ) {
    $URL = $_GET['redirect_url'];
} else {
    $URL = $ini->hasVariable( 'WP', 'LoginRedirectURI' ) ? $ini->variable( 'WP', 'LoginRedirectURI' ) : '/content/dashboard';
}

eZHTTPTool::headerVariable( 'Location', $URL );

eZExecution::cleanExit();
