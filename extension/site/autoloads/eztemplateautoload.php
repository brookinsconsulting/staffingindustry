<?php

$eZTemplateOperatorArray = SiteUtils::templateOperators('extension/site');

$myUser = eZUser::currentUser();
$myroles = $myUser->roleIDList();

$rolechange = false;
//NA BUYER
$role = eZRole::fetch( 180);

if (!(in_array(192,$myroles) || in_array(197,$myroles) || in_array(180,$myroles) || in_array(186,$myroles))) {
	
	if ((in_array(23, $myroles) || in_array(192,$myroles) || in_array(197,$myroles)) && in_array(6, $myroles) && !in_array(180, $myroles)) {

		$role->assignToUser( $myUser->id() );
		$rolechange = true;

	} else if (((!in_array(23, $myroles) && !in_array(192, $myroles) && !in_array(197, $myroles)) || !in_array(6, $myroles)) && in_array(180, $myroles)) {

		$role->removeUserAssignment( $myUser->id() );
		$rolechange = true;

	}
	//NA STAFFING
	$role = eZRole::fetch( 186);

	if ((in_array(23, $myroles) || in_array(192,$myroles) || in_array(197,$myroles)) && in_array(8, $myroles) && !in_array(186, $myroles)) {

		$role->assignToUser( $myUser->id() );
		$rolechange = true;

	} else if (((!in_array(23, $myroles) && !in_array(192, $myroles) && !in_array(197, $myroles)) || !in_array(8, $myroles)) && in_array(186, $myroles)) {

		$role->removeUserAssignment( $myUser->id() );
		$rolechange = true;

	}
	//EU BUYER
	$role = eZRole::fetch( 182);

	if ((in_array(25, $myroles) || in_array(195,$myroles) || in_array(199,$myroles)) && in_array(6, $myroles) && !in_array(182, $myroles)) {

		$role->assignToUser( $myUser->id() );
		$rolechange = true;

	} else if (((!in_array(25, $myroles) && !in_array(195, $myroles) && !in_array(199, $myroles)) || !in_array(6, $myroles)) && in_array(182, $myroles)) {

		$role->removeUserAssignment( $myUser->id() );
		$rolechange = true;

	}
	//EU STAFFING
	$role = eZRole::fetch( 184);

	if ((in_array(25, $myroles) || in_array(195,$myroles) || in_array(199,$myroles)) && in_array(8, $myroles) && !in_array(184, $myroles)) {

		$role->assignToUser( $myUser->id() );
		$rolechange = true;

	} else if (((!in_array(25, $myroles) && !in_array(195, $myroles) && !in_array(199, $myroles)) || !in_array(8, $myroles)) && in_array(184, $myroles)) {

		$role->removeUserAssignment( $myUser->id() );
		$rolechange = true;

	}

	if ($rolechange) {
		         // Expire role cache
		eZExpiryHandler::registerShutdownFunction();
		$handler = eZExpiryHandler::instance();
		$handler->setTimestamp( 'user-access-cache', time() );
		$handler->setTimestamp( 'user-info-cache', time() );
		$handler->setTimestamp( 'user-class-cache', time() );
		$handler->store();
		eZRole::expireCache();
	}

}


?>