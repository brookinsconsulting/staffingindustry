<?php

class AssignRoleUponPurchase extends eZWorkflowEventType
{
	const ID = "assignroleuponpurchase";

	function __construct()
	{
		$this->eZWorkflowEventType( AssignRoleUponPurchase::ID, "Assign Role Upon Purchase" );
		$this->setTriggerTypes(array(
				'shop' => array(
					'checkout' => array(
						'after'
					)
				)
		));
	}

	function execute($process, $event){
		$fileAttributeIdentifiers = array('file','file1');    // the attribute identifiers to check for content
		$roleID = 71;                                         // the role id to assign if the attribute has content

		$parameters = $process->attribute('parameter_list');
		$order = eZOrder::fetch($parameters['order_id']);
		

		$user_test = eZUser::fetchByEmail($order->accountEmail());
		if (is_object($user_test)) {
			$user_test->loginCurrent();
			$userID = $user_test->id();
		} else {

			if (eZUser::isUserLoggedIn() && $process->UserID !=10) {
				$userID = $process->UserID;
			} else {
	
				$cu = new CreateUser();
			
				// per discussion with david, we don't care that this will get lost
				// can always use /user/forgotpassword if they ever want to log in again
				$newPass = eZUser::createPassword(8);
				$newEmail = $order->accountEmail();
				$cu->make_node($newEmail, $newPass, 12);
				$newUser = eZUser::fetchByEmail($newEmail);
				$newUser->loginCurrent();

				$userID = $newUser->id();

				// the order is owned by anonymous, change owner to newly created user
				$db = eZDB::instance();
				// had to do this in two parts since mysql can't nested select on the same table it's updating
				// http://stackoverflow.com/questions/45494/sql-delete-cant-specify-target-table-for-update-in-from-clause
				$emailArr = $db->arrayQuery("SELECT email FROM ezorder WHERE email = '$newEmail' ORDER BY id DESC LIMIT 1;");
				$db->query("UPDATE ezorder SET user_id = '$userID' WHERE email = '{$emailArr[0]['email']}';");
			}
		
		}

		foreach ($order->productItems() as $key => $productItem) {
			$productNode = eZContentObjectTreeNode::fetch($productItem['node_id']);
			$productDataMap = $productNode->dataMap();
			foreach ($fileAttributeIdentifiers as $fileAttributeIdentifier) {
				$fileAttribute = $productDataMap[$fileAttributeIdentifier];
				if ($fileAttribute->hasContent()) {
					$file = $fileAttribute->content();
					$role = eZRole::fetch($roleID);
					$role->assignToUser($userID, 'subtree', $file->mainNodeID());
					eZCache::clearUserInfoCache();
					eZCache::clearContentCache();
				}
			}
		}


		return eZWorkflowType::STATUS_ACCEPTED;
	}
}

eZWorkflowEventType::registerEventType( AssignRoleUponPurchase::ID, "assignroleuponpurchase" );

?>
