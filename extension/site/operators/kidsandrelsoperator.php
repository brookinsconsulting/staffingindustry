<?php

class KidsAndRelsOperator
{
	var $Operators;
	var $Myid;
	var $Myid_n;
	var $sqlPermissionChecking_where;
	var $sqlPermissionChecking;
	var $attribute_identifier_s;
	var $lang_s;

	function KidsAndRelsOperator(){
		$this->Operators = array('kids_and_rels', 'real_rev_rels');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'kids_and_rels' => array('args' => array('type'=>'mixed', 'required'=>false, 'default'=>0)),
			'real_rev_rels' => array('args' => array('type'=>'mixed', 'required'=>false, 'default'=>0))
		);
	}
	
	static function get_good_kid_and_rels($params, $nodeID, $rels=array()) {
		
		$db = eZDB::instance();
		
		if ( $params === false )
        {
            $params = array( 'Depth'                    => false,
                             'Offset'                   => false,
                             //'OnlyTranslated'           => false,
                             'Language'                 => false,
                             'Limit'                    => false,
                             'SortBy'                   => false,
                             'AttributeFilter'          => false,
                             'ExtendedAttributeFilter'  => false,
                             'ClassFilterType'          => false,
                             'ClassFilterArray'         => false,
                             'GroupBy'                  => false );
        }

        $offset           = ( isset( $params['Offset'] ) && is_numeric( $params['Offset'] ) ) ? $params['Offset']             : false;
        //$onlyTranslated   = ( isset( $params['OnlyTranslated']      ) )                       ? $params['OnlyTranslated']     : false;
        $language         = ( isset( $params['Language']      ) )                             ? $params['Language']           : false;
        $limit            = ( isset( $params['Limit']  ) && is_numeric( $params['Limit']  ) ) ? $params['Limit']              : false;
        $depth            = ( isset( $params['Depth']  ) && is_numeric( $params['Depth']  ) ) ? $params['Depth']              : false;
        $depthOperator    = ( isset( $params['DepthOperator']     ) )                         ? $params['DepthOperator']      : false;
        $asObject         = ( isset( $params['AsObject']          ) )                         ? $params['AsObject']           : true;
        $loadDataMap      = ( isset( $params['LoadDataMap'] ) )                               ? $params['LoadDataMap']        : false;
        $groupBy          = ( isset( $params['GroupBy']           ) )                         ? $params['GroupBy']            : false;
        $mainNodeOnly     = ( isset( $params['MainNodeOnly']      ) )                         ? $params['MainNodeOnly']       : false;
        $ignoreVisibility = ( isset( $params['IgnoreVisibility']  ) )                         ? $params['IgnoreVisibility']   : false;
        $objectNameFilter = ( isset( $params['ObjectNameFilter']  ) )                         ? $params['ObjectNameFilter']   : false;

        if ( $offset < 0 )
        {
            $offset = abs( $offset );
        }

        if ( !isset( $params['SortBy'] ) )
            $params['SortBy'] = false;
        if ( !isset( $params['ClassFilterType'] ) )
            $params['ClassFilterType'] = false;

        $allowCustomSorting = false;
        if ( isset( $params['ExtendedAttributeFilter'] ) && is_array ( $params['ExtendedAttributeFilter'] ) )
        {
            $allowCustomSorting = true;
        }

        $sortingInfo             = eZContentObjectTreeNode::createSortingSQLStrings( $params['SortBy'], 'ezcontentobject_tree', $allowCustomSorting );
        $classCondition          = eZContentObjectTreeNode::createClassFilteringSQLString( $params['ClassFilterType'], $params['ClassFilterArray'] );
        if ( $classCondition === false )
        {
            eZDebug::writeNotice( "Class filter returned false" );
            return null;
        }

        $attributeFilter         = eZContentObjectTreeNode::createAttributeFilterSQLStrings( $params['AttributeFilter'], $sortingInfo, $language );
        if ( $attributeFilter === false )
        {
            return null;
        }
        $extendedAttributeFilter = eZContentObjectTreeNode::createExtendedAttributeFilterSQLStrings( $params['ExtendedAttributeFilter'] );
        $mainNodeOnlyCond        = eZContentObjectTreeNode::createMainNodeConditionSQLString( $mainNodeOnly );

        $pathStringCond     = '';
        $notEqParentString  = '';
        // If the node(s) doesn't exist we return null.
        if ( !eZContentObjectTreeNode::createPathConditionAndNotEqParentSQLStrings( $pathStringCond, $notEqParentString, $nodeID, $depth, $depthOperator ) )
        {
            return null;
        }

        if ( $language )
        {
            if ( !is_array( $language ) )
            {
                $language = array( $language );
            }
            // This call must occur after eZContentObjectTreeNode::createPathConditionAndNotEqParentSQLStrings,
            // because the parent node may not exist in Language
            eZContentLanguage::setPrioritizedLanguages( $language );
        }

        $groupBySelectText  = '';
        $groupBySQL         = $extendedAttributeFilter['group_by'];
        if ( !$groupBySQL )
        {
            eZContentObjectTreeNode::createGroupBySQLStrings( $groupBySelectText, $groupBySQL, $groupBy );
        }
        else if ( $groupBy )
        {
            eZDebug::writeError( "Cannot use group_by parameter together with extended attribute filter which sets group_by!", __METHOD__ );
        }

		if ($groupBySQL = '' || !$groupBySQL) {
			$groupBySQL = ' GROUP BY ezcontentobject.id';
		}
		
		//eZDebug::writeDebug($groupBy, 'groupBy');

        $languageFilter = eZContentLanguage::languagesSQLFilter( 'ezcontentobject' );
        $objectNameLanguageFilter = eZContentLanguage::sqlFilter( 'ezcontentobject_name', 'ezcontentobject' );

        if ( $language )
        {
            eZContentLanguage::clearPrioritizedLanguages();
        }
        $objectNameFilterSQL = eZContentObjectTreeNode::createObjectNameFilterConditionSQLString( $objectNameFilter );

        $limitation = ( isset( $params['Limitation']  ) && is_array( $params['Limitation']  ) ) ? $params['Limitation']: false;
        $limitationList = eZContentObjectTreeNode::getLimitationList( $limitation );
        $sqlPermissionChecking = eZContentObjectTreeNode::createPermissionCheckingSQL( $limitationList );

        // Determine whether we should show invisible nodes.
        $showInvisibleNodesCond = eZContentObjectTreeNode::createShowInvisibleSQLString( !$ignoreVisibility );

		$good_ids = "'" . implode("','", $rels) . "'";

		$pathStringCond = "((" . trim($pathStringCond, 'and ') . ") OR (ezcontentobject_tree.contentobject_id in ($good_ids))) AND";    

        $query = "SELECT DISTINCT
                       ezcontentobject.*,
                       ezcontentobject_tree.*,
                       ezcontentclass.serialized_name_list as class_serialized_name_list,
                       ezcontentclass.identifier as class_identifier,
                       ezcontentclass.is_container as is_container
                       $groupBySelectText,
                       ezcontentobject_name.name as name,
                       ezcontentobject_name.real_translation
                       $sortingInfo[attributeTargetSQL]
                       $extendedAttributeFilter[columns]
                   FROM
                      ezcontentobject_tree
                      INNER JOIN ezcontentobject ON (ezcontentobject_tree.contentobject_id = ezcontentobject.id)
                      INNER JOIN ezcontentclass ON (ezcontentclass.version = 0 AND ezcontentclass.id = ezcontentobject.contentclass_id)
                      INNER JOIN ezcontentobject_name ON (
                          ezcontentobject_tree.contentobject_id = ezcontentobject_name.contentobject_id AND
                          ezcontentobject_tree.contentobject_version = ezcontentobject_name.content_version
                      )
                      $sortingInfo[attributeFromSQL]
                      $attributeFilter[from]
                      $extendedAttributeFilter[tables]
                      $sqlPermissionChecking[from]
                   WHERE
                      $pathStringCond
                      $extendedAttributeFilter[joins]
                      $sortingInfo[attributeWhereSQL]
                      $attributeFilter[where]
                      $notEqParentString
                      $mainNodeOnlyCond
                      $classCondition
                      $objectNameLanguageFilter
                      $showInvisibleNodesCond
                      $sqlPermissionChecking[where]
                      $objectNameFilterSQL AND
                      $languageFilter
                $groupBySQL";

        if ( $sortingInfo['sortingFields'] )
            $query .= " ORDER BY $sortingInfo[sortingFields]";

        $server = count( $sqlPermissionChecking['temp_tables'] ) > 0 ? eZDBInterface::SERVER_SLAVE : false;

		if ( !$offset && !$limit )
		{
		    $nodeListArray = $db->arrayQuery( $query, array(), $server );
		}
		else
		{
		    $nodeListArray = $db->arrayQuery( $query, array( 'offset' => $offset,
		                                                     'limit'  => $limit ),
		                                              $server );
		}

		if ( $asObject )
		{
		    $retNodeList = eZContentObjectTreeNode::makeObjectsArray( $nodeListArray );
		    if ( $loadDataMap === true )
		        eZContentObject::fillNodeListAttributes( $retNodeList );
		    else if ( $loadDataMap && is_numeric( $loadDataMap ) && $loadDataMap >= count( $retNodeList ) )
		        eZContentObject::fillNodeListAttributes( $retNodeList );
		}
		else
		{
		    $retNodeList = $nodeListArray;
		}

		// cleanup temp tables
		$db->dropTempTableList( $sqlPermissionChecking['temp_tables'] );

		return $retNodeList;

	}
	
	function get_related_content($namedParameters, $id_type) {
		
		$db = eZDB::instance();
		
		$limit_s = "";
		$limit_i = 0;
		$offset_i = 0;
		
		if (is_array($namedParameters['args']) && array_key_exists('limit', $namedParameters['args'])) {
			if (is_numeric($namedParameters['args']['limit'])) $limit_i = $namedParameters['args']['limit'];
		}
	
		$offset_s = "0";
		if (is_array($namedParameters['args']) && array_key_exists('offset', $namedParameters['args'])) {
			if (is_numeric($namedParameters['args']['offset'])) $offset_i = $namedParameters['args']['offset'];
		}
	
		if ($offset_i || $limit_i) $limit_s = "Limit $offset_i, $limit_i";			
		
		$from = $this->sqlPermissionChecking['from'];
	
		$new_q = "SELECT ezcontentobject_tree.$id_type
		FROM ezcontentobject_link,
		ezcontentobject_tree,
		ezcontentobject_attribute,
		ezcontentobject,
		ezcontentobject as co2
		$from
		WHERE to_contentobject_id = $this->Myid
		and parent_node_id != $this->Myid_n
		$this->attribute_identifier_s
		$this->sqlPermissionChecking_where
		and ezcontentobject.id = $this->Myid
		and ezcontentobject_tree.contentobject_id = ezcontentobject_link.from_contentobject_id
		and ezcontentobject_attribute.contentclassattribute_id = 191
		and ezcontentobject_attribute.contentobject_id = ezcontentobject_link.from_contentobject_id
		and ezcontentobject_attribute.version = ezcontentobject_link.from_contentobject_version
		and co2.id = ezcontentobject_tree.contentobject_id
		and $this->lang_s
		and from_contentobject_version IN (
		SELECT max( version ) 
		FROM `ezcontentobject_version` 
		WHERE `contentobject_id` = from_contentobject_id
		GROUP BY `initial_language_id`
		)";	
		
		eZDebug::writeDebug($new_q);

		$results = $db->arrayQuery($new_q);

		$out = array();
		foreach ($results as $r) {
			$out[] = $r[$id_type];
		}
		
		return array_unique($out);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		
		$lm = eZContentLanguage::maskForRealLanguages();
		$lang_r = array();
		foreach (eZContentLanguage::prioritizedLanguageCodes() as $loc) {
			$lm = eZContentLanguage::idByLocale($loc);
			$lang_r[] = "(co2.language_mask & $lm = $lm || co2.language_mask%2 = 1)";
		}
		
		$this->lang_s = "(" . implode(" || ", $lang_r) . ")";		
		
		$limitation = false;
		$limitationList = eZContentObjectTreeNode::getLimitationList($limitation);
		$this->sqlPermissionChecking = eZContentObjectTreeNode::createPermissionCheckingSQL( $limitationList );
		$this->sqlPermissionChecking_where = str_replace("ezcontentobject.", "co2.", $this->sqlPermissionChecking['where']);
		$this->attribute_identifier_s = "";

		if (is_array($namedParameters['args']) && array_key_exists('attribute_identifier', $namedParameters['args'])) {
			$myati = $namedParameters['args']['attribute_identifier'];
			if (!is_array($myati)) $myati = array($myati);
			$myatq_out = '';
			foreach ($myati as $ati_k => $ati) {
				$myatq_out .= " ezcontentobject_link.contentclassattribute_id=".$ati;
				if ($ati_k < (count($myati) -1)) $myatq_out .= " OR";
			}
			
			$this->attribute_identifier_s = " and (" . trim($myatq_out) . ") ";
		}
		
		$this->Myid = $operatorValue->object()->ID;
		$this->Myid_n = $operatorValue->attribute("node_id");

		switch ($operatorName) {
			
			case 'kids_and_rels':{
				
				$params = array();
				
				foreach ($namedParameters['args'] as $key => $val) {
					$params[str_replace(' ', '', ucwords(str_replace('_', ' ', $key)))] = $val;
				}
				
				$rels = KidsAndRelsOperator::get_related_content($namedParameters, 'contentobject_id');
				
				$kids_and_rels = KidsAndRelsOperator::get_good_kid_and_rels($params, $this->Myid_n, $rels);
				
				$operatorValue = $kids_and_rels;
				
				return true;
				break;
				
			}   
			
			       
			case 'real_rev_rels':{   
				
				$rels = KidsAndRelsOperator::get_related_content($namedParameters, 'main_node_id');    
				$operatorValue = (count($rels) > 0) ? eZContentObjectTreeNode::fetch($rels) : array();
				
				return true;
				break;
				
			}
			
		}
		
	}
}

?>
