<?php

	class ParentNodesOperator
{
	var $Operators;

	function ParentNodesOperator(){
		$this->Operators = array('parent_nodes');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'parent_nodes' => array(
				'id' => array('type'=>'mixed', 'required'=>false, 'default'=>0)
				)
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		$CurrentObjectID=$operatorValue->ID;
		$operatorValue=array();
		foreach(eZDB::instance()->arrayQuery("SELECT * FROM eznode_assignment WHERE contentobject_id=$CurrentObjectID") as $Record){
			$operatorValue[]=$Record['parent_node'];
		}
		$operatorValue=array_unique($operatorValue);
		if($namedParameters['id']){
			$operatorValue=(int)in_array($namedParameters['id'],$operatorValue);
		}
		return true;
	}
}

?>
