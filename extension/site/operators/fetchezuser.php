<?php

	class fetcheZUser
{
	var $Operators;

	function fetcheZUser(){
		$this->Operators = array('fetchezuser');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'fetchezuser' => array(
				'id' => array('type'=>'integer', 'required'=>true, 'default'=>14)
				)
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		return $operatorValue = eZUser::fetch($namedParameters['id']);
	}
}

?>
