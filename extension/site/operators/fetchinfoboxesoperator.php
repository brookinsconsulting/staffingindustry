<?php

/* intended for use on article full view for getting infoboxes to display at the bottom of the right-hand column
 * copied mostly from pagedataoperator around "// Set Infoboxes", line 313 atm
 * see http://issuetrack.thinkcreative.com/view.php?id=12417
 */

	class fetchInfoboxes
{
	var $Operators;

	function fetchInfoboxes(){
		$this->Operators = array('fetchinfoboxes');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return false;
	}

	function namedParameterList(){
		return false;
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		$CurrentNode = $tpl->variable('node');
		$CurrentNodeID = $CurrentNode->attribute('node_id');

		$ExtrainfoItems=eZContentObjectTreeNode::subTreeByNodeID(array('Depth'=>1,'SortBy'=>array('priority',true),'ClassFilterType'=>'include','ClassFilterArray'=>array('infobox')),$CurrentNodeID);

		$CurrentPath = $CurrentNode->pathArray();array_pop($CurrentPath);

		foreach(array_reverse($CurrentPath) as $PathNodeID){
			if($PersistentItems=eZContentObjectTreeNode::subTreeByNodeID(array('Depth'=>1,'SortBy'=>array('priority',true),'ClassFilterType'=>'include','ClassFilterArray'=>array('infobox'),'AttributeFilter'=>array(array('infobox/persistent','=',1))),$PathNodeID)){
				$ExtrainfoItems=array_merge($ExtrainfoItems,$PersistentItems);
			}
		}

		return $operatorValue = $ExtrainfoItems;
	}
}

?>
