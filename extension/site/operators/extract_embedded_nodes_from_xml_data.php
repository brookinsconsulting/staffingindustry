<?php

class extractEmbeddedObjects {
	var $Operators;

	function __construct(){
		$this->Operators = array('extract_embedded_objects');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){	
		$attribute = $operatorValue;

		if ( $attribute == '' ) return array();
		$parser = new eZXMLInputParser();
		
		$raw_xml = $attribute->DataText;
		preg_match_all("/<literal class=\"html\">(.*)?<\/literal>/mis", $attribute->DataText, $matches);

		$objects = array();
		$ad_tag = false;
                $ad_tag_added = false;

                // print_r( $attribute->DataText );

		if ( count( $matches ) > 1 && count( $matches[1] ) > 0)
		{
			$ad_tag = array( 'class_identifier' => 'article', 'data_map' => array('title' => array('content' => html_entity_decode( $matches[1][0] ) ) ) );
			$raw_xml = str_replace($matches[0][0], "", $raw_xml);
		}
                // print_r( $ad_tag );

		$xml = new SimpleXMLElement( $raw_xml );

                // Next rewrite consider using http://www.php.net/manual/en/class.simplexmliterator.php
                // print_r( $xml );

		foreach( $xml->paragraph as $paragraph )
                {
			$useme = $paragraph;

                        // print_r($paragraph->literal);
                        /*
			if (count($paragraph->literal) > 0) {
				// $literalLines = $paragraph->literal;
				// $literalLinesContent = $literalLines[0];
                                if( $ad_tag )
                                {
                                    $objects[] = $ad_tag;
                                    $ad_tag_added = true;
                                }
			} */

			if ( count( $paragraph->line ) > 0 )
                        {
				$lines = $paragraph->line;
				$useme = $lines[0];
			}

                        // print_r( $useme );

			foreach ( $useme->embed as $embed )
                        {
				$objectID = (int) $embed->attributes()->object_id;

                                $objectEmbedClass = (string) $embed->attributes()->class;

				if ( $objectID > 0 )
                                {
					$object = eZContentObject::fetch( $objectID );

					if ( is_a( $object, 'eZContentObject' ) )
                                        {
                                                if( in_array( $objectEmbedClass, array( 'direct_link_eng_us', 'direct_link_eng_gb', 'direct_link_eng_rw' ) ) )
                                                {
                                                        $node = $object->attribute( 'main_node' );
                                                        $nodeID = $node->attribute( 'node_id' );
                                                        $objectEmbedClassLanguageCode = str_replace('direct_link_', '', $objectEmbedClass );

                                                        switch( $objectEmbedClassLanguageCode )
                                                        {
                                                            case "eng_us":
                                                              $languageCode = 'eng-US';
                                                              break;
                                                            case "eng_gb":
                                                              $languageCode = 'eng-GB@euro';
                                                              break;
                                                            case "eng_rw":
                                                              $languageCode = 'eng-RW';
                                                              break;
                                                        }
                                                        $node = eZContentObjectTreeNode::fetch( $nodeID, $languageCode );

                                                        $node->DataMap = $node->dataMap();
                                                        $objects[] = $node;

                                                        $node = false;
                                                        $objectEmbedClass = false;

                                                        continue;
                                                }

						$initialLanguageID = $object->attribute( 'initial_language_id' );
						$initialLanguageMask = $object->attribute( 'language_mask' );
						$object->setAlwaysAvailableLanguageID( $initialLanguageID );

						$object->DataMap = $object->dataMap();
						$objects[] = $object;

						$object->setAttribute( 'initial_language_id', $initialLanguageID );
						// setting language_mask simply does not work like this, direct db query is a compromise
						// $object->setAttribute( 'language_mask', $initialLanguageMask );
						$db = eZDB::instance();
						$q = "
							UPDATE ezcontentobject
							SET language_mask = $initialLanguageMask
							WHERE id = $objectID
						";
						$db->arrayQuery($q);
					}
				}
			}

                        if ( $ad_tag && $ad_tag_added == false )
                        {
                             $objects[] = $ad_tag;
                             $ad_tag_added = true;
                        }

                        // print_r( $lines[1] );

                        foreach( $lines[1]->embed as $embed )
                        {
                                $objectID = (int) $embed->attributes()->object_id;

                                $objectEmbedClass = (string) $embed->attributes()->class;

                                if ( $objectID > 0 )
                                {
                                        $object = eZContentObject::fetch( $objectID );
                                        if ( is_a( $object, 'eZContentObject' ) )
                                        {
                                                if( in_array( $objectEmbedClass, array( 'direct_link_eng_us', 'direct_link_eng_gb', 'direct_link_eng_rw' ) ) )
                                                {
                                                        $node = $object->attribute( 'main_node' );
                                                        $nodeID = $node->attribute( 'node_id' );
                                                        $objectEmbedClassLanguageCode = str_replace('direct_link_', '', $objectEmbedClass );

                                                        switch( $objectEmbedClassLanguageCode )
                                                        {
                                                            case "eng_us":
                                                              $languageCode = 'eng-US';
                                                              break;
                                                            case "eng_gb":
                                                              $languageCode = 'eng-GB@euro';
                                                              break;
                                                            case "eng_rw":
                                                              $languageCode = 'eng-RW';
                                                              break;
                                                        }
                                                        $node = eZContentObjectTreeNode::fetch( $nodeID, $languageCode );

                                                        $node->DataMap = $node->dataMap();
                                                        $objects[] = $node;

                                                        $node = false;
                                                        $objectEmbedClass = false;

                                                        continue;
                                                }

                                                $initialLanguageID = $object->attribute( 'initial_language_id' );
                                                $initialLanguageMask = $object->attribute( 'language_mask' );
                                                $object->setAlwaysAvailableLanguageID( $initialLanguageID );

                                                $object->DataMap = $object->dataMap();
                                                $objects[] = $object;

                                                $object->setAttribute( 'initial_language_id', $initialLanguageID );
                                                // setting language_mask simply does not work like this, direct db query is a compromise
                                                // $object->setAttribute( 'language_mask', $initialLanguageMask );
                                                $db = eZDB::instance();
                                                $q = "
                                                        UPDATE ezcontentobject
                                                        SET language_mask = $initialLanguageMask
                                                        WHERE id = $objectID
                                                ";
                                                $db->arrayQuery($q);
                                        }
                                }
                        }
		}

		$operatorValue = $objects;
		return true;
	}
}

?>