<?php

class extractOASListpos {

	public $Operators;

	function __construct() {
		$this->Operators = array('extract_oas_listpos');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return false;
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		eZDebug::accumulatorStart('extractOASListpos', false, 'extractOASListpos');

		preg_match_all('/realmedia_([a-zA-Z0-9_-]*)/', $operatorValue, $positions);

		/* this does not apply since the 2012 redesign.
		// this is in page_header.tpl and on every page, so it won't be in operatorValue - include it here always
		// except login overlay!
		if (strpos($_SERVER['REQUEST_URI'], 'layout/set/overlay') === false) {
			$positions[1][] = 'Top1';
		}
		 */

		$operatorValue = implode(',', $positions[1]);

		eZDebug::accumulatorStop('extractOASListpos');
		return $operatorValue;
	}
}

?>
