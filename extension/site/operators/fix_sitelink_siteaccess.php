<?php

	class fixSitelinkSiteaccess
{
	var $Operators;

	function fixSitelinkSiteaccess(){
		$this->Operators = array('fix_sitelink_siteaccess', 'fix_sitelink_siteaccess_block');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'fix_sitelink_siteaccess' => array(),
			'fix_sitelink_siteaccess_block' => array()	
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		
		if ($operatorName == 'fix_sitelink_siteaccess') {
			$operatorValue = $this->fix_url($operatorValue);
			return true;
		}
		
		preg_match_all("/href(\s*)?=(\s*)?(['\"][^'\"]+['\"])/miS", $operatorValue, $links);
		
		if (isset($links[3]) && count($links[3]) > 0) {
			foreach($links[3] as $l) {
				$new_l = $this->fix_url($l);
				$operatorValue = str_replace($l, $new_l, $operatorValue);
			}
		}
		return true;
	}
	
	function fix_url($URL) {
		 
		if (strpos($URL, '"') !== false || strpos($URL, "'") !== false) {
			$URL = preg_replace('/["\']/', '', $URL);
			$quotes = true;
		} else {
			$quotes = false;
		}
		
		if (strpos($URL, "/") !== 0 ) return $this->quoteme($URL, $quotes);
		if (strpos($URL, "://") !== false ) return $this->quoteme($URL, $quotes);

		$explodedURL = explode('/', substr($URL, 1));

		// first remove the siteaccess
		switch ($explodedURL[0]) {
			case 'news':
			case 'site':
			case 'site_member':
			case 'eng':
			case 'eng_member':
			case 'row':
			case 'row_member':
				unset($explodedURL[0]);
				// put array back together since we unset [0]
				$explodedURL = explode('/', implode('/', $explodedURL));
				break;
			default:
		}


		// if a system url, we have a node id already
		if ($explodedURL[0] == 'content') {
			$node_id = $explodedURL[3];
		// else fetch by path, get node id from there
		} else {
			// replace all non-alphanumeric and non forward slash characters with underscores,
			// for matching in path_identification_string column
			
			if (implode('/', $explodedURL) == "")  return "/";
			
			$alias = eZURLAliasML::fetchByPath(implode('/', $explodedURL));

			if (is_object($alias[0])) {
				$node_id = array_pop(explode(':', $alias[0]->attribute('action')));
			} 
		}

		if (!isset($node_id)) {
			// abort, leave link as it is
			return $this->quoteme($URL, $quotes);
		}

		// now we should have a node id, figure out the right translation for it
		$db = eZDB::instance();
		$q = "
			SELECT real_translation
			FROM ezcontentobject_tree AS ezcot,ezcontentobject_name 
			WHERE
				ezcot.node_id = $node_id AND
				ezcontentobject_name.contentobject_id = ezcot.contentobject_id AND
				ezcot.contentobject_version IN (
					SELECT version
					FROM ezcontentobject_version
					WHERE ezcontentobject_version.contentobject_id = ezcot.contentobject_id
				)
		";

		$translation=$db->arrayQuery($q,array('column'=>'real_translation'));

		switch ($translation[0]) {
			case 'eng-US':
				$betterSiteaccess = 'site';
				break;
			case 'eng-GB':
				$betterSiteaccess = 'eng';
				break;
			case 'eng-GB@euro':
				$betterSiteaccess = 'eng';
				break;
			case 'eng-RW':
				$betterSiteaccess = 'row';
				break;
		}
		
		$URL = '/' . $betterSiteaccess . '/' . implode('/', $explodedURL);
	
		return $this->quoteme($URL, $quotes);
		
	}
	
	function quoteme($URL, $quotes) {
		if ($quotes)  $URL = '"' . $URL. '"';
		return $URL;
	}
}

?>
