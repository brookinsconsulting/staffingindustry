<?php

	class removeDefaultGuestAuthor
{
	var $Operators;

	function removeDefaultGuestAuthor(){
		$this->Operators = array('removedefaultguestauthor');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'removedefaultguestauthor' => array()
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){

		$db = eZDB::instance();

		$attribute = $operatorValue;

		$emptyAuthorsXML = '<?xml version="1.0" encoding="utf-8"?><ezauthor><authors/></ezauthor>';

		$classAttributeID = $attribute->contentClassAttribute()->ID;
		$objectID = $attribute->object()->ID;
		$version = $attribute->Version;

		$q = "
			UPDATE `ezcontentobject_attribute`
			SET `data_text` = '$emptyAuthorsXML'
			WHERE
				`contentclassattribute_id` = $classAttributeID AND
				`contentobject_id` = $objectID AND
				`version` = $version
		";

		// this cleans up $attribute for use in the edit template but leaves database ugly
		$attribute->content()->removeAuthors(array('0'));

		// this cleans up the database
		$result = $db->query($q);

		eZDebug::writeDebug($q, "removeguestauthor query = $result");

		return $operatorValue;
	}
}

?>
