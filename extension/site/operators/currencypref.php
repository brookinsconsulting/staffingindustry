<?php

	class currencyPrefOperator
{
	var $Operators;

	function currencyPrefOperator(){
		$this->Operators = array('currencypref', 'set_currencypref');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){

		if ($operatorName == 'currencypref') {
		
			$operatorValue = eZShopFunctions::preferredCurrencyCode();
			
		} else {

            $http = eZHTTPTool::instance();

            if (!$http->hasSessionVariable('eZUserLoggedInID')) {
				$http->setSessionVariable('eZUserLoggedInID', eZUser::currentUserID());
			}

			eZPreferences::setValue( 'user_preferred_currency', $operatorValue );
			$operatorValue = '';
			
		}
		
		return true;
		
	}
}

?>
