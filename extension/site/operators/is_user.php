<?php

class isUser {

	public $Operators;
	private $buyerGroupID = 461;
	private $staffingGroupID = 453;

	function __construct() {
		$this->Operators = array('is_buyer', 'is_staffing');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return false;
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		$user = $operatorValue;
		$operatorValue = false;
		switch ($operatorName) {
			case 'is_buyer':
				if (in_array($this->buyerGroupID, $user->groups()))
					$operatorValue = true;
				break;
			case 'is_staffing':
				if (in_array($this->staffingGroupID, $user->groups()))
					$operatorValue = true;
				break;
		}
		return $operatorValue;
	}
}

?>
