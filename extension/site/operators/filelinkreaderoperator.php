<?php

class FileLinkReaderOperator
{
  var $Operators;

  function __construct(){
    $this->Operators = array('filelinkreader', 'listreaders');
  }

  function &operatorList(){
    return $this->Operators;
  }

  function namedParameterPerOperator(){
    return true;
  }

  function namedParameterList(){
    return array(
      'filelinkreader' => array(
          "url" => array(
            "type"    =>  "string",
            "required"=>  true,
			"default" => ""
          ),
          "mime" => array(
            "type" => "string",
			"required" => false,
			"default" => ""
          ),
      ),
      'listreaders' => array(),
    );
  }

  function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){

    switch ($operatorName) {

      case 'listreaders':
		$documentReaders = PageDataOperator::getPersistentVariable($tpl, "documentReaders");
        if (!$documentReaders) return;

        // filelinkreader ini
        $ini = eZINI::instance("filelinkreader.ini");

        // output array
        $operatorValue = array();

        foreach ($documentReaders as $filetype) {
          if (!$ini->hasVariable("Type_{$filetype}", "ViewerName"))
            continue;
          $operatorValue[] = array(
            "icon"            =>  $ini->variable("Type_{$filetype}", "Icon"),
            'file_type_name'  =>  $ini->variable("Type_{$filetype}", "Name"),
            "viewer_name"     =>  $ini->variable("Type_{$filetype}", "ViewerName"),
            "viewer_url"      =>  $ini->variable("Type_{$filetype}", "ViewerUrl"),
          );
        }

        PageDataOperator::deletePersistentVariable("documentReaders");
        return True;

      case "filelinkreader":
      default:
        // filelinkreader ini
		$operatorValue = array();
		
		self::get_file_data($operatorValue, $namedParameters, $tpl);
		
        return true;

    }
  }

  static function get_file_data(&$operatorValue, $namedParameters, $tpl) {
	
	$ini = eZINI::instance("filelinkreader.ini");

	// Settings
	$MIME_ICO_DIR = $ini->variable("Settings","MimeIconDirectory");

	// params
	$url = iconv("ascii", "ascii//ignore", trim($namedParameters['url']));
	if (!$url) return false;
	$mimetype = iconv("ascii", "ascii//ignore", trim(isset($namedParameters['mime']) ? $namedParameters['mime'] : false));
	
	$documentReaders = PageDataOperator::getPersistentVariable($tpl, "documentReaders");

	// output url
	$operatorValue['url'] = urldecode(trim($url));

	// get mimetype
	if (!$mimetype) {
		$mime = eZMimeType::findByURL($url, False);
		$mimetype = $mime['name'];
	}

	// output filename
	$operatorValue['filename'] = basename(urldecode($url));

	// output mimetype
	$operatorValue['mimetype'] = $mimetype;

	// output default mimeicon
	$operatorValue['mimeicon'] = "";

	// output default file_type_name
	$operatorValue['file_type_name'] = "";

	// match mime for icon
	$initype = $ini->hasVariable($mimetype, "Type") ? $ini->variable($mimetype, "Type") : false;
	if ($initype && $ini->hasVariable("Type_{$initype}", "Name") && $ini->hasVariable("Type_{$initype}", "Icon")) {

	// output file type name
	$operatorValue['file_type_name'] = $ini->variable("Type_{$initype}", "Name");

	// output mimeicon
	$operatorValue['mimeicon'] = $ini->variable("Type_{$initype}", "Icon");
	}
	
	if (!in_array(trim($initype), $documentReaders)) {
		$documentReaders[] = trim($initype);
        PageDataOperator::setPersistentVariable("documentReaders", $documentReaders, $tpl, false);
	}

	return true;
		
  }

}

?>