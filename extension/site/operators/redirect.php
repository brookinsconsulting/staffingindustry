<?php

	class Redirect
{
	var $Operators;

	function Redirect(){
		$this->Operators = array('redirect');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'redirect' => array(
				'path' => array('type'=>'string', 'required'=>true)
			)
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		eZHTTPTool::redirect($namedParameters['path']);
		return true;
	}
}

?>
