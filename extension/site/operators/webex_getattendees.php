<?php

	class webExGetAttendees
{
	var $Operators;

	function webExGetAttendees(){
		$this->Operators = array('webex_getattendees');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'webex_getattendees' => array(
				'id' => array('type'=>'integer', 'required'=>true)
				)
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		$ini = eZINI::instance('webex.ini');

		$WebExID = $ini->variable('WebExSettings', 'WebExID');
		$Password = $ini->variable('WebExSettings', 'Password');
		$SiteID = $ini->variable('WebExSettings', 'SiteID');
		$SiteName = $ini->variable('WebExSettings', 'SiteName');
		$SiteURL = $ini->variable('WebExSettings', 'SiteURL');

		$tc = new TCWebEx($WebExID, $Password, $SiteID, $SiteName, $SiteURL);

		$attendees = $tc->whoRegistered($namedParameters['id']);


		
		if (is_array($attendees)) {
		    foreach ($attendees as $key => &$attendee) {
    			$attendee_id_array = explode('_', $key);
    			$attendee_id = $attendee_id_array[1];
    			
    			if (eZPreferences::value('attendee_' . $attendee_id) == false) {
    				unset($attendees[$key]);
    				continue;
    			}
    			$attendee = (array) $attendee;
    			foreach ($attendee as &$attribute) {
    				if (is_object($attribute)) {
    					$attribute = (array) $attribute;
    					foreach ($attribute as &$subattribute) {
    						if (is_object($subattribute)) {
    							$subattribute = (array) $subattribute;
    						}
    					}
    				}
    			}
    		}
		}



		$operatorValue = $attendees;
		return true;
	}
}

?>
