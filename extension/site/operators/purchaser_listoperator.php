<?php

class purchaserList
{
    var $Operators;

    function purchaserList( $name = "purchaser_list" )
    {
        $this->Operators = array( $name );
    }

    /*! Returns the template operators.
    */
    function operatorList()
    {
        return $this->Operators;
    }


    function namedParameterPerOperator()
    {
        return true;
    }   

    function namedParameterList()
    {
        return array( 'purchaser_list' => array() );
    }


    function modify( $tpl, &$operatorName, &$operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue, &$namedParameters )
    {

                if ($operatorValue != null) {
                        
                        $pid = $operatorValue->ID;
                
                        $query = "select ezorder.id as id, order_nr as orn, user_id from ezorder, ezproductcollection_item where is_temporary = 0 and ezorder.productcollection_id = ezproductcollection_item.productcollection_id and contentobject_id = $pid";

                        $db = eZDB::instance();

                        $results =  $db->arrayQuery( $query );
                
                        $out = array();
                
                        foreach ($results as $r) {
                                if (array_key_exists($r['user_id'], $out)) {
                                        $out[$r['user_id']][] = array('id' => $r['id'], 'on' => $r['orn'] );
                                } else {
                                        $out[$r['user_id']] = array(array('id' => $r['id'], 'on' => $r['orn'] ));
                                }
                        }
                        
                        
                    $operatorValue = $out;
                } else {
                        $operatorValue = array();
                }
                
		return true;
    }
}
?>
