<?php

	class siteaccessRedirect
{
	var $Operators;

	function siteaccessRedirect(){
		$this->Operators = array('siteaccess_redirect');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'siteaccess_redirect' => array(
				)
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		if (
			$GLOBALS['eZTemplateDesignResourceInstance']->Keys['class_identifier'] == 'xrow_product' ||
			$GLOBALS['eZRequestedModuleParams']['module_name'] == 'xrowecommerce' ||
			$GLOBALS['eZRequestedModuleParams']['view_name'] == 'login' ||
			$GLOBALS['eZRequestedModuleParams']['module_name'] == 'shop'
		) {
			// no redirect desired under these circumstances
			return true;
		}

		$currentSiteaccess = $GLOBALS['eZCurrentAccess']['name'];
		$nameArr = explode('_', $currentSiteaccess);
		$currentLang = $nameArr[0];
		$preferredLang = isset($_COOKIE['lang']) ? $_COOKIE['lang'] : $currentLang;

		$pathArr = explode('/', $_SERVER['REQUEST_URI']);
		if (strpos($pathArr[1], 'site') !== false || strpos($pathArr[1], 'eng') !== false || strpos($pathArr[1], 'row') !== false) {
			$requestedSiteaccess = $pathArr[1];
			unset($pathArr[1]);
		}
		
		$pathWithoutSiteaccess = implode('/', $pathArr);

		$storeme = $GLOBALS['eZContentLanguagePrioritizedLanguages'];
		$GLOBALS['eZContentLanguagePrioritizedLanguages'] = array(eZContentLanguage::fetchByLocale('eng-GB@euro'), eZContentLanguage::fetchByLocale('eng-US'), eZContentLanguage::fetchByLocale('eng-RW'));
		$is_real_node = count(eZURLAliasML::fetchByPath(str_replace($currentSiteaccess."/", "",$_SERVER['REQUEST_URI'])));
		$GLOBALS['eZContentLanguagePrioritizedLanguages'] = $storeme;
		if ($is_real_node) {
		
			switch ($currentLang) {
				case 'eng':
					$preferredLang = 'site';
					break;
				case 'site':
					$preferredLang = 'row';
					break;
				case 'row':
					$preferredLang = 'eng';
					break;
			}
		
		}

		if ($currentLang != $preferredLang) {
			$newSiteaccess = $preferredLang;
		}

		if (isset($newSiteaccess)) {
			$redirect = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $newSiteaccess . $pathWithoutSiteaccess;
			header("Location: $redirect");
		}
	}
}

?>
