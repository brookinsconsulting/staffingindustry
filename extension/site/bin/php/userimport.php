#!/usr/bin/env php
<?php

require 'autoload.php';

set_time_limit(0);

$cli = eZCLI::instance();

$script = eZScript::instance(
	array(
		'description' => 'create users from a csv',
		'use-session' => true,
		'use-modules' => true,
		'use-extensions' => true
	)
);

$options = $script->getOptions(
	"[timestamp:][email:][nodeid:][userid:][scriptid:]",
	"",
	array(
		'timestamp' => '',
		'email' => '',
		'nodeid' => 'node id underwhich to create users',
		'userid' => 'user id of script runner'
	),
	false,
	true
);


$scheduledScript = false;
if (
	isset($options['scriptid']) and
	in_array('ezscriptmonitor', eZExtension::activeExtensions()) and
	class_exists('eZScheduledScript')
) {
	$scriptID = $options['scriptid'];
	$scheduledScript = eZScheduledScript::fetch($scriptID);
}

$files = array(
	'uifile' => array(
		'tmp_name' => 'var/userimport/' . $options['timestamp']
	)
);

$params = array(
	'NodeID' => $options['nodeid'],
	'UserID' => $options['userid']
);

$result = TCUserImport::importCSV($files, $params, $scheduledScript);
$cli->output( $result );

// Create a new mail composer object
$mail = new ezcMailComposer();
// Specify the "from" mail address
$mail->from = new ezcMailAddress( 'memberservices@staffingindustry.com', 'Staffing Industry Analysts' );
// Add one "to" mail address (multiple can be added)
$mail->addTo( new ezcMailAddress($options['email'], '' ) );
// Specify the subject of the mail
$mail->subject = "User Import Results";
// Specify the plain text of the mail
if ($result == '') $result = 'Job complete';
$mail->plainText = "$result";
// Build the mail object
$mail->build();
// Create a new MTA transport object
$transport = new ezcMailMtaTransport();
// Use the MTA transport to send the created mail object
$transport->send( $mail ); 

$script->shutdown();

?>
