<?php

require 'autoload.php';

$cli = eZCLI::instance();

$script = eZScript::instance( array( 'description' => ( "Disable users."),
                                     'use-session' => true,
                                     'use-modules' => true,
                                     'use-extensions' => true ) );

								$script->startup();
								$scriptOptions = $script->getOptions( "","",array(),false,array( 'user' => true ));
								$script->initialize();
								
								
$user = eZUser::fetch(172);
$user->loginCurrent();

$move_ids = array(58371,58250,4582,58245,57641,57846,58346,58024,58025,57780,57892,57890,58251,57813,58183,58075,58533,57695,58403,57634,58480,58202,57701,58386,58537,57655,57693,57760,57856,57676,57851,57882,58033,58316,57607,57818,57919,58307,58334,58418,57906,57870,57772,57915,57718,57715,57568,57645,58384,58240,58295,57652,58180,58392,57922,58037,58150,57632,58071,57740,57584,58113,57674,57700,58012,57559,58111,57731,58519,58093,58462,57706,58054,10342,58368,57672,57964,58221,58084,58086,57831,58042,58422,58106,57566,58157,58168,58141,58358,57673,58282,58217,57713,58492,58434,57663,58306,57733,57876,10366,10368,57542,58313,58125,57902,57997,58039,58464,57853,57697,43800,58187,57555,57867,57779,57967,57702);

foreach ($move_ids as $objectID) {
	print_r($objectID);
	$object = eZContentObject::fetch( $objectID );
	if (!is_object($object)) continue;
	$cur_node = $object->mainNodeID();
	$out = eZContentObjectTreeNodeOperations::move( $cur_node, 44744 );
	eZContentObject::fixReverseRelations( $objectID, 'move' );
	
}

eZContentCacheManager::clearAllContentCache();
eZUser::cleanupCache();

?>