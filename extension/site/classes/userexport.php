<?php

class TCUserExport {

	static public function exportUsers($post, $scheduledScript = false) {

		// this used to be a view parameter, just set it here
		$ClassIdentifier = 'client';

		/*
		 * Most of what's below was copied straight from the module view.
		 * I added some code to update ezscriptmonitor in the loop going through the list of nodes near the end
		 */

		$UserClass=eZContentClass::fetchByIdentifier($ClassIdentifier);
		//eZDebug::writeDebug($UserClass,'UserClass');
		$DataMap=$UserClass->dataMap();
		$GroupedDataMap=createGroupedDataMap($DataMap);
		$db=eZDB::instance();

		//header('Content-Type: text/plain');

/*
		if(isset($post['format']) && $post['format']=='csv'){
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"users_information.csv\"");
		}
*/

		$UserGroupNodeID=$db->arrayQuery("SELECT DISTINCT ezcontentobject_tree.node_id as id FROM ezcontentobject, ezcontentobject_tree WHERE ezcontentobject_tree.contentobject_id = ezcontentobject.id AND ezcontentobject.contentclass_id = (SELECT ezcontentclass.id FROM ezcontentclass WHERE ezcontentclass.identifier = 'user_group')",array('column'=>'id'));
		//	var_dump($UserGroupNodeID);
		//echo "\n\n";
		//	var_dump($post);
		//echo "\n\n";
		$objectProperties=array();
		
		if(isset($post['object_properties']) && count($post['object_properties'])){
			foreach($post['object_properties'] as $item){
				$objectProperties[$item]=array(
					'identifier'=>$item,
					'filter'=>$post["object_properties-$item-filter"],
					'query'=>str_replace('*','%',$post["object_properties-$item-query"])
				);
			}
		}
		
		$objectAttributes=array();
		if(isset($post['object_attributes']) && count($post['object_attributes'])){
			foreach($post['object_attributes'] as $item){
				$objectAttributes[$item]=array(
					'identifier'=>"$ClassIdentifier/$item",
					'filter'=>$post["object_attributes-$item-filter"],
					'query'=>$post["object_attributes-$item-query"]
				);
				$filterAttributes[]=array($objectAttributes[$item]['identifier'],$objectAttributes[$item]['filter'],$objectAttributes[$item]['query']);
			}
		}

			if(isset($objectProperties['published'])){
				$publishedValues=$objectProperties['published'];
				$filterAttributes[]=array($publishedValues['identifier'],'between',array(strtotime($publishedValues['query'][0]),(strtotime($publishedValues['query'][1])+86399) ));
			}
	/*
			var_dump(array(
				'class_id'=>$UserClass->ID,
				'class_identifier'=>$ClassIdentifier,
				'properties'=>$objectProperties,
				'attributes'=>$objectAttributes,
				'attribute_filter'=>$filterAttributes
				));

		echo "\nOther Items\n";
		*/
		$classFilterArray=array($ClassIdentifier);
		$sortingInfo = eZContentObjectTreeNode::createSortingSQLStrings(false, 'ezcontentobject_tree', false);
		$classCondition = eZContentObjectTreeNode::createClassFilteringSQLString('include', $classFilterArray);
		$attributeFilter = eZContentObjectTreeNode::createAttributeFilterSQLStrings($filterAttributes, $sortingInfo);

		//echo "\nAttribute Filter:\n".var_export($attributeFilter,true)."\n\n";

		//eZContentObjectTreeNode::createExtendedAttributeFilterSQLStrings( $ExtendedAttributeFilter=false );
		$mainNodeOnlyCond = eZContentObjectTreeNode::createMainNodeConditionSQLString($mainNodeOnly=true);
		$pathStringCond     = '';
		$notEqParentString  = '';
		// If the node(s) doesn't exist we return null.
		eZContentObjectTreeNode::createPathConditionAndNotEqParentSQLStrings($pathStringCond, $notEqParentString, 5, false, false);
		$useVersionName = true;
		$versionNameTables = eZContentObjectTreeNode::createVersionNameTablesSQLString($useVersionName);
		$versionNameTargets = eZContentObjectTreeNode::createVersionNameTargetsSQLString($useVersionName);
		$versionNameJoins = eZContentObjectTreeNode::createVersionNameJoinsSQLString($useVersionName, false);
		$languageFilter = ' AND ' . eZContentLanguage::languagesSQLFilter('ezcontentobject');
		//if($language){eZContentLanguage::clearPrioritizedLanguages();}
		//$objectNameFilterSQL = eZContentObjectTreeNode::createObjectNameFilterConditionSQLString( $objectNameFilter );
		$limitation = ( isset( $params['Limitation']  ) && is_array( $params['Limitation']  ) ) ? $params['Limitation']: false;
		$limitationList = eZContentObjectTreeNode::getLimitationList( $limitation );
		//$sqlPermissionChecking = eZContentObjectTreeNode::createPermissionCheckingSQL( $limitationList );
		// Determine whether we should show invisible nodes.
		//$showInvisibleNodesCond = eZContentObjectTreeNode::createShowInvisibleSQLString( !$ignoreVisibility );

		$ownerSQL=array('from'=>'','where'=>'');
		if(isset($objectProperties['owner'])){
			$ownerSQL=array(
				'from'=>'',
				'where'=>'AND ezcontentobject.owner_id '.$objectProperties['owner']['filter']." '".$objectProperties['owner']['query']."'"
			);
		}
		//var_dump($ownerSQL);

		$emailSQL=array('from'=>'','where'=>'');
		if(isset($objectProperties['email'])){
			$emailSQL=array(
				'from'=>', ezuser',
				'where'=>'AND ezuser.contentobject_id = ezcontentobject.id AND ezuser.email '.$objectProperties['email']['filter']." '".$objectProperties['email']['query']."'"
			);
		}
		//var_dump($emailSQL);


		$roleSQL=array('from'=>'','where'=>'');
		if(isset($objectProperties['roles'])){
			$roleSQL=array(
				'from'=>', ezuser_role',
				'where'=>"AND ezuser_role.contentobject_id = ezcontentobject.id AND ezuser_role.role_id = ".$objectProperties['roles']['query']
			);
		}
		//var_dump($roleSQL);


		$directWhereSQL='';
		if(isset($objectProperties['direct'])){
			$directWhereSQL='AND ezcontentobject_tree.parent_node_id IN ('.implode($UserGroupNodeID,',').')';
		}
		//var_dump($directWhereSQL);

		$query = "SELECT DISTINCT
					ezcontentobject.*,
					ezcontentobject_tree.*,
					ezcontentclass.serialized_name_list as class_serialized_name_list,
					ezcontentclass.identifier as class_identifier,
					ezcontentclass.is_container as is_container
					$versionNameTargets
					$sortingInfo[attributeTargetSQL]
				FROM
					ezcontentobject_tree,
					ezcontentobject,ezcontentclass
					$versionNameTables
					$sortingInfo[attributeFromSQL]
					$attributeFilter[from]
					$emailSQL[from]
                                        $ownerSQL[from]
					$roleSQL[from]
				WHERE
					$pathStringCond
					$sortingInfo[attributeWhereSQL]
					$attributeFilter[where]
					ezcontentclass.version=0 AND
					$notEqParentString
					ezcontentobject_tree.contentobject_id = ezcontentobject.id  AND
					ezcontentclass.id = ezcontentobject.contentclass_id AND
					$mainNodeOnlyCond
					$classCondition
					$versionNameJoins
					$languageFilter
					$directWhereSQL
					$emailSQL[where]
                                        $ownerSQL[where]
					$roleSQL[where]";

		if( $sortingInfo['sortingFields']){$query .= " ORDER BY $sortingInfo[sortingFields]";}
		
		//$server = count( $sqlPermissionChecking['temp_tables'] ) > 0 ? eZDBInterface::SERVER_SLAVE : false;
		$server = false;

		// echo "\n\nQUERY:\n$query\n\n";

		$nodeListArray = $db->arrayQuery( $query, array(), $server );
		//echo "Query Count: ".count($nodeListArray)."\n\n";

		$IgnoreDataTypeList=SiteUtils::configSetting('GeneralSettings','IgnoreDataTypeList','website.ini');

		if (!$IgnoreDataTypeList) $IgnoreDataTypeList = array();

		foreach($DataFields=$UserClass->dataMap() as $FieldName=>$FieldValue){
			$DataType=$FieldValue->dataType()->DataTypeString;
			if(!in_array($DataType,$IgnoreDataTypeList)){
				$DataFields[$FieldName]=trim($FieldValue->name());
			}else{
				unset($DataFields[$FieldName]);
			}
		}

		$DataFields[] = 'is_enabled';
		$DataFields[] = 'user_account_email';
		$DataFields[] = 'roles';
		
		$UserCSVData = array();

		$Template=eZTemplate::factory();
		foreach($nodeListArray as $key=>$row){
			$UserObject=$nodeListObjects[]=eZUser::fetch($row['contentobject_id']);
			$UserContentObject=$UserObject->contentObject();
			$UserDataMap=$UserContentObject->dataMap();
			$UserAttributeData=array();

			foreach($UserDataMap as $identifier=>$attribute){
				$DataType=$attribute->dataType()->DataTypeString;
				if(!in_array($DataType,$IgnoreDataTypeList)){
		//			$UserAttributeData[] = $attribute->attribute('contentclass_attribute_name');
					$Template->setVariable('attribute',$attribute);
					$UserAttributeData[] = $Template->fetch("design:website/datatype/csv/$DataType.tpl");
				}
			}

			$UserAttributeData[] = (string) $UserObject->attribute('is_enabled');
			$UserAttributeData[] = (string) $UserObject->attribute('email');
			$UserAttributeData[] = (string) $UserContentObject->attribute('owner_id');

			$roles = array();
			foreach ($UserObject->roles() as $role) {
				$roles[] = $role->Name;
			}
			$UserAttributeData[] = implode(', ', $roles);

			$UserCSVData[] = '"' . implode('","',preg_replace('/"/', '\\"', $UserAttributeData)) . '"';

			if ( $scheduledScript ) {
				$percentComplete = (int) floor(($key+1)/count($nodeListArray)*100);
				$scheduledScript->updateProgress($percentComplete);
			}
		}

// old output, return this instead
/*
		echo implode(',',$DataFields)."\n";
		echo implode("\n",$UserCSVData);
*/
		$return = '';
		$return .= '"' . implode('","',$DataFields) . "\"\n";
		$return .= implode("\n",$UserCSVData);

		return $return;



		/*
		$Template=eZTemplate::factory();
		$Template->setVariable('users',$nodeListObjects);
		$Template->setVariable('data_fields',$DataFields);
		echo $Template->fetch('design:website/csv/users.tpl');
		*/
		//var_dump($nodeListObjects);

		//var_dump($UserCSVData);
		//$retNodeList = eZContentObjectTreeNode::makeObjectsArray($nodeListArray);
		//eZContentObject::fillNodeListAttributes($retNodeList);

		//var_dump($nodeListArray);
		//var_dump($retNodeList);

		//var_dump($nodeListObjects);

		/*
				foreach($users=$db->arrayQuery("SELECT contentobject_id FROM ezuser WHERE email ". $objectProperties['email']['filter'] ." '". $objectProperties['email']['query'] ."'") as $key=>$user){
					$users[$key]=eZUser::fetch($user['contentobject_id']);
				}
				var_dump($users);
		*/
			
		//	$db->dropTempTableList($sqlPermissionChecking['temp_tables']);
	}

	static public function cleanUpDataFile($timestamp) {
		unlink("var/userexport/$timestamp");
	}

}

function createGroupedDataMap( $contentObjectAttributes ) {
	$groupedDataMap  = array();
	$contentINI      = eZINI::instance( 'content.ini' );
	$categorys       = $contentINI->variable( 'ClassAttributeSettings', 'CategoryList' );
	$defaultCategory = $contentINI->variable( 'ClassAttributeSettings', 'DefaultCategory' );
	foreach( $contentObjectAttributes as $attribute ) {
		$classAttribute      = $attribute;
		$attributeCategory   = $classAttribute->attribute('category');
		$attributeIdentifier = $classAttribute->attribute( 'identifier' );
		if ( !isset( $categorys[ $attributeCategory ] ) || !$attributeCategory )
			$attributeCategory = $defaultCategory;

		if ( !isset( $groupedDataMap[ $attributeCategory ] ) )
			$groupedDataMap[ $attributeCategory ] = array();

		$groupedDataMap[ $attributeCategory ][$attributeIdentifier] = $attribute;
	}
	return $groupedDataMap;
}

?>
