<?php



class WebEx extends SimpleXmlElement{}
class TCWebEx
{
  private $webex;

  public function __construct($webExID, $password, $siteID, $siteName, $siteURL)
  {
    $this->webex = new WebExStub($webExID, $password, $siteID, $siteName, $siteURL);
  }

  public function addAttendee($webinar_id, $attendee_email, $attendee_name, $attendee_title, $attendee_company)
  {
    $attendees =  array(
                    array(
                      'info'    => array(
                        'name'        => $attendee_name,
                        'title'       => $attendee_title,
                        'company'     => $attendee_company,
                        'email'       => $attendee_email
                      ),
                      'options' => array(
                        'sessionKey'  => $webinar_id
                      )
                    )
                  );
                        
    $attendee = $this->scrub($this->webex->attendee_RegisterMeetingAttendee($attendees));
    if (isset($attendee->body->bodyContent->register->attendeeID))
    {
      return (string)$attendee->body->bodyContent->register->attendeeID;
    }
    return $attendee;
  }

  public function removeAttendee($attendee_id)
  {
    $attendee = $this->scrub($this->webex->attendee_DelMeetingAttendee($attendee_id));
    if ($attendee->header->response->result == 'SUCCESS')
    {
      return 1;
    }
    return -1;
  }

  public function getEvent($webinar_id)
  {
    $event = $this->scrub($this->webex->event_GetEvent($webinar_id));
    if (isset($event->body->bodyContent))
    {
      return $event->body->bodyContent;
    }
    return array();
  }

  public function whoRegistered($webinar_id)
  {
    $attendees = $this->scrub(
      $this->webex->attendee_LstMeetingAttendee(
        $webinar_id,
        array(
          array(
            "By" => "ATTENDEENAME", 
            "AD" => "ASC"
          )
        ),
        1,
        1000
      )
    );
    if (isset($attendees->body->bodyContent->attendee))
    {
      return $this->toArray($attendees->body->bodyContent->attendee, 'attendeeId');
    }
    return $attendees;
  }

  public function whoAttended($webinar_id)
  {
    //return $this->scrub($this->webex->history_LsteventattendeeHistory($webinar_id));
    $attendees = $this->scrub(
      $this->webex->history_LsteventattendeeHistory(
        $webinar_id,
        array(
          array(
            "By" => "ATTENDEENAME", 
            "AD" => "ASC"
          )
        ),
        '',
        '',
        '',
        '',
        array(
          'startFrom'   => 1,
          'maximumNum'  => 1000
        )
      )
    );
    if (isset($attendees->body->bodyContent->eventAttendeeHistory))
    {
      return $this->toArray($attendees->body->bodyContent->eventAttendeeHistory);
    }
    return $attendees;
  }
  
  public function getEvents()
  {
    $lastWeek = time() - (7 * 24 * 60 * 60);
    $events = $this->scrub(
      $this->webex->event_LstsummaryEvent(
        1, 
        '', 
        '', 
        array(
          array(
            "By" => "STARTTIME", 
            "AD" => "ASC"
          )
        ), 
        '', 
        array(
          'startDateStart' => date('m/d/Y 00:00:00', $lastWeek)
        )
      )
    );
    if (isset($events->body->bodyContent->event))
    {
      return $this->toArray($events->body->bodyContent->event, 'sessionKey');
    }
    return array();
  }
  function getKey(&$obj, $map)
  {
    if (str_replace('->', '', $map) == $map)
    {
      return $obj->{$map};
    }
    $parts = explode('->', $map);
    $key = array_shift($parts);
    return $this->getKey($obj->{$key}, implode('->', $parts));
  }
  
  function toArray(&$arr, $key_field=null)
  {
    $arr_new = array();
    foreach ($arr as $key => $val)
    {
      if (isset($key_field))
      {
        $arr_new['ID_'.strtolower($this->getKey($val, $key_field))] = $val;
      }
      else
      {
        $arr_new[] = $val;
      }
    }
    return $arr_new;
  }
  
  function cleanNamespaces($string)
  {
    return preg_replace(
      '/\<([^:>]+):/', 
      '<',
      preg_replace(
        '/\<\/([^:>]+):/', 
        '</', 
        $string
      )
    );
  }
  
  function scrub($string)
  {
    $string = $this->cleanNamespaces($string);
    $string = simplexml_load_string($string, 'WebEx');
    if (!$string) 
    {
      echo "Failed loading XML\n";
      foreach(libxml_get_errors() as $error) 
      {
        echo "\t", $error->message;
      }
    }
    return $string;
  }
}
?>
