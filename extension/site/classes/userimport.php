<?php

class TCUserImport {

	static public function importCSV($files, $params, $scheduledScript = false) {
		
		ini_set("auto_detect_line_endings", true);

		$fileHandler = eZClusterFileHandler::instance();

		// translated from a module view that depending on $_FILES ($files now) and $Params ($params now)
		// this function just returns an error string which is used in the template, left that intact from the view
		if (array_key_exists('uifile', $files) && array_key_exists('tmp_name', $files['uifile']) && array_key_exists('NodeID', $params) && $files['uifile']['tmp_name']) {

			$access = array(
				'name' => 'site_admin',
				'type' => eZSiteAccess::TYPE_DEFAULT,
				'uri_part' => array()
			);
			eZSiteAccess::change($access);
			eZSiteAccess::reInitialise();
			
			$user = eZUser::fetch($params['UserID']);
			
			$user->loginCurrent();			

			$db = eZDB::instance();

			$error_string = '';

			$class_id = 56;
			
			$parent_id = $params['NodeID'];

			$fileHandler->fileFetch($files['uifile']['tmp_name']);
			$file_array = file($files['uifile']['tmp_name']);
			//print_r(count($file_array));
			foreach ($file_array as $line_number =>$line) {

				/* this would normally be best at the end of the loop
				 * but there are "break" etc. that complicate that;
				 * rather than rewriting those i'll just put it here
				 */
				if ( $scheduledScript ) {
					$percentComplete = (int) floor(($line_number+1)/count($file_array)*100);
					$scheduledScript->updateProgress($percentComplete);
				}
			
			
				preg_match_all('/"[^"]*"/', $line, $matches);
			
				foreach ($matches as $m) {
					$line = str_replace($m, str_replace(",", "qqq", $m), $line);
				}
			
				$line = str_replace(",", '[tab]', $line);
				$line = str_replace("qqq", ',', $line);
				$line = str_replace('"', '', $line);
			
				$user_r = explode("[tab]", trim($line));
				////print_r($user_r);
				if ($line_number == 0) {
					$key_map = $user_r;
					
					array_walk($key_map, function(&$n) { 
					  $n = str_replace(" ", "_", strtolower($n)); 
					});
					$missing = array();
					$fatal = false;
					$testme = array("email_address","first_name","mi","last_name","name","member_id","company","location","descr","address_1","address_2","city","state","postal","country","phone","job_title","fs_gl_deptid","deptid","dept","username","password");
					foreach ($testme as $test) {
						if (!in_array($test, $key_map)) {
							$missing[] = $test;
						}
					}

					$key_missing = array_intersect(array("first_name","last_name","company", "email_address"), $missing);
					if (!in_array('contentobject_id', $key_map)) {
						if (count($key_missing) > 0) {
							$error_string = "Data could not be imported! The following vital fields were missing: " . implode(", ", $key_missing);
							break;
						} elseif (count($missing) ) {
							$error_string = "The following data fields were missing and have been ignored : " . implode(", ", $missing);
						}
					}
					$map_key = array_flip($key_map);

					continue;
				}
				
				
				$contentobject_id = false;
				
				if (in_array('contentobject_id', $key_map)) {
					$contentobject_id = $user_r[$map_key['contentobject_id']];
					
					if (count($map_key) < 3) {
						
						$ex_object = eZContentObject::fetch($contentobject_id);
						
						if (!is_object($ex_object)) continue;
						
						$version = $ex_object->attribute( 'current' );		
						
						$removeList = array();	
						$moved = false;		
						
						foreach ( $version->attribute( 'node_assignments' ) as $assignment )
					    {

						if ($assignment->attribute('parent_node') == 58054) $moved = true;
					
							$thisnode = $assignment->fetchNode();
							
							if (!$thisnode) continue;
							
							$removeList[$thisnode->attribute( 'node_id' )] =1;
					
					    }
						if (!$moved) {
							$ex_object->setAttribute( 'status', eZContentObject::STATUS_DRAFT );
						    $ex_object->store();
						    $version->setAttribute( 'status', eZContentObjectVersion::STATUS_DRAFT );
						    $version->store();

							$version->assignToNode( 58054, true );

							$operationResult = eZOperationHandler::execute( 'content', 'publish', array( 'object_id' => $contentobject_id,
						                                                                                 'version' => $version->attribute( 'version' ) ) );

						    if ( eZOperationHandler::operationIsAvailable( 'content_removelocation' ) )
					        {
					            $operationResult = eZOperationHandler::execute( 'content',
					                                                            'removelocation', array( 'node_list' => array_keys( $removeList ) ),
					                                                            null,
					                                                            true );
					        }
					        else
					        {
					            eZContentOperationCollection::removeNodes( array_keys( $removeList ) );
					        }
						}
				
						$operationResult = eZOperationHandler::execute( 'user',
			                                                           'setsettings', array( 'user_id'    => $contentobject_id,
			                                                                                  'is_enabled' => false,
			                                                                                  'max_login'  => 0 ) );
					
						continue;
						
					}
				}
				
				if (array_key_exists('username', $map_key)) $un = $user_r[$map_key['username']];
				
				if (!$un || $un == '') $un = $user_r[$map_key['email_address']];
				
				$un_slashed = addslashes($un);

				$query = "select main_node_id, o.contentobject_id as contentobject_id  from ezuser u, ezcontentobject_tree o where o.contentobject_id = u.contentobject_id AND u.login = '$un_slashed'";

				$results = $db->arrayQuery($query);
				if (count($results) > 0 & $contentobject_id == false) {
					$error_string .= "\nThe following record already exists and was skipped: $un";
					continue;
				}
				
				$email_slashed = addslashes($user_r[$map_key['email_address']]);
				
				$query = "select main_node_id, o.contentobject_id as contentobject_id  from ezuser u, ezcontentobject_tree o where o.contentobject_id = u.contentobject_id AND u.email = '$email_slashed'";

				$results = $db->arrayQuery($query);
				if (count($results) > 0 & $contentobject_id == false) {
					$error_string .= "\nThe following record already exists and was skipped: ".$user_r[$map_key['email_address']];
					continue;
				}
				
				$new_daata = array
				(
					'email_address' => (array_key_exists('email_address', $map_key)) ? $user_r[$map_key['email_address']] : false,
					'username' => (array_key_exists('username', $map_key)) ? $user_r[$map_key['username']] : false,
					'password' => (array_key_exists('password', $map_key)) ? $user_r[$map_key['password']] : false,
					'member_id' => (array_key_exists('member_id', $map_key)) ? $user_r[$map_key['member_id']] : false,
					'first_name' => (array_key_exists('first_name', $map_key)) ? $user_r[$map_key['first_name']] : false,
					'mi' => (array_key_exists('mi', $map_key)) ? $user_r[$map_key['mi']] : false,
					'last_name' => (array_key_exists('last_name', $map_key)) ? $user_r[$map_key['last_name']] : false,
					'company_additional' => (array_key_exists('job_title', $map_key)) ? $user_r[$map_key['job_title']] : false,
					'company_name' => (array_key_exists('company', $map_key)) ? $user_r[$map_key['company']] : false,
					'address1' => (array_key_exists('address_1', $map_key)) ? $user_r[$map_key['address_1']] : false,
					'address2' => (array_key_exists('address_2', $map_key)) ? $user_r[$map_key['address_2']] : false,
					'city' => (array_key_exists('city', $map_key)) ? $user_r[$map_key['city']] : false,
					'state' => (array_key_exists('state', $map_key)) ? $user_r[$map_key['state']] : false,
					'zip_code' => (array_key_exists('postal', $map_key)) ? $user_r[$map_key['postal']] : false,
					'newsletter_country' => (array_key_exists('country', $map_key)) ? $user_r[$map_key['country']] : false,
					'phone' => (array_key_exists('phone', $map_key)) ? $user_r[$map_key['phone']] : false
				);

				$page_name = $new_daata['company_name'] . " " . $new_daata['first_name'] . " " . $new_daata['last_name'];
				$page_name = preg_replace('/  +/', ' ', $page_name);
				
				if (!$new_daata['username']) $new_daata['username'] = $new_daata['email_address'];
				if (!$new_daata['password']) $new_daata['password'] = $new_daata['last_name'];
				
				$catch = make_node($new_daata, $db, $parent_id, $class_id, $page_name, $contentobject_id);
				if ($catch == 999) {
					$error_string .= "\nThe following record had an invalid country and was skipped: $un";
				}

			}
			if ( $scheduledScript ) {
				$scheduledScript->updateProgress( 100 );
			}
		} else {
			
			$error_string = 'vital';

		}
		return $error_string;
	}

}

function make_node($new_daata, $db, $parent_id, $class_id, $page_name, $contentobject_id = false) {

	//EZ SETTINGS

	$user_id = eZUser::currentUserID();

	// CREATE EZ OBJECT
	
	if ($contentobject_id) {
		
		$contentObjectID = $contentobject_id;
		
		$ex_object = eZContentObject::fetch($contentobject_id);

		$contentObject = $ex_object;

		$current_version = $ex_object->nextVersion();

		$version = $ex_object->createNewVersion();
		
	} else {

		$parent = eZContentObjectTreeNode::fetch($parent_id);

		$object = $parent->object();

		$remoteID = "staticpagesearchobject_".$page_name;

		$class = eZContentClass::fetch( $class_id );

		// Create object by user id in the section of the parent object
		$contentObject = $class->instantiate( $user_id, $object->attribute( 'section_id' ) );
		$contentObject->setAttribute('remote_id', $remoteID );
		$contentObject->setAttribute( 'name', $page_name );

		$nodeAssignment = eZNodeAssignment::create( array(
		                                                 'contentobject_id' => $contentObject->attribute( 'id' ),
		                                                 'contentobject_version' => $contentObject->attribute( 'current_version' ),
		                                                 'parent_node' => $parent->attribute( 'node_id' ),
		                                                 'sort_field' => 2,
		                                                 'sort_order' => 0,
		                                                 'is_main' => 1
		                                                 )
		                                             );
		$nodeAssignment->store();
	
			//print_r('time1');

		$version = $contentObject->version( 1 );

		$contentObjectID = $contentObject->attribute( 'id' );

		$current_version = 1;
	
	}

	$dataMap = $version->dataMap();

	foreach ($dataMap as $setme => $objectAttribute) {
		
		if (!array_key_exists($setme, $new_daata)) continue;
		
		$value = $new_daata[$setme];
		
		if ($value) {

			$dataType = $objectAttribute->attribute( 'data_type_string' );

			switch( $dataType )
			{
				
		  	 case 'ezboolean':
			  {
			      $objectAttribute->setAttribute( 'data_int', $value ? 1 : 0 );
			  } break;
			
			  case 'ezxmltext':
			  {
			      setEZXMLAttribute( $objectAttribute, $value );
			  } break;

			  case 'ezurl':
			  {
			      $objectAttribute->setContent( $value );
			  } break;

			  case 'ezkeyword':
			  {
			      $keyword = new eZKeyword();
			      $keyword->initializeKeyword( $value );
			      $objectAttribute->setContent( $keyword );
			  } break;

			  case 'ezdate':
			  {
			      $timestamp = strtotime( $value );
			      if ( $timestamp )
			          $objectAttribute->setAttribute( 'data_int', $timestamp );
			  } break;

			  case 'ezdatetime':
			  {
			      $objectAttribute->setAttribute( 'data_int', strtotime($value) );
			  } break;
			
			  case 'ezselection':
			  {	
			    $classContent = $objectAttribute->attribute( 'class_content' );
				$optionId = false;
				$goodtogo = false;
				foreach ( $classContent['options'] as $option )
				{
				   if ( $option['name'] == $value )
				   {
				         $optionId = $option['id'];
						 $objectAttribute->setAttribute('data_text', $optionId);
						 $goodtogo = true;
				         break;
				   }
				}
				if ($value && $goodtogo === false) {
					return 999;
				}
			  } break;
			
			  case 'ezobjectrelation':
			  {
			      $objectAttribute->setAttribute( 'data_int', strtotime($value) );
			  } break;
			
			  case 'ezbinaryfile':
			  {

			      $status = $objectAttribute->insertRegularFile( $contentObject, $contentObject->attribute( 'current_version' ), eZContentObject::defaultLanguage(), $value, $storeResult );
					
				  if ( $storeResult['require_storage'] ) $dataMap[$fileAttribute]->store();
			  } break;

			  default:
			  {
			      $objectAttribute->setAttribute( 'data_text', $value );
			  } break;
			}

			$objectAttribute->store();
		
		}

	}
	
			//print_r('time2');
	
	if (array_key_exists('password', $new_daata)) {
		
		if ($contentobject_id != false && is_numeric($contentobject_id)) {
			$query = "delete from ezuser where contentobject_id = $contentobject_id";
			$db->query($query);
		}
		
		makeNewEzUser($new_daata, $contentObjectID);
		
	}
	
	//print_r('time3');

	$operationResult = eZOperationHandler::execute( 'content', 'publish', array( 'object_id' => $contentObjectID,
	                                                                             'version' => $current_version ) );

	// END EZ OBJECT CREATION
	
	$assignedNodes = $contentObject->assignedNodes();
	
		//print_r('time4');
	
	return $assignedNodes[0]->attribute('node_id');
	
}


function makeNewEzUser($new_daata, $contentObjectID) {
	
	$username = $new_daata['username'];
	$password = $new_daata['password'];
	$email = $new_daata['email_address'];
	$user = new eZUser();
	$user->setInformation( $contentObjectID, $username, $email, $password, $password );
	$user->store();
	return true;
	
}

function setEZXMLAttribute( $attribute, $attributeValue, $link = false )
{
    $contentObjectID = $attribute->attribute( "contentobject_id" );
    $parser = new eZSimplifiedXMLInputParser( $contentObjectID, false, 0, false );

    $attributeValue = str_replace( "\r", '', $attributeValue );
    $attributeValue = str_replace( "\n", '', $attributeValue );
    $attributeValue = str_replace( "\t", ' ', $attributeValue );

    $document = $parser->process( $attributeValue );
    if ( !is_object( $document ) )
    {
        $cli = eZCLI::instance();
        $cli->output( 'Error in xml parsing' );
        return;
    }
    $domString = eZXMLTextType::domString( $document );

    $attribute->setAttribute( 'data_text', $domString );
    $attribute->store();
}

?>
