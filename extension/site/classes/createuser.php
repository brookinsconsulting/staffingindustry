<?php

/**
  * created this class from the newfollower.php script on hitachi. not doing a whole lot to it now in terms of refactoring,
  * but we use this functionality enough it seemed like time to make it a little more flexible/portable.
  *
  */

class CreateUser {

	function make_node($email, $password, $parent_id) {

		$currentUser = eZUser::fetchByEmail($email);
		if (is_a($currentUser, 'eZUser')) {
			$currentUser->loginCurrent();
			return false;
		}

		$db = eZDB::instance();

		$explodemail = explode('@', $email);
		$first_name = $explodemail[0];
		$last_name = $explodemail[1];
		$new_daata = array('first_name' => $first_name, 'last_name' => $last_name, 'email' => $email);

		//EZ SETTINGS

		$user_id = 14;

		// CREATE EZ OBJECT

		$parent = eZContentObjectTreeNode::fetch($parent_id);


		$object = $parent->object();

		$remoteID = "staticpagesearchobject_".$page_name;

		$class_id = 56; // 'client' 
		$class = eZContentClass::fetch( $class_id );

		// Create object by user id in the section of the parent object
		$contentObject = $class->instantiate( $user_id, $object->attribute( 'section_id' ) );
		$contentObject->setAttribute('remote_id', $remoteID );
		$contentObject->setAttribute( 'name', $page_name );

		$nodeAssignment = eZNodeAssignment::create( array(
														 'contentobject_id' => $contentObject->attribute( 'id' ),
														 'contentobject_version' => $contentObject->attribute( 'current_version' ),
														 'parent_node' => $parent->attribute( 'node_id' ),
														 'sort_field' => 2,
														 'sort_order' => 0,
														 'is_main' => 1
														 )
													 );
		$nodeAssignment->store();

		$version = $contentObject->version( 1 );

		$contentObjectID = $contentObject->attribute( 'id' );

		$current_version = 1;

		$dataMap = $version->dataMap();

		foreach ($dataMap as $setme => $objectAttribute) {
			
			if (!array_key_exists($setme, $new_daata)) continue;
			
			$value = $new_daata[$setme];
			
			if ($value) {

				$dataType = $objectAttribute->attribute( 'data_type_string' );

				switch( $dataType )
				{
					
				 case 'ezboolean':
				  {
					  $objectAttribute->setAttribute( 'data_int', $value ? 1 : 0 );
				  } break;
				
				  case 'ezxmltext':
				  {
					  $this->setEZXMLAttribute( $objectAttribute, $value );
				  } break;

				  case 'ezurl':
				  {
					  $objectAttribute->setContent( $value );
				  } break;

				  case 'ezkeyword':
				  {
					  $keyword = new eZKeyword();
					  $keyword->initializeKeyword( $value );
					  $objectAttribute->setContent( $keyword );
				  } break;

				  case 'ezdate':
				  {
					  $timestamp = strtotime( $value );
					  if ( $timestamp )
						  $objectAttribute->setAttribute( 'data_int', $timestamp );
				  } break;

				  case 'ezdatetime':
				  {
					  $objectAttribute->setAttribute( 'data_int', strtotime($value) );
				  } break;
				
				  case 'ezobjectrelation':
				  {
					  $objectAttribute->setAttribute( 'data_int', strtotime($value) );
				  } break;
				
				  case 'ezbinaryfile':
				  {

					  $status = $objectAttribute->insertRegularFile( $contentObject, $contentObject->attribute( 'current_version' ), eZContentObject::defaultLanguage(), $value, $storeResult );
						
					  if ( $storeResult['require_storage'] ) $dataMap[$fileAttribute]->store();
				  } break;

				  default:
				  {
					  $objectAttribute->setAttribute( 'data_text', $value );
				  } break;
				}

				$objectAttribute->store();
			
			}

		}
		
		$email = $new_daata['email'];
		
		$email_r = explode('@', preg_replace("/[^a-zA-Z@]/", "", $email));

		$username = $email_r[0].$email_r[1];
		
		$user = new eZUser( $contentObjectID );

		$user->setAttribute('login', $username );
		$user->setAttribute('email', $email);

		$hashType = eZUser::hashType() . "";
		$newHash = $user->createHash( $username, $password, eZUser::site(), $hashType );

		$user->setAttribute( "password_hash_type", $hashType );
		$user->setAttribute( "password_hash", $newHash );

		$user->store();

		$operationResult = eZOperationHandler::execute( 'content', 'publish', array( 'object_id' => $contentObjectID,
																					 'version' => $current_version ) );

		// END EZ OBJECT CREATION
		
		
		/*
		$nodeIDList = eZSubtreeNotificationRule::fetchNodesForUserID( $user->attribute( 'contentobject_id' ), false );
		if ( !in_array( 2, $nodeIDList ) )
		{
			$rule = eZSubtreeNotificationRule::create( 2, $user->attribute( 'contentobject_id' ) );
			$rule->store();
			$alreadyExists = false;
		}
		*/
		
		return true;
		
	}

	function setEZXMLAttribute( $attribute, $attributeValue, $link = false )
	{
		$contentObjectID = $attribute->attribute( "contentobject_id" );
		$parser = new eZSimplifiedXMLInputParser( $contentObjectID, false, 0, false );

		$attributeValue = str_replace( "\r", '', $attributeValue );
		$attributeValue = str_replace( "\n", '', $attributeValue );
		$attributeValue = str_replace( "\t", ' ', $attributeValue );

		$document = $parser->process( $attributeValue );
		if ( !is_object( $document ) )
		{
			$cli = eZCLI::instance();
			$cli->output( 'Error in xml parsing' );
			return;
		}
		$domString = eZXMLTextType::domString( $document );

		$attribute->setAttribute( 'data_text', $domString );
		$attribute->store();
	}
}

//eZExecution::cleanExit();
?>
