<?php

class SiteLinkLargeBinaryFile implements SiteLinkDataTypeInterface
{
	public function modify($Attribute,$LinkType,$SiteLink){
                /* Commenting out as authentication is no longer required for this site's files */
		/*
		$awsAccessKey = eZINI::instance( 's3.ini' )->variable( 'S3Settings', 'Key' );
		$awsSecretKey = eZINI::instance( 's3.ini' )->variable( 'S3Settings', 'SecretKey' );
		$awsBucket = eZINI::instance( 's3.ini' )->variable( 'S3Settings', 'Bucket' );
		
		$s3 = new S3( $awsAccessKey, $awsSecretKey );
		
		$url = $s3->getAuthenticatedURL($awsBucket, $Attribute->content(), 3600*24*2);
		
		return $url;
                */

                $awsBucket = eZINI::instance( 's3.ini' )->variable( 'S3Settings', 'Bucket' );

                return 'http://' . $awsBucket . '.s3.amazonaws.com/' . urlencode($Attribute->content());
		
	}
}

?>