<?php

class BinaryFile implements SiteLinkDataTypeInterface
{
	public function modify($Attribute,$LinkType,$SiteLink){
		if(stripos($SiteLink->siteAccess['name'],'member')===false){
			return eZContentObjectTreeNode::fetch(40809);
		}
		$ContentObjectID = $Attribute->ContentObjectID;
		$ContentObjectAttributeID = $Attribute->ID;
		$FileName = urlencode($Attribute->content()->OriginalFilename);
		return "content/download/$ContentObjectID/$ContentObjectAttributeID/$FileName";
	}
}

?>