<?php
class WebExStub {

	private $webExID;
	private $password;
	private $siteID;
	private $siteName;
	private $siteURL;		

	public function __construct($webExID, $password, $siteID, $siteName, $siteURL) {
		$this->webExID = $webExID;
		$this->password = $password;
		$this->siteID = $siteID;
		$this->siteName = $siteName;
		$this->siteURL = $siteURL;
	}
	
	private function transmit($payload) {
		
		// Generate XML Payload
		$xml = '<serv:message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
		$xml .= '<header>';
		$xml .= '<securityContext>';
		$xml .= '<webExID>'. $this->webExID .'</webExID>';
		$xml .= '<password>'. $this->password .'</password>';
		$xml .= '<siteID>'. $this->siteID .'</siteID>';
		$xml .= '<siteName>'. $this->siteName .'</siteName>';
		//$xml .= '<partnerID>'. $this->partnerID .'</partnerID>';
		$xml .= '</securityContext>';
		$xml .= '</header>';
		$xml .= '<body>';
		$xml .= '<bodyContent xsi:type="java:com.webex.service.binding.' . $payload['service']  . '">';		
		$xml .= $payload['xml'];
		$xml .= '</bodyContent>';
		$xml .= '</body>';
		$xml .= '</serv:message>';
		
		//pre($xml);
				
		// Separate $siteURL into Host and URI for Headers
        $host = substr($this->siteURL, 0, strpos($this->siteURL, "/"));
        $uri = strstr($this->siteURL, "/");	
		
		// Generate Request Headers
		$content_length = strlen($xml);
		$headers = array(
			"POST $uri HTTP/1.0",
			"Host: $host",
			"User-Agent: PostIt",
			"Content-Type: application/x-www-form-urlencoded",
			"Content-Length: ".$content_length,
			);
			
		/*print_r($headers);
		print "\n";
		print $xml;
		print "\n";*/
			
		// Post the Request
		$ch = curl_init('https://' . $this->siteURL);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);	
		
		$response = curl_exec($ch);
		
		//$response = do_post_request('https://' . $this->siteURL
		return $response;
	}
	
	
	//public function user_AuthenticateUser()
	//public function user_CreateUser()
	//public function user_DelUser()
	//public function user_DelSessionTemplates()
	//public function user_GetloginTicket()
	//public function user_GetloginurlUser()
	//public function user_GetlogouturlUser()
	//public function user_GetUser()
	//public function user_LstsummaryUser()
	public function user_LstsummaryUser($startFrom = '1', $maximumNum = '', $listMethod = '', $orderOptions = '', $dateScope = '' ) {
		$xml = '<listControl>';
		if($startFrom) $xml .= '<startFrom>'. $startFrom .'</startFrom>';
		if($maximumNum) $xml .= '<maximumNum>'. $maximumNum .'</maximumNum>';
		if($listMethod) $xml .= '<listMethod>'. $listMethod .'</listMethod>';	
		$xml .= '</listControl>';
		
		if($orderOptions) {
			$xml .= '<order>';
			foreach ($orderOptions as $options) {
				$xml .= '<orderBy>'. $options['By'] .'</orderBy>';
				$xml .= '<orderAD>'. $options['AD'] .'</orderAD>';
			}
			$xml .= '</order>';
		}
		
		if($dateScope) {
			$xml .= '<dataScope>';
			if($dateScope['regDateStart']) $xml .= '<regDateStart>'. $dateScope['regDateStart'] .'</regDateStart>';
			if($dateScope['timeZoneID']) $xml .= '<timeZoneID>'. $dateScope['timeZoneID'] .'</timeZoneID>';
			if($dateScope['regDateEnd']) $xml .= '<regDateEnd>'. $dateScope['regDateEnd'] .'</regDateEnd>';
			$xml .= '</dataScope>';
		}

		$payload['xml'] = $xml;
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);	
	
		return $this->transmit($payload);						
	}
	
	//public function user_SetUser()
	//public function user_UploadPMRIImage()

	
	//public function meeting_CreateMeeting();
	//public function meeting_CreateTeleconferenceSession();
	//public function meeting_DelMeeting();
	//public function meeting_GethosturlMeeting();
	//public function meeting_GetMeeting();
	//public function meeting_GetTeleconferenceSession();
	public function meeting_LstsummaryMeeting($startFrom = '1', $maximumNum = '', $listMethod = '', $orderOptions = '', $programID = '', $dateScope = '') {
		
		$xml = '<listControl>';
		if($startFrom) $xml .= '<startFrom>'. $startFrom .'</startFrom>';
		if($maximumNum) $xml .= '<maximumNum>'. $maximumNum .'</maximumNum>';
		if($listMethod) $xml .= '<listMethod>'. $listMethod .'</listMethod>';	
		$xml .= '</listControl>';
		
		if($orderOptions) {
			$xml .= '<order>';
			foreach ($orderOptions as $options) {
				$xml .= '<orderBy>'. $options['By'] .'</orderBy>';
				$xml .= '<orderAD>'. $options['AD'] .'</orderAD>';
			}
			$xml .= '</order>';
		}
		
		if($programID)
			$xml .= '<programID>' . $programID . '</programID>';
		
		if($dateScope) {
			$xml .= '<dateScope>';
			if($dateScope['startDateStart']) $xml .= '<startDateStart>'. $dateScope['startDateStart'] .'</startDateStart>';
			if($dateScope['startDateEnd']) $xml .= '<startDateEnd>'. $dateScope['startDateEnd'] .'</startDateEnd>';
			if($dateScope['endDateStart']) $xml .= '<endDateStart>'. $dateScope['endDateStart'] .'</endDateStart>';
			if($dateScope['endDateEnd']) $xml .= '<endDateEnd>'. $dateScope['endDateEnd'] .'</endDateEnd>';
			$xml .= '</dateScope>';
		}
		
		$payload['xml'] = $xml;
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);
		
		return $this->transmit($payload);
	}
	//public function meeting_SetMeeting();
	//public function meeting_SetTeleconferenceSession();	

	//public function meeting_GetjoinurlMeeting()
	public function meeting_GetjoinurlMeeting($sessionKey, $attendeeName = '') {
		$xml = '<sessionKey>'. $sessionKey .'</sessionKey>';
		if($attendeeName) $xml = '<attendeeName>'. $attendeeName .'</attendeeName>';

		$payload['xml'] = $xml;
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);
		
		return $this->transmit($payload);					
	}
	
	//public function event_CreateEvent();
	//public function event_DelEvent();
	//public function event_GetEvent();
	public function event_GetEvent($sessionKey, $attendeeName = '') {
		$xml = '<sessionKey>'. $sessionKey .'</sessionKey>';
		if($attendeeName) $xml = '<attendeeName>'. $attendeeName .'</attendeeName>';

		$payload['xml'] = $xml;
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);
		
		return $this->transmit($payload);					
	}
	//public function event_LstRecordedEvent();
	
	//public function event_LstsummaryProgram();
	public function event_LstsummaryProgram($programID = '') {
		if($programID)
			$xml = '<programID>' . $programID . '</programID>';
			
		$payload['xml'] = (!empty($xml)) ? $xml : '';
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);	
	
		return $this->transmit($payload);
	}
	//public function event_SendInvitationEmail();
	//public function event_SetEvent();
	//public function event_UploadEventImage();	
	
	//public function event_LstsummaryEvent()		
	public function event_LstsummaryEvent($startFrom = '1', $maximumNum = '', $listMethod = '', $orderOptions = '', $programID = '', $dateScope = '') {
		
		$xml = '<listControl>';
		if($startFrom) $xml .= '<startFrom>'. $startFrom .'</startFrom>';
		if($maximumNum) $xml .= '<maximumNum>'. $maximumNum .'</maximumNum>';
		if($listMethod) $xml .= '<listMethod>'. $listMethod .'</listMethod>';	
		$xml .= '</listControl>';
		
		if($orderOptions) {
			$xml .= '<order>';
			foreach ($orderOptions as $options) {
				$xml .= '<orderBy>'. $options['By'] .'</orderBy>';
				$xml .= '<orderAD>'. $options['AD'] .'</orderAD>';
			}
			$xml .= '</order>';
		}
		
		if($programID)
			$xml .= '<programID>' . $programID . '</programID>';
		
		if($dateScope) {
			$xml .= '<dateScope>';
			if(isset($dateScope['startDateStart'])) $xml .= '<startDateStart>'. $dateScope['startDateStart'] .'</startDateStart>';
			if(isset($dateScope['startDateEnd'])) $xml .= '<startDateEnd>'. $dateScope['startDateEnd'] .'</startDateEnd>';
			if(isset($dateScope['endDateStart'])) $xml .= '<endDateStart>'. $dateScope['endDateStart'] .'</endDateStart>';
			if(isset($dateScope['endDateEnd'])) $xml .= '<endDateEnd>'. $dateScope['endDateEnd'] .'</endDateEnd>';
			$xml .= '</dateScope>';
		}
		
		$payload['xml'] = $xml;
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);
		
		return $this->transmit($payload);
	}
	
	/*
	 *  Meeting Attendee Services
	 */
	//public function attendee_CreateMeetingAttendee()
	public function attendee_CreateMeetingAttendee($attendees) {
		$xml = '';
		foreach($attendees as $attendee) {
			//$xml .= '<attendees>';
			$xml .= '<person>';
			foreach($attendee['info'] as $attr => $val){
				if(!is_array($val) && !empty($val))
					$xml .= '<'.$attr.'>'.$val.'</'.$attr.'>';				
			
				if(is_array($val)) {
						$xml .= '<'.$attr.'>';					
					foreach($val as $att => $val) {
						if(!empty($val))
							$xml .= '<'.$att.'>'.$val.'</'.$att.'>';						
					}
						$xml .= '</'.$attr.'>';					
				}
			}
			$xml .= '</person>';
		
			foreach($attendee['options'] as $attr => $val){
				if(!empty($val))
					$xml .= '<'.$attr.'>'.$val.'</'.$attr.'>';		
			}
			//$xml .= '</attendees>';
		}
		
		$payload['xml'] = $xml;
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);
		
		return $this->transmit($payload);			
	}
	
	//public function attendee_LstMeetingAttendee()
	public function attendee_LstMeetingAttendee($sessionKey, $orderOptions='', $startFrom=null, $maximumNum=null, $listMethod=null) {
		$xml = '';
		$xml .= '<sessionKey>' . $sessionKey . '</sessionKey>';
		if($orderOptions) {
			$xml .= '<order>';
			foreach ($orderOptions as $options) {
				$xml .= '<orderBy>'. $options['By'] .'</orderBy>';
				$xml .= '<orderAD>'. $options['AD'] .'</orderAD>';
			}
			$xml .= '</order>';
		}
		$xml .= '<listControl>';
		if(isset($startFrom)) $xml .= '<startFrom>'. $startFrom .'</startFrom>';
		if(isset($maximumNum)) $xml .= '<maximumNum>'. $maximumNum .'</maximumNum>';
		if(isset($listMethod)) $xml .= '<listMethod>'. $listMethod .'</listMethod>';	
		$xml .= '</listControl>';
		
		$payload['xml'] = $xml;
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);
		
		return $this->transmit($payload);				
	}
	
	public function attendee_DelMeetingAttendee($attendeeId) {
		$xml = '<attendeeID>' . $attendeeId . '</attendeeID>';
		
		$payload['xml'] = $xml;
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);
		
		return $this->transmit($payload);				
	}
	
	//public function history_LstmeetingattendeeHistory()
	public function history_LsteventattendeeHistory($sessionKey='', $orderOptions='', $startTimeScope='', $endTimeScope='', $confName='', $confID='', $listControl = '', $inclAudioOnly = false) {
		$xml = '';	
		
		if($sessionKey)
			$xml .= '<sessionKey>' . $sessionKey . '</sessionKey>';

		if($orderOptions) {
			$xml .= '<order>';
			foreach ($orderOptions as $options) {
				$xml .= '<orderBy>'. $options['By'] .'</orderBy>';
				$xml .= '<orderAD>'. $options['AD'] .'</orderAD>';
			}
			$xml .= '</order>';
		}
		
		if($startTimeScope) {
			$xml .= '<startTimeScope>';
			$xml .= '<sessionStartTimeStart>'. $startTimeScope['sessionStartTimeStart'] .'</sessionStartTimeStart>';
			$xml .= '<sessionStartTimeEnd>'. $startTimeScope['sessionStartTimeEnd'] .'</sessionStartTimeEnd>';
			$xml .= '</startTimeScope>';
		}
		
		if($endTimeScope) {
			$xml .= '<endTimeScope>';
			$xml .= '<sessionEndTimeStart>'. $endTimeScope['sessionEndTimeStart'] .'</sessionEndTimeStart>';
			$xml .= '<sessionEndTimeEnd>'. $endTimeScope['sessionEndTimeEnd'] .'</sessionEndTimeEnd>';
			$xml .= '</endTimeScope>';			
		}					

		if($confName)
			$xml .= '<confName>' . $confName . '</confName>';

		if($confID)
			$xml .= '<confID>' . $confID . '</confID>';
			
		if($listControl) {
			$xml .= '<listControl>';
			if (isset($listControl['startFrom']))
			{
			  $xml .= '<serv:startFrom>'. $listControl['startFrom'] .'</serv:startFrom>';
			}
			if (isset($listControl['maximumNum']))
			{
			  $xml .= '<serv:maximumNum>'. $listControl['maximumNum'] .'</serv:maximumNum>';
			}			
			if (isset($listControl['listMethod']))
			{
			  $xml .= '<serv:listMethod>'. $listControl['listMethod'] .'</serv:listMethod>';
			}
			$xml .= '</listControl>';				
		}

		if($inclAudioOnly)
			$xml .= '<inclAudioOnly>' . $inclAudioOnly . '</inclAudioOnly>';							
					
		$payload['xml'] = $xml;
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);		
		return $this->transmit($payload);	
	}
				
	//public function attendee_RegisterMeetingAttendee()
	public function attendee_RegisterMeetingAttendee($attendees) {
		$xml = '';
		foreach($attendees as $attendee) {
			$xml .= '<attendees>';
			$xml .= '<person>';
			foreach($attendee['info'] as $attr => $val){
				if(!is_array($val) && !empty($val))
					$xml .= '<'.$attr.'>'.$val.'</'.$attr.'>';				
			
				if(is_array($val)) {
						$xml .= '<'.$attr.'>';					
					foreach($val as $att => $val) {
						if(!empty($val))
							$xml .= '<'.$att.'>'.$val.'</'.$att.'>';						
					}
						$xml .= '</'.$attr.'>';					
				}
			}
			$xml .= '</person>';
		
			foreach($attendee['options'] as $attr => $val){
				if(!empty($val))
					$xml .= '<'.$attr.'>'.$val.'</'.$attr.'>';		
			}
			$xml .= '</attendees>';
		}
		
		$payload['xml'] = $xml;
		$payload['service'] =  str_replace("_", ".", __FUNCTION__);
		
		return $this->transmit($payload);		
	}
	
	function do_post_request($url, $data, $optional_headers = null)
  {
    $params = array('http' => array(
                'method' => 'POST',
                'content' => $data
              ));
    if ($optional_headers !== null) {
      $params['http']['header'] = $optional_headers;
    }
    $ctx = stream_context_create($params);
    $fp = @fopen($url, 'rb', false, $ctx);
    if (!$fp) {
      throw new Exception("Problem with $url, $php_errormsg");
    }
    $response = @stream_get_contents($fp);
    if ($response === false) {
      throw new Exception("Problem reading data from $url, $php_errormsg");
    }
    return $response;
  }
	 
}
?>
