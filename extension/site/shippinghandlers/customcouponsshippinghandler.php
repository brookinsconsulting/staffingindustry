<?php
class customcouponsShippingHandler
{
	
	function customcouponsShippingHandler() {
		
	}
	
    function getShippingInfo( $productCollectionID )
    {
	
		$col = eZProductCollection::fetch($productCollectionID);
		
		$items = $col->itemList();
		
		$discount_s = 'Automatic discounts:';
		$discount_v = 0;
		$discount_v_ex = 0;
		
		$db = eZDB::instance();
		
		$free = array();
		
		if (customcouponsShippingHandler::getCouponsFoCollection($productCollectionID, $db) == 0) {
		
			$sub_end = array();
		
			$user = eZUser::currentUser();
			$user_o = $user->contentObject();
			$parent_id = $user_o->mainParentNodeID();
			$parent_node = eZContentObjectTreeNode::fetch($parent_id);
			$parent_class = $parent_node->object()->contentClassIdentifier();
		
			$sub_search_id = ($parent_class == 'client') ? $parent_id : $user_o->mainNodeID();
		
			$lang_r = array();
			foreach (eZContentLanguage::fetchList() as $l) {
				$lang_r[] = $l->attribute('locale');
			}
		

			$params = array('ClassFilterType'=>'include', 'ClassFilterArray'=>array(56), 'Limitation' => array(), 'Language' => $lang_r, 'Depth'=>1);
			//get subscription data
			$subs = eZContentObjectTreeNode::subTreeByNodeID($params,$sub_search_id);

			$group_count = count($subs);

			$subs_type = 'Double Seat';
			$subs_scope = 'region';
			if ($group_count == 0) {
				$subs_type = 'Single Seat';
			}
		
			$myroles = $user->roleIDList();
			if((in_array(23,$myroles) || in_array(192,$myroles) || in_array(197,$myroles)) && (in_array(25,$myroles) || in_array(195,$myroles) || in_array(199,$myroles))) {
				$subs_scope = ' multiregion';
			}
		
			$q = "select contentobject_id from ezcontentobject_tree, ezcontentobject where (path_string like '%/$sub_search_id/%' or path_string like '%/$sub_search_id')and ezcontentobject.id = ezcontentobject_tree.contentobject_id and ezcontentobject.contentclass_id in (56,4)";
			$reults = $db->arrayQuery($q);
			$group_user_ids = array();
			foreach ($reults as $r) {
				$group_user_ids[] = $r['contentobject_id'];
			}
		
			$group_user_ids_s = implode(",", $group_user_ids);
		
			foreach ($subs as $sub) {
				$sub_data = $sub->object()->dataMap();
				if (is_object($sub_data['product'])) $sub_end[$sub_data['product']->content()->mainNodeID()]=$sub_data['end_date']->content()->timestamp();
			}
		
			/*
		
			$params = array('ClassFilterType'=>'include', 'ClassFilterArray'=>array('subscription'), 'Limitation' => array(), 'Language' => $lang_r);
			//get subscription data
			
			$subs = eZContentObjectTreeNode::subTreeByNodeID($params,$sub_search_id);
			//eZDebug::writeDebug($subs);	

			if (count($subs)) {
				$sub= $subs[0];
				$sub_data = $sub->object()->dataMap();
				$sub_end = $sub_data['end_date']->content()->timestamp();
				$sub_prod = $sub_data['product']->content();
				$prod_data = $sub_prod->dataMap();
				$sub_prod_id = $sub_prod->ID;
			
				$prod_options = array();
				foreach ($prod_data['options']->content()->Options as $o) {
					$prod_options[$o['id']] = $o['comment'];
				}

				$q = "select option_item_id from ezproductcollection_item, ezorder, ezproductcollection_item_opt where ezproductcollection_item_opt.item_id = ezproductcollection_item.id and ezorder.productcollection_id = ezproductcollection_item.id and contentobject_id = $sub_prod_id and user_id in ($group_user_ids_s) order by ezorder.id DESC";
				$reults = $db->arrayQuery($q);
			
				$subs_type = $prod_options[$reults[0]['option_item_id']];
				eZDebug::writeDebug($subs_type);
			}
			*/
		
			$xini = eZINI::instance( 'xrowecommerce.ini' );
			$CWS_Summit_Registration = (int)$xini->variable( 'AutomaticDiscounts', 'CWS_Summit_Registration' );
			$CWS_Summit_CW_Risk_Forum_Combo_Registration = (int)$xini->variable( 'AutomaticDiscounts', 'CWS_Summit_CW_Risk_Forum_Combo_Registration' );
			$CW_Risk_Forum_Registration = (int)$xini->variable( 'AutomaticDiscounts', 'CW_Risk_Forum_Registration' );
			$CWS_London_Summit_Registration = (int)$xini->variable( 'AutomaticDiscounts', 'CWS_London_Summit_Registration' );
					
			foreach ($items as $i) {
			
				$item_ob_id = $i->attribute('contentobject_id');
				$item_ob = eZContentObject::fetch($item_ob_id);
			
				$q=$i->attribute('item_count');
				$p = $i->attribute('price') + round($i->attribute('vat_value') * $i->attribute('price') / 100, 2)  - round($i->attribute('discount') * $i->attribute('price') / 100 , 2); 
				$p_ex = $i->attribute('price'); 
				$top_quantity_applied=0; //variable to store min order number of biggest coupon applied - prevents multiple coupons being applied

				//is a coupon for quantity discounts set up?
				$rev_rels = $item_ob->reverseRelatedObjectList(false, false, 518, array('AllRelations' => true));
				if (array_key_exists(518, $rev_rels)) {
					foreach($rev_rels[518] as $coupon) {
						if ($coupon->mainParentNodeID() != 56301) continue; 
						$coupon_data = $coupon->dataMap();
						$coupon_coupon = $coupon_data['coupon']->content();
						if (strpos($coupon_coupon['code'], 'AUTO') == 0) {
						
							if ($q<$coupon_data['min_quantity']->content()) continue;
							if (time()<$coupon_coupon['from']->timestamp()) continue;
							if (time()>$coupon_coupon['till']->timestamp()) continue;
							if ($coupon_data['min_quantity']->content() < $top_quantity_applied) continue;

							$perc = $coupon_coupon['discount'];
							$top_quantity_applied = $coupon_data['min_quantity']->content();
							$desc = strip_tags($coupon_data['description']->content()->attribute('output')->attribute('output_text'));
						}
					}		
				}
				/*					
				//early subscription discounts
				$rev_rels = eZContentObject::fetch($i->attribute('contentobject_id'))->reverseRelatedObjectCount(false, 500, array('AllRelations' => false));		
				if ($rev_rels && $sub_end[$i->contentObject()->mainNodeID()] != 0 && $sub_end[$i->contentObject()->mainNodeID()] > strtotime('+2 months')) {
					$discount_s .= '<br/>10% discount for +2 month early subscription renewal (' . $i->attribute('name') . ').';
					$discount_v += round($p / 100 * 10 * $q,2);
					$discount_v_ex += round($p_ex / 100 * 10 * $q,2);
				} elseif ($rev_rels && $sub_end[$i->contentObject()->mainNodeID()] != 0 && $sub_end[$i->contentObject()->mainNodeID()] > strtotime('+1 month')) {
					$discount_s .= '<br/>5% discount for +1 month early subscription renewal (' . $i->attribute('name') . ').';
					$discount_v += round($p / 100 * 5 * $q,2);
					$discount_v_ex += round($p_ex / 100 * 5 * $q,2);
				}
				*/
				//4. When user is a single seat council member provide one 100% discount for CWS Summit conference
				if (strpos(strtolower($subs_type), 'single seat') !== false && $item_ob_id == $CWS_Summit_Registration && (in_array(23,$myroles) || in_array(192,$myroles) || in_array(197,$myroles))) {
					$item_count = customcouponsShippingHandler::getGroupOrderCount($CWS_Summit_Registration, $db, $group_user_ids_s);
					if ($item_count == 0) {
						$discount_s .= '<br/>Single Seat Council Member Discount (' . $i->attribute('name') . ').';
						$discount_v += round($p,2);
						$discount_v_ex += round($p_ex,2);
						$free['ob'.$item_ob_id] = 1;
					} 
				}
			
				if (strpos(strtolower($subs_type), 'single seat') !== false && $item_ob_id == $CWS_London_Summit_Registration && (in_array(25,$myroles) || in_array(195,$myroles) || in_array(199,$myroles))) {
					$item_count = customcouponsShippingHandler::getGroupOrderCount($CWS_London_Summit_Registration, $db, $group_user_ids_s);
					if ($item_count == 0) {
						$discount_s .= '<br/>Single Seat Council Member Discount (' . $i->attribute('name') . ').';
						$discount_v += round($p,2);
						$discount_v_ex += round($p_ex,2);
						$free['ob'.$item_ob_id] = 1;
					} 
				}
			
				if (strpos(strtolower($subs_type), 'single seat') !== false && $item_ob_id == $CW_Risk_Forum_Registration && (in_array(23,$myroles) || in_array(192,$myroles) || in_array(197,$myroles))) {
					$item_count = customcouponsShippingHandler::getGroupOrderCount($CW_Risk_Forum_Registration, $db, $group_user_ids_s);
					if ($item_count == 0) {
						$discount_s .= '<br/>Single Seat Council Member Discount (' . $i->attribute('name') . ').';
						$discount_v += round($p,2);
						$discount_v_ex += round($p_ex,2);
						$free['ob'.$item_ob_id] = 1;
					} 
				}
			
				if (strpos(strtolower($subs_type), 'single seat') !== false && $item_ob_id == $CWS_Summit_CW_Risk_Forum_Combo_Registration && (in_array(23,$myroles) || in_array(192,$myroles) || in_array(197,$myroles))) {
					$item_count = customcouponsShippingHandler::getGroupOrderCount($CWS_Summit_CW_Risk_Forum_Combo_Registration, $db, $group_user_ids_s);
					if ($item_count == 0) {
						$discount_s .= '<br/>Single Seat Council Member Discount (' . $i->attribute('name') . ').';
						$discount_v += round($p,2);
						$discount_v_ex += round($p_ex,2);
						$free['ob'.$item_ob_id] = 1;
					} 
				}
			
				// 5. When user is a single seat council member and an additional user wants a conference registration, additional user receives council member pricing for conference registration for CWS Summit US or Europe depending on where membership is held
				// no action required

				//6.	When user is a double seat council member provide two 100% discounts for CWS Summit/Risk conference
				$effective_q = ($q > 1) ? 2 : 1;
				$effective_sub = ($group_count>1) ? 'Corporate' : 'Double Seat';
				if (strpos(strtolower($subs_type), 'double seat') !== false && $item_ob_id == $CWS_Summit_Registration && (in_array(23,$myroles) || in_array(192,$myroles) || in_array(197,$myroles))) {
					$item_count = customcouponsShippingHandler::getGroupOrderCount($CWS_Summit_Registration, $db, $group_user_ids_s);
					if ($item_count < 2) {
						if ($item_count == 1) $effective_q = 1;
						$discount_s .= "<br/>$effective_sub  Council Member Discount (" . $i->attribute('name') . ').';
						$discount_v += round($p * $effective_q,2);
						$discount_v_ex += round($p_ex * $effective_q,2);
						$free['ob'.$item_ob_id] = $effective_q;
					} 
				}
				if (strpos(strtolower($subs_type), 'double seat') !== false && $item_ob_id == $CWS_London_Summit_Registration && (in_array(25,$myroles) || in_array(195,$myroles) || in_array(199,$myroles))) {
					$item_count = customcouponsShippingHandler::getGroupOrderCount($CWS_London_Summit_Registration, $db, $group_user_ids_s);
					if ($item_count < 2) {
						if ($item_count == 1) $effective_q = 1;
						$discount_s .= "<br/>$effective_sub Council Member Discount (" . $i->attribute('name') . ').';
						$discount_v += round($p * $effective_q,2);
						$discount_v_ex += round($p_ex * $effective_q,2);
						$free['ob'.$item_ob_id] = $effective_q;
					} 
				}
				if (strpos(strtolower($subs_type), 'double seat') !== false && $item_ob_id == $CW_Risk_Forum_Registration && (in_array(23,$myroles) || in_array(192,$myroles) || in_array(197,$myroles))) {
					$item_count = customcouponsShippingHandler::getGroupOrderCount($CW_Risk_Forum_Registration, $db, $group_user_ids_s);
					if ($item_count < 2) {
						if ($item_count == 1) $effective_q = 1;
						$discount_s .= "<br/>$effective_sub Council Member Discount (" . $i->attribute('name') . ').';
						$discount_v += round($p * $effective_q,2);
						$discount_v_ex += round($p_ex * $effective_q,2);
						$free['ob'.$item_ob_id] = $effective_q;
					} 
				}
				if (strpos(strtolower($subs_type), 'double seat') !== false && $item_ob_id == $CWS_Summit_CW_Risk_Forum_Combo_Registration && (in_array(23,$myroles) || in_array(192,$myroles) || in_array(197,$myroles))) {
					$item_count = customcouponsShippingHandler::getGroupOrderCount($CWS_Summit_CW_Risk_Forum_Combo_Registration, $db, $group_user_ids_s);
					if ($item_count < 2) {
						if ($item_count == 1) $effective_q = 1;
						$discount_s .= "<br/>$effective_sub Council Member Discount (" . $i->attribute('name') . ').';
						$discount_v += round($p * $effective_q,2);
						$discount_v_ex += round($p_ex * $effective_q,2);
						$free['ob'.$item_ob_id] = $effective_q;
					} 
				}
		
				//7.	When user is a double seat council member and two users have signed up for CWS Summit conference, additional user receives council member pricing for conference registration for CWS Summit/Risk US or Europe depending on where membership is held
				// no action required

			
				//8.	When user is a multi-regional council member provide two 100% discounts for both US and Europe CWS Summit/Risk conferences 
				//no action required
			
				//9.	When user is multi-regional council member and 2 each users have signed up for US and Europe CWS Summit/Risk conferences, additional user receives council member pricing for conference registration in either place where they are attempting to register
				// no action required
			
				//10.	When user is corporate council member provide two 100% discounts for CWS Summit/Risk Conference
				// added to rule 6
			
				//11.	When user is a corporate council member and 2 users have signed up for CWS Summit/Risk conference, apply council membership price to conference
				// no action required
			
				//12.	When the user is a premium corporate member DO NOT ALLOW for signing up for CWS Summit/Risk conferences
				// part of product tpl
			
				//fallback quantity discounts or apply whatever we found in above section
			
				if (!array_key_exists('ob'.$item_ob_id, $free)) $free['ob'.$item_ob_id] = 0;
			
				if ($q>5 && 5>$top_quantity_applied) {
					$discount_s .= '<br/>15% discount for quantity greater than five (' . $i->attribute('name') . ').';
					$discount_v += round($p/ 100 * 15 * ($q-$free['ob'.$item_ob_id]),2);
					$discount_v_ex += round($p_ex/ 100 * 15 * ($q-$free['ob'.$item_ob_id]),2);
				} elseif ($q>1 && 1>$top_quantity_applied) {
					$discount_s .= '<br/>10% discount for quantity greater than one (' . $i->attribute('name') . ').';
					$discount_v += round($p / 100 * 10 * ($q-$free['ob'.$item_ob_id]),2);
					$discount_v_ex += round($p_ex / 100 * 10 * ($q-$free['ob'.$item_ob_id]),2);
				} elseif ($top_quantity_applied) {
					$discount_s .= "<br/>$desc (" . $i->attribute('name') . ").";
					$discount_v += round($p/ 100 * $perc * ($q-$free['ob'.$item_ob_id]),2);
					$discount_v_ex += round($p_ex/ 100 * $perc * ($q-$free['ob'.$item_ob_id]),2);
				}
			
			}
		
		}	
		
		$vat_inc = ($discount_v_ex - $discount_v);
		
		$data =  array(
		'free' => $free,
        'description' => $discount_s,
        'cost' => -1 * $discount_v_ex,
		'cost_inc' => -1 * $discount_v,
		'is_vat_inc' => true,
		'vat_value' => ($discount_v_ex == 0) ? 0 : ($discount_v_ex - $discount_v) / $discount_v_ex * -100
        );

		$newdata = $data;
		$newdata['is_vat_inc'] = false;
		
		$data['shipping_items'] = array($newdata);
		
        return $data;

    }
    function purgeShippingInfo( $productCollectionID )
    {
        // nothing to purge
    }
    function updateShippingInfo( $productCollectionID )
    {
        // nothing to update
    }

	static function getGroupOrderCount($contentobject_id, $db, $group_user_ids_s) {
		$q = "select count(ezorder.id) as mycount from ezproductcollection_item, ezorder where ezorder.productcollection_id = ezproductcollection_item.id and contentobject_id = $contentobject_id and user_id in ($group_user_ids_s) order by ezorder.id DESC";
		$reults = $db->arrayQuery($q);
		$item_group_purchases_count = $reults[0]['mycount'];
		return $item_group_purchases_count;
	}
	
	static function getCouponsFoCollection($productCollectionID, $db) {
		$q = "select count(ezorder.id) as mycount from ezorder, ezorder_item where ezorder.productcollection_id = $productCollectionID and ezorder_item.order_id = ezorder.id and ezorder_item.type='coupon'";
		$reults = $db->arrayQuery($q);
		$coupon_count = $reults[0]['mycount'];
		eZDebug::writeDebug($q, 'reults');
		return $coupon_count;
	}
	
}
?>