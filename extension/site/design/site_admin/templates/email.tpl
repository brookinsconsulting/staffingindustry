{def
	$current_user = fetchezuser($node.contentobject_id)
	$my_name = ''
	$my_CompanyName = ''
	$my_JobTitle = ''
	$my_Address1 = ''
	$my_Address2 = ''
	$my_City = ''
	$my_StateorProvince = ''
	$my_ZIPorPostalCode = ''
	$my_PhoneNumber = ''
}


{* for subusers, play by the parent's rules *}
{def $user_cids = array('client', 'user')}
{if $user_cids|contains($node.parent.class_identifier)}
	{set $current_user = fetchezuser($node.parent.contentobject_id)}
{/if}

{def
	$list_id = '49b6031764f7283bed4dacd81fe6cabb'
	$current_user_object = fetch('content', 'object', hash('object_id', $current_user.contentobject_id))
	$subscriptions = fetch('content', 'list', hash(
		'parent_node_id', $current_user_object.main_node_id,
		'class_filter_type', 'include',
		'class_filter_array', array('subscription'),
		'limitation', array()
	))
}


<script>
{literal}
$(function(){
	var url = '/subscriber/segments';
	var data = {
		"list_id" : "{/literal}{$list_id}{literal}",
	{/literal}
	{* make sure we don't use parent's email, despite everything else belonging to the parent *}
	{if $user_cids|contains($node.parent.class_identifier)}
		{set $current_user = fetchezuser($node.contentobject_id)}
	{/if}
	{literal}
		"email" : "{/literal}{$current_user.email}{literal}",
	{/literal}
	{set $my_name=$node.name
		 $my_PhoneNumber = $node.data_map.phone.content
		 $my_JobTitle = $node.data_map.job_title.content
	}
	{if $user_cids|contains($node.parent.class_identifier)}
		{set $current_user = fetchezuser($node.parent.contentobject_id)}
	{/if}
	{set $my_CompanyName=$node.data_map.company_name.content
		 $my_Address1 = $node.data_map.address1.content
		 $my_Address2 = $node.data_map.address2.content
		 $my_City = $node.data_map.city.content
		 $my_StateorProvince = $node.data_map.state.content
		 $my_ZIPorPostalCode = $node.data_map.zip_code.content
	}
	{if is_set($node.data_map.newsletter_country)}{set-block variable=my_Country}{attribute_view_gui attribute=$node.data_map.newsletter_country}{/set-block}{/if}
	{if $my_PhoneNumber|eq('')}{set $my_PhoneNumber = $node.data_map.phone.content}{/if}
	{if $user_cids|contains($node.parent.class_identifier)}
		{set $current_user = fetchezuser($node.parent.contentobject_id)}
	{/if}
	{literal}
	}
	$.ajax({
		type: 'POST',
		url: url,
		data: data,
		success: function(result){
			result = $.parseJSON(result);
			$.each(result, function(segmentID, segmentName){
				if (segmentName.indexOf("Daily News") != -1) segmentName = 'Global Daily News';
				$('input[name="' + segmentName + '"]').attr('checked', 'checked');
			});
		},
	});


	$('#segments input[type=checkbox]').change(function(){
		$('#segments .feedback').hide();
		var url = '/subscriber/add';
		{/literal}
		var custom_fields = new Array({ldelim}
			"Key" : "CompanyName",
			"Value" : "{$my_CompanyName}"
		{rdelim},
		{ldelim}
			"Key" : "JobTitle",
			"Value" : "{$my_JobTitle}"
		{rdelim},
		{ldelim}
			"Key" : "Address1",
			"Value" : "{$my_Address1}"
		{rdelim},
		{ldelim}
			"Key" : "Address2",
			"Value" : "{$my_Address2}"
		{rdelim},
		{ldelim}
			"Key" : "City",
			"Value" : "{$my_City}"
		{rdelim},
		{ldelim}
			"Key" : "StateorProvince",
			"Value" : "{$my_StateorProvince}"
		{rdelim},
		{ldelim}
			"Key" : "ZIPorPostalCode",
			"Value" : "{$my_ZIPorPostalCode}"
		{rdelim},
		{ldelim}
			"Key" : "Country",
			"Value" : "{$my_Country|trim("\r\n")}"
		{rdelim},
		{ldelim}
			"Key" : "PhoneNumber",
			"Value" : "{$my_PhoneNumber}"
		{rdelim});
		{literal}
		var rb_newsletter = false;
		$('#segments input:checked').each(function(){
			if ($(this).hasClass('daily_news')) {
				custom_fields.push({
					"Key" : "Newsletters",
					"Value" : $(this).attr('name')
				});
			}
			if ($(this).hasClass('research_bulletin')) {
				custom_fields.push({
					"Key" : "Subscription Type",
					"Value" : $(this).attr('name')
				});
				if (!rb_newsletter) {
					custom_fields.push({
						"Key" : "Newsletters",
						"Value" : "Research Bulletin"
					});
					rb_newsletter = true;
				}
			}
		});
		if ($('#segments input:checked').length == 0) {
			custom_fields.push({
				"Key" : "Newsletters",
				"Value" : "blank"
			});
			custom_fields.push({
				"Key" : "Subscription Type",
				"Value" : "blank"
			});
		} else if ($('#segments input.research_bulletin:checked').length == 0) {
			custom_fields.push({
				"Key" : "Subscription Type",
				"Value" : "blank"
			});
		}
		var data = {
			"list_id" : "{/literal}{$list_id}{literal}",
		{/literal}
		{* make sure we don't use parent's email, despite everything else belonging to the parent *}
		{if $user_cids|contains($node.parent.class_identifier)}
			{set $current_user = fetchezuser($node.contentobject_id)}
		{/if}
		{literal}
			"email" : "{/literal}{$current_user.email}{literal}",
			"name" : "{/literal}{$my_name}{literal}",
		{/literal}
		{if $user_cids|contains($node.parent.class_identifier)}
			{set $current_user = fetchezuser($node.parent.contentobject_id)}
		{/if}
		{literal}
			"custom_fields" : custom_fields
		}
		$.ajax({
			type: 'POST',
			url: url,
			data: data,
			success: function(result){
				result = $.parseJSON(result);
				if (result.http_status_code == 201) {
					var feedback = 'Update successful.';
					var color = 'green';
				} else {
					var feedback = result.response.Message;
					var color = 'red';
				}
				$('span.feedback').css('color', color).html(feedback).show().delay(3000).fadeOut(1000);
			},
		});
	});
});
{/literal}
</script>

<h3>{'Subscriptions'|i18n('extension/xrowecommerce')}: <span class="feedback">&nbsp;</span></h3>
{def
	$buyer = $current_user.groups|contains(461)
	$staffing = $current_user.groups|contains(453)
	$na_sub_node_id = 455
	$we_sub_node_id = 456
	$row_sub_node_id = 97208
	$na_sub = false()
	$we_sub = false()
	$$row_sub = false()
}

{foreach $subscriptions as $subscription}
	{if $subscription.data_map.product.content.main_node_id|eq($na_sub_node_id)}
		{set $na_sub = true()}
	{elseif $subscription.data_map.product.content.main_node_id|eq($we_sub_node_id)}
		{set $we_sub = true()}
	{elseif $subscription.data_map.product.content.main_node_id|eq($row_sub_node_id)}
		{set $row_sub = true()}
	{/if}
{/foreach}
<ul id="segments">
	<li><input type="checkbox" class="cws30 daily_news" name="CWS30" value="" />CWS30</li>
	<li><input type="checkbox" class="daily_news" name="Global News Digest" value="" />Global News Digest</li>
	<li><input type="checkbox" class="daily_news" name="Global Daily News" value="" />Global Daily News</li>
	<li><input type="checkbox" class="daily_news" name="Global Daily News - CWS" value="" />Global Daily News - CWS</li>
	<li><input type="checkbox" class="daily_news" name="Global Daily News - CWS" value="" />Global Daily News - CWS</li>
	<li><input type="checkbox" class="it_staffing_report daily_news" name="IT Staffing Report" value="" />IT Staffing Report</li>
	<li><input type="checkbox" class="healthcare_staffing_report daily_news" name="Healthcare Staffing Report" value="" />Healthcare Staffing Report</li>
	<li><input type="checkbox" class="engineering_staffing_report daily_news" name="Engineering Staffing Report" value="" />Engineering Staffing Report</li>
	<li><input type="checkbox" class="industrial_staffing_report daily_news" name="Industrial Staffing Report" value="" />Industrial Staffing Report</li>
	<li>
		<input type="checkbox" class="research_bulletin" name="Research Bulletin - NA CM Only" value="" />Research Bulletin - NA CM Only
		{if $na_sub}
		{else}
			<span class="email_sub_ineligible">(ineligible)</span>
		{/if}
	</li>
	<li>
		<input type="checkbox" class="research_bulletin" name="Research Bulletin - NA CWSC" value="" />Research Bulletin - NA CWSC
		{if and($na_sub,$buyer)}
		{else}
			<span class="email_sub_ineligible">(ineligible)</span>
		{/if}
	</li>
	<li>
		<input type="checkbox" class="research_bulletin" name="Research Bulletin - WE CM" value="" />Research Bulletin - WE CM
		{if $we_sub}
		{else}
			<span class="email_sub_ineligible">(ineligible)</span>
		{/if}
	</li>
	<li>
		<input type="checkbox" class="research_bulletin" name="Research Bulletin - WE CWSC" value="" />Research Bulletin - WE CWSC
		{if and($we_sub,$buyer)}
		{else}
			<span class="email_sub_ineligible">(ineligible)</span>
		{/if}
	</li>
	<li>
		<input type="checkbox" class="research_bulletin" name="Research Bulletin - ROW CM" value="" />Research Bulletin - ROW CM
		{if $row_sub}
		{else}
			<span class="email_sub_ineligible">(ineligible)</span>
		{/if}
	</li>
	<li>
		<input type="checkbox" class="research_bulletin" name="Research Bulletin - ROW CWSC" value="" />Research Bulletin - ROW CWSC
		{if and($row_sub,$buyer)}
		{else}
			<span class="email_sub_ineligible">(ineligible)</span>
		{/if}
	</li>
</ul>
