$(function(){
	// issue 10064
	$('form[name=roles] input[title="Remove selected roles."]').hide();
	$('form[name=roles] input[title="Remove selected roles."]').click(function(){
		return confirm("CAUTION!\nYou're about to remove the checked role(s) entirely. Are you sure you want to continue?");
	});
});

function OAS_RICH() {
	return true;
}
