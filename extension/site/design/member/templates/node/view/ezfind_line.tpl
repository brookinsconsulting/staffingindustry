{def $show_image=and(cond(is_set($hide_image),not($hide_image),true()), cond(is_set($node.data_map.image),$node.data_map.image.has_content,false()))
     $mylink = $node|sitelink(no)}
     
 {if $node.class_identifier|eq('file')}
     {def $rr = fetch(content, reverse_related_objects, hash(object_id, $node.object.id))}
     {if $rr|count|gt(0)}{set $mylink = $rr[0]|sitelink(no)}{/if}
 {/if}
     
<div class="content-view-line class-{$node.class_identifier}{if $show_image} line-image{/if} {$bgColor}" style="background-position:{$node.score_percent|wash()|int()}% 0;">
{if and($show_image,$node.class_identifier|ne('topic'))}
	{set-block variable='left_column'}
		<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image image_class='search_result'}</div>
	{/set-block}
{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$left_column class='search_result'}
{/if}

{set-block variable='right_column'}
<h2><a href={$mylink}>{if eq($node.class_identifier,'article')}{$node.data_map.title.data_text|wash()}{else}{$node.name|wash()}{/if}</a>
	<span class="search_result_date"> - 
	{if  $node.data_map.publish_date.has_content}
		 {$node.data_map.publish_date.content.timestamp|datetime('custom','%n/%d/%Y')}
	{else}
		{$node.published|datetime( 'custom', '%n/%d/%Y' )}
	{/if}
	</span>
</h2>
	{if $node.highlight}
		{$node.highlight}
	{else}
	<p>{$node.data_map.intro.content.output.output_text|striptags|shorten( 300 )}</p>
	{/if}
	<div><em><a href={$mylink}>/{$node.url_alias|shorten(70, '...', 'middle')|wash}</a></em></div>
	<span class="score-percent">Relevance: {$node.score_percent|wash}%</span>
{/set-block}
	{if $show_image}
		{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$right_column class='r-search_result' position='right'}
	{else}
		{$right_column}
	{/if}
</div>
