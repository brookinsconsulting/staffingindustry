{* Article - Full view *}

{def $my_roles = fetch(user,current_user).role_id_list
	 $this_siteaccess = current_siteaccess().name
	 $check_ar=hash('eng_member7', 199, 'site_member7', 197, 'eng_member8', 195, 'site_member8', 192, 'eng_member11', 199, 'site_member11', 197, 'eng_member12', 195, 'site_member12', 192, 'row_member7', 247, 'row_member8', 249, 'row_member11', 247, 'row_member12', 249)}


{if and($node.object.section_id|ne(18), $node.object.section_id|ne(9), $node.object.section_id|ne(1), $my_roles|contains($check_ar[concat($this_siteaccess, $node.object.section_id)])|not, $my_roles|contains(23)|not, $my_roles|contains(25)|not )}

{node_view_gui content_node=$node view='restricted_article'}

{else}

<div id="article-utility">
	<h2>{if and(ne($node.data_map.type.data_text,''),ne($node.data_map.type.data_text,'na'))}{attribute_view_gui attribute=$node.data_map.type}{if eq($node.parent.class_identifier,'issue')}: {$node.parent.name|wash()}{/if}{else}Article{/if}</h2>
	<a class="button print" href="#" onclick="window.print();"  class="print">Print</a>
</div>

<header>
	<h1>{$node.data_map.title.content|wash()}</h1>
</header>
{if $node.data_map.display_author_email.data_int}
{def $author_list=array()}
	{foreach $node.data_map.authors.content.author_list as $author}
		{if ne($author['id'],10)}{set $author_list=$author_list|append($author)}{/if}
	{/foreach}
{/if}

<div class="attribute-byline">
{if $node.data_map.publish_date.has_content}<h3 class="date">{$node.data_map.publish_date.data_int|datetime('custom','%F %j, %Y')}</h3>{/if}
{if $node.data_map.tags.has_content}<span class="tags">Tags: {attribute_view_gui attribute=$node.data_map.tags}</span>{/if}
{*<span class="category">Category: <a href="#">Staffing Firms</a></span>*}
{*if $node.data_map.author.content.is_empty|not()}<span class="author">{attribute_view_gui attribute=$node.data_map.author}</span>{/if*}
{if $node.data_map.display_author_email.data_int}
	{if count($author_list)}
		<div class="author">
		<span class="author_label">Guest Author{if gt(count($author_list),1)}s{/if}: </span>
		<div class="authors">
			{foreach $author_list as $author}
				<span>{$author.name|wash()}{if $node.data_map.display_author_email.data_int} {$author.email}{/if}</span>{delimiter}<br />{/delimiter}
			{/foreach}
		</div>
		</div>
	{/if}
{/if}
{include uri="design:parts/sharethis.tpl"}
</div>
<section class="article-content">
	<aside class="content-sidebar align-right">
		{if eq(ezini('article','ImageInFullView','content.ini'),'enabled')}
		{*
			{if $node.data_map.image.has_content}
				<section class="attribute-image">
					{attribute_view_gui attribute=$node.data_map.image image_class=medium}
					{if $node.data_map.caption.has_content}
						<div class="caption">
						{attribute_view_gui attribute=$node.data_map.caption}
						</div>
					{/if}
				</section>
			{/if}
		*}
			{if $node.data_map.ml_image.has_content}
				<section class="attribute-image">
					{attribute_view_gui attribute=$node.data_map.ml_image.content.data_map.image image_class="medium"}
					{if $node.data_map.caption.has_content}
						<div class="caption">
						{attribute_view_gui attribute=$node.data_map.caption}
						</div>
					{/if}
				</section>
			{elseif $node.data_map.image.content.is_valid}
				<section class="attribute-image">
					{attribute_view_gui attribute=$node.data_map.image image_class="medium"}
					{if $node.data_map.caption.has_content}
						<div class="caption">
						{attribute_view_gui attribute=$node.data_map.caption}
						</div>
					{/if}
				</section>
			{elseif null($node.data_map.image.content.is_valid)}
				<section class="attribute-image">
					{attribute_view_gui attribute=fetch('content','node',hash('node_id',40588)).data_map.image image_class="medium"}
				</section>
			{/if}
		{/if}
		{if $node.data_map.at_a_glance.has_content}
			<section>
				<h2>Why This Matters</h2>
				{attribute_view_gui attribute=$node.data_map.at_a_glance}
			</section>
		{/if}


		{def $analyst_list=cond($node.data_map.analyst.has_content,$node.data_map.analyst.content.relation_list,false())}
		{if count($analyst_list)}
			{def $analyst_node=false()
				 $analyst_link=false()
				 $recent_list=false()
				 $recent1=false()
				 $recent2=false()
			}
			{if is_array($analyst_list)}
			{def $ala=$analyst_list}
				{foreach $ala as $key=>$item}
				{if $item}
					{set $item=fetch('content','object',hash('object_id',$item.contentobject_id))
						 $analyst_list=cond(eq($key,0),array($item),$analyst_list|append($item))
					}
				{/if}
				{/foreach}
			{else}
			{set $analyst_list=array()}
			{/if}

		{foreach $analyst_list as $analyst}
			{set $analyst_node=$analyst.main_node}
			{if gt(count($analyst.assigned_nodes),1)}
				{foreach $analyst.assigned_nodes as $anode}
					{if $anode.path_array|contains(2)}{set $analyst_node=$anode}{break}{/if}
				{/foreach}
			{/if}
			{set $analyst_link=$analyst_node|sitelink()}
				<section>
					<hgroup>
						<h2>{$analyst.name|wash()}</h2>
						<h3>{$analyst_node.data_map.title.data_text|wash()}</h3>
					</hgroup>
					{if $analyst.data_map.image.has_content}{attribute_view_gui attribute=$analyst_node.data_map.image image_class='small' href=$analyst_link}{/if}
					<a href={$analyst_link}>Bio</a> &bull; <a href={$analyst_link}>Contact</a>
			{set $recent1=fetch('content','tree',hash('parent_node_id',2,
													'limit', 5,
													'sort_by',array('published',false()),
													'attribute_filter',array('and',
														array('owner','=',$analyst.id),
														array('class_identifier','=','article'))
													))
				 $recent2=fetch('content','related_objects',hash('object_id',$analyst.id,
				 										'limit', 5,
																'attribute_filter','analyst',
																'sort_by',array('published',false())
																))
				 $recent3=$node|real_rev_rels(hash('attribute_identifier', 417, 'limit', 5))
				 $recent_list=cond(count($recent1),$recent1|merge(cond($recent3,$recent3,array())),array())
			}
				{if and($recent_list,count($recent_list))}
					<h3>Recent Posts</h3>
					<ul>
					{foreach $recent_list|asort(array('published'),1,'DESC') as $recent_node max 5}
						<li><a href={$recent_node|sitelink()}>{$recent_node.name|wash()}</a></li>
					{/foreach}
					</ul>
				{/if}
				</section>
		{/foreach}
		{/if}

		{if not($node.data_map.star_rating.data_int)}
			<section class="attribute-star-rating">
				<h2>Article Rating</h2>
				{attribute_view_gui attribute=$node.data_map.star_rating}
			</section>
		{/if}
		{foreach fetchinfoboxes() as $infobox}
			{if $infobox.data_map.content.content.output.output_text|contains('with_realmedia')}
				{node_view_gui content_node=$infobox view='infobox' ad=true()}
			{/if}
		{/foreach}
		</aside>
{if eq(ezini('article','SummaryInFullView','content.ini'),'enabled')}
	{if $node.data_map.intro.content.is_empty|not}
		<div class="attribute-short">
			{attribute_view_gui attribute=$node.data_map.intro}
		</div>
	{/if}
{/if}

{if $node.data_map.body.content.is_empty|not}
		{attribute_view_gui attribute=$node.data_map.body}
{/if}
</section>

{if $node.data_map.filepath.has_content}
<div class='article-file-download'>
		<a href="{$node.data_map.filepath.data_text}">Download File</a>
</div>
{/if}

{include uri="design:parts/comments.tpl" class=array('advanced','module') section=true()}
{include uri='design:parts/related_content.tpl' class=array('advanced','module') section=true() header='Related'}

{/if}