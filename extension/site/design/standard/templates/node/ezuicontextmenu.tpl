{* Add an "Upload User Data" item in Admin Content tree context menu*}
<hr/>
<script type="text/javascript">
<!--
menuArray['ContextMenu']['elements']['menu-uiupload'] = new Array();
menuArray['ContextMenu']['elements']['menu-uiupload']['url'] = {"/ezuiupload/upload/%nodeID%"|ezurl};
menuArray['ContextMenu']['elements']['menu-uidownload'] = new Array();
menuArray['ContextMenu']['elements']['menu-uidownload']['url'] = {"/ezuiupload/download/%nodeID%"|ezurl};

// -->
</script>

<a id="menu-uiupload" href="#" onmouseover="ezpopmenu_mouseOver( 'ContextMenu' )" >Upload user data</a>
<a id="menu-uidownload" href="#" onmouseover="ezpopmenu_mouseOver( 'ContextMenu' )" >Download user data</a>