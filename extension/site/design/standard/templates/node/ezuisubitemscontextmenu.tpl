{* Add an "Upload User Data" item in Subitems context menu*}
<hr/>
<script type="text/javascript">
<!--
menuArray['SubitemsContextMenu']['elements']['child-menu-uiupload'] = new Array();
menuArray['SubitemsContextMenu']['elements']['child-menu-uiupload']['url'] = {"/ezuiupload/upload/%nodeID%"|ezurl};
menuArray['SubitemsContextMenu']['elements']['child-menu-uidownload'] = new Array();
menuArray['SubitemsContextMenu']['elements']['child-menu-uidownload']['url'] = {"/ezuiupload/download/%nodeID%"|ezurl};
// -->
</script>

<a id="child-menu-uiupload" href="#" onmouseover="ezpopmenu_mouseOver( 'ContextMenu' )" >Upload user data</a>
<a id="child-menu-uidownload" href="#" onmouseover="ezpopmenu_mouseOver( 'ContextMenu' )" >Download user data</a>
