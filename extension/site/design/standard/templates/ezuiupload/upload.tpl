{if is_set($script_id)}
<div class="message-feedback">
	<h2>Your file is being processed</h2>
	<p>Results will be emailed to {$email} when complete. You can monitor its progress <a href={concat('/scriptmonitor/view/', $script_id)|sitelink()}>here</a>.</p>
</div>
{/if}

<h2>Import User Data</h2>
<form method='POST' enctype='multipart/form-data' action=''>
	<lable for='uifile'>Select a CSV file of user data to upload&nbsp;</label>
	<input name='uifile' type='file'/>
	<input name='submit' type='submit' value='Upload'/>
</form>

{* no longer relevant as this information now goes in an email once the script has completed
{if $error_string|ne('vital')}<p>{$error_string}</p>{/if}
{if $error_string|contains('vital')|not}
<p>Successful import.</p>
{/if}
*}
