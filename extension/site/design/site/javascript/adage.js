/* global js vars */
/* LEGACY ONLY */
var img_path="/img";

function ltrim(s) { return s.replace( /^\s*/, "" ); }
function rtrim(s) { return s.replace( /\s*$/, "" ); } 
function trim(s) { return rtrim(ltrim(s)); }
function putCursor(obj) { if(window.focus)obj.focus(); }
function putSelection(obj) { putCursor(obj); if(window.select)obj.select(); }
function setCursor(field,value) { if(field.value==value) { field.value=""; } }
function checkCursor(field,value) { if(isBlank(field.value)) { field.value=value; } }

function isBlank(s) {
	if(s.length==0)return true;
	for(var i=0; i<s.length; i++) {
		if(s.charAt(i)!=" ")return false;
	}
	return true;
}

function isNumeric(str) {
	str=str+"";
	digits="0123456789-.,";
	for (var i=0; i<str.length; i++) {
		if (digits.indexOf(str.charAt(i))==-1) {
			return false;
		}	
	}
	return true;
}

function getRadioValue(set) {
	for(var n=0; n<set.length; n++) {
		if(set[n].checked) {
			return set[n].value;
		}
	}
	return "";
}

function getRadioIndex(set,value) {
	for(var n=0; n<set.length; n++) {
		if(set[n].value==value) {
			return n;
		}
	}
	return -1;
}

function getCheckedRadioIndex(set) {
	for(var n=0; n<set.length; n++) {
		if(set[n].checked) {
			return n;
		}
	}
	return -1;
}

function checkedRadio(set) {
	checked=false;
	for(var n=0; n<set.length; n++) {
		if(set[n].checked) {
			checked=true;
			break;
		}
	}
	return checked;
}

function checkBox(box) {
	box.checked=(box.checked)?false:true;
}

function getPath(loc) {
    var loc=loc.toString();
	var normal=loc.lastIndexOf("/");
	var reverse=loc.lastIndexOf("\\");
	return (reverse>normal)?loc.substring(0,reverse+1):loc.substring(0,normal+1);
}

function getFilename(loc) {
	var loc=loc.toString();
	return loc.substring(loc.lastIndexOf('/')+1,loc.length);
}

function getLocationHash(loc) {
	loc=(!loc) ? window.location : loc;
	loc=(loc.hash) ? loc.hash : loc;
	loc=loc.toString();
	return (loc.indexOf('#')!=-1) ? loc.substring(loc.indexOf('#')+1, loc.length) : null;
}

function error(msg) {
	if(document.getElementById) {
		errm=getLayer('errmsg');
		errm.innerHTML=msg;
	} else {
		alert(msg);
	}
}

function getLayer(lyr) {
	if(document.getElementById) return document.getElementById(lyr);
	if(document.layers) return document.layers[lyr];
	if(document.all) return document.all[lyr];
	return null;
}

function getLyrObj(lyr) {
	if(document.getElementById) return document.getElementById(lyr).style;
	if(document.layers) return document.layers[lyr];
	if(document.all) return document.all[lyr].style;
	return null;
}

function swapClass(id,name) {
    lyr=getLayer(id);
    if(lyr != null) {
        lyr.className=name;
    }
}

function write(lyr,html) {
	lyr=getLayer(lyr);
	if (document.layers) {
		with(lyr.document) { open(); write(html); close(); }
	} else {
		lyr.innerHTML=html;
	}
}

function writeHTML(lyr,html) {
	lyr=getLayer(lyr);
	if (document.layers) {
		with(lyr.document) { open(); write(html); close(); }
	}
	else {
		lyr.innerHTML=html;
	}
}

function isEmail(email) {
	if(email.trim()=='') { //check if email field is blank
		return false;
	}
	if(email.indexOf('@')==-1 || email.indexOf('.')==-1) { //make sure the field has an '@' and a '.'
		return false;
	}
	if (email.indexOf('@') > email.lastIndexOf('.')) { //then make sure the '@' is before the last occurence of the '.'
		return false;
	}
	if (email.indexOf('@') < 1) { // make sure there's at least one char before the '@' symbol
		return false;
	}
	if ((email.lastIndexOf('.') - email.indexOf('@')) < 3) { // check if there's at least 2 chars between the '@' and the '.'
		return false;
	}
	if ((email.length - email.lastIndexOf('.')) < 3) { // and make sure there are at least 2 chars between the '.' and the end
		return false;
	}
	return true;
}
function getYouTubeVideo(dom_id, id, width, height) {
	var embed = [
		'<object width="', width, '" height="', height, '">',
		'<param name="movie" value="http://www.youtube.com/v/', id, '&hl=en&fs=1&"></param>',
		'<param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param>',
		'<embed src="http://www.youtube.com/v/', id, '&hl=en&fs=1&" type="application/x-shockwave-flash"',
		'allowscriptaccess="always" allowfullscreen="true" width="', width, '" height="', height, '"></embed></object>'
	];

	DOM.getElementById(dom_id).innerHTML = embed.join('');
}

function str_replace(needle, replacement, haystack) {
	var temp = haystack.split(needle);
	return temp.join(replacement);
}

/**
 * @author: Timothy Dalbey
 * @date: 
 * 
 */
function matchAll(string, d_s, d_e){
	var pattern = new RegExp(d_s+'[^'+d_e+']*'+d_e,'g');
	var mathces =  [];
	matches = string.match(pattern);
	for (i in matches) {
		if(typeof matches[i] == 'string') {
			matches[i] = matches[i].substring(d_s.length,matches[i].length - (d_e.length));
		}
	}
	return matches;
}

function getGETParams() {
	params={};

	// see if string is being passed;  otherwise, grab location URL
	get_string=(getGETParams.arguments[0])?getGETParams.arguments[0]:window.location;

	// force string conversion
	get_string=get_string.toString();

	// chop up GET string into name/value pairs
	get_string=get_string.split('&');

	// loop through all name/value pairs
	for(var n=0; n<get_string.length; n++) {

		// split name/value pair by '=' sign
		get_param=get_string[n].split('=');

		// set params
		params[get_param[0]]=get_param[1];
	}
	return params;
}

function isArray(obj) {
	if(obj.constructor.toString().indexOf("Array")==-1) {
		return false;
	}
	else {
		return true;
	}
}

function isObject(obj) {
	if(obj.constructor.toString().indexOf("Object")==-1) {
		return false;
	}
	else {
		return true;
	}
}

/**
 * JavaScript class extensions
 *
 */
String.prototype.trim=function() {
	a=this.replace(/^\s+/,'');
	return a.replace(/\s+$/,'');
}

String.prototype.ucfirst=function() {
	return this.charAt(0).toUpperCase() + this.substring(1,this.length).toLowerCase();
}

String.prototype.autolink = function() {
	var hlink = /\s(ht|f)tp:\/\/([^ \,\;\:\!\)\(\"\'\<\>\f\n\r\t\v])+/g;
	return this.replace (
		hlink, function ($0, $1, $2) {
			s = $0.substring(1, $0.length);

			// remove trailing dots, if any
			while (s.length > 0 && s.charAt(s.length-1)=='.') {
				s = this.substring(0, s.length-1);
			}
			// add hlink
			return " " + s.link(s); 
		}
	);
}

// returns the index of an array element, -1 if element not found
Array.prototype.indexOf=function(el) {
	for(var n=0; n<this.length; n++) {
		if(this[n]==el) {
			return n;
		}
	}
	return -1;
}

// search each array element for existence of string (case-sensitive), returns array index or -1
Array.prototype.search=function(str) {
	for(var n=0; n<this.length; n++) {
		if(this[n].indexOf(str)!=-1) {
			return n;
		}
	}
	return -1;
}

// search each array element for existence of string (case-insensitive), returns array index or -1
Array.prototype.searchNoCase=function(str) {
	for(var n=0; n<this.length; n++) {
		if(this[n].toLowerCase().indexOf(str.toLowerCase())!=-1) {
			return n;
		}
	}
	return -1;
}

// see if array contains an item
Array.prototype.contains=function(el) {
	return this.indexOf(el)!=-1;
}

// return last item from array
Array.prototype.last=function() {
	return this[this.length-1];
}

// routine to clear an array
Array.prototype.clear=function() {
	this.length=0;
	return this;
}

Array.prototype.arrange=function() {
	minor=[];
	major=[];

	// split array into two arrays (a 1 char array, and a 2+ char array)
	for(var i=0; i<this.length; i++) {
		if(this[i].toString().trim().length==1) {
			minor[minor.length]=this[i];
		}
		else {
			major[major.length]=this[i];
		}
	}

	minor.sort(); // sort 1 char array
	major.sort(); // sort 2+ char array
	this.clear(); // clear current array

	// rebuild current array
	for(var i=0; i<minor.length; i++) { this[this.length]=minor[i]; }
	for(var i=0; i<major.length; i++) { this[this.length]=major[i]; }

	// cleanup
	minor.clear();
	major.clear();
}

/**
 * DOM Routines
 *
 * create our own DOM object to centralize DOM-based routines
 *
 */
var DOM = {
	getElementById:function(id, tag, parent) {
		if(document.getElementById(id)) {
			return document.getElementById(id);
		}
		else if(document.layers && document.layers[id]) {
			return document.layers[id];
		}
		else if(document.all && document.all[id]) {
			return document.all[id];
		}
		else if(document[id]) {
			return document[id];
		}
		else if(window[id]) {
			return window[id];
		}
		if(this.createElement) {
			tag=(tag)? tag : 'div';
			el=this.createElement(tag);
			el.setAttribute('id', id);
			parent=(parent)? parent : 'body';
			this.getElementsByTagName(parent)[0].appendChild(el);
			return el;
		}
		return null;
	},
	getElementsByTagName:function(tag, el) {
		var el=(el)? el : document;
		if(el.getElementsByTagName) { return el.getElementsByTagName(tag); }
		return null;
	},
	createElement:function(el) {
		if(document.createElement) { return document.createElement(el); }
		return null;
	},
	getTagNameFromElement:function(el) {
		if(el.tagName) { return el.tagName.toLowerCase(); }
		if(el) { return el.toString().toLowerCase(); }
		return null;
	},
	setOpacity:function(id, opacity) {
    	el=this.getElementById(id);
		el.style.opacity=(opacity / 100);
		el.style.MozOpacity=(opacity / 100);
		el.style.KhtmlOpacity=(opacity / 100);
		el.style.filter="alpha(opacity=" + opacity + ")";
	},
	getInclude:function(src) {
		if(document.createElement) {
			var js=document.createElement('script');
			js.setAttribute('type', 'text/javascript');
			js.setAttribute('src', src);
			document.getElementsByTagName('head')[0].appendChild(js);
		}
	},
	getCSS:function(src) {
		if(document.createElement) {
			var js=document.createElement('link');
			js.setAttribute('type', 'text/css');
			js.setAttribute('rel', 'stylesheet');
			js.setAttribute('media', 'all');
			js.setAttribute('src', src);
			document.getElementsByTagName('head')[0].appendChild(js);
		}
	},
	setClass:function(id, className) {
		this.getElementById(id).className=className;
	}
};

/**
 * DHTML Routines
 *
 * create our own DHTML object to centralize cross-browser DHTML routines
 *
 */
var DHTML={
	// centralize inner window width/height
	getInnerWidth:function() {
		if(window.innerWidth) { return innerWidth; }
		if(document.body.offsetWidth) { return document.body.offsetWidth; }
		return null;
	},
	getInnerHeight:function() {
		if(window.innerHeight) { return innerHeight; }
		if(document.body.clientWidth) { return document.body.clientWidth; }
		if(document.body.offsetHeight) { return document.body.offsetHeight; }
		return null;
	},
	// centralize number of pixels scrolled to
	getScrollX:function() {
		if(window.pageXOffset) { return pageXOffset; }
		if(document.body.scrollLeft) { return document.body.scrollLeft; }
		if(document.documentElement.scrollLeft) { return document.documentElement.scrollLeft; }
		return null;
	},
	getScrollY:function() {
		if(window.pageYOffset) { return pageYOffset; }
		if(document.body.scrollTop) { return document.body.scrollTop; }
		if(document.documentElement.scrollTop) { return document.documentElement.scrollTop; }
		return null;
	},
	getPageX:function(e) {
		if(!e) { var e=window.event; }
		if(e.pageX) { return e.pageX; }
		if(e.clientX) { return e.clientX + DHTML.getScrollX(); }
		if(e.offsetLeft) { return e.offsetLeft; }
		return null;
	},
	getPageY:function(e) {
		if(!e) { var e=window.event; }
		if(e.pageY) { return e.pageY; }
		if(e.clientY) { return e.clientY + DHTML.getScrollY(); }
		if(e.offsetTop) { return e.offsetTop; }
		return null;
	},
	getPosX:function(obj) {
		posX=0;
		if(obj.offsetParent) { // loop through parent elements
			while(obj.offsetParent) {
				posX+=obj.offsetLeft;
				if(!obj.offsetParent) {
					break; // no more parents
				}
				obj=obj.offsetParent;
			}
		}
		else if(obj.x) {
			posX+=obj.x;
		}
		return parseInt(posX);
	},
	getPosY:function(obj) {
		posY=0;
		if(obj.offsetParent) { // loop through parent elements
			while(obj.offsetParent) {
				posY+=obj.offsetTop;
				if(!obj.offsetParent) {
					break; // no more parents
				}
				obj=obj.offsetParent;
			}
		}
		else if(obj.y) {
			posX+=obj.y;
		}
		return parseInt(posY);
	},
	fade:function(id, start, end, time) {
		var speed=Math.round(time / 100);
		var timer=0;

		if(start > end) {
			for(var i=start; i>=end; i--) {
				//setTimeout(function() { DOM.setOpacity(id, i); }, (timer * speed));
				setTimeout("DOM.setOpacity('" + id + "', " + i + ")",(timer * speed));
				timer++;
			}
		}
		else if(start < end) {
			for(var i=start; i <= end; i++) {
				//setTimeout(function() { DOM.setOpacity(id, i); }, (timer * speed));
				setTimeout("DOM.setOpacity('" + id + "', " + i + ")",(timer * speed));
				timer++;
			}
		}
	},
	getElementByEvent:function(e) {
		if(!e) { var e=window.event; }
		if(e.srcElement) { return e.srcElement; }
		if(e.target) { return e.target; }
		return null;
	},
	getKeyCode:function(e) {
		if(!e) { var e=window.event; }
		return (e.keyCode)? e.keyCode : e.which;
	}
};

/**
 * AJAX Routines
 *
 * create our own AJAX object to centralize cross-browser AJAX routines
 *
 */

var AJAX={};

// return HTTP responses
AJAX.ReadyState={};
AJAX.ReadyState['Uninitialized']=0;
AJAX.ReadyState['Loading']		=1;
AJAX.ReadyState['Loaded']		=2;
AJAX.ReadyState['Interactive']	=3;
AJAX.ReadyState['Complete']		=4;

// initialize cross-browser HTTP request
AJAX.getHTTPRequest=function() {
	if(window.XMLHttpRequest) { return new XMLHttpRequest(); }
	if(window.ActiveXObject) {
		try { return new ActiveXObject("Msxml2.XMLHTTP"); }
		catch(e) { return new ActiveXObject("Microsoft.XMLHTTP"); }
	}
	return null;
}

// Constructor to setup HTTP file requests
AJAX.FileRequest=function(url, onload, onerror, method, params, contentType) {
	this.url=url;
	this.params=(params)? params : null;
	this.method=(method)? method : 'get';

	this.url+=(this.params!=null && this.method.toLowerCase()=='get')?'?'+this.params:'';
	this.contentType=(contentType)? contentType : "application/x-www-form-urlencoded";
	this.contentType=(!this.contentType && this.method.toLowerCase()=='post')? "application/x-www-form-urlencoded" : this.contentType;

	this.request=null;
	this.response=null;
	this.onload=(onload)? onload : this.OnLoad;
	this.onerror=(onerror)? onerror : this.OnError;
	this.sendFileRequest();
}

// method to generate an HTTP request for selected file
AJAX.FileRequest.prototype.sendFileRequest=function() {
	this.request=AJAX.getHTTPRequest();
	if(this.request) {
		try {
			// pass reference of current instance to event handler
			var ref=this;
			this.request.onreadystatechange = function() {
				ref.onReadyState.call(ref);
				delete ref;
			}

			// open HTTP connection
			this.request.open(this.method, this.url, 1);

			// set content-type header
			if(this.contentType) { this.request.setRequestHeader('content-type', this.contentType); }

			// send request
			this.request.send(this.params);
		}
		catch(err) {
			//alert('err 1')
			this.onerror.call(this);
		}
	}
}
AJAX.FileRequest.prototype.onReadyState=function() {
	if(this.request) {
		if(this.request.readyState == AJAX.ReadyState['Complete']) {
			if(this.request.status==200 || this.request.status==0) {
				this.response=this.request.responseText;
				this.onload.call(this);
			}
			else {
				//alert('err 2')
				this.onerror.call(this);
			}
		}
		else {
			//this.response=AJAX.ReadyState.getKeyProperty(this.request.readyState);
			//this.onload.call(this);
		}
	}
	else {
		//alert('err 3')
		this.onerror.call(this);
	}
}
AJAX.FileRequest.prototype.OnError=function() {
	if(this.request) {
		alert(
			"Error fetching data"
			+ "\n\tReadyState: " + this.request.readyState
			+ "\n\tStatus: " + this.request.status
			+ "\n\tHeaders: " + this.request.getAllResponseHeaders()
		);
	}
}
AJAX.FileRequest.prototype.OnLoad=function() {
	if(this.request) {
		alert(this.response);
	}
}

// parse XML tree
AJAX.parseXML=function(xml) {
	var doc=null;
	if(window.DOMParser) {
		try { 
			doc=(new DOMParser()).parseFromString(xml, "text/xml"); 
		} 
		catch (e) { doc=null; }
	}
	else if (window.ActiveXObject) {
		try {
			doc=new ActiveXObject('Microsoft.XMLHTTP');
			doc.async=false;
			if(!doc.loadXML(xml)) {
				//doc=doc.parseError.reason + doc.parseError.srcText;
				doc=null;
			}
		} 
		catch (e) { doc=null; }
	}
	return doc;
}

/**
 * JSON Routines
 *
 * create our own JSON object to centralize cross-browser routines
 *
 */
var adageJSON={
	parse:function(code) {
		if (/^[\],:{}\s]*$/.test(code.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(:?[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
			return eval('('+code+')');
		}
		return null;
	}
};

/* custom routines */

function toggleOpinion(id) {
	var expand=(document["opinion_img_"+id].src.indexOf("plus.gif")!=-1);
	document["opinion_img_"+id].src=(expand)?img_path+"/minus.gif":img_path+"/plus.gif";

	var comments=getLyrObj("comments_"+id);
	comments.display=(expand)?"block":"none";
}

function popURL(file,w,h) {
	new_win=window.open(file,'','width='+w+',height='+h+',top=0,menubar=no,toolbar=no,scrollbars=yes,resizable=yes,location=yes,status=no');
	return false;
}

function pop(file,w,h) {
	if(file.indexOf('datapopup.php')!=-1) {
	//if(file=='datapopup.php') {
		file='/datacenter/'+file;
	}
	var admin_win=window.open(file,'','width='+w+',height='+h+',top=0,menubar=no,toolbar=yes,scrollbars=yes,resizable=yes,location=no,status=no');
}

function popWin(file,w,h) {
	var admin_win=window.open(file,'','width='+w+',height='+h+',top=0,location=no,directories=no,menubar=no,status=no,titlebar=no,resizable=yes,location=no,status=no');
}

function popImage(path) {
	//new AJAX.FileRequest('/functions/ajax/get_image_size.php', imagePopup, null, 'get', 'path='+path);

	new AJAX.FileRequest(
		'/functions/ajax/get_image_pop.php',
		function() {
			if(this.request && this.response) {

				// parse JSON results
				image=adageJSON.parse(this.response);

				// dynamically set size
				img_pop=DOM.getElementById('image_popup');
				img_pop.style.width=parseInt(image.width) + 20 + "px";
				img_pop.style.height=parseInt(image.height) + 30 + "px";
				//alert(image.width+ ' '+ image.height);
				// dynamically set position
				img_pop.style.top=(DHTML.getScrollY() + 120) + "px";
				img_pop.style.left=(DHTML.getInnerWidth() / 4) + "px";

				DOM.getElementById('image_popup_image_tag').innerHTML=image.template;
				img_pop.style.display="block";
			}else {
				alert(this.response);
			}
		},
		null,
		'get',
		'path='+path
	);
}
/*
function captureEventAtMouse() {
	if(window.Event) { document.captureEvents(Event.MOUSEMOVE); }
}
function releaseEventAtMouse() {
	if(window.Event) { document.releaseEvents(Event.MOUSEMOVE); }
}
*/
function popImageAtMouse(path) {
	new AJAX.FileRequest(
		'/functions/ajax/get_image_pop.php',
		function() {
			if(this.request && this.response) {
				image = adageJSON.parse(this.response);
				if(image){
					img_pop	= DOM.getElementById('image_popup');
					
					img_pop.style.width 	= parseInt(image['width']) + 20 + "px";
					img_pop.style.height 	= parseInt(image['height']) + 30 + "px";
					img_pop.style.top		= (mapMouseY - (parseInt(image['height']) + 40)) + "px";
					img_pop.style.left		= (mapMouseX - (parseInt(image['width']) / 2)) + "px";
					
					DOM.getElementById('image_popup_image_tag').innerHTML = image['template'];
					img_pop.style.visibility = 'visible';
					img_pop.style.display = "block";

				}else{
					alert('Could not parse response: '+this.response);
				}
			}else{
				alert(this.response);
			}
		},
		null,
		'get',
		'path='+path
	);
}

/*
function imagePopup() {
	if(this.request && this.response) {
		image=getGETParams(this.response);
		if(image["path"] && image["width"] && image["height"]) {
			img=DOM.getElementById('image-pop');
			//img.className='story-photo';
			img.style.width=parseInt(image["width"]);
			img.style.height=parseInt(image["height"]);
			HTML=[
				'<a href="javascript:imagePopClose()" title="Click to Close"><img src="', image["path"], '" width="', image["width"], '" height="', image["height"], '" alt="" /></a>'
			];
			img.innerHTML=HTML.join('');
			img.style.top=(DHTML.getScrollY() + 120) + "px";
			img.style.left=(DHTML.getInnerWidth() / 4) + "px";
			img.style.visibility="visible";
		}
		else {
			alert('We\'re sorry, this image is temporarily unavailable');
		}
	}
	else {
		alert('We\'re sorry, un expected error occurred');
	}
}
*/
function imagePopClose() {
	img_pop=DOM.getElementById('image_popup');
	img_pop.style.display="none";
	//img=DOM.getElementById('image-pop');
	//img.style.visibility="hidden";
}

function previewContent(content) {
	var preview_content=window.open('','preview_content','width=640,height=480,toolbar=no,scrollbars=yes,resizable=yes,location=no,status=no');
	with(preview_content.document) {
		open(); write(content); close();
	}
	if(preview_content.focus)preview_content.focus();
}

function clear(menu) {
	for(var j=0; j<menu.options.length; j++) {
		menu.options[j]=null;
	}
	menu.options.length=0;
	menu.options[0]=new Option("*Select from list*",0);
}

function set(menu) {
	var n=1;
	for(var i=1; i<set.arguments.length; i++) {
		entry=set.arguments[i].split("__");
		menu.options[n]=new Option(entry[1],entry[0]);
		n++;
	}
	//menu.options[1].selected=true;
}

var submitting=0;
function nice(form) {
	if(!submitting) {
		submitting=1;
		form.submit_btn.disabled=true;
		return true;
	}
	return false;
}
function popupWindow_middlecenter(purl,pname,pwidth,pheight,dx,dy,pstr) {
	var availWidth=window.screen.availWidth;
	var availHeight=window.screen.availHeight;

	if (window.screen.availWidth>1600) {
		availWidth-=window.screen.availWidth/2;
	}
    var px=(availWidth -pwidth )/2 + dx;
    var py=(availHeight-pheight)/2 + dy;

    PopupWindow_middlecenter = 
    window.open(purl,pname,'left='+px+',top='+py+',width='+pwidth+',height='+pheight+','+pstr+'');
	PopupWindow_middlecenter.focus();

}

// rss menu
var rssTimer=null;
function getRSSMenu() {
	hideRSSMenu();

	rssMenu=DOM.getElementById('rss-toolbar');
	rssButton=DOM.getElementById('toolbar-rss-button');

	rssMenu.style.top=parseInt(DHTML.getPosY(rssButton) + 10)+'px';
	rssMenu.style.left=parseInt(DHTML.getPosX(rssButton) - 275) +'px';
	rssMenu.style.visibility='visible';
}
function hideRSSMenu() {
	rssMenu=DOM.getElementById('rss-toolbar');
	rssMenu.style.visibility='hidden';
	if(rssTimer!=null) { clearTimeout(rssTimer); }
}
function toggleRSSMenu() {
	rssMenu=DOM.getElementById('rss-toolbar');
	if(rssMenu.style.visibility=='hidden' || rssMenu.style.visibility=='') {
		getRSSMenu();
	}
	else {
		hideRSSMenu();
	}
}
function offMenu() {
	rssTimer=setTimeout('hideRSSMenu()','3000');
}

// sponsorship tag
var sponsorTimer=null;
function hideSponsorTag() {
	st=DOM.getElementById('sponsorTag');
	st.style.visibility='hidden';	
	if(sponsorTimer!=null) { clearTimeout(sponsorTimer); }
}
function getSponsorTag() {
	st=DOM.getElementById('sponsorTag');
	eggnetwork_bird=DOM.getElementById('aa_eggnetwork_bird');

	st.innerHTML='Advertisement';
	st.style.top=parseInt(DHTML.getPosY(eggnetwork_bird) + 25)+'px';
	st.style.left=parseInt(DHTML.getPosX(eggnetwork_bird) - 75)+'px';
	st.style.visibility='visible';
	sponsorTimer=setTimeout('hideSponsorTag()','2000');
}

function toggleMost() {
	most_read=DOM.getElementById('most-read');
	most_emailed=DOM.getElementById('most-emailed');

	if(most_read.style.display=='block' || most_read.style.display=='') {
		most_read.style.display='none';
		most_emailed.style.display='block';
	}
	else {
		most_read.style.display='block';
		most_emailed.style.display='none';
	}
}
function addCommas(string){
	string += '';
	x = string.split('.');
	x1 = x[0];
	if(x.length > 1){
		x2 = '.' + x[1];
	}else{
		x2 = '';
	}
	var regex = /(\d+)(\d{3})/;
	while (regex.test(x1)) {
		x1 = x1.replace(regex, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

