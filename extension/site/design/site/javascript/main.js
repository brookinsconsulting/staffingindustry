if(typeof(console)!='undefined'){window.alert=console.log;}

if (typeof OAS_RICH == 'undefined') {
    function OAS_RICH() {
        return false;
    }
}

/*
AcceptCookies.initialize({
    mask:{
        color:'#111',
        zIndex:10000
    },
    link:function(){
        var link = $('<a name="notification" href="#">Change Cookie Settings</a>'),
            item = $('<li id="change-cookie"></li>')
        ;
        $('#user-pane').append(item.append(link));
        return link;
    }
});
*/

// IE9 compatible browser version detection
function getInternetExplorerVersion()   {
    // Returns the version of Internet Explorer or a -1 ( -1 indicating the use of another browser).
    var rv = -1; // Return value assumes failure.
     if (navigator.appName == 'Microsoft Internet Explorer')    {
        var ua = navigator.userAgent;
        var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)    {
            rv = parseFloat( RegExp.$1 );
        }
  }
  return rv;
}



    function openOverlay(olEl) {
        $oLay = $(olEl);

        if ($('#overlay-shade').length == 0)
            $('body').prepend('<div id="overlay-shade"></div>');

        $('#overlay-shade').fadeTo(300, 0.6, function() {
            var props = {
                oLayWidth       : $oLay.width(),
                scrTop          : $(window).scrollTop(),
                viewPortWidth   : $(window).width()
            };

            var leftPos = (props.viewPortWidth - props.oLayWidth) / 2;

            $oLay
                .css({
                    display : 'block',
                    opacity : 0,
                    top : '-=300',
                    left : leftPos+'px'
                })
                .animate({
                    top : props.scrTop + 40,
                    opacity : 1
                }, 600);
        });
    }

    function closeOverlay() {
        $('.overlay').animate({
            top : '-=300',
            opacity : 0
        }, 400, function() {
            $('#overlay-shade').fadeOut(300);
            $(this).css('display','none');
        });
    }

    $('#overlay-shade, .overlay a').live('click', function(e) {
        closeOverlay();
        if ($(this).attr('href') == '#') e.preventDefault();
    });


    // Usage
    $('#overlaylaunch-inAbox').click(function(e) {
       openOverlay('#overlay-inAbox');
       e.preventDefault();
    });






$('.customtag header h1,.customtag header h2,.infobox header h2,.customtag header h3,.content-view-line h2,.content-view-embed.class-article h1 a,footer .custom-tag-scrollable h1, .content-view-full header h1,.content-sidebar h2').each(function(){
    $(this).parent().css('wordWrap','break-word')
});

$(function(){
        jQuery('a[href*="content/download"]').click(function(){
                _gaq.push(['_trackEvent', 'File', 'Download', '/content/download file download']);
        });

    if ($.cookie('site_sidebar') == 'false') {
        $('#site_sidebar_toggle').html('+');
        $('#site_sidebar_toggle').siblings('.toggle').hide();
    } else {
        $('#site_sidebar_toggle').html('-');
    }
    $('#site_sidebar_toggle').click(function(e){
        e.preventDefault();

        var sidebar = $(this).siblings('.toggle');

        // chrome does not reposition $('#site_sidebar_toggle') when the transition is animated
        //$(this).siblings('.toggle').animate({width: 'toggle'}, 300);

        sidebar.toggle();

        if (sidebar.is(':visible')) {
            $.cookie('site_sidebar', true, {'path':'/','expires':365});
            $(this).html('-');
        } else {
            $.cookie('site_sidebar', false, {'path':'/','expires':365});
            $(this).html('+');
        }
    });

    $('#login_form li.loginbox input').focus(function(){
        if ($('#login_form li.first input').css('display') == 'none') {
            $('#login').removeClass('no-display').addClass('display');
            $('#login_form li.first a, #login_form li.first input').toggle()
        }
    });





    $('[border]').each(function(){if ($(this).css('border') == '') $(this).css('border', $(this).attr('border')+'px solid black' )})

/*
    $('.content-view-line.line-image .attribute-image img,.content-view-embed.line-image .attribute-image img').each(function(){
        $(this).parent('.attribute-image').css({'left':((120-this.width)/2)+'px'});
    });
*/
/*
    $('body.module-view-edit #site-main-content').click(function(){
        alert('expose');
        $(this).expose({'color':'#778899'});
        alert('done');
    });
*/

    // enable/disable accounts link on /user/edit/
    $('.module-user.module-view-edit a.enable, .module-user.module-view-edit a.disable').click(function(){
            var
                data = {"UpdateSettingButton": "OK"},
                link = $(this)
            ;

            if ($(this).hasClass('enable')) data.is_enabled = true; // value doesn't matter, post var just needs to exist

            link.fadeOut();
            $('span.enable_disable_info').html('updating..');

            $.ajax({
                type: 'POST',
                url: '/user/setting/' + $(this).attr('rel'),
                data: data,
                success: function() {
                    if (data.is_enabled) { // user was just enabled
                        $('span.enable_disable_info').html('enabled');
                        link.removeClass('enable').addClass('disable').html('disable').fadeIn();
                    } else { // user was just disabled
                        $('span.enable_disable_info').html('disabled');
                        link.removeClass('disable').addClass('enable').html('enable').fadeIn();
                    }
                },
                error: function() {
                    confirm('There was a problem, user status was not updated. Refresh the page and try again.');
                }
            });

            return false;
    });

    $('table[name=Enter Credit Card details] input[name=validate]').click(function(){
        //$(this).attr('disabled', 'disabled');
    });

    // set language preference

    $('#languages input')
        .click(function(){
            myout = $(this).val();
            if ($(this).attr('rel') != '') myout = myout + "/" + $(this).attr('rel');
            document.location.href='/switchlanguage/to/'+ myout + window.location.search;
        }
    );

    // cws30 subscribe machform
    $('form#form_3').submit(function(){
        if ($('#element_3_1').attr('checked')) {
            // first find the segments this user is already subscribed to
            var url = '/subscriber/segments';
            var data = {
                "email" : $('input#element_10').val(),
                "list_id" : "49b6031764f7283bed4dacd81fe6cabb"
            }
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                async: false,
                success: function(result){
                    var custom_fields = new Array();
                    result = $.parseJSON(result);
                    $.each(result, function(segmentID, segmentName){
                        custom_fields.push({
                            "Key" : "Newsletters",
                            "Value" : segmentName
                        });
                    });

                    // now add them to cws30 as well
                    var url = '/subscriber/add';
                    custom_fields.push({
                        "Key" : "Newsletters",
                        "Value" : "CWS30"
                    });
                    var data = {
                        "email" : $('input#element_10').val(),
                        "list_id" : "49b6031764f7283bed4dacd81fe6cabb",
                        "custom_fields" : custom_fields
                    }
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        async: false,
                        success: function(result){
                            result = $.parseJSON(result);
                            if (result.http_status_code == 201) {
                                confirm("You've been successfully subscribed to CWS30.");
                            } else {
                                confirm(result.response.Message);
                            }
                        }
                    });
                }
            });
        };
    });

    $("div.legacycontent > div > img:first-child, div.legacycontent > p > img:first-child, div.legacycontent > img:first-child").remove();


    $('#create-user').click(function(){
        var obj=$(this);
        $(obj.attr('rel')).submit();
        return false;
    });

    $('#dashboard-customize-{/literal}{$block.identifier}{literal}-link').click(function(){
        $('#dashboard-customize-{/literal}{$block.identifier}{literal}-window').dialog({
            modal: true,
            width: '60%'
        });
        return false;
    });

    $('#user-control-panel a.close').click(function(){
        var obj=$(this),
            panel=$('#user-control-panel'),
            top=parseInt(panel.css('top')),
            header=$('header[role=banner]');

        if(top==0){
            $.cookie('usercontrolpanel',escape('hide'),{'path':'/','expires':365});
            obj.text('Show Toolbar');
            panel.animate({'top':'-30px'},1000);
            header.animate({'marginTop':'-30px'},1000);
        }else{
            $.cookie('usercontrolpanel',escape('show'),{'path':'/','expires':365});
            obj.text('Hide Toolbar');
            panel.animate({'top':'0'},1000);
            header.animate({'marginTop':'0'},1000);
        }
        return false;
    }).each(function(){
        if($.browser.version == 7.0){
            $('#user-control-panel a.close').appendTo('header[role=banner]').css({'background-color':'#45535e','color':'#ffffff','font-family':'Arial','left':'50%','margin-left':'380px','padding':'3px 8px','position':'absolute','text-decoration':'none','z-index':10}).click();
        }
        if(!$.browser.mozilla){
            $('#user-control-panel a.close').click();
        }
    });
/*
    $('#language-switcher').hover(function(){
        if($.browser.version == 7.0){
            $('#languages').show().css({'width':'145px','margin-top':'-13px','right':'50%','position':'absolute','margin-right':'225px','zIndex':1000000}).appendTo('header[role=banner]');
        }else{
            $('#languages').slideDown(0);
        }
    },function(){
        if($.browser.version == 7.0){
            $('#languages').mouseleave(function() {
                $('#languages').hide();
            })
        }else{
            $('#languages').slideUp(0);
        }
    });

    $('.change-siteaccess').click(function(){
        alert("Please do not change the language when in the shop.");
    });
*/
    $('.search-results .content-view-line').each(function(){
        if(!$.browser.msie){
            var item=$(this),
                bkgndPosition=item.css('background-position').split(' ')
                width=item.outerWidth();
            item.animate({
                'backgroundPosition':(parseInt(bkgndPosition[0])/100)*width + 'px 0'
            },1000);
        }
    });

    $("#site-columns [title]:not(img)").tooltip({
        'effect':'slide',
        'predelay':250,
        'delay':250,
        'slideInSpeed':500,
        'slideOutSpeed':500,
        'position':'bottom center',
        'offset':[10,0],
        'relative':false,
        'opacity':0.9,
        'direction':'down',
        'onBeforeShow':function(){
            var trigger=this.getTrigger(),
                tip=this.getTip();
            tip.empty().append(trigger.data('title')).prepend($('<div class="tip"></div>'));
        }
    })/*.dynamic({
        'bottom':{
            'direction':'down',
            'bounce':true
        }
    })*/;


/*
    $("#site-columns [title]:not(img)").bt({positions: ['most'],
        'shrinkToFit': true,
//      'trigger': ['mouseover', 'none'],
        'padding': '20px',
        'textzIndex': 19999,
        'boxzIndex': 19998,
        'wrapperzIndex': 19997,
        'spikeGirth': 30,
        'spikeLength': 15,
        'fill': "rgba(20, 20, 20, .60)",
        'strokeWidth': 0,
        'strokeStyle': "rgb(225, 225, 225)",
        'cornerRadius': 15,
        'shadow': true,
        'shadowOffsetX': 0,
        'shadowOffsetY': 0,
        'shadowBlur': 6,
        'shadowColor': "#000",
        'noShadowOpts':{
            'strokeStyle': '#999',
            'strokeWidth': 4
            },
        'hideTip':function(box, callback) {
            alert('text',box.data('bt.active'));
//          $(box).hide();
//          callback();   // you MUST call "callback" at the end of your animations
        }
    });
*/

/*
$('body #site-main-content .line-image:not(aside,.noalign,.align-center) .attribute-image img').each(function(){
        var item=$(this),
            cont=item.parents('.line-image:not(.class-alert)'),
            wrap=item.parents('.line-image:not(.class-alert) .attribute-image'),
            cont_css={
            'paddingLeft':this.width+10+'px',
            'position':'relative',
            'width':(item.parents('.line-image:not(.class-alert)').width()-(this.width+10))+'px'
            };
        if(item.parents('.module-view-search').length){
            cont_css.width='auto';
        }
        cont.css(cont_css);
        wrap.css({
            'left':0,
            'top':cont.hasClass('content-view-line')?0:'10px',
            'position':'absolute'
            });
    });
*/

/*
$('body #site-main-content .line-image:not(aside,.noalign,.align-center) .attribute-image img').each(function(){
        var item=$(this),
            cont=item.parents('.line-image:not(.class-alert)'),
            wrap=item.parents('.line-image:not(.class-alert) .attribute-image'),
            wrap_height=item.position().top,
            wrap_width=this.width+10,
            cont_css={
            'paddingLeft':this.width+10+'px',
            'position':'relative',
            'width':(item.parents('.line-image:not(.class-alert)').width()-(this.width+10))+'px'
            };
        if(item.parents('.module-view-search').length){
            cont_css.width='auto';
        }
        cont.css(cont_css);
        var item=$(this),
            wrap_height=item.parents('.line-image').find('h1,h2').outerHeight(true)
            wrap.css({
                'marginTop':0-wrap_height+'px',
                'marginLeft':0-wrap_width+'px',
                });
    });
*/

    // nav dropdowns
    $("#global-menubar ul.menu > li").each(function() {
        if ($(this).find("ul").length > 0){
            var obj=$(this),
            ul=obj.find("ul");
            // $("<span>").text("+").appendTo(obj.children(":first"));
            obj.hover(function(){
                if($.browser.msie){
                    ul.stop(true,true).show();
                }else{
                    ul.stop(true,true).slideDown(0);
                }
            },function(){
                if($.browser.msie){
                    ul.stop(true,true).hide();
                }else{
                    ul.stop(true,true).slideUp(0);
                }
            });
        }
    });


    $('#comments a.button').click(function(){
        if($.browser.version == 7.0){
            $('#comments form').show().css({'height':'420px'});
            return false;
        }else{
            $('#comments form').slideDown().css({'height':'420px'});
            return false;
        }
    });

    $("ul.tabs").tabs('div.panes > div');

    if ($('#site-columns .horizontal .scrollable').length){
        $('#site-columns .horizontal .scrollable').siblings('.browse').css({'display':'inline-block'}).end().scrollable({
            'circular':true
        }).navigator().autoscroll({
            'interval':10000
        });
        // randomize homepage featured articles
        var scrollabled = $('#site-columns .horizontal .scrollable').data('scrollable');
        scrollabled.seekTo(Math.floor(Math.random() * scrollabled.getSize()));
    };

    if ($('#site-columns .vertical .scrollable').length){
        $('#site-columns .vertical .scrollable').siblings('.browse').css({'display':'block'}).end().scrollable({
            'circular':true,
            'speed':1000,
            'vertical':true
        })/*.autoscroll({
            'interval':1,
        })*/;
    };

    if ($('#event-scrollable .scrollable').length){
        $('#event-scrollable .scrollable').siblings('.browse').css({
            'display':'block',
            'position':'absolute'
            }).end().scrollable({
                'speed':1000
            });
    };

    $('footer .scrollable').each(function(){
        var scrollable=$(this);
        scrollable.find('.items>div').each(function(){
            var item=$(this);
            item.data('month',item.attr('title')).removeAttr('title');
        }).end().siblings('.browse').css({'display':'inline-block'}).end().scrollable({
            'circular':true,
            'keyboard':false,
            'onSeek':function(){
                var item=this.getItems().eq(this.getIndex());
                $('footer .custom-tag-scrollable .display').text(item.data('month'));
            }
        });
    });

    $('.class-alert .close').css({'cursor':'pointer','display':'block'}).click(function(){
        $(this).parents('.class-alert').slideUp();
    });

    var overlayid='site-overlay';
    $('<div class="module overlay" id="'+overlayid+'"></div>').prependTo('body');
    $('.popup').each(function(){
        var obj=$(this);
        var href = obj.attr('href');
        obj.attr('href','/layout/set/overlay'+href).attr('rel','#'+overlayid);
    }).overlay({
        'target':'#'+overlayid,
        'expose':'#4B83A9',
        'effect':'apple',
        'fixed':false,
        'onBeforeLoad':function(e){
            $('body>img').remove();
            var api=this;
            this.getOverlay().css({
                'width':'765px',
                'height':'320px'
            }).append('<div class="close"></div><iframe frameborder="0" src="'+this.getTrigger().attr('href')+'" width="765px" height="320px"></iframe>')
            .find('.close').click(function(){
                api.close();
            });

        },
        'onLoad':function(){
            $('body>img').remove();
            this.getOverlay().css({
                'zIndex':100000,
                'margin':'0px auto'
            });
        },
        'onClose':function(){
            this.getOverlay().empty();
        }
    });

    if ($.browser.msie && parseInt($.browser.version) == 7) {
    } else {
        $('div.attribute-image.article_overlay a').each(function(){
            var obj=$(this),
            img=new Image(),
            href = obj.attr('href');
            obj.data('overlay.image',$(img).attr('src', href));
        }).overlay({
            'target':'#'+overlayid,
            'expose':'#4B83A9',
            'effect':'apple',
            'fixed':false,
            'onBeforeLoad':function(e){
                $('body>img').remove();
                var api=this,
                    img = this.getTrigger().data('overlay.image').get(0);
                this.getOverlay().css({'width':img.width,'height':img.height}).append('<div class="close"></div>').append(img)
                .find('.close').click(function(){
                    api.close();
                });

            },
            'onLoad':function(){
                $('body>img').remove();
                this.getOverlay().css({
                    'zIndex':100000,
                    'margin':'0px auto'
                });
            },
            'onClose':function(){
                this.getOverlay().empty();
            }
        });
    }

});

jQuery(document).ready(function($) {
    docolheights();
    tabs = $(".homepage .custom-tag-tabbox .tabs a");
    if (tabs.length) {
        var rand = Math.floor(Math.random()*(tabs.length));
        $(".homepage .custom-tag-tabbox .tabs a").eq(rand).click();
    }
});

function docolheights() {
//  equal_col_height($('.top_left_zone, .top_center_zone, .top_right_zone'));
//  equal_col_height($('.bottom_left_zone, .bottom_center_zone, .bottom_right_zone'));

    $('.realmedia_TopRight img').parents('header').find('.shrinkwrap').css({'width':'155px', 'line-height' : '1.2em', 'margin-top' : '-6px'})
}

function utf8_encode ( argString ) {
    // http://kevin.vanzonneveld.net
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: sowberry
    // +    tweaked by: Jack
    // +   bugfixed by: Onno Marsman
    // +   improved by: Yves Sucaet
    // +   bugfixed by: Onno Marsman
    // +   bugfixed by: Ulrich
    // *     example 1: utf8_encode('Kevin van Zonneveld');
    // *     returns 1: 'Kevin van Zonneveld'

    var string = (argString+''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");

    var utftext = "",
        start, end,
        stringl = 0;

    start = end = 0;
    stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;

        if (c1 < 128) {
            end++;
        }
        else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
        }
        else {
            enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);
            }
            utftext += enc;
            start = end = n+1;
        }
    }

    if (end > start) {
        utftext += string.slice(start, stringl);
    }

    return utftext;
}

function utf8_decode ( str_data ) {
    // http://kevin.vanzonneveld.net
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +      input by: Aman Gupta
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Norman "zEh" Fuchs
    // +   bugfixed by: hitwork
    // +   bugfixed by: Onno Marsman
    // +      input by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // *     example 1: utf8_decode('Kevin van Zonneveld');
    // *     returns 1: 'Kevin van Zonneveld'

    var tmp_arr = [], i = 0, ac = 0, c1 = 0, c2 = 0, c3 = 0;

    str_data += '';

    while ( i < str_data.length ) {
        c1 = str_data.charCodeAt(i);
        if (c1 < 128) {
            tmp_arr[ac++] = String.fromCharCode(c1);
            i++;
        }
        else if (c1 > 191 && c1 < 224) {
            c2 = str_data.charCodeAt(i + 1);
            tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = str_data.charCodeAt(i + 1);
            c3 = str_data.charCodeAt(i + 2);
            tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }
    }

    return tmp_arr.join('');
}

function base64_encode (data) {
    // http://kevin.vanzonneveld.net
    // +   original by: Tyler Akins (http://rumkin.com)
    // +   improved by: Bayron Guevara
    // +   improved by: Thunder.m
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Pellentesque Malesuada
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // -    depends on: utf8_encode
    // *     example 1: base64_encode('Kevin van Zonneveld');
    // *     returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='

    // mozilla has this native
    // - but breaks in 2.0.0.12!
    //if (typeof this.window['atob'] == 'function') {
    //    return atob(data);
    //}

    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, ac = 0, enc="", tmp_arr = [];

    if (!data) {
        return data;
    }

    data = this.utf8_encode(data+'');

    do { // pack three octets into four hexets
        o1 = data.charCodeAt(i++);
        o2 = data.charCodeAt(i++);
        o3 = data.charCodeAt(i++);

        bits = o1<<16 | o2<<8 | o3;

        h1 = bits>>18 & 0x3f;
        h2 = bits>>12 & 0x3f;
        h3 = bits>>6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);

    enc = tmp_arr.join('');

    switch (data.length % 3) {
        case 1:
            enc = enc.slice(0, -2) + '==';
        break;
        case 2:
            enc = enc.slice(0, -1) + '=';
        break;
    }

    return enc;
}

function base64_decode (data) {
    // http://kevin.vanzonneveld.net
    // +   original by: Tyler Akins (http://rumkin.com)
    // +   improved by: Thunder.m
    // +      input by: Aman Gupta
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // +   bugfixed by: Pellentesque Malesuada
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // -    depends on: utf8_decode
    // *     example 1: base64_decode('S2V2aW4gdmFuIFpvbm5ldmVsZA==');
    // *     returns 1: 'Kevin van Zonneveld'

    // mozilla has this native
    // - but breaks in 2.0.0.12!
    //if (typeof this.window['btoa'] == 'function') {
    //    return btoa(data);
    //}

    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, ac = 0, dec = "", tmp_arr = [];

    if (!data) {
        return data;
    }

    data += '';

    do {  // unpack four hexets into three octets using index points in b64
        h1 = b64.indexOf(data.charAt(i++));
        h2 = b64.indexOf(data.charAt(i++));
        h3 = b64.indexOf(data.charAt(i++));
        h4 = b64.indexOf(data.charAt(i++));

        bits = h1<<18 | h2<<12 | h3<<6 | h4;

        o1 = bits>>16 & 0xff;
        o2 = bits>>8 & 0xff;
        o3 = bits & 0xff;

        if (h3 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1);
        } else if (h4 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1, o2);
        } else {
            tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
        }
    } while (i < data.length);

    dec = tmp_arr.join('');
    dec = this.utf8_decode(dec);

    return dec;
}

function equal_col_height(group){
    group.css('height', 'auto');
    var $tallest=0;
    group.each(function(){
        var obj=$(this);
        if(obj.height()>$tallest){
            $tallest=obj.height();
        }
    }).height($tallest);
}


// Touch Menu Handeler
function isTouchDevice(){
    return typeof window.ontouchstart !== 'undefined';
}

jQuery(document).ready(function(){
    /* If mobile browser, prevent click on parent nav item from redirecting to URL */
    if(isTouchDevice()) {
        // 1st click, add "clicked" class, preventing the location change. 2nd click will go through.
        jQuery("#global-menubar > ul > li > linked").click(function(event) {
            // Perform a reset - Remove the "clicked" class on all other menu items
            jQuery("#global-menubar > ul > li > linked").not(this).removeClass("clicked");
            jQuery(this).toggleClass("clicked");
            if (jQuery(this).hasClass("clicked")) {
                event.preventDefault();
            }
        });
    }

    // Give active facets and acceptable css hook
    $("#facet-list li ul li").has('strong').addClass('active');

});