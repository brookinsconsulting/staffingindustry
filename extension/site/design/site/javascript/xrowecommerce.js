$(function() {
	if ($('#country').length) {
		updateSubdivisions($('#country'));
	}

	if ($('#shipping-checkbox').length) {
		if ($('#s_country').length) {
			updateSubdivisions($('#s_country'));
		}

		$('#country, #s_country').change(function() {
			updateSubdivisions($(this));
		});

		$('#shipping-checkbox')
			.change(changeShipping)
			.trigger('change');
	}
});

function updateSubdivisions(country_node) {
	var country = country_node.find(':selected').val();

	$('#s_state, #state').attr('disabled', 'disabled');

	$.ajax({
		url: '/ezjscore/call/',
		type: 'POST',
		async: false, // necessary to ensure that s_state gets updated after this function is finished in changeShipping()
		data: {
			ezjscServer_function_arguments: 'xrowecommerce::getSubdivisions::' + country
		},
		dataType: 'json',
		success: function(data) {
			var states = data.content;

			if(country_node.is('#country')) {
				var subdivision_node = $('#state');
			} else {
				var subdivision_node = $('#s_state');
			}

			subdivision_node.find('option').remove();

			var node = '<option>&nbsp;</option>';
			subdivision_node.append(node);

			for (i in states ) 
			{
				var node = '<option value="' + i + '">' + states[i] + '</option>';
				subdivision_node.append(node);
			}

			$('#s_state').removeAttr('disabled');
			$('#state').removeAttr('disabled');
		}
	});
}


function changeShipping() {
	if ($('#shipping-checkbox').is(':checked')) {
		$('#shippinginfo').hide();
	} else {
		$('#shippinginfo').show();

		var inputs = [
			'company_name',
			'company_additional',
			'first_name',
			'mi',
			'last_name',
			'address1',
			'address2',
			'city',
			'zip',
			'country',
			'state',
			'phone',
			'fax',
			'email'
		];

		$.each(inputs, function(i, input) {
			var billing_value = $('#' + input).val(),
			    shipping_input = $('#s_' + input);

			if (billing_value != '') {
				shipping_input.val(billing_value);
				if (input == 'country') {
					updateSubdivisions(shipping_input);
				}
			}
		});
	}
};

function toggleCOS() {
	$('#cos-content').toggle();
};

function updateShipping() {
	// we don't use this feature
	return false;
}

/* don't think we need this, but here it is
function ShowHide(id)
{
	$('#' + id).toggle();
}
*/
