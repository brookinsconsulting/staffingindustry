{* Folder - Embed *}
<div class="content-view-embed class-folder">
	<h2><a href={$object.main_node|sitelink()}>{$object.name|wash()}</a></h2>
	<div class="attribute-short">
		{attribute_view_gui attribute=$object.main_node.data_map.short_description}
	</div>
</div>
{*
{def $show_image=and(cond(is_set($hide_image),not($hide_image),true()), $node.data_map.image.has_content)}
<div class="content-view-embed class-article{if $show_image} line-image{/if}">
	<h1><a href={$node|sitelink()}>{if ne($node.data_map.type.data_text,'')}<span class="parent">{attribute_view_gui attribute=$node.data_map.type} -</span> {/if}{$node.name|wash()}</a></h1>
	{if $show_image}<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='articlethumbnail'}</div>{/if}
	{attribute_view_gui attribute=$node.data_map.intro htmlshorten=400}
	{if $node.data_map.tags.has_content}<div class="tags">Tags: {attribute_view_gui attribute=$node.data_map.tags}</div>{/if}
	<div class="author">Analyst: {$node.object.owner.name|wash()}</div>
</div>
*}