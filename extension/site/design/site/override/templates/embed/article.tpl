{* Article - Embed View *}
{def $show_image=and(cond(is_set($hide_image),not($hide_image),true()), or($node.data_map.image.has_content, $node.data_map.ml_image.has_content))}
<div class="content-view-embed class-article{if $show_image} line-image{/if}">
{if $show_image}
	{set-block variable='left_column'}
		<div class="attribute-image">
			{if $node.data_map.ml_image.content}
				{attribute_view_gui attribute=$node.data_map.ml_image.content.data_map.image href=$node|sitelink() image_class='articlethumbnail'}
			{elseif $node.data_map.image.content}
				{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='articlethumbnail'}
			{/if}
		</div>
	{/set-block}
{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$left_column class='articlethumbnail'}
{/if}

{set-block variable='right_column'}
	<h1><a href={$node|sitelink()}>{if ne($node.data_map.type.data_text,'')}<span class="parent">{attribute_view_gui attribute=$node.data_map.type} -</span> {/if}{$node.data_map.title.data_text|wash()}</a></h1>
	{attribute_view_gui attribute=$node.data_map.intro htmlshorten=400}
	{if $node.data_map.tags.has_content}<div class="tags">Tags: {attribute_view_gui attribute=$node.data_map.tags}</div>{/if}
		{def $analyst_list=cond($node.data_map.analyst.has_content,$node.data_map.analyst.content.relation_list,false())}
		{if count($analyst_list)}
			{def $analyst_node=false()
				 $analyst_link=false()
				 $recent_list=false()
				 $recent1=false()
				 $recent2=false()
			}
			{if is_array($analyst_list)}
			{def $ala=$analyst_list}
				{foreach $ala as $key=>$item}
				{if $item}
					{set $item=fetch('content','object',hash('object_id',$item.contentobject_id))
						 $analyst_list=cond(eq($key,0),array($item),$analyst_list|append($item))
					}
				{/if}
				{/foreach}
			{else}
			{set $analyst_list=array()}
			{/if}
		{/if}
		{if $node.data_map.display_author_email.data_int}
			{def $author_list=array()}
			{foreach $node.data_map.authors.content.author_list as $author}
				{if ne($author['id'],10)}{set $author_list=$author_list|append($author)}{/if}
			{/foreach}
		{/if}

		{if and($analyst_list,count($analyst_list))}<div class="author">Analyst: {foreach $analyst_list as $analyst}<a title="<h2><a href='{$analyst|sitelink(no)}'>{$analyst.name|wash()}</a><small>{$analyst.data_map.title.content|wash()}</small></h2>
{if $analyst.data_map.image.has_content}<div class='attribute-image'><a href='{$analyst|sitelink('no')}'><img src='{$analyst.data_map.image.content[product_tiny].url|ezroot('no')}' width='{$analyst.data_map.image.content[product_tiny].width}' height='{$analyst.data_map.image.content[product_tiny].height}' /></a></div>{/if}
{set-block scope=relative variable=bio}{attribute_view_gui attribute=$analyst.data_map.bio htmlshorten=120}{/set-block}{$bio|htmlspecialchars()}
<p class='see-articles'><a href='{$analyst|sitelink(no)}'>See Recent Articles</a></p>" href={$analyst|sitelink()}>{$analyst.name|wash()}</a>{delimiter} {/delimiter}{/foreach}</div>{/if}
		{*if count($author_list)}<div class="author">Guest Author: {foreach $author_list as $author}<span>{$author.name|wash()}</span>{delimiter} {/delimiter}{/foreach}</div>{/if*}
		{if $node.data_map.display_author_email.data_int}
			{if count($author_list)}
				<div class="author">
				<span class="author_label">Guest Author{if gt(count($author_list),1)}s{/if}: </span>
				<div class="authors">
					{foreach $author_list as $author}
						<span>{$author.name|wash()}{if $node.data_map.display_author_email.data_int} {$author.email}{/if}</span>{delimiter}<br />{/delimiter}
					{/foreach}
				</div>
				</div>
			{/if}
		{/if}

{/set-block}
{if $show_image}
	{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$right_column class='r-articlethumbnail' position='right'}
{else}
	{$right_column}
{/if}
</div>
