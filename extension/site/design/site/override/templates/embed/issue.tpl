{* Issue - Embed View *}
{def $show_image=and(cond(is_set($hide_image),not($hide_image),true()), or($node.data_map.image.has_content,$node.data_map.ml_image.has_content))
	 $date_ts=$node.data_map.publish_date.content.timestamp
	 $date=cond($node.data_map.date.has_content,$node.data_map.date.content|explode(' '),false())
}
<div class="content-view-embed class-issue{if $show_image} line-image{/if}">
	{if $show_image}
		{set-block variable='left_column'}
			<div class="attribute-image">{attribute_view_gui attribute=cond($node.data_map.ml_image.has_content,$node.data_map.ml_image.content.data_map.image,$node.data_map.image) href=$node|sitelink() image_class='small_embed'}</div>
		{/set-block}
	{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$left_column class='small'}
	{/if}
	{set-block variable='right_column'}
		{*	<h3 class="date">{$date_ts|datetime('custom','%F')} <small>{$date_ts|datetime('custom','%Y')}</small></h3>*}
		{if $date}<h3 class="date">{$date[0]} <small>{$date[1]} {$date[2]}</small></h3>{/if}
		<h3><a href={$node|sitelink()}>{$node.name|wash()}</a></h3>
		{attribute_view_gui attribute=$node.data_map.short_description htmlshorten=180}
	{/set-block}
	{if $show_image}
		{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$right_column class='r-small' position='right'}
	{else}
		{$right_column}
	{/if}
	<a class="button" href={$node|sitelink()}>Read This Issue Online</a>
</div>