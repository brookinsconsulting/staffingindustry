{if and(is_set(ezgetvars()['SearchText'])|not, is_set(ezgetvars()['SubTreeArray'])|not)}<!-- Topic - Embed /extension/site/design/site/override/templates/embed/topic.tpl -->

{def $kids_and_rels = $object.main_node|kids_and_rels(hash('limit', 3, 'sort_by', array('attribute', false(), '191'), 'attribute_identifier', array(673), 'main_node_only', true(), 'class_filter_type', 'include', 'class_filter_array', array('article')))}

<div class="content-view-embed class-topic">
	<div class='topic_embed_header'>
		<div class="topic_embed_image">
			{attribute_view_gui attribute=$object.data_map.image image_class='original'}
			<h2><a href={$object.main_node|sitelink()}>{$object.name|wash()}</a></h2>
		</div>
		
		<div class="items{if $dataNodes_us[1]|contains('site')} active{/if}">
			{foreach $kids_and_rels as $key=>$subitem}
				{if $key|eq(0)}
					<div class='top_topic_match_img'>
						{if $subitem.data_map.ml_image.content}
							{attribute_view_gui attribute=$subitem.data_map.ml_image.content.data_map.image href=$subitem|sitelink() image_class='article_small'}
						{elseif $subitem.data_map.image.content}
							{attribute_view_gui attribute=$subitem.data_map.image href=$subitem|sitelink() image_class='article_small'}
						{/if}
					</div>
				{/if}
				<h2 class="item_count_{$key}"><a class='embed_topic_line_article' href={$subitem|sitelink}>{if $subitem.data_map.headline.has_content}{$subitem.data_map.headline.content|wash()}{else}{$subitem.data_map.title.content|html_shorten(36)|wash()}{/if}</a></h2>

			{/foreach}
		</div>
		
	</div>


</div>
{/if}