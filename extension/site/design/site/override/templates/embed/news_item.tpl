<li class="rss-list-item">
	<a href={$item|sitelink()|fix_sitelink_siteaccess()} title="{$item.data_map.name.content|html_shorten(1000)}">{$item.data_map.name.content|html_shorten(1000)}</a>
	<div class="rss-list-item-description">{$item.data_map.description.content.output.output_text|html_shorten(150)}</div>
</li>
<br>
