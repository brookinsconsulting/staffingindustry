{* Conference - Embed *}
<div class="content-view-embed class-conference">
	<div class="attribute-image">{attribute_view_gui attribute=$object.data_map.image href=$object|sitelink() image_class='newsletter'}</div>
	<h3 class="class-conference-title"><a{if $classification|contains('newpage')} target='_blank'{/if} href={$object|sitelink()}>{$object.name|wash()}<br/>
	{attribute_view_gui attribute=$object.data_map.date}</a></h3>
	<span class="location">{attribute_view_gui attribute=$object.data_map.location}</span>
	<div class="attribute-short">
		{attribute_view_gui attribute=$object.data_map.short_description}
	</div>
</div>
