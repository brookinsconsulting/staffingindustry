{if eq($classification, 'featured')}
<header>
	<h2>Featured Webinar</h2>
</header>
{/if}
	<h3>
	{if $object.data_map.image.has_content}
			{attribute_view_gui image_class=product_medium attribute=$object.data_map.image}
	{/if}
	<a href="{$object|sitelink('no')}">{$object.name}</a>
</h3>
{if eq($classification, 'featured')}
	{attribute_view_gui attribute=$object.data_map.short_description}
	<p><a href="{$object|sitelink('no')}">Read More</a></p>
{else} 
{attribute_view_gui attribute=$object.data_map.short_description htmlshorten=array(110,concat(' <a href="',$object|sitelink('no'),'">Read More</a>'))}
{/if} 
