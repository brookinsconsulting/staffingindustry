{* Large File - List embed view *}

{def $large_file_attribute_content = $object.data_map.aws_s3_upload_client.content
     $linkreader = filelinkreader( $large_file_attribute_content )
     $path_array=$large_file_attribute_content|explode( '/' )
     $array_length=$path_array|count
     $file_path=$path_array|remove( $array_length|sub( 1 ) )|implode( '/' )
     $file=$path_array|extract( $array_length|sub( 1 ), 1 )[0]
     $url = concat( 'http://', ezini( 'S3Settings', 'Bucket', 's3.ini' ),'.s3.amazonaws.com/', $file_path, '/', $file|urlencode )
     $mimetype = $linkreader.mimetype
     $icon_size='small'
	 $icon_title=$file.content.mime_type
     $filesize = $object.data_map.aws_s3_upload_client.data_int}
     
<div class="content-body large-file attribute-{$mimetype}">
	{$mimetype|mimetype_icon($icon_size,$icon_title)}<a href="{$url}">{$file|wash("xhtml")}</a> {$filesize|si(byte)}
</div>