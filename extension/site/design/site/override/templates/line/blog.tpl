{* Blog - Line view *}
{def $page_limit = 10
	 $blogs_count = 0
}
<div class="content-view-line">
	<div class="class-blog">
	<h2><a href={$node|sitelink()} title="{$node.name|wash()}">{$node.name|wash()}</a></h2>
	{*
	<div class="attribute-description">
		{attribute_view_gui attribute=$node.data_map.description}
	</div>
	
	{set $blogs_count = fetch('content','list_count',hash('parent_node_id',$node.node_id))}
	{if $blogs_count}
		{foreach fetch('content','list',hash('parent_node_id',$node.node_id,
												'classid','blog_post',
												'sort_by',array('attribute',false(),'blog_post/publication_date'),
												'limit',$page_limit)) as $key => $blog}

			<section class="content-view-list class-blog-post">
				<a href={$node|sitelink()} title="{$blog.data_map.title.content|wash()}"><h3>{$blog.data_map.title.content|wash()}</h3></a>
			<span class="date">{$blog.data_map.publication_date.content.timestamp|datetime('custom','%F %j, %Y')}</span>
			<div class="attribute-body">
			{attribute_view_gui attribute=$blog.data_map.body htmlshorten=array(180,concat('... <a href="',$node|sitelink('no'),'">More</a>'))}
			</div>
			</section>
		{/foreach}
	{/if}
	*}
	</div>
</div>