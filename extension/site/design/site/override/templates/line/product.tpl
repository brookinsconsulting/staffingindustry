{* Product - Line view *}
{def $show_image=and(cond(is_set($hide_image),not($hide_image),true()), or($node.data_map.image.has_content, $node.data_map.ml_image.has_content))}
<div class="content-view-line class-product">
	<h2><a href={$node|sitelink()}>{$node.name|wash()}</a></h2>
    {if $show_image}
        <div class="attribute-image">
			{if $node.data_map.ml_image.content}
				<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.ml_image.content.data_map.image href=$node|sitelink() image_class='small'}</div>
			{elseif $node.data_map.image.content}
				<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='small'}</div>
			{/if}
        </div>
    {/if}
    {if gt($node.object.data_map.price.data_float, 0)}
    	<div class="attribute-price">
    		{attribute_view_gui attribute=$node.object.data_map.price}
		</div>
    {/if}
    {attribute_view_gui attribute=$node.object.data_map.short_description}    
</div>
