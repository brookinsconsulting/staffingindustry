{* User - Line View *}
<div class="content-view-line class-user">
<h2><a href={$node|sitelink()}>{$node.name|wash()}</a><small>{$node.data_map.title.content|wash()}</small></h2>
{attribute_view_gui attribute=$node.data_map.bio htmlshorten=120}
<p><a href={$node|sitelink()}>See Recent Articles</a></p>
</div>