<div class="content-view-line class-news-item">
	<h2><a href={$node|sitelink()}>{$node.name|wash()}</a></h2>
	<span class="date">{$node.data_map.date.content.timestamp|datetime('newsdate')}</span>
	<p>{$node.data_map.description.content.output.output_text|html_shorten( 300 )}</p>
</div>