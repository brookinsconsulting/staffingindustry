{* Event Calendar - Line view *}
<div class="content-view-line class-event-calendar">
	<h2><a href={$node|sitelink()}>{$node.data_map.title.content|wash()}</a></h2>
</div>