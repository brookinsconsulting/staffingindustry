{* Folder - Line view *}
{def $show_image=and(cond(is_set($hide_image),not($hide_image),true()), or($node.data_map.image.has_content,$node.data_map.ml_image.has_content))}
<div class="content-view-line class-topic{if $show_image} line-image{/if}">
{if $show_image}
	{set-block variable='left_column'}
	{if $node.data_map.ml_image.has_content}
		<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.ml_image.content.data_map.image href=$node|sitelink() image_class='small'}</div>
	{else}
		<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='small'}</div>
	{/if}
	{/set-block}
{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$left_column class='small'}
{/if}

{set-block variable='right_column'}
	<h2><a href={$node|sitelink()}>{$node.name|wash()}</a></h2>
	{if $node.data_map.short_description.has_content}
		<div class="attribute-short">{attribute_view_gui attribute=$node.data_map.short_description htmlshorten=first_set($htmlshorten,false())}</div>
	{/if}
{/set-block}
{if $show_image}
	{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$right_column class='r-small' position='right'}
{else}
	{$right_column}
{/if}
</div>