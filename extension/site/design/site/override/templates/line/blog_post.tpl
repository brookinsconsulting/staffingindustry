{* Blog post - Line view *}
<section class="content-view-line class-blog-post">
	<h2><a href={$node|sitelink()} title="{$node.data_map.title.content|wash()}">{$node.data_map.title.content|wash()}</a></h2>

<div class="attribute-byline">
<h3 class="date">{$node.data_map.publication_date.content.timestamp|datetime('newsdate')} 
	<span class="author">
		by: 
		{if $node.data_map.author.content.is_empty} 
			{$node.object.owner.name}
		{else}
			{$node.data_map.author.content.author_list.0.name|wash}
		{/if}
	</span>
</h3>
{if $node.data_map.tags.has_content}
<p class="tags"> {"Tags:"|i18n("design/ezwebin/line/blog_post")}{foreach $node.data_map.tags.content.keywords as $keyword}
<a href={concat( $node.parent.url_alias, "/(tag)/", $keyword|rawurlencode )|sitelink()} title="{$keyword}">{$keyword}</a>{delimiter}, {/delimiter}
{/foreach}
</p>
{/if}
</div>


<div class="attribute-body">
{attribute_view_gui attribute=$node.data_map.body htmlshorten=array(400,concat('... <a href="',$node|sitelink('no'),'">More</a>'))}
</div>
</section>