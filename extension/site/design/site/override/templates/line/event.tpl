{* Event - Line view *}
{def $show_image=and(cond(is_set($hide_image),not($hide_image),true()), or($node.data_map.image.has_content, $node.data_map.ml_image.has_content))
	 $from_ts=$node.data_map.from_time.content.timestamp
	 $to_ts=cond($node.data_map.to_time.has_content,$node.data_map.to_time.content.timestamp,false())
}
<div class="content-view-line class-event{if $show_image} line-image{/if}{if and($node.data_map.to_time.has_content,gt(currentdate(),$node.data_map.to_time.content.timestamp))} ezagenda_event_old{/if}">
	{if $show_image}
		{set-block variable='left_column'}
			{*<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='message'}</div>*}
			{if $node.data_map.ml_image.content}
				<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.ml_image.content.data_map.image href=$node|sitelink() image_class='message'}</div>
			{elseif $node.data_map.image.content}
				<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='message'}</div>
			{/if}			
		{/set-block}
	{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$left_column class='message'}
	{/if}
	{set-block variable='right_column'}
		<h2><a href={$node|sitelink()}>{$node.name|wash()}</a></h2>
		{$node.object.initial_language_id|debug('rw lang')}
		{if $node.object.initial_language_id|ne(2)} {* not eng-US, show european date *}
			<h3 class="date">{$from_ts|datetime(custom,"%j")}{if $to_ts}-{if ne($from_ts|datetime(custom,"%M"),$to_ts|datetime(custom,"%M"))}{$to_ts|datetime(custom,"%M")} {/if}{$to_ts|datetime(custom,"%j")}{/if} {$from_ts|datetime(custom,"%M")}, {$from_ts|datetime(custom,"%Y")}</h3>
		{else}
			<h3 class="date">{$from_ts|datetime(custom,"%M")} {$from_ts|datetime(custom,"%j")}{if $to_ts}-{if ne($from_ts|datetime(custom,"%M"),$to_ts|datetime(custom,"%M"))}{$to_ts|datetime(custom,"%M")} {/if}{$to_ts|datetime(custom,"%j")}{/if}, {$from_ts|datetime(custom,"%Y")}</h3>
		{/if}
		{if $node.data_map.category.has_content}
			<span class="keyword">{"Category"|i18n("design/ezwebin/line/event")}:{attribute_view_gui attribute=$node.data_map.category}</span>
		{/if}
		{if $node.data_map.text.has_content}{attribute_view_gui attribute=$node.data_map.text htmlshorten=first_set($htmlshorten,150)}{/if}
	{/set-block}
	{if $show_image}
		{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$right_column position='right'}
	{else}
		{$right_column}
	{/if}
</div>
