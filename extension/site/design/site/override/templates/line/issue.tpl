{* Issue - Line view *}
{def 	$show_image=and(cond(is_set($hide_image),not($hide_image),true()), $node.data_map.image.has_content)
		$date=cond($node.data_map.date.has_content,$node.data_map.date.content|explode(' '),false())
}
<div class="content-view-line class-issue{if $show_image} line-image{/if}">
{if $show_image}
	{set-block variable='left_column'}
		<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='small'}</div>
	{/set-block}
{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$left_column class='small'}
{/if}

{set-block variable='right_column'}
	<h2><a href={$node|sitelink()}>{$node.data_map.name.content|wash()}</a></h2>
	{if $date}<h3 class="date format">{$date[0]} <small>{$date[1]} {$date[2]}</small></h3>{/if}
	{if $node.data_map.short_description.has_content}
		{attribute_view_gui attribute=$node.data_map.short_description htmlshorten=first_set($htmlshorten,array(200,concat('... <a href="',$node|sitelink('no'),'">More</a>')))}
	{/if}
{/set-block}
{if $show_image}
	{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$right_column class='r-small' position='right'}
{else}
	{$right_column}
{/if}
</div>