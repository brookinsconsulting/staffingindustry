{def $is_featured = $node.data_map.featured.data_int
	 $image = cond(or($node.data_map.image.content.is_valid, $node.data_map.ml_image.has_content), cond($node.data_map.image.content.is_valid, $node.data_map.image, $node.data_map.ml_image.content.data_map.image), false())
}
<div class="content-view-line class-conference{if $is_featured} featured{/if}">
{if $is_featured}
	<h2 class="class-conference-title"><a href={$node|sitelink()}{if $node.data_map.open_in_new_window.content} target="_blank"{/if}>{$node.name|wash()}</a></h2>
	<div>{attribute_view_gui attribute=$node.data_map.date} | {attribute_view_gui attribute=$node.data_map.location}</div>
	{attribute_view_gui attribute=$node.data_map.description}
{else}
	<div class="column attribute-image">
		{attribute_view_gui attribute=$image}
	</div>
	<div class="column">
		<h2 class="class-conference-title"><a href={$node|sitelink()}{if $node.data_map.open_in_new_window.content} target="_blank"{/if}>{$node.name|wash()}</a></h2>
		{attribute_view_gui attribute=$node.data_map.short_description}
	</div>
{/if}
</div>
