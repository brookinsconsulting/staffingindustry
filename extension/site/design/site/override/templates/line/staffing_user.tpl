{* User - Line View *}
<div class="content-view-line class-staffing-user">
<div class="attribute-image">
{if $node.data_map.image.has_content}
	{attribute_view_gui attribute=$node.data_map.image image_class='product_tiny'}
{else}
	{def $ml_image = fetch('content', 'object', hash('object_id', $node.data_map.ml_image.content.id))}
	{attribute_view_gui attribute=$ml_image.data_map.image image_class='product_tiny'}
{/if}
</div>
<h2><a href={$node|sitelink()}>{$node.data_map.first_name.content|wash()} {$node.data_map.last_name.content|wash()}</a></h2>
<h3>{$node.data_map.title.content|wash()}{if $node.data_map.company.has_content} - {$node.data_map.company.content|wash()}{/if}</h3>
<span>{$node.data_map.nationality.content|wash()}</span>
<div class="attribute-bio">{attribute_view_gui attribute=$node.data_map.bio htmlshorten=120}</div>
</div>