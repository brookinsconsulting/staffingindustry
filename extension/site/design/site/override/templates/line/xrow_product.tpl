{set-block scope=global variable=cache_ttl}0{/set-block}

{def $cur_locale = ezini('RegionalSettings', 'Locale')
	 $mask = $node.object.language_mask
	 $trans = fetch( 'content', 'translation_list' )
	 $good_locales = array()
	 $c_link = false()
	 $nid = $node.node_id
	 $trans_r=array()
	 $tmp = ''}
	
	{foreach $trans as $t}
		{set $tmp = cond($t.locale_code|eq('eng-GB'), concat($t.locale_code, '@euro'), $t.locale_code)
			 $trans_r = $trans_r|array_merge(hash($tmp, $t))}
	{/foreach}

	{if array(2,3,10,11,6,7)|contains($mask)}
		{set $good_locales = $good_locales|append("eng-US")}
		{set $good_locales_links = $good_locales_links|append(concat("Location: /site", $node.url_alias|ezroot(no)))}
	{/if}

	{if array(8,9,10,11,12,13)|contains($mask)}
		{set $good_locales = $good_locales|append("eng-GB@euro")}
		{set $good_locales_links = $good_locales_links|append(concat("Location: /eng", $node.url_alias|ezroot(no)))}
	{/if}

	{if array(4,5,6,7,12,13)|contains($mask)}
		{set $good_locales = $good_locales|append('eng-RW')}
		{set $good_locales_links = $good_locales_links|append(concat("Location: /row", $node.url_alias|ezroot(no)))}
	{/if}

	{$trans_r[$node.object.current_language].currency_short_name|set_currencypref}

{set-block scope=relative variable=langlinks}
{def $site_a_r=hash('USD', concat('row/', $node.url_alias), 'EUR', concat('eng/', $node.url_alias), 'USD', concat('site/', $node.url_alias))}
This product is also available in 
{foreach $node.data_map.price.content.currency_list as $k => $c}
	{if $c['locale']|eq('eng-GB@euro')}{continue}{/if}
	{if or($good_locales|contains($c['locale'])|not, $c.code|eq($trans_r[$node.object.current_language].currency_short_name))}{continue}{/if}
	{set $c_link = true()}
	<a href='/{$site_a_r[$k]}'>{$c['symbol']} ({$k})</a>, 
{/foreach}	
{/set-block}



{def $price_display=ezini( 'Settings', 'ShowPriceAs', 'xrowecommerce.ini' )
	 $partprice=0
}
<div class="content-view-line class-xrow-product productline">
<div class="product_title"><h2><a href="{$node.url_alias|ezurl('no')}" title="{$node.name|wash()}">{$node.name|wash()}</a></h2></div>
        {if $node.data_map.image.has_content}
    	<div class="productcategory-img">
        	<div class="productline-img">
	            {attribute_view_gui image_class=small attribute=$node.data_map.image href=$node.url_alias|ezurl()}
	        </div>
    	</div>
    	{/if}
		<div class="description-wrap">
			{if is_set($node.object.data_map.description)|not}<div class="attribute-short">{attribute_view_gui attribute=$node.object.data_map.short_description}</div>{/if}
			{if and($node.object.data_map.webinar.has_content, $node.object.data_map.end_date.has_content, lt($node.object.data_map.end_date.content.timestamp, currentdate()))}
				{include node=$node uri="design:parts/subscribe_notice.tpl"}
			{else}
				{if is_set($node.object.data_map.description)}<div class="attribute-long">{attribute_view_gui attribute=$node.object.data_map.description htmlshorten=array(800,concat('... <a href="',$node|sitelink('no'),'">Read More</a>'))}</div>{/if}
			{/if}
		</div>
{if or($node.data_map.webinar.has_content|not, and($node.data_map.end_date.content.timestamp|ge(currentdate()),$node.data_map.webinar.has_content))} {* only show details if webinar is not expired *}
{$site_a_r|debug}
{currencypref()|debug}

<div class="productwrapper float-break">
	{def $add_to_cart_siteaccess = $site_a_r[currencypref()]|explode('/')[0]}
	<form name="buy" id="buy_{$nid}"  class="{$node.data_map.price.value.currency}" method="post" action="/{$add_to_cart_siteaccess}/xrowecommerce/multiadd">
		<input type="hidden" name="ContentNodeID" value="{$node.node_id}" />
		<input type="hidden" name="ContentObjectID" value="{$node.object.id}" />
		<input type="hidden" name="ViewMode" value="full" />
		<div>
		{if
			and(
				$node.data_map.show_add_to_cart.content,
				or(	
					and(
						$node.data_map.end_date.content.timestamp|gt(currentdate()),
						$node.data_map.webinar.has_content
					),
					not($node.data_map.webinar.has_content)
				)
			)
		}
		{def $dropdown = $node.data_map.dropdown.content}
			{if $dropdown}
				<script>
				
				var select_html_{$nid} = '';

				$(function(){ldelim}
					// make the dropdown
					// select_html is built inline elsewhere
					$('#attribute-multi-options-select_{$nid}').html(select_html_{$nid});

					// fix hidden real attribute on change
					$('#attribute-multi-options-fake_{$nid}').change(function(){ldelim}
						var choice = $(this).find('option:selected').attr('class');
						$('#attribute-multi-options_{$nid} td.quantity input[type=text]').val('0');
						$('#attribute-multi-options_{$nid} td.quantity input[type=text]#' + choice + "_{$nid}").val('1');
					{rdelim});
				{rdelim});


				</script>
				<div id="attribute-multi-options-fake_{$nid}" class="attribute-multi-options">
					<select id="attribute-multi-options-select_{$nid}">
					</select>
				</div>
			{elseif $node.data_map.bogo.content}
				<script>
				{literal}
					$(function(){
				{/literal}
						// only three rows expected, so :last is the third (free) and :odd is the second (paid) (first is header)
						var paid = $('#attribute-multi-options_{$nid} tr:odd input[name$="[quantity]"]'),
						    free = $('#attribute-multi-options_{$nid} tr:last input[name$="[quantity]"]');

						// get the table set up 
						totalrow = '<tr class="bogo-total"><td colspan="2">Total</td><td align="right" id="bogo-total"></td><td></td>';
						$('#attribute-multi-options_{$nid} tbody').append(totalrow);
				{literal}
						free.css('background-color', '#ddd').focus(function(){
							$(this).blur();
						});

						// initialize
						bogoUpdate();

						paid.change(function(){
							bogoUpdate();
						});

						function bogoUpdate() {
							free.val(paid.val());
							$('#bogo-total').html(Number(paid.val()) + Number(free.val()));
						}
					});
				{/literal}
				</script>
			{/if}
			<table {if $dropdown}style="display:none" {/if}id="attribute-multi-options_{$nid}"  class="list" summary="This table contains information about the product, like image, product number, description and the form to orderthe product.">
				<tr>
				{* if $node.data_map.options.content.option_list|count|gt(0)}
					<th>{'Image'|i18n('extension/xrowecommerce')}</th>
				{/if}
					<th>{'Number'|i18n('extension/xrowecommerce')}</th> *}
				{if $node.data_map.options.content.option_list|count|gt(0)}
					<th>{'Item'|i18n('extension/xrowecommerce')}</th>
				{/if}
					<th>{'Description'|i18n('extension/xrowecommerce')}</th>
					<th class="quantity">{'Quantity'|i18n('extension/xrowecommerce')}</th>
					<th class="price">{'Price'|i18n('extension/xrowecommerce')}</th>
				</tr>
				{if $node.data_map.options.content.option_list|count|gt(0)}
				{section var=Options loop=$node.data_map.options.content.option_list}
				<tr>
					{*<td>
						{if $Options.item.image.current.data_map.image.content.is_valid}
						{attribute_view_gui image_class=galleryline attribute=$Options.item.image.current.data_map.image}
						{else}
						<div class="s_nopic"><img src={'shop/nopic_tiny.jpg'|ezimage()} alt="{'No image available'|i18n('extension/xrowecommerce')}" /></div>
						{/if}
					</td>
					<td>{$Options.item.value}</td> *}
					<td>{$Options.item.comment|wash()}</td>
					<td>{$Options.item.description|wash|nl2br}</td>
					<td align="right" class="quantity">
						<input type="hidden" name="AddToBasketList[{$Options.index}][object_id]" value="{$node.object.id}" />
						<input type="hidden" name="AddToBasketList[{$Options.index}][variations][{$node.data_map.options.id}]" value="{$Options.item.id}" />
						<input id="q{$Options.index}_{$nid}" type="text" name="AddToBasketList[{$Options.index}][quantity]" value="{if eq($Options.index,0)}1{else}0{/if}" />
					</td>
					<td class="price" align="right">
					{if $Options.multi_price}
						{def $price=get_multiprice( $node.data_map.price, $Options.multi_price, $price_display )}
						{$price|l10n( currency, $node.object.current_language )}
					{/if}
					</td>
					{if $dropdown}
					<script>
						select_html_{$nid} += '<option class="q{$Options.index}">{$Options.item.comment} - {$Options.item.description} - {$price|l10n( currency, $node.object.current_language )}</option>';
					</script>
					{/if}
					{undef $price}
				</tr>
				{/section}
				{else}
				<tr>
					<td>{$node.object.data_map.product_id.data_text}</td>
					<td>{attribute_view_gui attribute=$node.object.data_map.short_description}</td>
					<td align="right" class="quantity">
						<input type="hidden" name="AddToBasketList[{$Options.index}][object_id]" value="{$node.object.id}" />
						<input type="hidden" name="AddToBasketList[{$Options.index}][variations][{$node.data_map.options.id}]" value="{$Options.item.id}" />
						<input type="text" name="AddToBasketList[0][quantity]" value="1" />
					</td>
					<td align="right">{$node.data_map.price.content.$price_display|l10n( currency, $node.object.current_language )}</td>
				</tr>
				{/if}
			</table>
			<div class="block">
				<div class="right">
					{if count(fetch(shop, basket).items)|gt(0)}
						{if eq($node.data_map.price.value.currency, cartcurrency())}
							<input type="submit" class="button right-arrow" name="ActionAddToBasket" value="{"Add to Shopping Cart"|i18n("extension/xrowecommerce")}" />
						{else}
							<input disabled="disabled" type="button" class="button right-arrow" value="{"Currency incompatible with current basket."|i18n("extension/xrowecommerce")}" />
						{/if}
					{else}
						<input type="submit" class="button right-arrow" name="ActionAddToBasket" value="{"Add to Shopping Cart"|i18n("extension/xrowecommerce")}" />
					{/if}
				</div>
			</div>
		{/if}
		</div>

		{if eq(ezini( 'AutomaticDeliverySettings', 'AutomaticDelivery', 'automaticdelivery.ini' ), 'enabled' )}
		<div id="overlay-text_{$nid}">
		{def $user=fetch( 'user', 'current_user' )}
		{if and($node.data_map.no_auto_delivery.content|not(), $user.is_logged_in)}
			<p>{'Add your selections to'|i18n( 'extension/xrowecommerce')} <a id="show_auto_tip_{$nid}">{'Automatic Delivery'|i18n( 'extension/xrowecommerce')}</a>?</p>
			<input class="flat-right2 button" type="submit" onclick="$("#buy_{$nid}").attr('action', {"recurringorders/add"|ezurl(no)})'; $("#buy_{$nid}").submit(); return true;" name="ActionAddToRecurring" value="{"Add to Automatic Delivery"|i18n("extension/xrowecommerce")}" />
		{elseif $node.data_map.no_auto_delivery.content|not()}
			<p>{'This product is available for'|i18n( 'extension/xrowecommerce')} <a id="show_auto_tip_{$nid}">{'Automatic Delivery'|i18n( 'extension/xrowecommerce')}</a>. {'To add this product to your Automatic Delivery you have to'|i18n( 'extension/xrowecommerce')} <a href={'user/login'|ezurl}>{'login'|i18n( 'extension/xrowecommerce')}</a>.</p>
		{/if}
		</div>
		{/if}
	</form>
</div>

{/if}


{if $c_link}<p class='cur_list'>{$langlinks|preg_replace("/[\s,]*$/", "")}</p>{/if}
	
	
</div>
{false()|set_currencypref}
{undef $price_display $partprice}
