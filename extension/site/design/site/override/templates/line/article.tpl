{debug-accumulator id='article/line'}
{* Article - Line view *}
{def
	$show_image=and(cond(is_set($hide_image),not($hide_image),true()), or($node.data_map.image.has_content, $node.data_map.ml_image.has_content))
	$author_list=array()
}
<div class="content-view-line class-article{if $show_image} line-image{/if}">
{if $show_image}
	{set-block variable='left_column'}
{*
		<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='articlethumbnail'}</div>
*}
		{if $node.data_map.ml_image.content}
			<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.ml_image.content.data_map.image href=$node|sitelink() image_class='articlethumbnail'}</div>
		{elseif $node.data_map.image.content}
			<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='articlethumbnail'}</div>
		{/if}
	{/set-block}
{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$left_column class='articlethumbnail'}
{/if}

{set-block variable='right_column'}
	<h2><a href={$node|sitelink()}>{$node.data_map.title.content|wash()}</a></h2>
	<h3 class="date">
		{if $node.data_map.publish_date.content.timestamp}
			{$node.data_map.publish_date.content.timestamp|datetime('newsdate')}
		{else}
			{$node.object.published|datetime('newsdate')}
		{/if}
	</h3>
	{if not($node.data_map.intro.content.is_empty)}
		{attribute_view_gui attribute=$node.data_map.intro htmlshorten=first_set($htmlshorten,array(300,concat('... <a href="',$node|sitelink('no'),'">More</a>')))}
	{/if}

	{def $analyst_list=cond($node.data_map.analyst.has_content,$node.data_map.analyst.content.relation_list,false())}
	{if count($analyst_list)}
		{def $analyst_node=false()
			 $analyst_link=false()
			 $recent_list=false()
			 $recent1=false()
			 $recent2=false()
		}
		{if is_array($analyst_list)}
		{def $ala=$analyst_list}
			{foreach $ala as $key=>$item}
			{if $item}
				{set $item=fetch('content','object',hash('object_id',$item.contentobject_id))
					 $analyst_list=cond(eq($key,0),array($item),$analyst_list|append($item))
				}
			{/if}
			{/foreach}
		{else}
		{set $analyst_list=array()}
		{/if}
	{/if}
	{if $node.data_map.authors.has_content}
		{foreach $node.data_map.authors.content.author_list as $author}
			{if ne($author['id'],10)}{set $author_list=$author_list|append($author)}{/if}
		{/foreach}
	{/if}
	{if or(count($author_list),count($analyst_list),$node.data_map.tags.has_content)}
	<div class="attribute-byline">
		{if $node.data_map.tags.has_content}
			<div class="tags">Tags: 
				{foreach $node.data_map.tags.content.keywords as $keyword}
					{*<a href={concat('/',$node.data_map.tags.object.main_node.parent.url_alias,'/(tag)/',$keyword|urlencode())} title="{$keyword}">{$keyword}</a>{delimiter}, {/delimiter}*}
					<a href={concat('/content/search?SearchText=',$keyword|urlencode())} title="{$keyword}">{$keyword}</a>{delimiter}, {/delimiter}
				{/foreach}
			</div>
		{/if}
		{if and($analyst_list,count($analyst_list))}
			<div class="author">
				Analyst:
				{foreach $analyst_list as $analyst}
						{set-block variable=$a_title}
							{* this codes significantly increases the time taken to generate this template and doesn't seem to be used
							<h2>
								<a href='{$analyst|sitelink(no)}'>{$analyst.name|wash()}</a>
								<small>{$analyst.data_map.title.content|wash()}</small>
							</h2>
							{if $analyst.data_map.image.has_content}
								<div class='attribute-image'>
									<a href='{$analyst|sitelink('no')}'>
										<img
											src='{$analyst.data_map.image.content[product_tiny].url|ezroot('no')}'
											width='{$analyst.data_map.image.content[product_tiny].width}'
											height='{$analyst.data_map.image.content[product_tiny].height}'
										/>
									</a>
								</div>
							{/if}
							{attribute_view_gui attribute=$analyst.data_map.bio htmlshorten=120}
							<p class='see-articles'><a href='{$analyst|sitelink(no)}'>See Recent Articles</a></p>
							*}
							{$analyst.name}
						{/set-block}
						<a title="{$a_title|trim()|explode("\n")|implode('')|explode("\t")|implode('')|htmlspecialchars()}" href={$analyst|sitelink()}>{$analyst.name|wash()}</a>
						{delimiter}&nbsp;{/delimiter}
				{/foreach}
			</div>
		{/if}

			{if count($author_list)}
				<div class="author">
				<span class="author_label">Guest Author{if gt(count($author_list),1)}s{/if}: </span>
				<div class="authors">
					{foreach $author_list as $author}
						<span>{$author.name|wash()}{if $node.data_map.display_author_email.data_int} {$author.email}{/if}</span>{delimiter}<br />{/delimiter}
					{/foreach}
				</div>
				</div>
			{/if}

	</div>
{/if}
{/set-block}
{if $show_image}
	{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$right_column class='r-articlethumbnail' position='right'}
{else}
	{$right_column}
{/if}
</div>
{/debug-accumulator}
