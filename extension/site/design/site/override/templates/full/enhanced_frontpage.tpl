{* enhanced_frontpage full *}

{pagedata_merge(hash(
	'sidebar', false(),
	'extrainfo', false(),
	'bottomarea', false()
))}

{* top zones *}
{foreach array($node.data_map.top_left_zone, $node.data_map.top_center_zone, $node.data_map.top_right_zone) as $zone}
	<div class="column {$zone.contentclass_attribute_identifier}">
		{attribute_view_gui attribute=$zone}
	</div>
{/foreach}

{* middle zone *}
<div class="{$node.data_map.middle_zone.contentclass_attribute_identifier}">
	{attribute_view_gui attribute=$node.data_map.middle_zone}
</div>

{* bottom zones *}
{foreach array($node.data_map.bottom_left_zone, $node.data_map.bottom_center_zone, $node.data_map.bottom_right_zone) as $zone}
	<div class="column {$zone.contentclass_attribute_identifier}">
		{attribute_view_gui attribute=$zone}
	</div>
{/foreach}
