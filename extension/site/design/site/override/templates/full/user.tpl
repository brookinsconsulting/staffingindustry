{* User - Full View *}
<header>
	<hgroup>
		<h1>{$node.name|wash()}</h1>
		<h2>{$node.data_map.title.content|wash()}</h2>
	</hgroup>
</header>
{if or($node.data_map.image.has_content, $node.data_map.ml_image.has_content)}
	<div class="attribute-image analyst-image">
	{if $node.data_map.image.has_content}
		{attribute_view_gui attribute=$node.data_map.image image_class='medium'}
	{else}
		{def $ml_image = fetch('content', 'object', hash('object_id', $node.data_map.ml_image.content.id))}
		{attribute_view_gui attribute=$ml_image.data_map.image image_class='medium'}
	{/if}
	</div>
{/if}
<div class="attribute-long">
<a href="mailto:{$node.data_map.user_account.content.email}">{$node.data_map.user_account.content.email|wash('email')}</a>
{attribute_view_gui attribute=$node.data_map.bio}
</div>
{*

from talking to michael and david, it does not appear the second and third elements actually do anything in this context.
so the custom fetch that replaces it only uses the technique from the first element below.

{def
	$recent1=fetch('content','tree',hash(
		'parent_node_id',2,
		'limit', $page_limit,
		'offset', $view_parameters.offset,
		'sort_by',array('published',false()),
		'attribute_filter',array('and',
			array('owner','=',$node.object.id),
			array('class_identifier','=','article'))
	))
	$recent2=fetch('content','related_objects',hash(
		'object_id',$node.object.id,
		'limit', $page_limit,
		'offset', $view_parameters.offset,
		'attribute_filter','analyst',
		'sort_by',array('published',false())
	))
	$recent3=$node|real_rev_rels(hash('attribute_identifier', 417, 'limit', 15))
	$recent_list=cond(count($recent1),$recent1|merge(cond($recent3,$recent3,array())),array())
}
*}


{def
	$recent_research_hash = hash(
		'analyst', $node.data_map.user_account.content,
		'offset', $view_parameters.offset
	)
	$recent_research = fetch('site', 'research', $recent_research_hash)
	$recent_research_count = fetch('site', 'research_count', $recent_research_hash)
}


{if $recent_research_count}
<div class="separator">
	<h3>Recent Articles</h3>
	{foreach $recent_research as $recent_node}
		{node_view_gui content_node=$recent_node view=line}
	{/foreach}
	{include name=navigator
		uri='design:navigator/google.tpl'
		page_uri=$node.url_alias
		item_count=$recent_research_count
		view_parameters=$view_parameters
		item_limit=10
	}
</div>
{/if}