{* User - Full View *}
<header>
	<hgroup>
		<h1>{$node.data_map.first_name.content|wash()} {$node.data_map.last_name.content|wash()}<a class="button print" href="#" onclick="window.print();"  class="print">Print Bio</a></h1>
	</hgroup>
</header>
{if or($node.data_map.image.has_content, $node.data_map.ml_image.has_content)}
<div class="attribute-image">
{if $node.data_map.image.has_content}
	{attribute_view_gui attribute=$node.data_map.image image_class='biomedium'}
{else}
	{def $ml_image = fetch('content', 'object', hash('object_id', $node.data_map.ml_image.content.id))}
	{attribute_view_gui attribute=$ml_image.data_map.image image_class='biomedium'}
{/if}
</div>
{/if}
<div class="attribute-long">
{if $node.data_map.title.has_content}<h2>{$node.data_map.title.content|wash()}</h2>{/if}
{if $node.data_map.company.has_content}<h3>{$node.data_map.company.content|wash()}</h3>{/if}
<span>{$node.data_map.nationality.content|wash()}</span>
{attribute_view_gui attribute=$node.data_map.bio}
</div>