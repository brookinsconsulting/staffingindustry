{* Folder - Full view *}

{def $rssexport=rssexport($node.node_id)}

	<header><h1>{$node.data_map.name.content|wash()}</h1></header>
	{if count($rssexport)}{def $rsssource = fetch(content, node, hash(node_id, $rssexport.source_node_id))}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezroot('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /> Get the RSS Feed</a>{/if}


{if $node.data_map.description.has_content}
	<div class="">{attribute_view_gui attribute=$node.data_map.description}</div>
{/if}

{if $node.data_map.show_children.data_int}

	{def $page_limit = 10
		 $classes = cond($node.path_string|contains("/195/"), array('news_item'), array('article'))
		 $children = array()
		 $children_count = ''
		 $mysort = cond($node.path_string|contains("/195/"), array('attribute', false(), 402), $node.sort_array)
		 $mysort_a = cond($node.path_string|contains("/195/"), array('date'), array('publish_date'))
		 $children_hash = hash(
			'parent_node_id', 2,
			'class_filter_type', 'include',
			'class_filter_array', array('news_item'),
			'sort_by', array('published', false()),
			'language', array('eng-GB@euro'),
			'offset', $view_parameters.offset,
			'limit', $page_limit
		)
	}

	{if $node.depth|le(3)}
		{set $classes = $classes|merge(array('folder'))}
	{/if}


	{set $children = fetch('content', 'tree', $children_hash)}


	{def $rrs =  $node|real_rev_rels(hash('attribute_identifier', 417, 'class_filter_type', 'include', 'class_filter_array', $classes))}

	{if $children|count|eq(0)}
		{set $children = $rrs}
	{elseif $rrs|count}
		{set $children = $children|merge($rrs)}
	{/if}

	{set $children_count = fetch('content', 'tree_count', $children_hash)}

	{def $offset = first_set($view_parameters.offset, 0)}

	{if $children_count|gt(0)} 

		<div class="content-view-children">
			{foreach $children as $ck => $child}
				{node_view_gui view='line' content_node=$child}
				{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
			{/foreach}
		</div>
	
		{include name=navigator
			uri='design:navigator/google.tpl'
			page_uri=$node.url_alias
			item_count=$children_count
			view_parameters=$view_parameters
			item_limit=$page_limit
		}
	
	{else}
		<div class="article-file-download" style="width:233px;margin:0px auto">
	        	<p>There are not any research articles published in this topic at this time</p>
		</div>                                                                                 
	{/if}
{/if}
