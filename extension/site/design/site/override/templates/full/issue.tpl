{* Issue - Full view *}
{def 	$show_image=and(cond(is_set($hide_image),not($hide_image),true()), or($node.data_map.image.has_content,$node.data_map.ml_image.has_content))
		$date=cond($node.data_map.date.has_content,$node.data_map.date.content|explode(' '),false())
}
<header>
	<h1>{$node.data_map.name.content|wash()}</h1>
</header>
{if $show_image}
	<div class="attribute-image">
		{attribute_view_gui attribute=cond($node.data_map.ml_image.has_content,$node.data_map.ml_image.content.data_map.image,$node.data_map.image) image_class=medium}	
	</div>
{/if}
<div class="attribute-dateline">
	{$date|attribute(show,2,false())|debug('Date')}
	{if $date}<h3 class="date">{$date[0]} <small>{$date[1]} {$date[2]}</small></h3>{/if}
</div>
{if $node.data_map.short_description.has_content}
	<div class="attribute-short">
		{attribute_view_gui attribute=$node.data_map.short_description}
	</div>
{/if}
{if $node.data_map.show_children.data_int}
	<div class="content-view-children">
		{foreach fetch('content','list', hash('parent_node_id', $node.node_id, 'sort_by', $node.sort_array, 'class_filter_type', 'include', 'class_filter_array', array('article'), 'limit', first_set(subitem_limit, 10))) as $child}
			{node_view_gui view='line' content_node=$child}
			{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
		{/foreach}
	</div>
{/if}
