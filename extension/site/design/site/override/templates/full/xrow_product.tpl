{set-block scope=global variable=cache_ttl}0{/set-block}

{def $cur_locale = ezini('RegionalSettings', 'Locale')
	 $good_locales = array()
	 $good_locales_links = array()
	 $c_link = false()
	 $trans_r=array()
	 $trans = fetch( 'content', 'translation_list' )
	 $mask = $node.object.language_mask}
	
	{foreach $trans as $t}
		{set $trans_r = $trans_r|array_merge(hash($t.locale_code, $t))}
	{/foreach}

{if array(2,3,10,11,6,7)|contains($mask)}
	{set $good_locales = $good_locales|append("eng-US")}
	{set $good_locales_links = $good_locales_links|append(concat("Location: /site", $node.url_alias|ezroot(no)))}
{/if}

{if array(8,9,10,11,12,13)|contains($mask)}
	{set $good_locales = $good_locales|append("eng-GB@euro")}
	{set $good_locales_links = $good_locales_links|append(concat("Location: /eng", $node.url_alias|ezroot(no)))}
{/if}

{if array(4,5,6,7,12,13)|contains($mask)}
	{set $good_locales = $good_locales|append('eng-RW')}
	{set $good_locales_links = $good_locales_links|append(concat("Location: /row", $node.url_alias|ezroot(no)))}
{/if}

{if $good_locales|contains($cur_locale)|not}
	{$good_locales_links[0]|header}
{/if}

{def $site_a_r=hash('USD', concat('row/', $node.url_alias), 'EUR', concat('eng/', $node.url_alias), 'USD', concat('site/', $node.url_alias))}

{set-block scope=relative variable=langlinks}

This product is also available in 
{foreach $node.data_map.price.content.currency_list as $k => $c}
	{if $c['locale']|eq('eng-GB@euro')}{continue}{/if}
	{if or($good_locales|contains($c['locale'])|not, $c.locale|eq($node.object.current_language))}{continue}{/if}
	{set $c_link = true()}
	{if $c['locale']|eq($cur_locale)}
		{$c['symbol']} ({$k}) 
	{else}
		<a href='/{$site_a_r[$k]}'>{$c['symbol']} ({$k})</a>, 
	{/if}
{/foreach}	
{/set-block}

{$trans_r[$node.object.current_language].currency_short_name|set_currencypref}

{def $price_display=ezini( 'Settings', 'ShowPriceAs', 'xrowecommerce.ini' )}
<div class="class-xrow-commerce">
	<header>
		<h1>{$node.name|wash()}</h1>
	</header>

	{if $node.data_map.image.has_content}
	<div class="attribute-image">
		<img src={$node.data_map.image.content.small.full_path|ezroot} alt="{$node.name|wash()}" title="{$node.name|wash()}" />
	</div>

	<!--  <p class="note">{'Double click on above image to view full picture'|i18n('extension/xrowecommerce')}</p> -->
	{if $node.data_map.caption.has_content}<div class="caption">{attribute_view_gui attribute=$node.data_map.caption}</div>{/if}

	{/if}

	<div class="description-wrap">
		{if is_set($node.object.data_map.description)|not}<div class="attribute-short">{attribute_view_gui attribute=$node.object.data_map.short_description}</div>{/if}
		{if and($node.object.data_map.webinar.has_content, $node.object.data_map.end_date.has_content, lt($node.object.data_map.end_date.content.timestamp, currentdate()))}
			{include node=$node uri="design:parts/subscribe_notice.tpl"}
		{else}
			{if is_set($node.object.data_map.description)}<div class="attribute-long">{attribute_view_gui attribute=$node.object.data_map.description}</div>{/if}
		{/if}
	</div>

	<div class="xrow-feature-list">
		<div class="xrow-product-wishlist">
			<form method="post" action={"content/action"|ezurl}>
				<input type="hidden" name="ContentNodeID" value="{$node.node_id}" />
				<input type="hidden" name="ContentObjectID" value="{$node.object.id}" />
				<input type="hidden" name="ViewMode" value="full" />
				<input type="hidden" name="FromPage" value="{$node.url_alias}" />
				<input class="button" type="submit" name="ActionAddToWishList" value="{"Add to wish list"|i18n("design/ezwebin/full/product")}" />
			</form>
		</div>
		<p>
		{def $tipafriend_access=fetch( 'user', 'has_access_to', hash( 'module', 'content','function', 'tipafriend' ) )}
		{if and( ezmodule( 'content/tipafriend' ), $tipafriend_access )}<a href={concat( "/captcha/tipafriend/", $node.node_id )|ezurl} title="{'Tip a friend'|i18n( 'design/ezwebin/full/article' )}">{'Tip a friend'|i18n( 'design/ezwebin/full/article' )}</a> | {/if}<a href="#related-products" >{'Related products'|i18n( 'design/ezwebin/full/article' )}</a> | <a href="#product-reviews">{'Product reviews'|i18n( 'design/ezwebin/full/article' )}</a>
		</p>
	</div>
</div>

<div class="productwrapper float-break">
	<form name="buy" id="buy" method="post" action={"xrowecommerce/multiadd"|ezurl}>
		<input type="hidden" name="ContentNodeID" value="{$node.node_id}" />
		<input type="hidden" name="ContentObjectID" value="{$node.object.id}" />
		<input type="hidden" name="ViewMode" value="full" />
		<div>
		{if
			and(
				$node.data_map.show_add_to_cart.content,
				or(	
					and(
						$node.data_map.end_date.content.timestamp|gt(currentdate()),
						$node.data_map.webinar.has_content
					),
					not($node.data_map.webinar.has_content)
				)
			)
		}
		{def $dropdown = $node.data_map.dropdown.content}
			{if $dropdown}
				<script>
				{literal}
				var select_html = '';
				$(function(){
					// make the dropdown
					// select_html is built inline elsewhere
					$('#attribute-multi-options-select').html(select_html);

					// fix hidden real attribute on change
					$('#attribute-multi-options-fake').change(function(){
						var choice = $(this).find('option:selected').attr('class');
						$('#attribute-multi-options td.quantity input[type=text]').val('0');
						$('#attribute-multi-options td.quantity input[type=text]#' + choice).val('1');
					});
				});

				{/literal}
				</script>
				<div id="attribute-multi-options-fake" class="attribute-multi-options">
					<select id="attribute-multi-options-select">
					</select>
				</div>
			{elseif $node.data_map.bogo.content}
				<script>
				{literal}
						// only three rows expected, so :last is the third (free) and :odd is the second (paid) (first is header)
						var paid = $('#attribute-multi-options tr:odd input[name$="[quantity]"]'),
						    free = $('#attribute-multi-options tr:last input[name$="[quantity]"]');

						// get the table set up 
						totalrow = '<tr class="bogo-total"><td colspan="2">Total</td><td align="right" id="bogo-total"></td><td></td>';
						$('#attribute-multi-options tbody').append(totalrow);
						free.css('background-color', '#ddd').focus(function(){
							$(this).blur();
						});

						// initialize
						bogoUpdate();

						paid.change(function(){
							bogoUpdate();
						});

						function bogoUpdate() {
							free.val(paid.val());
							$('#bogo-total').html(Number(paid.val()) + Number(free.val()));
						}
					});
				{/literal}
				</script>
			{/if}
			<table {if $dropdown}style="display:none" {/if}id="attribute-multi-options"  class="list" summary="This table contains information about the product, like image, product number, description and the form to orderthe product.">
				<tr>
				{* if $node.data_map.options.content.option_list|count|gt(0)}
					<th>{'Image'|i18n('extension/xrowecommerce')}</th>
				{/if *}
				{*
					<th>{'Number'|i18n('extension/xrowecommerce')}</th>
					*}
				{if $node.data_map.options.content.option_list|count|gt(0)}
					<th>{'Item'|i18n('extension/xrowecommerce')}</th>
				{/if}
					<th>{'Description'|i18n('extension/xrowecommerce')}</th>
					<th class="quantity">{'Quantity'|i18n('extension/xrowecommerce')}</th>
					<th class="price">{'Price'|i18n('extension/xrowecommerce')}</th>
				</tr>
				{if $node.data_map.options.content.option_list|count|gt(0)}
				{section var=Options loop=$node.data_map.options.content.option_list}
				<tr>
					{*<td>
						{if $Options.item.image.current.data_map.image.content.is_valid}
						{attribute_view_gui image_class=galleryline attribute=$Options.item.image.current.data_map.image}
						{else}
						<div class="s_nopic"><img src={'shop/nopic_tiny.jpg'|ezimage()} alt="{'No image available'|i18n('extension/xrowecommerce')}" /></div>
						{/if}
					</td>*}
					{*
					<td>{$Options.item.value}</td>
					*}
					<td>{$Options.item.comment|wash()}</td>
					<td>{$Options.item.description|wash|nl2br}</td>
					<td align="right" class="quantity">
						<input type="hidden" name="AddToBasketList[{$Options.index}][object_id]" value="{$node.object.id}" />
						<input type="hidden" name="AddToBasketList[{$Options.index}][variations][{$node.data_map.options.id}]" value="{$Options.item.id}" />
						<input id="q{$Options.index}" type="text" name="AddToBasketList[{$Options.index}][quantity]" value="{if eq($Options.index,0)}1{else}0{/if}" />
					</td>
					<td class="price" align="right">
					{if $Options.multi_price}
						{def $price=get_multiprice( $node.data_map.price, $Options.multi_price, $price_display )}
						{$price|l10n( currency )}
					{/if}
					</td>
					{if $dropdown}
					<script>
						select_html += '<option class="q{$Options.index}">{$Options.item.comment} - {$Options.item.description} - {$price|l10n( currency )}</option>';
					</script>
					{/if}
					{undef $price}
				</tr>
				{/section}
				{else}
				<tr>
					<td>{$node.object.data_map.product_id.data_text}</td>
					<td>{attribute_view_gui attribute=$node.object.data_map.short_description}</td>
					<td align="right" class="quantity">
						<input type="hidden" name="AddToBasketList[{$Options.index}][object_id]" value="{$node.object.id}" />
						<input type="hidden" name="AddToBasketList[{$Options.index}][variations][{$node.data_map.options.id}]" value="{$Options.item.id}" />
						<input type="text" name="AddToBasketList[0][quantity]" value="1" />
					</td>
					<td align="right">{$node.data_map.price.content.$price_display|l10n( currency )}</td>
				</tr>
				{/if}
			</table>
			<div class="block">
				<div class="right">
					{if count(fetch(shop, basket).items)|gt(0)}
						{if eq($node.data_map.price.value.currency, cartcurrency())}
							<input type="submit" class="button right-arrow" name="ActionAddToBasket" value="{"Add to Shopping Cart"|i18n("extension/xrowecommerce")}" />
						{else}
							<input disabled="disabled" type="button" class="button right-arrow" value="{"Currency incompatible with current basket."|i18n("extension/xrowecommerce")}" />
						{/if}
					{else}
						<input type="submit" class="button right-arrow" name="ActionAddToBasket" value="{"Add to Shopping Cart"|i18n("extension/xrowecommerce")}" />
					{/if}
				</div>
			</div>
		{/if}
		</div>

		{if eq(ezini( 'AutomaticDeliverySettings', 'AutomaticDelivery', 'automaticdelivery.ini' ), 'enabled' )}
		<div id="overlay-text">
		{def $user=fetch( 'user', 'current_user' )}
		{if and($node.data_map.no_auto_delivery.content|not(), $user.is_logged_in)}
			<p>{'Add your selections to'|i18n( 'extension/xrowecommerce')} <a id="show_auto_tip">{'Automatic Delivery'|i18n( 'extension/xrowecommerce')}</a>?</p>
			<input class="flat-right2 button" type="submit" onclick="document.buy.action='{"recurringorders/add"|ezurl(no)}'; document.buy.submit(); return true;" name="ActionAddToRecurring" value="{"Add to Automatic Delivery"|i18n("extension/xrowecommerce")}" />
		{elseif $node.data_map.no_auto_delivery.content|not()}
			<p>{'This product is available for'|i18n( 'extension/xrowecommerce')} <a id="show_auto_tip">{'Automatic Delivery'|i18n( 'extension/xrowecommerce')}</a>. {'To add this product to your Automatic Delivery you have to'|i18n( 'extension/xrowecommerce')} <a href={'user/login'|ezurl}>{'login'|i18n( 'extension/xrowecommerce')}</a>.</p>
		{/if}
		</div>
		{/if}
	</form>
</div>
<div class="xrow-product-features">
	<a name="related-products"></a>
	{include node=$node uri="design:shop/related_products.tpl"}
	<a name="product-reviews"></a>
	{include node=$node uri="design:shop/review_children.tpl"}
</div>
{undef $price_display $tipafriend_access}

{if eq(ezini( 'AutomaticDeliverySettings', 'AutomaticDelivery', 'automaticdelivery.ini' ), 'enabled' )}
<div id="AutomaticDeliveryTooltip">
<h3>{'What is Automatic Delivery?'|i18n( 'extension/xrowecommerce')}</h3>
<p>{"Use our Automatic Delivery service to have this item sent to you as often as you like. You'll get priority on our inventory and save time."|i18n( 'extension/xrowecommerce')}</p>
<p>{'By placing your initial Automatic Delivery order and setting up an Automatic Delivery schedule, you authorize us to charge the same credit card for future Automatic Delivery orders until you cancel.'|i18n( 'extension/xrowecommerce')}</p>
<p>{'Since the accuracy of your credit card, shipping and billing information is vital to Automatic Delivery, please promptly submit changes through the my account section.'|i18n( 'extension/xrowecommerce')}</p>
</div>
{/if}


{*
* Draft of the zoom feature.
* Images can be zoomed in via a moving the slider and dragging the image
<div id="slider" class="yui-skin-sam yui-widget yui-slider"><!-- boundingBox --></div>
{literal}
<script> 
YUI(YUI3_config).use('node', 'dd', 'slider', 'stylesheet', 'event-delegate', function(Y) {

var dd1 = new Y.DD.Drag({
node: '#product_image'
}).plug(Y.Plugin.DDConstrained, {
constrain2node: '#dd-demo-canvas1'
});

//Create and render the slider
var sl = new Y.Slider({
boundingBox : '#slider',
railSize: '100px', value: 100, max: 1000, min: 100,
thumbImage: Y.config.base+'slider/assets/skins/sam/thumb-classic-x.png'
}).render();
var css = ".yui-skin-sam .yui-slider-rail-x { " +
" background: url(" + Y.config.base + "slider/assets/skins/sam/rail-classic-x.png) repeat-x 0 7px;" +
" min-height: 19px" +
" *height: 19px" +
" } ";

var sheet = new Y.StyleSheet(css);

//Listen for the change

sl.after('valueChange',function (e) {
//Insert a dynamic stylesheet rule:
var sheet = new Y.StyleSheet('image_slider');
sheet.set('#product_image', {
width: e.newVal + '%',
height: e.newVal + '%'
});
});

});
</script>   
{/literal}
*}

{if $c_link}<p class='cur_list'>{$langlinks|preg_replace("/[\s,]*$/", "")}</p>{/if}
{false()|set_currencypref()}