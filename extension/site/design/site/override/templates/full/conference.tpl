{* Conference - Full view *}
<header><h1>{$node.name|wash()}</h1></header>
<div class="attribute-byline">
	<h3>{attribute_view_gui attribute=$node.data_map.date}</h3>
	<span class="location">{attribute_view_gui attribute=$node.data_map.location}</span>
</div>

<section class="conference-content">
{if $node.data_map.description.content.is_empty|not}
	{attribute_view_gui attribute=$node.data_map.description}
{/if}
</section>
