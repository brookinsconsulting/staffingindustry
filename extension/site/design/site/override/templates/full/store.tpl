{* Folder - Full view *}

{def $rssexport=rssexport($node.node_id)}

	<header><h1>{$node.data_map.name.content|wash()}</h1></header>
	{if count($rssexport)}
	{def $rsssource = fetch(content, node, hash(node_id, $rssexport.source_node_id))}
	<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezroot('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /> Get the RSS Feed</a>{/if}

	{if $node.data_map.description.has_content}
	<div class="">{attribute_view_gui attribute=$node.data_map.description}</div>
	{/if}

	{if $node.data_map.show_children.data_int}

		{def $page_limit = 10
			 $classes = ezini('MenuContentSettings', 'ExtraIdentifierList', 'menu.ini')
			 $children = array()
			 $children_count = ''
			$grandchildren = array()
		}

		{if le( $node.depth, '3')}
			{set $classes = $classes|merge(ezini('ChildrenNodeList', 'ExcludedClasses', 'content.ini'))}
		{/if}

		{set $children_count=fetch('content','list_count', hash('parent_node_id', $node.node_id,
									'class_filter_type', 'exclude',
									'class_filter_array', $classes))}
																
		<div class="content-view-children">
		{if $children_count}
			{foreach fetch('content','list', hash('parent_node_id', $node.node_id, 'offset', $view_parameters.offset, 'sort_by', $node.sort_array, 'class_filter_type', 'exclude', 'class_filter_array', $classes, 'limit', $page_limit)) as $child}
				{node_view_gui view='line' content_node=$child}

				{set
					$grandchildren = fetch('content', 'list', hash(
						'parent_node_id', $child.node_id,
						'offset', $view_parameters.offset,
						'sort_by', $child.sort_array,
						'class_filter_type', 'exclude',
						'class_filter_array', $classes,
						'limit', $page_limit))
				}
				<ul>
				{foreach $grandchildren as $grandchild}
					<li>
						{node_view_gui view='listitem' content_node=$grandchild}
					</li>
				{/foreach}
				</ul>

				{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
			{/foreach}
		{/if}
		</div>

		{include name=navigator
			uri='design:navigator/google.tpl'
			page_uri=$node.url_alias
			item_count=$children_count
			view_parameters=$view_parameters
			item_limit=$page_limit
		}

	{/if}
