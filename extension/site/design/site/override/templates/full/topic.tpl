{* Folder - Full view *}

{if and($node.path_string|contains("/70472/")|not, $node.path_string|contains("/213/")|not, $node.node_id|ne(195))}


	{def $rssexport=rssexport($node.node_id)}

	<header class='case1'><h1>{$node.data_map.name.content|wash()}</h1></header>
	{if count($rssexport)}
	{def $rsssource = fetch(content, node, hash(node_id, $rssexport.source_node_id))}
	<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezroot('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /> Get the RSS Feed</a>{/if}

	{if $node.data_map.description.has_content}
	<div class="">{attribute_view_gui attribute=$node.data_map.description}</div>
	{/if}

	{if $node.data_map.show_children.data_int}

		{def $page_limit = 10
			 $classes = ezini('MenuContentSettings', 'ExtraIdentifierList', 'menu.ini')
			 $children = array()
			 $children_count = ''
		}

		{if le( $node.depth, '3')}
			{set $classes = $classes|merge(ezini('ChildrenNodeList', 'ExcludedClasses', 'content.ini'))}
		{/if}

		{set $children_count=fetch('content','list_count', hash('parent_node_id', $node.node_id,
																'class_filter_type', 'exclude',
																'class_filter_array', $classes))}
																
		<div class="content-view-children">
		{if $children_count}
			{if eq($node.node_id, 291)}
				{foreach fetch('content','list', hash('parent_node_id', $node.node_id, 'offset', $#view_parameters.offset, 'sort_by', array('attribute', false(), 191 ), 'class_filter_type', 'exclude', 'class_filter_array', $classes, 'limit', $page_limit)) as $child}
					{node_view_gui view='line' content_node=$child}
					{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
				{/foreach}
			{else}
				{foreach fetch('content','list', hash('parent_node_id', $node.node_id, 'offset', $#view_parameters.offset, 'sort_by', $node.sort_array, 'class_filter_type', 'exclude', 'class_filter_array', $classes, 'limit', $page_limit)) as $child}
					{node_view_gui view='line' content_node=$child}
					{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
				{/foreach}
			{/if}
		{/if}
		</div>
		{include name=navigator
			uri='design:navigator/google.tpl'
			page_uri=$#node.url_alias
			item_count=$children_count
			view_parameters=$view_parameters
			item_limit=$page_limit
		}

	{/if}





{elseif $node.node_id|eq(195)}

{def $rssexport=rssexport($node.node_id)}

	<header class='case2'><h1>{$node.data_map.name.content|wash()}</h1></header>
	{if count($rssexport)}{def $rsssource = fetch(content, node, hash(node_id, $rssexport.source_node_id))}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezroot('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /> Get the RSS Feed</a>{/if}


{if $node.data_map.description.has_content}
	<div class="">{attribute_view_gui attribute=$node.data_map.description}</div>
{/if}

{if $node.data_map.show_children.data_int}

	{def $page_limit = 10
		 $classes = cond($node.path_string|contains("/195/"), array('news_item'), array('article'))
		 $children = array()
		 $children_count = ''
		 $mysort = cond($node.path_string|contains("/195/"), array('attribute', false(), 402), $node.sort_array)
		 $mysort_a = cond($node.path_string|contains("/195/"), array('date'), array('publish_date'))
	}

	{if $node.depth|le(3)}
		{set $classes = $classes|merge(array('folder'))}
	{/if}


	{set $children = fetch('content','list', hash('parent_node_id', $node.node_id, sort_by, $mysort, 'class_filter_type', 'include', 'class_filter_array', $classes, 'limit', $page_limit, 'offset', $#view_parameters.offset))}

	{set $children_count  = fetch('content','list_count', hash('parent_node_id', $node.node_id, 'class_filter_type', 'include', 'class_filter_array', $classes))}

	{if $children_count|gt(0)} 

		<div class="content-view-children">
			{foreach $children as $ck => $child}
				{node_view_gui view='line' content_node=$child}
				{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
			{/foreach}
		</div>
	
		{include name=navigator
			uri='design:navigator/google.tpl'
			page_uri=$#node.url_alias
			item_count=$children_count
			view_parameters=$view_parameters
			item_limit=$page_limit
		}
	
	{else}
		<div class="article-file-download" style="width:233px;margin:0px auto">
	        	<p>There are not any research articles published in this topic at this time</p>
		</div>                                                                                 
	{/if}
{/if}


































{else}

{def $rssexport=rssexport($node.node_id)}

	<header class='case3'><h1>{$node.data_map.name.content|wash()}</h1></header>
	{if count($rssexport)}{def $rsssource = fetch(content, node, hash(node_id, $rssexport.source_node_id))}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezroot('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /> Get the RSS Feed</a>{/if}


{if $node.data_map.description.has_content}
	<div class="">{attribute_view_gui attribute=$node.data_map.description}</div>
{/if}

{if $node.data_map.show_children.data_int}

	{def $page_limit = 10
		 $classes = cond($node.path_string|contains("/195/"), array('news_item'), array('article'))
		 $offset = first_set($#view_parameters.offset, 0)
		 $children_count = ''
	}

	{if $node.depth|le(3)}
		{set $classes = $classes|merge(array('folder'))}
	{/if}
	
	{def $all_children =  $node|kids_and_rels(hash('attribute_identifier', array(673), 'main_node_only', true(), 'class_filter_type', 'include', 'class_filter_array', $classes))
		 $children =  $node|kids_and_rels(hash('attribute_identifier', array(673), 'main_node_only', true(), 'class_filter_type', 'include', 'class_filter_array', $classes, 'offset', $offset, 'limit', $page_limit, 'sort_by', array('attribute', false(), '191')))}

	{set $children_count  = $all_children|count}


	{if $children_count|gt(0)} 

		<div class="content-view-children">
			{foreach $children as $ck => $child}
				{node_view_gui view='line' content_node=$child}
				{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
			{/foreach}
		</div>
	
		{include name=navigator
			uri='design:navigator/google.tpl'
			page_uri=$#node.url_alias
			item_count=$children_count
			view_parameters=$view_parameters
			item_limit=$page_limit
		}
	
	{else}
		<div class="article-file-download" style="width:233px;margin:0px auto">
	        	<p>There are not any research articles published in this topic at this time</p>
		</div>                                                                                 
	{/if}
{/if}
	

{/if}
