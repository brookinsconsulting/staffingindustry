{set-block scope=root variable=cache_ttl}600{/set-block}
{* Event - Full view *}
{def $show_image=and(cond(is_set($hide_image),not($hide_image),true()), $node.data_map.image.has_content)
	$from_ts=$node.data_map.from_time.content.timestamp
     $to_ts=cond($node.data_map.to_time.has_content,$node.data_map.to_time.content.timestamp,false())
     $from_hr=$from_ts|datetime('custom','%g')|int()
     $from_min=$from_ts|datetime('custom','%i')|int()
     $from_format=cond(and(eq($from_ts|datetime('custom','%G'),0),eq($from_min,0)),'%F %j, %Y','%F %j, %Y %g:%i%a')
}
{* $from_format=cond(and(not($to_ts),eq($from_hr,0),eq($from_min,0)),'%F %j, %Y','%F %j, %Y %g:%i%a') *}
<header>
 	{if $node.data_map.title.has_content}
        <h1>{$node.data_map.title.content|wash()}</h1>
    {else}
        <h1>{$node.name|wash()}</h1>
    {/if}
</header>
<h3 class="date">
{if $to_ts}
    {if and(eq($to_ts|datetime('custom','%F'),$from_ts|datetime('custom','%F')),eq($to_ts|datetime('custom','%j'),$from_ts|datetime('custom','%j')))}
        {$from_ts|datetime('custom',$from_format)} - {$to_ts|datetime('custom','%g:%i%a')}
    {else}
        {$from_ts|datetime('custom',$from_format)} - {$to_ts|datetime('custom',$from_format)}
    {/if}
{else}
{$from_ts|datetime('custom',$from_format)}
{/if}
</h3>
<div class="attribute-byline">
    {if $node.object.data_map.category.has_content}
    <span class="ezagenda_keyword">
    {"Category"|i18n("design/ezwebin/full/event")}:
    {attribute_view_gui attribute=$node.object.data_map.category}
    </span>
    {/if}

</div>
{if $show_image}
	<div class="attribute-image align-right">
		{attribute_view_gui attribute=$node.data_map.image image_class=medium}	
	</div>
{/if}
{if $node.object.data_map.text.has_content}
    {attribute_view_gui attribute=$node.object.data_map.text}
{/if}
{*if $node.object.data_map.url.has_content}
    <p style="text-align:center;">
      <a href={$node.object.data_map.url.content|ezurl}>{$node.object.data_map.url.data_text|wash()}</a>
    </p>
{/if *}
