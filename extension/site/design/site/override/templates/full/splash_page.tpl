{if $node.data_map.heading.has_content}<h2>{$node.data_map.heading.content|wash()}</h2>{/if}
<div class="attribute-content">
	{attribute_view_gui attribute=$node.data_map.description}
</div>