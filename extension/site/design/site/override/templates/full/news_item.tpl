{* News Item - Full view *}
{def $date=cond($node.data_map.date.has_content,$node.data_map.date.content|explode(' '),false())}
<div id='article-utility'>
	<h2>Daily News</h2>
	<a class="button viewnews" href={"/Research-Publications/Daily-News"|sitelink()}>View All News</a>
</div>
<header><h1>{$node.data_map.name.content|wash()}</h1></header>
<div>{$node.data_map.date.content.timestamp|datetime('newsfull')}</div>
<section class="attribute-dateline">
{if $date}<h3 class="date format">{$date[0]} <small>{$date[1]} {$date[2]}</small></h3>{/if}
{include uri="design:parts/sharethis.tpl"}
</section>
{if $node.data_map.description.content.is_empty|not}
	{attribute_view_gui attribute=$node.data_map.description}
{/if}
{include uri="design:parts/comments.tpl" class=array('advanced','module') section=true()}
