{* Folder - Full view *}

{if $node.path_string|contains("/185/")|not}

{def $rssexport=rssexport($node.node_id)}

	<header><h1>{$node.data_map.name.content|wash()}</h1></header>
	{if count($rssexport)}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezroot('no')}" title="{$rssexport.title|wash()}"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rssexport.title|wash()}" /> Get the RSS Feed</a>{/if}


{if $node.data_map.description.has_content}
	<div class="">{attribute_view_gui attribute=$node.data_map.description}</div>
{/if}

{if $node.data_map.show_children.data_int}
{def $page_limit = 10
	 $classes = ezini('MenuContentSettings', 'ExtraIdentifierList', 'menu.ini')
	 $children = array()
	 $children_count = ''
}

{if le( $node.depth, '3')}{set $classes = $classes|merge(ezini('ChildrenNodeList', 'ExcludedClasses', 'content.ini'))}{/if}

{set $children_count=fetch('content','list_count', hash('parent_node_id', $node.node_id,
														'class_filter_type', 'exclude',
														'class_filter_array', $classes))
}
	<div class="content-view-children">
	{if $children_count}
		{foreach fetch('content','list', hash('parent_node_id', $node.node_id, 'offset', $view_parameters.offset, 'sort_by', $node.sort_array, 'class_filter_type', 'exclude', 'class_filter_array', $classes, 'limit', $page_limit)) as $child}
			{node_view_gui view='line' content_node=$child}
			{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
		{/foreach}
	{/if}
	</div>

	{include name=navigator
		uri='design:navigator/google.tpl'
		page_uri=$node.url_alias
		item_count=$children_count
		view_parameters=$view_parameters
		item_limit=$page_limit
	}

{/if}













{else}

{def $rssexport=rssexport($node.node_id)}

	<header><h1>{$node.data_map.name.content|wash()}</h1></header>
	{if count($rssexport)}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezroot('no')}" title="{$rssexport.title|wash()}"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rssexport.title|wash()}" /> Get the RSS Feed</a>{/if}


{if $node.data_map.description.has_content}
	<div class="">{attribute_view_gui attribute=$node.data_map.description}</div>
{/if}



{if and($blogs|count|eq(0), $node.can_create)}
	<form action={"/content/action"|sitelink()} method="post">
		<input type="hidden" value="340" name="ContentNodeID">
		<input type="hidden" value="340" name="NodeID">
		<input type="hidden" name="ClassID" value="19" />
		<input type="hidden" value="" name="to-dialog-container">
		<input type="hidden" name="NewButton" />
		<input type="submit" value="Create blog" />
	</form>
{/if}



{if $node.data_map.show_children.data_int}
{def $page_limit = 10
	 $classes = ezini('MenuContentSettings', 'ExtraIdentifierList', 'menu.ini')
	 $children = array()
	 $children_count = ''
	 $mysort = cond($node.path_string|contains("/195/"), array('attribute', false(), 402), $node.sort_array)
	 $mysort_a = cond($node.path_string|contains("/195/"), array('date'), array('publish_date'))
}

{if le( $node.depth, '3')}{set $classes = $classes|merge(ezini('ChildrenNodeList', 'ExcludedClasses', 'content.ini'))}{/if}

{set $children_count=fetch('content','list_count', hash('parent_node_id', $node.node_id,
														'class_filter_type', 'exclude',
														'class_filter_array', $classes))
}

{set $children = fetch('content','list', hash('parent_node_id', $node.node_id, 'offset', $view_parameters.offset, 'sort_by', $mysort, 'class_filter_type', 'exclude', 'class_filter_array', $classes, 'limit', $page_limit))}

{def $rrs =  $node|real_rev_rels(hash('attribute_identifier', 417, 'limit', $page_limit, offset, $view_parameters.offset))
     $rrs_count =  $node|real_rev_rels_count}


{set $children_count = $children_count|sum($rrs_count)}

{if $children|count|eq(0)}
	{set $children = $rrs}
{elseif $rrs|count}
	{set $children = $children|merge($rrs)}
{/if}


	<div class="content-view-children">
	{if $children_count}
		{foreach $children|asort($mysort_a,1,'DESC') as $child max $page_limit}
			{node_view_gui view='line' content_node=$child}
			{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
		{/foreach}
	{/if}
	</div>

	{include name=navigator
		uri='design:navigator/google.tpl'
		page_uri=$node.url_alias
		item_count=$children_count
		view_parameters=$view_parameters
		item_limit=$page_limit
	}

{/if}


{/if}
