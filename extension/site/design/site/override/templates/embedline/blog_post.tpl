{* TODO: image attribute (do any blog posts even use this? it's on the mockup) *}
{* blog_post embed *}
<section class="content-view-embed class-blog-post">
	<h3>
                {* if $node.data_map.blog_author.has_content}
				{attribute_view_gui attribute=$node.data_map.blog_author image_class='message'}
		{elseif $node.object.owner.data_map.ml_image.has_content}
				{attribute_view_gui attribute=$node.object.owner.data_map.ml_image.content.data_map.image image_class='message'}
		{elseif $node.object.owner.data_map.image.content.is_valid}
				{attribute_view_gui attribute=$node.object.owner.data_map.image image_class='message'}
		{else}
			<img alt="" title="" src="/extension/site/design/site/images/anon-user.png" width="70" height="70" />
		{/if *}

		<a href={$node|sitelink()} title="{$node.data_map.title.content|wash()}">{$node.data_map.title.content|wash()}</a>
	</h3>
	{attribute_view_gui attribute=$node.data_map.body htmlshorten=array(200,concat('... <a href="',$node|sitelink('no'),'">Read More</a>'))}
</section>
