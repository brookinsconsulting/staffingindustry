{def $comment_count=0
	 $view_comments=''
	 $view_comments_link=''
	 $title_content=$node.data_map.description.content.output.output_text|explode('"')|implode("'")|html_shorten(250)
	 $date=cond($node.data_map.date.has_content,$node.data_map.date.content.timestamp|datetime('custom','%n/%j/%Y %g:%i:%s%A')|explode(' '),false())
}
<div class="content-view-listitem class-news-item">
	{if and(is_set($node.data_map.comments), fetch('user','current_user').is_logged_in)}
    	{set $comment_count=fetch('comment','comment_count',hash('contentobject_id',$node.data_map.comments.object.id,'language_id',$node.data_map.comments.language_id,'status' ,1))}                                                                    
		{if $comment_count|gt( 0 )}
			{set $view_comments_link = concat( $node.url_alias, '#comments' )|sitelink('no')}
			{set $view_comments = concat('<a href=', $view_comments_link, '>Comments (', $comment_count, ')</a> &bull; ')}
		{else}
			{set $view_comments_link = ""}
			{set $view_comments = ""}
		{/if}
	{else}
		{set $view_comments = ""}
	{/if}


	<h2><a href={$node|sitelink()} title="<h4><a href='{$node|sitelink('no')}'>{$node.name|wash()}</a></h4>{$title_content}<p>{$view_comments} <a href='{$node|sitelink('no')}'>Read more</a></p>">{$node.name|shorten(36)}</a></h2>
	{*<span class="date">{$node.data_map.date.content.timestamp|datetime('custom','%n/%j/%Y %g:%i:%s%A')}</span>*}
	{*if $date}<p class="date">{$date[0]}{if $date|count|gt(2)} <small>{$date[1]} {$date[2]}</small>{/if}</p>{/if*}
	{$node.data_map.date.content.timestamp|datetime('newsdate')}
	<p>{$node.data_map.description.content.output.output_text|striptags()|shorten(80)}</p>
</div>