{def $comment_count = 0
	$view_comments = ""
	$view_comments_link = ""
	$content = $node.data_map.body.content.output.output_text|explode('"')|implode("'")|html_shorten(80)
}
{if and(is_set($node.data_map.comments), fetch('user','current_user').is_logged_in)}
    {set $comment_count=fetch('comment','comment_count',hash('contentobject_id',$node.data_map.comments.object.id,'language_id',$node.data_map.comments.language_id,'status' ,1))}                                                                    
	{if $comment_count|gt( 0 )}
		{set $view_comments_link = concat( $recent_update.url_alias, '#comments' )|sitelink('no')}
		{set $view_comments = concat('<a href=', $view_comments_link, '>Comments (', $comment_count, ')</a> &bull; ')}
	{else}
		{set $view_comments_link = ""}
		{set $view_comments = ""}
	{/if}       		
{else}
	{set $view_comments = ""}
{/if}
<div class="content-view-listitem class-blog-post line-image">
	{set-block variable='left_column'}
		<div class="attribute-image">
			{if $node.object.owner.data_map.ml_image.has_content}
					{attribute_view_gui attribute=$node.object.owner.data_map.ml_image.content.data_map.image image_class='message'}
			{elseif $node.object.owner.data_map.image.content.is_valid}
					{attribute_view_gui attribute=$node.object.owner.data_map.image image_class='message'}
			{else}
				<img alt="" title="" src="/extension/site/design/site/images/anon-user.png" width="70" height="70" />
			{/if}
		</div>
	{/set-block}
	{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$left_column class='message'}
	{set-block variable='right_column'}
		<h1><a href={$node|sitelink()} title="{$content}<p>{$view_comments} <a href={$node|sitelink('no')}>Read more</a></p>">{$node.name|wash()}</a><span class="author">- {$node.parent.name|wash()}</span></h1>
		{attribute_view_gui attribute=$node.data_map.body htmlshorten=first_set($htmlshorten,60)}
	{/set-block}
	{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$right_column class='r-message' position='right'}
</div>