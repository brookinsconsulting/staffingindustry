
<!DOCTYPE html>
<html lang="{$site.http_equiv.Content-language|wash()}" xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$site.http_equiv.Content-language|wash()}">

{def $pagedata = pagedata()}
<head>
{include uri="design:page_head.tpl" enable_link=false()}

{include uri='design:page/head_style.tpl'}

<script type="text/javascript" src={'javascript/jquery-1.5.1.min.js'|ezdesign()} charset="utf-8"></script>

</head>

<body class="{$pagedata.site_classes|implode(' ')}"{if $current_node.data_map.hex_color.has_content} style="background-color:{$current_node.data_map.hex_color.content} !important"{/if}>
	<div>
		{$module_result.content}
	</div>
	{* This comment will be replaced with actual debug report (if debug is on). *}
	<!--DEBUG_REPORT-->

</body>

</html>
{kill_debug()}