{* Article - Embed View *}
{def $node = $object.main_node}
<div class="content-view-embed class-article{if $show_image} line-image{/if}">
	<h1><a href={$node|sitelink()}>{if ne($node.data_map.type.data_text,'')}<span class="parent">{attribute_view_gui attribute=$node.data_map.type} -</span> {/if}{$node.data_map.title.data_text|wash()}</a></h1>
	<div class="attribute-image">
		{if $node.data_map.ml_image.content}
			<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.ml_image.content.data_map.image href=$node|sitelink() image_class='message'}</div>
		{elseif $node.data_map.image.content}
			<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='message'}</div>
		{/if}
	</div>
	{attribute_view_gui attribute=$node.data_map.intro htmlshorten=400}
</div>
