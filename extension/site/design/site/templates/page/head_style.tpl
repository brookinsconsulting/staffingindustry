{def $default_css=array('reset.css','core.css','pagecore.css')
     $user_css=hash('debug','debug.css','toolbar','websitetoolbar.css','edit','edit.css')
     $ini_css = cond(is_unset($load_css_file_list),ezini('StylesheetSettings', 'CSSFileList', 'design.ini')|merge(ezini('StylesheetSettings', 'FrontendCSSFileList', 'design.ini')),false())
     $stylesheets_list = $default_css
     $debug_by_user=ezini('DebugSettings','DebugByUser','site.ini')
     $debug_user_list=ezini('DebugSettings','DebugUserIDList','site.ini')
}
{if or($pagedata.website_toolbar,$pagedata.is_edit)}{set $stylesheets_list=$stylesheets_list|append($user_css.toolbar)}{/if}
{if eq(ezini('DebugSettings','DebugOutput','site.ini'),'enabled')}
{if or(eq($debug_by_user,'disabled'), and(eq($debug_by_user,'enabled'),$current_user.is_logged_in,$debug_user_list|contains($current_user.contentobject_id)))}
{set $stylesheets_list=$stylesheets_list|append($user_css.debug)}
{/if}
{/if}
{if $pagedata.is_edit}{set $stylesheets_list=$stylesheets_list|append($user_css.edit)}{/if}
{ezcss_load(cond($ini_css,$stylesheets_list|merge($ini_css),$stylesheets_list),'screen')}<link rel="stylesheet" type="text/css" href={"stylesheets/print.css"|ezdesign()} media="print" />
<link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href={"fonts/fonts.css"|ezdesign()} media="screen" />
<link rel="stylesheet" type="text/css" href={"stylesheets/stylesheets/main.css"|ezdesign()} media="screen" />
<!--[if IE]><link rel="stylesheet" type="text/css" href={'stylesheets/ie/ie.css'|ezdesign()} /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href={'stylesheets/ie/lte7.css'|ezdesign()} /><![endif]-->
<script type="text/javascript" src="//www.bdg001a.com/js/41124.js" ></script>
<noscript><img src="//www.bdg001a.com/41124.png" style="display:none;" /></noscript>
