{if $has_cufon}<script type="text/javascript">Cufon.now();</script>{/if}
{if first_set($include,false())}{include uri=$include}{/if}
{if $pagedesign.data_map.footer_script.has_content}
<script language="javascript" type="text/javascript">
    {$pagedesign.data_map.footer_script.content}

{if $current_user.contentobject.class_identifier|eq('user')}dataLayer.push({ldelim}'UserNameID': '{$current_user.contentobject.main_node_id}'{rdelim})
{else}dataLayer.push({ldelim}'UserNameID': '{$current_user.contentobject.main_node.data_map.company_name.content} - {$current_user.contentobject.main_node_id}'{rdelim}){/if}

// pageTracker._trackPageview();
</script>
{/if}
