{foreach $subusers as $subuser}
{def $status = cond($subuser.data_map.user_account.content.is_enabled, 'enabled', 'disabled')}
	<li><a href='/user/edit/{$subuser.object.id}' class="subuser-{$status}">{$subuser.name|wash()}</a></li>
{undef $status}
{/foreach}
