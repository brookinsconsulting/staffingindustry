{def $has_ezcomments=ezini('ExtensionSettings','ActiveExtensions','site.ini')|contains('ezcomments')
	 $wrapper=cond(first_set($section,false()),'section','div')
}
{if or($has_ezcomments,is_set($node.data_map.enable_comments))}
<{$wrapper} id="comments"{if first_set('class',false())} class="{$class|implode(' ')}"{/if}>
	{if eq($wrapper,'section')}<header><h1>Comments</h1><a class="button" href="#">Add New Comment</a></header>{else}<h1>Comments</h1>{/if}
	{if $has_ezcomments}
		{attribute_view_gui attribute=$node.data_map[first_set($comments_identifier,'comments')]}
	{elseif and(is_set($node.data_map.enable_comments),$node.data_map.enable_comments.data_int)}
		<div class="content-view-children">
		{foreach fetch_alias('comments',hash('parent_node_id',$node.node_id)) as $comment}
			{node_view_gui view='line' content_node=$comment}
		{/foreach}
		</div>
		{if fetch('content','access',hash('access','create','contentobject',$node,contentclass_id,'comment'))}
			<form method="post" action={"content/action"|sitelink()}>
			<input type="hidden" name="ClassIdentifier" value="comment" />
			<input type="hidden" name="NodeID" value="{$node.object.main_node.node_id}" />
			<input type="hidden" name="ContentLanguageCode" value="{ezini('RegionalSettings','ContentObjectLocale','site.ini')}" />
			<input class="button new_comment" type="submit" name="NewButton" value="{'New comment'|i18n('design/ezwebin/full/article')}" />
		</form>
		{else}
			<p>{if ezmodule('user/register')}{'%login_link_startLog in%login_link_end or %create_link_startcreate a user account%create_link_end to comment.'|i18n('design/ezwebin/full/blog_post',, hash('%login_link_start',concat('<a href="','/user/login'|sitelink(no),'">'),'%login_link_end','</a>','%create_link_start',concat('<a href="', "/user/register"|sitelink(no),'">'), '%create_link_end', '</a>'))}{else}{'%login_link_startLog in%login_link_end to comment.'|i18n('design/ezwebin/article/comments',,hash('%login_link_start',concat('<a href="','/user/login'|sitelink(no),'">'),'%login_link_end','</a>'))}{/if}</p>
		{/if}
	{/if}
</{$wrapper}>
{/if}