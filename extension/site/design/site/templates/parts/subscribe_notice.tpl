{* Xrow Product Parts View  *}
<div id="overlay">
</div>

<div class="attribute-long locked_text"><br/><br/><br/><br/>
<p style="font-weight:bold">This information can only be accessed by qualified members. If you are already a member, please <a href="/user/login" rel="#overlay" class="popup">Sign In</a>. 
If you have signed in, and still don't have access, please call Member Services at 800-950-9496 (North America) or 00 44 207 194 7759 (everywhere else). If you need information about becoming a member, please call us or email us at <a href="mailto:memberservices@staffingindustry.com">memberservices@staffingindustry.com</a>.</p>
<p style="font-weight:bold">Current Members <a href="/user/login" rel="#overlay" class="popup">Sign In</a> | <a href="/site/Subscribe">Become a Member Now</a></p></div>

{literal}

<script type="text/javascript">

$(function(){
	if (OAS_sitepage) {
		$(".locked_text a.popup").each(function(){
			href = $(this).attr('href');
			$(this).attr('href', href + "?OAS_sitepage=" + OAS_sitepage);
		})
	}
})

</script>

{/literal}