{def 
	 $sidebar_node = fetch('content', 'tree', hash('parent_node_id', 2, limit, 1, class_filter_type, include, class_filter_array, array('site_sidebar'))).0
}

{array($sidebar_node.node_id, $current_user.contentobject_id)|debug(keys)}

<nav id="site_sidebar">
	<a href="#" id="site_sidebar_toggle">+</a>
	<div class="toggle">
		{attribute_view_gui attribute=$sidebar_node.data_map.sidebar_content}
	</div>
</nav>
