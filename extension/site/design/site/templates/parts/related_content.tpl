{def $related_content=fetch('ezfind','moreLikeThis',hash('query_type', 'nid',
														'query', $node.node_id,
														'class_id',array('article','news_item','blog_post')
))
	$image_count = 0
	$image_max = 4
	$list_related = array()

	$wrapper=cond(first_set($section,false()),'section','div')
}
{if count($related_content['SearchResult'])}
{def $threshold=15}
<{$wrapper} class="related-content{if first_set('class',false())} {$class|implode(' ')}{/if}">
	{if eq($wrapper,'section')}<header><h1>{first_set($header,'Related Content')}</h1></header>{else}<h1>{first_set($header,'Related Content')}</h1>{/if}

	{foreach $related_content['SearchResult'] as $related_object}
		{if eq($node.node_id, $related_object.node_id)}{continue}{/if}
		{if and($related_object.data_map.image.has_content, le($image_count, $image_max))}
			{set $image_count = inc($image_count)}
			{set-block variable='archive'}
				<a href={$related_object|sitelink()} title="{$related_object.name|wash()}">{attribute_view_gui attribute=$related_object.data_map.image image_class='related_content'}<div>{$related_object.name|wash()}</div></a>
			{/set-block}
			{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$archive}
		{else}
			{set $list_related = $list_related|append($related_object)}
		{/if}
	{/foreach}
	<ul>
	{foreach $list_related as $related_object}
	{if gt($related_object.score_percent,$threshold)}
		<li><a href={$related_object|sitelink()} title="{$related_object.name|wash()}">{$related_object.name|wash()}</a><!--{$related_object.score_percent}--></li>
	{/if}
	{/foreach}
	</ul>
</{$wrapper}>
{/if}