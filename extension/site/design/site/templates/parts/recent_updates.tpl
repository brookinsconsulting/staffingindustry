{* Recent Updates - Articles *}
{set-block scope=root variable=cache_ttl}0{/set-block}

{def $recent_updates = fetch( ezfind, search, hash( query, '*:*', subtree_array, array( 185 ), limit, 6, class_id, 16, sort_by, hash( 'article/publish_date', 'desc' ) ) )
	 $view_comments = ""
	 $view_comments_link = ""
	 $comment_count = 0
 	 $content = false()
}

{$recent_updates['SearchCount']|debug}

{if $recent_updates['SearchCount']|gt(0)}
	<div class="recent-updates">
		<header>
			<h1>Recent Updates</h1>
			<a  class="viewall" href={'/website/updates/published/article/(subtree)/185'|sitelink('no')} title="View All Articles">View All</a>
		</header>
		{foreach $recent_updates['SearchResult'] as $key=>$recent_update}

			{if fetch('user','current_user').is_logged_in}
            	{set $comment_count=fetch('comment','comment_count',hash('contentobject_id',$recent_update.data_map.comments.object.id,'language_id',$recent_update.data_map.comments.language_id,'status' ,1))}                                                                    
    			{if $comment_count|gt( 0 )}
    				{set $view_comments_link = concat( $recent_update.url_alias, '#comments' )|sitelink('no')}
    				{set $view_comments = concat('<a href=', $view_comments_link, '>Comments (', $comment_count, ')</a> &bull; ')}
    			{else}
    				{set $view_comments_link = ""}
    				{set $view_comments = ""}
    			{/if}       		
    		{else}
    			{set $view_comments = ""}
    		{/if}
			{set-block variable='archive'}
				{set $content=$recent_update.data_map.intro.content.output.output_text|explode('"')|implode("'")|html_shorten(80)}
				<h2>{if and(ne($recent_update.data_map.type.data_text,''),ne($recent_update.data_map.type.data_text,'na'))}{attribute_view_gui attribute=$recent_update.data_map.type}{else}Article{/if}</h2>
				<a href={$recent_update|sitelink()} title="{$content}<p>{$view_comments} <a href={$recent_update|sitelink('no')}>Read more</a></p>">
				{if $recent_update.data_map.ml_image.has_content}
					<div class="recent-update-image">
						{attribute_view_gui attribute=$recent_update.data_map.ml_image.content.data_map.image image_class='related_content'}
						{if $recent_update.data_map.caption.has_content}
							<div class="caption" style="width: {$recent_update.data_map.image.content.medium.width}px">
							{attribute_view_gui attribute=$recent_update.data_map.caption}
							</div>
						{/if}
					</div>
				{elseif $recent_update.data_map.image.content.is_valid}
					<div class="recent-update-image">
						{attribute_view_gui attribute=$recent_update.data_map.image image_class='related_content'}
						{if $recent_update.data_map.caption.has_content}
							<div class="caption" style="width: {$recent_update.data_map.image.content.medium.width}px">
							{attribute_view_gui attribute=$recent_update.data_map.caption}
							</div>
						{/if}
					</div>
				{elseif null($recent_update.data_map.image.content.is_valid)}
					<div class="recent-update-image">{attribute_view_gui attribute=fetch('content','node',hash('node_id',40588)).data_map.image image_class='related_content'}</div>
				{/if}

{*
				<div class="recent-update-image">{attribute_view_gui attribute=$recent_update.data_map.image image_class='related_content'}</div>
*}
				<div>{$recent_update.name|wash()|shorten(25)}</div></a>
			{/set-block}
			{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$archive}

		{/foreach}
		{*include uri="design:content/datatype/view/ezxmltags/separator.tpl"*}
	</div>
{/if}