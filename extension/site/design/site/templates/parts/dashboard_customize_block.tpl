{def
	$topic_node = fetch('content', 'node', hash('node_id', 70472))
	$topics = fetch('content', 'list', hash(
		'parent_node_id', $topic_node.node_id,
		'sort_by', $topic_node.sort_array
	))
	$permanent = ezini(concat('DashboardBlock_', $block.identifier), 'Permanent', 'dashboard.ini')
	$customizeable = ezini(concat('DashboardBlock_', $block.identifier), 'Customizeable', 'dashboard.ini')
	$selected_topic = false()
	$selected_topics = ezpreference('dashboard-my-top-articles')|explode(',')
	$dashlimit=cond(
		ezpreference(concat('dashboard-block-', $block.identifier, '-limit'))|ne(''),
		ezpreference(concat('dashboard-block-', $block.identifier, '-limit')),
		"3"
	)
}

<div class="block-controls{if $permanent} permanent{/if}">
	{if $customizeable}<a class="dashboard-customize-link" id="dashboard-customize-{$block.identifier}-link" href="#">customize</a>{/if}
	<div class="dashboard-customize-window" id="dashboard-customize-{$block.identifier}-window">
		<header>
			<h1>Configure My Top Articles Module</h1>
		</header>
		<form id="dashboard-customize-{$block.identifier}-form" method="post" action="/content/dashboard" onsubmit="return dashboardCustomSave{$block.identifier}();">
		{if true()}
			<p class='dashhelp'>This module presents the <input id="limit" name="limit" value="{$dashlimit}" /> most recent Staffing Industry Analysts research articles from the topics selected below each time you visit your dashboard.</p>
			<br />
			<table>
				<tr>
				{foreach $topics as $key => $topic}
					{set $selected_topic = $selected_topics|contains($topic.node_id)}
					<td>
						<input type="checkbox" id="topic_{$topic.node_id}" name="topic_{$topic.node_id}" value="{$topic.node_id}"{if $selected_topic} checked="checked"{/if} />
						<label for="topic_{$topic.node_id}"{if and(first_set($topic.data_map.short_description,false()),$topic.data_map.short_description.has_content)} title='{attribute_view_gui attribute=$topic.data_map.short_description}'{/if}>{$topic.name}</label>
					</td>
					{if $key|mod(3)|eq(2)}
				</tr>
				<tr>
					{/if}
				{/foreach}
				</tr>
			</table>
			<p><a href="#" onclick="return checkAll();">check all</a> <a href="#" onclick="return uncheckAll();">uncheck all</a></p>
		{else}
			<label for="limit">Limit:</label>
			<input id="limit" name="limit" />
			<br />
			<label for="children_view">Children view:</label>
			<select id="children_view" name="children_view">
				<option value="line">Line</option>
				<option value="embed">Embed</option>
				<option value="listitem">List Item</option>
			</select>
		{/if}
			<br />
			<input type="submit" value="Save" />
		</form>
	</div>
	<script>
	{literal}
	$(function(){
		$('#dashboard-customize-{/literal}{$block.identifier}{literal}-link').click(function(){
			$('#dashboard-customize-{/literal}{$block.identifier}{literal}-window').dialog({
				modal: true,
				width: '60%'
			});
			return false;
		});

		// check everything if no preferences set
		if ($('.dashboard-customize-window form input[type=checkbox]:checked').length == 0)
			checkAll();
	});

	function checkAll() {
		$('.dashboard-customize-window form input[type=checkbox]').each(function(){
			$(this).attr('checked', 'checked');
		});
		return false;
	}

	function uncheckAll() {
		$('.dashboard-customize-window form input[type=checkbox]').each(function(){
			$(this).attr('checked', '');
		});
		return false;
	}

	function admin2ppAjaxSavePreference( name, value, reload ) // need for this to wait after post, redefining from admin2pp_utils.js
	{
		var url = jQuery.ez.url.replace( 'ezjscore/', 'user/preferences/' ) + 'set_and_exit/' + name + '/' + value;
		jQuery.post( url, {}, function(){
			if (reload) {
				/* this prompts the user in firefox to resend data
				window.location.reload();
				*/
				window.location.href = "/content/dashboard";
			}
		});
	}

	function dashboardCustomSave{/literal}{$block.identifier}{literal}(){
		$('.dashboard-customize-window input[type=submit]').attr('disabled', 'disabled');
		$('.dashboard-customize-window input[type=submit]').val('Saving...');
		var options = $('#dashboard-customize-{/literal}{$block.identifier}{literal}-form').find('input[type!=submit],select');
		var topics = new Array();
		options.each(function(index){
			var last = false;

			if (index == (options.length - 1))
				last = true;
			
			if ($(this).attr('type') == 'checkbox') {
				if ($(this).attr('checked')) {
					topics.push($(this).attr('name').split('_')[1]);
				}
				if (last) {
					var url = '/user/preferences/set_and_exit/dashboard-my-top-articles/' + topics.join(',');
					$.ajax({
						url: url,
						success: function(){
							$('.dashboard-customize-window input[type=submit]').val('Saved!');
							window.location.href = "/content/dashboard";
						}
					});
				}
			} else {
				admin2ppAjaxSavePreference(
					'dashboard-block-{/literal}{$block.identifier}{literal}-' + $(this).attr('name'),
					$(this).val(),
					last
				);
			}
		});
		return false;
	}
	{/literal}
	</script>
</div>
