{def $language_list=language_switcher($site.uri.original_uri)
	 $a_t_n = $access_type.name|explode("_")[0]
	 $current_language=$language_list[$a_t_n].text|wash()
	 $ucp_hide=eq(cookie('usercontrolpanel'),'hide')
}


<div id="user-control-panel"{if $ucp_hide} style="top:-30px;"{/if}>
	<a class="close" href="#">{cond(eq($ucp_hide,'hide'),'Show Toolbar','Hide Toolbar')}</a>

	<div id="user-control-panel-inner">
		<ul id="user-panel" class="menu horizontal">

			<li id="language-switcher">
				<strong>Current Region:</strong>

				<div id="languages">
					<form id='lang_switch'>
						{foreach $language_list as $siteaccess=>$language}
							<input{if array('shop', 'xrowecommerce')|contains($pagedata.module_parameters.module_name)} disabled='true'{/if} type='radio' name='cur_lang' rel='{$requested_uri_string|trim('/')}' value='{$siteaccess}' {if eq($siteaccess,$a_t_n)} checked='true'{/if}/><label>{$language.text|wash()}</label>
						{/foreach}
					</form>
				</div>
				
			</li>
		</ul>
		<nav id="utility">
			<ul class="menu horizontal">
				
	{if $current_user.is_logged_in}
				<li><a href="/user/logout"><strong>Logout:</strong> {$current_user.contentobject.data_map.first_name.content|wash()} {$current_user.contentobject.data_map.last_name.content|wash()}</a></li>
	{else}
				<li class="first"><a href="/user/login">Login</a></li>
				<li class="last"><a class="nav-link" href="/user/register">Register</a></li>
	{/if}
			</ul>
	{*
			{include uri="design:menu/menubar.tpl"
				root_node=fetch('content','node',hash('node_id',$indexpage))
				show_header=false()
				orientation='horizontal'
				delimiter='|'
				attribute_filter=array('and', array('priority','between',array(100,110)))
				menu=hash('prepend',hash(
						'Login','/user/login'
					))
			}
	*}
		</nav>
	</div>
</div>
