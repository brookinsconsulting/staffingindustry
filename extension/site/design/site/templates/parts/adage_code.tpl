{def $ad_container_node = false()}
{if $current_node}
	{set $ad_container_node = $current_node}
{/if}
{if $ad_container_node|not}
	{def $ad_container_url_alias = false()}
	{if ezhttp_hasvariable( 'OriginalRedirectURI', 'post' )}
		{set $ad_container_url_alias = ezhttp( 'OriginalRedirectURI', 'post' )}
	{else}
		{set $ad_container_url_alias = ezservervars()['REQUEST_URI']}
	{/if}
	{set $ad_container_url_alias = $ad_container_url_alias|trim( '/' )}
	{if $ad_container_url_alias}
		{set $ad_container_node = fetch( 'site', 'fetch_node_by_url', hash( 'url', $ad_container_url_alias ) )}
	{/if}
{/if}

{if and( $ad_container_node|not, module_params()['function_name']|eq('login') )}
	{if ezservervars()['REQUEST_URI']|contains('layout/set/overlay')}
		{def $oas_sitepage = 'www2.staffingindustry.com/loginpopup'}
		{if ezgetvars()['OAS_sitepage']}
		{set $oas_sitepage = ezgetvars()['OAS_sitepage']}
		{/if}
	{else}
		{def $oas_sitepage = 'www2.staffingindustry.com/login'}
	{/if}
{else}
	{def $oas_sitepage = cond(array(2, 70419)|contains($ad_container_node.node_id), 'www2.staffingindustry.com/home', 'www2.staffingindustry.com')}

	{foreach $ad_container_node.path|reverse()|prepend($ad_container_node) as $path_node}
		{def
			$zones = fetch('content', 'list', hash(
				'parent_node_id', $path_node.node_id,
				'limitation', array(),
				'class_filter_type', 'include',
				'class_filter_array', array('ad_zone')
			))
		}
		{if count($zones)|gt(0)}
			{set $oas_sitepage = concat($oas_sitepage, '/', $zones[0].data_map.zone.content)}
			{undef $zones}
			{break}
		{/if}
		{undef $zones}
	{/foreach}
{/if}

{* put together a big string containing all elements we expect realmedia custom tags to be *}
{def $listpos_content = $module_result.content|append($pagedata.persistent_variable.extrainfo)}
{* on some pages (Research & Publications) $infoboxes is an array *}
{if $infoboxes|is_array()}
	{set $listpos_content = $listpos_content|append($infobox_html)}
{* on the homepage, it's a string *}
{elseif $infoboxes|is_string()}
	{set $listpos_content = $listpos_content|append($infoboxes)}
{/if}

{ezscript_require('adage.js')}

<script type="text/javascript">

	//configuration
	{if ezservervars()['HTTP_X_FORWARDED_PROTO']|ne('https')}
		OAS_url = 'http://oascentral.staffingindustry.com/RealMedia/ads/';
	{else}
		OAS_url = 'https://oasc10.247realmedia.com/RealMedia/ads/';
	{/if}
	OAS_sitepage = '{$oas_sitepage}';
	OAS_listpos = '{$listpos_content|extract_oas_listpos()}';
	OAS_query = '';
	OAS_target = '_top';
	//end of configuration
	{literal}

	OAS_version = 10;

	OAS_rn = '001234567890'; OAS_rns = '1234567890';
	OAS_rn = new String (Math.random()); OAS_rns = OAS_rn.substring (2, 11);

	function OAS_NORMAL(pos) {
		document.write('<a href="' + OAS_url + 'click_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + '?' + OAS_query + '" target=' + OAS_target + '>');
		document.write('<img src="' + OAS_url + 'adstream_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '!' + pos + '?' + OAS_query + '" /></a>');
	}

	OAS_version = 11;

	if (navigator.userAgent.indexOf('Mozilla/3') != -1 || navigator.userAgent.indexOf('Mozilla/4.0 WebTV') != -1)
		OAS_version = 10;

	if (OAS_version >= 11)
		document.write('<scr' + 'ipt type="text/javascript" src="' + OAS_url + 'adstream_mjx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + '?' + OAS_query + '"><\/script>');

	function OAS_AD(pos) {
		if (OAS_version >= 11) {
			OAS_RICH(pos);
		} else {
			OAS_NORMAL(pos);
		}
	}

{/literal}
</script>
