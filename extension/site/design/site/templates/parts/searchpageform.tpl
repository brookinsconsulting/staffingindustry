<form action={"/content/search"|sitelink()} method="get">
{if first_set($hidden,false())}
{foreach $hidden as $key=>$value}
	<input type="hidden" name="{$key}" value="{$value}" />
{/foreach}
{/if}
	<fieldset>
		<input class="searchtext" type="text" name="SearchText"{if first_set($placeholder,false())} placeholder="{$placeholder|wash()}"{/if}{if first_set($search_text,false())} value="{$search_text|wash()}"{/if} />
		<input class="searchbutton" type="image" name="SearchButton" alt="Search" src={'images/search.png'|ezdesign()} />
                <label id="searchCreationTimeDateFilterLabel">Publish Date:</label>
                <select name="dateFilter" id="searchCreationTimeDateFilter" onchange="this.form.submit()">
                    <option value=""{if ezhttp('dateFilter', 'get')|eq('')} selected{/if}>Any</option>
                    <option value="1"{if ezhttp('dateFilter', 'get')|eq('1')} selected{/if}>Last day</option>
                    <option value="2"{if ezhttp('dateFilter', 'get')|eq('2')} selected{/if}>Last week</option>
                    <option value="3"{if ezhttp('dateFilter', 'get')|eq('3')} selected{/if}>Last month</option>
                    <option value="4"{if ezhttp('dateFilter', 'get')|eq('4')} selected{/if}>Last three months</option>
                    <option value="5"{if ezhttp('dateFilter', 'get')|eq('5')} selected{/if}>Last year</option>
                </select>
	</fieldset>
</form>
