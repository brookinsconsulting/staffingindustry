<div class="message-error">
<p><br />You are accessing protected content.<br /><br />

If you have already logged in, you do not have access privilege to this content. If you have not logged in, please log in below. Contact Member Services at 800-950-9496 if you have further questions.</p>
</div>
{if eq( $current_user.contentobject_id, $anonymous_user_id )}
	{if $embed_content}
	{$embed_content}
	{else}
		<form method="post" action={'/user/login/'|ezurl}>
		<p>{'Click the "Log in" button in order to log in.'|i18n( 'design/admin/error/kernel' )}</p>
		<div class="buttonblock">
		<input class="button" type="submit" name="LoginButton" value="{'Login'|i18n( 'design/admin/error/kernel','Button')}" />
		</div>
		<input type="hidden" name="Login" value="" />
		<input type="hidden" name="Password" value="" />
		<input type="hidden" name="RedirectURI" value="{$redirect_uri}" />
		</form>
	{/if}
{/if}