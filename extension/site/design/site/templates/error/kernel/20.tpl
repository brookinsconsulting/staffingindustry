<header><h2>Oops! We’re sorry, the page you have requested cannot be found.</h2></header>
<p>The content you attempted to access may not be available in the region of your choice. Please use the links below to continue or check the URL and try again.</p>
<ul>
	<li><a href='/'>Homepage</a></li>
	<li><a href='/content/view/sitemap/2'>Sitemap</a></li>
	<li><a href='/content/search'>Search</a></li>
</ul>
{if $embed_content}{$embed_content}{/if}