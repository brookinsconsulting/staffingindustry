{* override template *}
{def $content_object_version=fetch(notification,event_content,hash(event_id,$collection_item.event_id))}
{if $content_object_version}
{def $object_URL= concat("http://",ezini("SiteSettings","SiteURL")|trim('/'),$content_object_version.contentobject.main_node_id|sitelink('no',false()))}
<p> {$content_object_version.contentobject.name} - <a href="{$object_URL}">{$object_URL}</a> </p>
{/if}
