{def $role_list=$current_user.role_id_list}<!DOCTYPE html>
<html lang="{$site.http_equiv.Content-language|wash()}" xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$site.http_equiv.Content-language|wash()}">

{if and($module_result.uri|contains('content/download')|not, $module_result.uri|contains('cookies=disabled')|not)}
    {if or($role_list|contains(23), $role_list|contains(192), $role_list|contains(197))}
        {if not(has_cookie('american_access'))}{cookie('american_access',$current_user.contentobject_id, sum(currentdate(), 3600|mul(30)), '/')}{/if}
    {/if}
    {if or($role_list|contains(25), $role_list|contains(195), $role_list|contains(199))}
        {if not(has_cookie('european_access'))}{cookie('european_access',$current_user.contentobject_id, sum(currentdate(), 3600|mul(30)), '/')}{/if}
    {/if}
{/if}
{if not(is_set($extra_cache_key))}{def $extra_cache_key=''}{/if}
{if ezhttp( 'SearchText', 'get', true() )}
    {set $extra_cache_key = concat( $extra_cache_key, ';', ezhttp( 'SearchText', 'get' ) )}
{/if}
{def $pagedata = pagedata()
     $basket_is_empty = cond($current_user.is_logged_in,fetch(shop,basket).is_empty,1)
     $basket_count = count(fetch(shop, basket).items)
     $locales = fetch('content','translation_list')
     $pagedesign = $pagedata.template_look
     $indexpage = $pagedata.root_node_id
     $content_info = first_set($module_result.content_info, false())
     $persistent_infoboxes = array()
     $cufon_fonts=cond(ezini_hasvariable('JavaScriptSettings','CufonFontsList','design.ini'),ezini('JavaScriptSettings','CufonFontsList','design.ini'),false())
     $has_cufon=and($cufon_fonts,count($cufon_fonts))
}

<head>
{include uri="design:page_head.tpl" enable_link=false()}

{include uri='design:page/head_style.tpl'}
{include uri='design:page/head_script.tpl'}

{if $role_list|contains(6)}
<script>
$(function(){ldelim}
    $('.homepage .custom-tag-tabbox li:nth-child(2) a').click();
{rdelim});
</script>
{/if}
</head>

{set-block variable=$infobox_html}
    {if $pagedata.has_extrainfo}
        <aside id="extrainfo" role="complementary">
        {if and($pagedata.has_sidemenu,eq($pagedata.sidemenu_position,'right'))}{include uri="design:menu/sidemenubar.tpl" show_header=false()}{/if}
        {include uri="design:parts/extrainfo.tpl" infoboxes=$infoboxes infoboxes_only=false()}
        </aside>
    {/if}
{/set-block}

<body class="{$pagedata.site_classes|implode(' ')}{if eq($pagedata.persistent_variable.staffing_users,true())} staffing-users{/if}">
    {literal}
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N8VZ8W"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N8VZ8W');</script>
        <!-- End Google Tag Manager -->
    {/literal}
    {include infobox_html=$infobox_html uri='design:parts/adage_code.tpl'}

<div id="overlay-inAbox">
    <div class="wrapper">
        <div class="toolbar"><a class="close" href="#"><span>x</span> close</a></div>
        {include uri='design:user/login.tpl'}
        {literal}
            <script type="text/javascript">$('#overlay-inAbox .wrapper .close').click(function() {$('#overlay-inAbox').hide().css('display', 'none');});</script>
        {/literal}
    </div>
    <div class="scr_overlay"></div>
</div>

        {include uri="design:page_header.tpl" logo_alt=$site.title|wash() logo_width=150 logo_height=120}

        <nav id="secondary-navigation">
            <ul class="menu horizontal">
                <li class=" first link_ob_class_folder"><a href="/Conferences-Webinars/Conferences">Conferences</a></li>
                <li class="link_ob_class_folder"><a href="/Research-Publications/ ">Research/Publications</a></li>
                <li class="last link_ob_class_folder"><a href="/About/Analysts ">Advisory</a></li>
            </ul>
        </nav>
        <section id="site-columns">
            <div class="document">
                {if $pagedata.website_toolbar}
                    {include uri='design:page_toolbar.tpl'}
                {/if}
                {if and($pagedata.show_path, eq($pagedata.has_sidebar, false()) )}
                    {include uri='design:page_toppath.tpl' delimiter='&raquo;'}
                {/if}
                {if and(is_set($pagedata.persistent_variable.searchbar),$pagedata.persistent_variable.searchbar)}
                    {$pagedata.persistent_variable.searchbar}
                {/if}
                {if $pagedata.has_sidebar}
                    <aside id="sidebar" role="complementary">
                        {if and($pagedata.has_sidemenu,eq($pagedata.sidemenu_position,'left'))}{include uri="design:menu/sidemenubar.tpl" show_header=true() return=true()}{/if}
                        {if and(is_set($pagedata.persistent_variable.sidebar), $pagedata.persistent_variable.sidebar)}
                            <section>{$pagedata.persistent_variable.sidebar}</section>
                        {/if}
                    </aside>
                {/if}
                    <section class="{$pagedata.content_classes|implode(' ')}" id="site-main-content" role="main">
                        {if and($pagedata.show_path, eq($pagedata.has_sidebar, true()) )}
                            {include uri='design:page_toppath.tpl' delimiter='&raquo;'}
                        {/if}


                        {$module_result.content}

                    </section>
                    {$infobox_html}

                {if eq($pagedata.class_identifier,'article')}
                    {include uri="design:parts/recent_updates.tpl" limit=6}
                {/if}
                {if and(is_set($pagedata.persistent_variable.bottomarea), $pagedata.persistent_variable.bottomarea)}
                    <section id="bottomarea">{$pagedata.persistent_variable.bottomarea}</section>
                {/if}
                {if $pagedata.show_path}
                    {include uri='design:page_toppath.tpl' delimiter='&raquo;'}
                {/if}
            </div>
        </section>

    {include uri="design:page_footer.tpl"}

    <div id="subfooter">
        <div class="subfooter-container">
            <p>Copyright &#169;{currentdate()|datetime(custom, '%Y')} Crain Communications Inc | <a href="/site_member/Privacy-Policy/">Privacy</a> | <a href="/content/view/sitemap/2">Sitemap</a></p>
        </div>
    </div>

    {* This comment will be replaced with actual debug report (if debug is on). *}
    <!--DEBUG_REPORT-->

{production_only()}
{* <a title="Web Statistics" href="http://clicky.com/100738882"><img alt="Web Statistics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a> *}
<script type="text/javascript">
{literal}
var clicky_custom = clicky_custom || {};
clicky_custom.visitor = { {/literal}{if $current_user.is_logged_in}
  username: '{$current_user.login}',
  email: '{$current_user.email}'
{/if}{literal} };
var clicky_site_ids = clicky_site_ids || [];
clicky_site_ids.push(100738882);
(function() {
  var s = document.createElement('script');
  s.type = 'text/javascript';
  s.async = true;
  s.src = '//static.getclicky.com/js';
  ( document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild( s );
})();

// Give 'Tags' infoboxes a class
$(".class-blog-post .infobox h1:contains('Tags')").parent().parent().addClass('tags');
$(".class-blog .infobox h1:contains('Tags')").parent().parent().addClass('tags');
{/literal}
</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100738882ns.gif" /></p></noscript>
</body>

</html>
