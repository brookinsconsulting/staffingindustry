{def $footer_node = get_footer(true())}
<footer>
    <div class="body-container">
       {attribute_view_gui attribute=$footer_node.data_map.content}
    </div>
</footer>

{include uri="design:page/footer_scripts.tpl"}
