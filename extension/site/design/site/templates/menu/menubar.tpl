{* Menubar *}

{cache-block keys=array($root_node.node_id, $current_user.contentobject_id)}

{def $siteRootID=ezini('NodeSettings', 'RootNode', 'content.ini')
	 $rootNode = first_set($root_node, fetch('content','node',hash('node_id',$siteRootID)))
	 $menuDepth = first_set($menu.depth,1)
	 $menuIdentifier = cond( eq($rootNode.node_id, $siteRootID), 'TopIdentifierList', 'LeftIdentifierList')
	 $classFilterType = first_set($class_filter.type, 'include')
	 $classFilterArray = first_set($class_filter.array, ezini('MenuContentSettings', $menuIdentifier, 'menu.ini'))
	 $menuItems=fetch('content', 'list', hash('parent_node_id', $rootNode.node_id,
						'sort_by', $rootNode.sort_array,
						'class_filter_type', $classFilterType,
						'class_filter_array', cond( is_set($class_filter_array), $classFilterArray|merge($class_filter_array), $classFilterArray ),
						'attribute_filter', first_set($attribute_filter, false()),
						'ignore_visibility',false() ))
	 $menuItemChildren = false()
	 $itemClass=array()
	 $itemInclude=false()
}

{if and(is_set($include_root_node),ne($include_root_node,false()))}
	{set $menuItems = cond(or(and(is_boolean($include_root_node),$include_root_node), eq($include_root_node,'prepend')) , $menuItems|prepend($rootNode) , $menuItems|append($rootNode))}
{/if}
{if eq($rootNode.node_id,$current_node_id)}{set $link_header=false()}{/if}
{if first_set($show_header,true())}{if and(is_set($link_header), $link_header)}<a href={$rootNode|sitelink()}>{/if}<h2>{first_set($menu.header, $rootNode.name|wash())}{if and(is_set($link_header), $link_header)}</a>{/if}</h2>{/if}
	<ul{if and(is_set($menu.id), $menu.id)} id="{$menu.id}"{/if} class="menu {cond(first_set($orientation,false()),cond(is_string($orientation),$orientation,'horizontal'),'vertical')}">

	{if first_set($menu.prepend,false())}
		{foreach $menu.prepend as $text=>$href}
		<li><a href="{$href}">{$text}</a></li>
		{/foreach}
	{/if}

	{foreach $menuItems as $menuItemKey => $menuItem}

	{if is_set($item_include.condition)}
		{switch match=$item_include.condition.type}
			{case match='node_id'}
				{if is_array($item_include.condition.value)}
					{set $itemInclude = cond($item_include.condition.value|contains($menuItem.node_id),true(),false())}
				{else}
					{set $itemInclude = eq($item_include.condition.value,$menuItem.node_id)}
				{/if}
			{/case}
			{case match='all'}
				{set $itemIncude=true()}
			{/case}
		{/switch}
	{/if}


		{if eq($menuItemKey,0)}{set $itemClass = $itemClass|append('first')}{/if}
		{if eq($menuItemKey,sub(count($menuItems),1))}{set $itemClass = $itemClass|append('last')}{/if}
		{set $itemClass=$itemClass|append(concat('link_ob_class_',$menuItem.class_identifier))}

		{if and(is_set($menuItems[sum(1,$menuItemKey)]), $menuItems[sum(1,$menuItemKey)].class_identifier|eq('nav_divider'))}
			{set $itemClass=$itemClass|append('next_is_divider')}
		{/if}
		<li{if count($itemClass)} class="{$itemClass|implode(' ')}"{/if}>{if eq( $menuItem.data_map.disable_link.data_int, 1 )}<p class="global-menu-item nav-link {if ne( $menuItem.data_map.no_submenu.data_int, 1 )} linked{/if}">{$menuItem.name|wash()}</p>{else}{if $menuItem.class_identifier|ne('nav_divider')}<a{if $itemInclude} class="{$item_include.class}" rel="#{$menuItem.name|wash()|explode(' ')|implode('-')|downcase()}-tab"{else} class="nav-link linked"{/if} href={$menuItem|sitelink()}{if and(eq($menuItem.class_identifier,'link'), $menuItem.data_map.open_in_new_window.data_int)} target="_blank"{/if}>{/if}{cond(eq($menuItem.node_id,$#indexpage),'Home',$menuItem.name|wash())}{if $menuItem.class_identifier|ne('nav_divider')}</a>{/if}{/if}

{if first_set($submenu,false())}
	{if fetch('content', 'list_count', hash('parent_node_id', $menuItem.node_id,
					'sort_by', $menuItem.sort_array,
					'class_filter_type', $classFilterType,
					'class_filter_array', $classFilterArray))
	}
		{if eq( $menuItem.node_id,$rootNode.node_id )}
			{set $menuItemChildren = fetch(
				'content', 'list',
				hash(
					'parent_node_id', $menuItem.node_id,
					'sort_by', $menuItem.sort_array,
					'class_filter_type', $classFilterType,
					'class_filter_array', array( 'link' )
				)
			)}
		{else}
			{set $menuItemChildren = fetch( 'content', 'list', hash( 'parent_node_id', $menuItem.node_id,
											 'sort_by', $menuItem.sort_array,
											 'class_filter_type', $classFilterType,
											 'class_filter_array', $classFilterArray
					) )}
		{/if}
		<ul class="menu vertical">
		{foreach $menuItemChildren as $submenuItemKey => $submenuItem}
                        {if eq( $submenuItem.data_map.hide_from.data_int, 1 )}
                            {continue}
                        {/if}
			<li><a href={$submenuItem.node_id|sitelink()}{if and(eq($submenuItem.class_identifier,'link'), $submenuItem.data_map.open_in_new_window.data_int)} target="_blank"{/if}>{$submenuItem.name|wash()}</a></li>
		{/foreach}
		</ul>
	{/if}
{/if}

			</li>
		{if and($itemInclude,is_set($item_include.template) )}{include uri=$item_include.template node=$menuItem}{/if}
	{/foreach}
	</ul>
	
{/cache-block}