{def 
	 $cu = fetch(content, object, hash(object_id, $current_user.contentobject_id))
	 $max_subs = $cu.data_map.max_subs.content
	$subusers_count = fetch(content, list_count, hash(
	 	parent_node_id, $cu.main_node.node_id,
		class_filter_type, 'include',
		class_filter_array, array('client')
	))
	 $role_list=$current_user.role_id_list
	 $subs_permitted  = 0
	 $disable_template_cache = rand()
}

{if or($role_list|contains(23), $role_list|contains(192), $role_list|contains(197), $role_list|contains(25), $role_list|contains(195), $role_list|contains(199))}
	{set $subs_permitted = sub($max_subs, $sub_us_c)}
{/if}
<section class="dashboard-menu preferences">
	<header><h1>Preferences</h1></header>
	<h2>Dashboard</h2>
	<ul class="menu vertical dashboard">
		<li><a href="/user/password">Change password</a></li>
		<li><a href="/content/dashboard">Dashboard</a></li>
		<li><a href="/user/edit">Edit Profile</a></li>
		<li class="hidden"><a href="/notification/settings">My notifications</a></li>
		<li><a href="/order/history">Order History</a></li>
		<li><a id="admin2pp-db-load" href="/content/dashboard">Add content/modules</a></li>
		<li><a href={'/user/preferences/set/admin2pp_dashboard_blocks/'|ezurl()}>{'Restore original dashboard'|i18n( 'admin2pp/dashboard' )}</a></li>
	</ul>
	{include uri='design:content/datatype/view/ezxmltags/separator.tpl'}
	<h2>Manage Account</h2>
	<ul class="menu vertical manage-account">
		<li><a href="/user/edit">Email Preferences</a></li>
		{*<li><a href="#">Manage Users</a></li>*}
		<li>{if $subs_permitted}<a id="create-user" href="#" rel="form[name=CreateLanguages]">Create New User</a>{else}<span>Create New User</span>{/if}
			<form method="post" action="/content/action" name="CreateLanguages">
			{def $parent_node_id=fetch('content','object',hash('object_id',fetch('user','current_user').contentobject_id)).main_node_id}
				<input type="hidden" value="eng-US" name="ContentLanguageCode">
				<input type="hidden" value="{$parent_node_id}" name="NodeID">
				<input type="hidden" value="56" name="ClassID">
				<input type="hidden" value="/content/dashboard/{$parent_node_id}" name="RedirectIfDiscarded">
				<input type="hidden" value="OK" name="NewButton">
			</form>
		</li>
		<li><a href="/user/edit">Change Company Information</a></li>
	{if $subusers_count}
		<li>Company Users{if $max_subs|ne(0)} (max {$max_subs}){/if}:
			<script>
			{literal}
				$(function(){
					var limit = 10, offset = 0, total = {/literal}{$subusers_count}{literal};
					if (total < limit) {
						limit = total;
					}
					updateSubUsersList(limit, offset, total);

					$('.first').click(function(){
						if (offset != 0) {
							offset = 0;
							updateSubUsersList(limit, offset, total);
						}
						return false;
					});
					$('.prev').click(function(){
						if ((offset - limit) >= 0) {
							offset = offset - limit;
							updateSubUsersList(limit, offset, total);
						}
						return false;
					});
					$('.next').click(function(){
						if ((offset + limit) < total) {
							offset = offset + limit;
							updateSubUsersList(limit, offset, total);
						}
						return false;
					});
					$('.last').click(function(){
						if (offset + limit != total) {
							offset = {/literal}{$subusers_count}{literal} - limit;
							updateSubUsersList(limit, offset, total);
						}
						return false;
					});
				});

				function updateSubUsersList(limit, offset, total) {
					var userid = {/literal}{$current_user.contentobject_id}{literal};
					var pageinfo = (offset + 1) + " - " + (offset + limit) + " of " + (total);

					$('.first, .prev, .next, .last').css({
						'cursor': 'pointer',
						'text-decoration': 'underline',
						'color': '#C4122F'
					});

					if (offset == 0) {
						$('.first, .prev').css({
							'cursor': 'default',
							'text-decoration': 'none',
							'color': 'black'
						});
					} else if (offset + limit == total) {
						$('.next, .last').css({
							'cursor': 'default',
							'text-decoration': 'none',
							'color': 'black'
						});
					}

					$('#subusers-target').html('<li>Loading...</li>');
					$.get('/site_member/site/listsubusers/'+userid+'/'+limit+'/'+offset, function(data) {
						$('#subusers-target').html(data);
						$('.page-info').html(pageinfo);
					});
				}
			{/literal}
			</script>
			<div id="subusers-controls">
				<div class="page-info"></div>
				<a href="#" class="first">first</a>&nbsp;
				<a href="#" class="prev">prev</a>&nbsp;
				<a href="#" class="next">next</a>&nbsp;
				<a href="#" class="last">last</a>&nbsp;
			</div>
			<ul id="subusers-target"></ul>
			<div id="subusers-controls">
				<div class="page-info"></div>
				<a href="#" class="first">first</a>&nbsp;
				<a href="#" class="prev">prev</a>&nbsp;
				<a href="#" class="next">next</a>&nbsp;
				<a href="#" class="last">last</a>&nbsp;
			</div>
		</li>
	{/if}
	</ul>
	{include uri='design:content/datatype/view/ezxmltags/separator.tpl'}
	<h2>Subscriptions</h2>
	<ul class="menu vertical subscriptions">
		<li><a href={42348|sitelink()}>Manage</a></li>
		<li><a href={188|sitelink()}>Renew</a></li>
		{*
		<li><a href="#">Recommend</a></li>
		*}
	</ul>
</section>
<section class="dashboard-menu support">
	<header><h1>Support</h1></header>
	{*
	<h2>Topic</h2>
	*}
	<ul class="menu vertical topic">
		<li><a href={42347|sitelink()}>Contact Us</a></li>
		{*
		<li><a href="#">Subscriptions</a></li>
		<li><a href="#">Event</a></li>
		<li><a href="#">Other</a></li>
		*}
	</ul>
	{*
	{include uri='design:content/datatype/view/ezxmltags/separator.tpl'}
	<h2>Dashboard</h2>
	<ul class="menu vertical dashboard">
		<li><a href="#">Using this site</a></li>
		<li><a href="#">About the dashboard</a></li>
	</ul>
	*}
</section>
