{set-block scope=local variable=$content}
{if not($node.data_map.hide_header.data_int)}<header class="{attribute_view_gui attribute=$node.data_map.header_alignment options=array('text-align-left','text-align-center','text-align-right')}"><h1>{$node.name|wash()}</h1>{if $node.data_map.sub_header.has_content}<h2>{$node.data_map.sub_header.content|wash()}</h2>{/if}</header>{/if}
{if $node.data_map.image.has_content}<div class="frame">{attribute_view_gui attribute=$node.data_map.image image_class='infoboximage' css_class='attribute-image'}</div>{/if}
<section class="infobox-content {if $ad}ads{/if}">
	{attribute_view_gui attribute=$node.object.data_map.content}
	{if $node.data_map.external_link.has_content}
		<div class="infobox-link"><a href="{$node.data_map.external_link.content}">{$node.data_map.external_link.data_text|wash()}</a></div>
	{/if}
</section>
{if or($node.object.can_edit, $node.object.can_remove)}
	<div class="controls">
		<form action={"/content/action"|ezroot()} method="post">
		{if $node.object.can_edit}
			<input type="image" name="EditButton" src={"edit-infobox-ico.gif"|ezimage()} alt="Edit" />
			<input type="hidden" name="ContentObjectLanguageCode" value="{$node.object.current_language}" />
		{/if}
		{if $node.object.can_remove}
			<input type="image" name="ActionRemove" src={"trash-infobox-ico.gif"|ezimage()} alt="Remove" />
		{/if}
			<input type="hidden" name="ContentObjectID" value="{$node.object.id}" />
			<input type="hidden" name="NodeID" value="{$node.node_id}" />
			<input type="hidden" name="ContentNodeID" value="{$node.node_id}" />
		</form>
	</div>
{/if}
{/set-block}
<aside class="module infobox{if $content|contains('with_realmedia')} infobox_ad{/if}">
	{$content}
</aside>