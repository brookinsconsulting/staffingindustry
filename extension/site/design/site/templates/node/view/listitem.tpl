{* Listitem Template *}
<a href={$node|sitelink()}{if and(eq($node.class_identifier, 'link'), $node.data_map.open_in_new_window.content)} target="_blank"{/if}>{$node.name|wash}</a>
