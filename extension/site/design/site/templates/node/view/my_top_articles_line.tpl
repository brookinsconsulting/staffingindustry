{* Article - Embed View *}
{def $show_image=and(cond(is_set($hide_image),not($hide_image),true()), or($node.data_map.image.has_content, $node.data_map.ml_image.has_content))}
<div class="content-view-embed class-article{if $show_image} line-image{/if}">
{set $show_image = false()}
{if $show_image}
	{set-block variable='left_column'}
				{if $node.data_map.ml_image.content}
					<div class="attribute-image ml1">{attribute_view_gui attribute=$node.data_map.ml_image.content.data_map.image href=$node|sitelink() image_class='articlethumbnail'}</div>
				{elseif $node.data_map.image.has_content}
					<div class="attribute-image i1">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='articlethumbnail'}</div>
				{/if}
	{/set-block}
{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$left_column class='articlethumbnail'}
{/if}

{set-block variable='right_column'}
{*
"{$node.data_map.type.data_text}"
*}
	{set-block variable=article_type_output}
		{attribute_view_gui attribute=$node.data_map.type}
	{/set-block}
	<h2><a href={$node|sitelink()}>{if and(ne($node.data_map.type.data_text,''), ne($node.data_map.type.data_text|downcase,'na'), $article_type_output|trim|ne(''))}<span class="parent">{$article_type_output} -</span> {/if}{$node.data_map.title.content|wash()}</a></h2>
		{if $node.data_map.ml_image.content}
			<div class="attribute-image ml2">{attribute_view_gui attribute=$node.data_map.ml_image.content.data_map.image href=$node|sitelink() image_class='articlethumbnail'}</div>
		{elseif $node.data_map.image.has_content}
			<div class="attribute-image i2">{attribute_view_gui attribute=$node.data_map.image href=$node|sitelink() image_class='articlethumbnail'}</div>
		{/if}
	{attribute_view_gui attribute=$node.data_map.intro htmlshorten=400}
	<h3 class="date">
		{if $node.data_map.publish_date.content.timestamp}
			{$node.data_map.publish_date.content.timestamp|datetime('custom','%M. %j, %Y')}
		{else}
			{$node.object.published|datetime('custom','%M. %j, %Y')}
		{/if}
	</h3>
	{if $node.data_map.tags.has_content}<div class="tags">Tags: {attribute_view_gui attribute=$node.data_map.tags}</div>{/if}
	{*<div class="author">Analyst: {$node.object.owner.name|wash()}</div>*}
		{def $analyst_list=cond($node.data_map.analyst.has_content,$node.data_map.analyst.content.relation_list,false())}
		{if $analyst_list}
			{def $analyst_node=false()
				 $analyst_link=false()
				 $recent_list=false()
				 $recent1=false()
				 $recent2=false()
			}
			{if is_array($analyst_list)}
			{def $ala=$analyst_list}
				{foreach $ala as $key=>$item}
				{if $item}
					{set $item=fetch('content','object',hash('object_id',$item.contentobject_id))
						 $analyst_list=cond(eq($key,0),array($item),$analyst_list|append($item))
					}
				{/if}
				{/foreach}
			{else}
			{set $analyst_list=array()}
			{/if}
		{/if}
		{if $node.data_map.authors.data_text}
		{def $author_list=array()}
			{foreach $node.data_map.authors.content.author_list as $author}
				{if ne($author['id'],10)}{set $author_list=$author_list|append($author)}{/if}
			{/foreach}
		{/if}
		{if and($analyst_list,count($analyst_list))}<div class="author">Analyst: {foreach $analyst_list as $analyst}<a href="#">{$analyst.name|wash()}</a>{delimiter} {/delimiter}{/foreach}</div>{/if}
		{if count($author_list)}<div class="author">Guest Author: {foreach $author_list as $author}<a href="#">{$author.name|wash()}</a>{delimiter} {/delimiter}{/foreach}</div>{/if}

{/set-block}
{if $show_image}
	{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$right_column class='r-articlethumbnail' position='right'}
{else}
	{$right_column}
{/if}
</div>
