<div class="content-view-line class-comment">
{if $comment.title}
	<h2>{$comment.title|strip_tags()|wash()}</h2>
{/if}
	<h4 class="author"><span class="author-name">{$comment.name|wash}</span><span class="date">{$comment.created|l10n('shortdatetime')}</span></h4>
	<p>{$comment.text|strip_tags()|wash()|nl2br()}</p>
{def $can_edit=fetch('comment','has_access_to_function',hash('function','edit',
																'contentobject',$contentobject,
																'language_code',$language_code,
																'comment',$comment,
																'scope','role',
																'node',$node))
	$can_delete=fetch('comment','has_access_to_function',hash('function','delete',
																'contentobject',$contentobject,
																'language_code',$language_code,
																'comment',$comment,
																'scope','role',
																'node',$node))
	$user_display_limit_class=concat(' class="limitdisplay-user limitdisplay-user-',$comment.user_id,'"')}
{if or($can_edit,$can_self_edit,$can_delete,$can_self_delete)}
	<div class="ezcom-comment-tool">
		{if or($can_edit,$can_self_edit)}
			{if and($can_self_edit,not($can_edit))}
				{def $displayAttribute=$user_display_limit_class}
			{else}
				{def $displayAttribute=''}
			{/if}
			<span{$displayAttribute}>
				<a href={concat('/comment/edit/',$comment.id)|sitelink()}>{'Edit'|i18n('ezcomments/comment/view')}</a>
			</span>
			{undef $displayAttribute}
		{/if}
		{if or($can_delete,$can_self_delete)}
			{if and($can_self_delete,not($can_delete))}
				{def $displayAttribute=$user_display_limit_class}
			{else}
				{def $displayAttribute=''}
			{/if}
			<span {$displayAttribute}>
				<a href={concat('/comment/delete/',$comment.id)|sitelink()}>
					{'Delete'|i18n('ezcomments/comment/view')}
				</a>
			</span>
			{undef $displayAttribute}
		{/if}
	</div>
{/if}
{undef $can_edit $can_delete}
</div>
{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}
