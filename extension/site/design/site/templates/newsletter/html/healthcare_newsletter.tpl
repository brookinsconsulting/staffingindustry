{set-block variable=$bottom}
    <p style="font-size: 9px;"><a href="http://www.staffingindustry.com/corporate-overview/">ABOUT</a> | <a href="mailto:memberservices@staffingindustry.com">CONTACT</a> | <a href="http://www2.staffingindustry.com/Newsletters/Newsletter-Subscriptions-for-Staffing-Firms">Subscribe Now</a> </p>
	<p style="font-size: 9px;">STAFFING INDUSTRY ANALYSTS’ HEALTHCARE STAFFING REPORT is published once a month by Staffing Industry Analysts and is delivered to firms that provide Healthcare staffing services. For change of email address write to <a href="mailto:subservices@staffingindustry.com">subservices@staffingindustry.com</a>. If you do not wish to receive the Staffing Industry Analysts Healthcare Staffing Report, choose the appropriate option below. You will continue to have access to Daily News and Research on the website.</p>
	<p style="font-size: 9px;">Copyright 2009-{currentdate()|datetime(custom, '%Y')} Crain Communications Inc</p>
{/set-block}

{def
	$newsletter = fetch('content', 'node', hash('node_id', $node_id))
}
{set-block variable=type}{attribute_view_gui attribute=$newsletter.data_map.type}{/set-block}

{include
	uri="design:newsletter/parts/enewsletter_healthcare.tpl"
	banner="hc_newsletter_StffngRprt_Banner.gif"
	header="Top Stories"
	type=cond($type|ne(''), $type, "NT")
	data = hash('NT', array('News & Trends', 'site', $newsletter.data_map.news_trends|extract_embedded_objects()), 'PC', array('Premium Content', 'site', $newsletter.data_map.premium_content|extract_embedded_objects()))
}
