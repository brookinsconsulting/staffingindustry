{set-block variable=$bottom}
	<p><b>About the Staffing Industry Analysts' Research Bulletin</b></p>
	<p>The Research Bulletin is a consolidated e-newsletter intended for Staffing Industry Analysts members on the latest research available from Staffing Industry Analysts. The research is included as part of Staffing Industry Analysts' membership. You may also view the latest research by going to <a href="http://www.staffingindustry.com">www.staffingindustry.com</a>.</p>
	<p>For change of email address, to discontinue receipt of this bulletin or for more information, please contact <a href="mailto:memberservices@staffingindustry.com">memberservices@staffingindustry.com</a> or call +44 207 194 7759.</p>
	<p>Copyright 2009-{currentdate()|datetime(custom, '%Y')} Crain Communications Inc</p>
{/set-block}

{def
	$newsletter = fetch('content', 'node', hash('node_id', $node_id))
}

{set-block variable='topright'}
<a href="http://www.staffingindustry.com/corporate-overview/" style="color: white; text-decoration: none;">ABOUT</a> | <a href="mailto:memberservices@staffingindustry.com" style="color: white; text-decoration: none;">CONTACT</a>
{/set-block}

{include
	uri="design:newsletter/parts/enewsletter_research.tpl"
	banner="EUresearch_banner1608.jpg"
	header="Latest Research"
	items=$newsletter.data_map.daily_news_stories_1|extract_embedded_objects()
}
