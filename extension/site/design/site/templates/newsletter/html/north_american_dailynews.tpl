{set-block variable=$topright}
	<a href="http://twitter.com/#!/SIADailyNews" style="color: white; text-decoration: none;">Follow us on Twitter at @SIADailyNews</a>
{/set-block}

{set-block variable=$bottom}
	<p><a href={190|sitelink()}>ABOUT</a> | <a href="mailto:memberservices@staffingindustry.com">CONTACT</a></p>
	<p>STAFFING INDUSTRY ANALYSTS DAILY NEWS is published Monday through Friday by Staffing Industry Analysts and is delivered to Corporate Members. For change of email address write to <a href="mailto:subservices@staffingindustry.com">subservices@staffingindustry.com</a> or call 1-800-950-9496 or 650-390-6200. If you do not wish to receive the Staffing Industry Analysts Daily News, please click the Singular Unsubscribe button below. If you wish to stop receiving the Staffing Industry Analysts Daily News and the Staffing Industry Analysts Research Bulletin, please click on the Unsubscribe Completely button. You will continue to have access to Daily News and Research on the website.</p>
	<p>To advertise, contact Hugo Traeger at <a href='mailto:htraeger@staffingindustry.com'>htraeger@staffingindustry.com</a> or call 650.390.6181.</p>
	<p>Copyright 2008-{currentdate()|datetime(custom, '%Y')} Crain Communications Inc.</p>
{/set-block}

{def
	$newsletter = fetch('content', 'node', hash('node_id', $node_id))
}

{include
	uri="design:newsletter/parts/enewsletter.tpl"
	banner="NAdaily_banner1203.jpg"
	header="Top Stories - North America"
	items=$newsletter.data_map.daily_news_stories_1|extract_embedded_objects()
}
