{set-block variable=$bottom}
    <p style="font-size: 9px;"><a href={69807|sitelink()}>ABOUT</a> | <a href="mailto:memberservices@staffingindustry.com">CONTACT</a></p>
	<p style="font-size: 9px;">STAFFING INDUSTRY ANALYSTS’ IT STAFFING REPORT is published once a month by Staffing Industry Analysts and is delivered to firms that provide IT staffing services. For change of email address write to <a href="mailto:subservices@staffingindustry.com">subservices@staffingindustry.com</a>. If you do not wish to receive the Staffing Industry Analysts IT Staffing Report, choose the appropriate option below. You will continue to have access to Daily News and Research on the website.</p>
	<p style="font-size: 9px;">Copyright 2009-{currentdate()|datetime(custom, '%Y')} Crain Communications Inc</p>
{/set-block}

{def
	$newsletter = fetch('content', 'node', hash('node_id', $node_id))
}
{set-block variable=type}{attribute_view_gui attribute=$newsletter.data_map.type}{/set-block}

{include
	uri="design:newsletter/parts/enewsletter_it.tpl"
	banner="IT_Newsletter_Bnr.jpeg"
	header="Top Stories"
	type=cond($type|ne(''), $type, "NT")
	data = hash('NT', array('News & Trends', 'site', $newsletter.data_map.news_trends|extract_embedded_objects()), 'PC', array('Premium Content', 'site', $newsletter.data_map.premium_content|extract_embedded_objects()))
}
