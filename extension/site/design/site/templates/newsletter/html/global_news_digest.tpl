{set-block variable=$bottom}
	<p>STAFFING INDUSTRY ANALYSTS DAILY NEWS is published Monday through Friday by Staffing Industry Analysts and is delivered to Corporate Members. For change of email address write to <a href="mailto:subservices@staffingindustry.com">subservices@staffingindustry.com</a>. If you do not wish to receive the Staffing Industry Analysts Daily News, please click the Singular Unsubscribe button below. If you wish to stop receiving the Staffing Industry Analysts Daily News and the Staffing Industry Analysts Research Bulletin, please click on the Unsubscribe Completely button. You will continue to have access to Daily News and Research on the website.</p>
	<p>Copyright 2009-{currentdate()|datetime(custom, '%Y')} Crain Communications Inc</p>
{/set-block}

{def
	$newsletter = fetch('content', 'node', hash('node_id', $node_id))
}

{include
	uri="design:newsletter/parts/enewsletter.tpl"
	banner="GlobalDigest_banner1202.gif"
	header="Top Stories"
	items=$newsletter.data_map.daily_news_stories_1|extract_embedded_objects()
}
