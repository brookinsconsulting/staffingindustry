{set-block variable=$bottom}
    <p>STAFFING INDUSTRY ANALYSTS' CWS GLOBAL DAILY NEWS is published Monday through Friday by Staffing Industry Analysts. For change of email address, write to <a href="mailto:subservices@staffingindustry.com">subservices@staffingindustry.com</a> or call 650-390-6200. If you no longer wish to receive the Staffing Industry Analysts’ CWS Global Daily News, please use the "Manage Your Subscription Preferences" link below. If you wish to stop receiving emails from Staffing Industry Analysts, please use the "Unsubscribe from all SIA Emails" link instead. You will continue to have access on the website, <a href="http://www2.staffingindustry.com">www2.staffingindustry.com</a>.</p>
    <p>To advertise, contact Richard Thorne at <a href="mailto:rthorne@staffingindustry.com">rthorne@staffingindustry.com</a> or call +44 (0) 207 194 7749.</p>
    <p>Copyright 2009-{currentdate()|datetime(custom, '%Y')} Crain Communications Inc</p>
{/set-block}

{def
    $newsletter = fetch('content', 'node', hash('node_id', $node_id))
}
{set-block variable=type}{attribute_view_gui attribute=$newsletter.data_map.type}{/set-block}

{include
    uri="design:newsletter/parts/cws_global_dailynews.tpl"
    banner="CWSdaily_banner1202.jpg"
    header="Top Stories - Europe"
    type=cond($type|ne(''), $type, "NA")
    data=hash('NA', array('North America', 'site', $newsletter.data_map.daily_news_stories_na|extract_embedded_objects(), 'SIADailyNews'), 'RW', array('Asia Pacific/Latin America/Middle East &amp; Africa', 'row', $newsletter.data_map.daily_news_stories_rw|extract_embedded_objects(), 'SIAEurope'), 'PC', array('Latest Research', 'site', $newsletter.data_map.premium_content|extract_embedded_objects()), 'WE', array('Europe', eng, $newsletter.data_map.daily_news_stories_we|extract_embedded_objects(), 'SIAEurope'), 'EU-ADS2', array('EU-Ads2', 'EU-Ads2', $newsletter.data_map.ad_code2.content), 'UE', array('Upcoming Events & Information', 'site', $newsletter.data_map.event_content|extract_embedded_objects()) )
        ads=hash('EU-ADS', array('EU-Ads', 'EU-Ads', $newsletter.data_map.ad_code.content))
}