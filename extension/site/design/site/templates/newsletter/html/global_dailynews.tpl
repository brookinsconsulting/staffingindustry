{set-block variable=$bottom}
    <p><a style="color: #000" href={190|sitelink()}>ABOUT</a> | <a style="color: #000" href="mailto:memberservices@staffingindustry.com">CONTACT</a></p>
    <p>STAFFING INDUSTRY ANALYSTS GLOBAL DAILY NEWS is published Monday through Friday by Staffing Industry Analysts. For change of email address, write to <a style="color: #000" href="mailto:subservices@staffingindustry.com">subservices@staffingindustry.com</a> or call 650-390-6200. If you no longer wish to receive the Staffing Industry Analysts’ Global Daily News, please use the "Manage Your Subscription Preferences” link below. If you wish to stop receiving emails from Staffing Industry Analysts, please use the "Unsubscribe from all SIA Emails” link instead. You will continue to have access on the website, <a href="http://www2.staffingindustry.com">www2.staffingindustry.com</a>.</p>
    <p>To advertise, contact Hugo Traeger at <a style="color: #000" href="mailto:htraeger@staffingindustry.com">htraeger@staffingindustry.com</a> or call 650-390-6181.</p>
    <p>Copyright 2009-{currentdate()|datetime(custom, '%Y')} Crain Communications Inc</p>
{/set-block}

{def
    $newsletter = fetch('content', 'node', hash('node_id', $node_id))
}
{set-block variable=type}{attribute_view_gui attribute=$newsletter.data_map.type}{/set-block}

{include
    uri="design:newsletter/parts/enewsletter_global.tpl"
    banner="GlobalNws_Redesign_Bnr.jpeg"
    header="Top Stories"
    type=cond($type|ne(''), $type, "NA")
    data=hash('NA', array('North America', 'site', $newsletter.data_map.daily_news_stories_na|extract_embedded_objects(), 'SIADailyNews'), 'PC', array('Latest Research', 'site', $newsletter.data_map.premium_content|extract_embedded_objects()), 'UE', array('Upcoming Events', 'site', $newsletter.data_map.event_content|extract_embedded_objects()), 'NA-ADS2', array('NA-Ads2', 'NA-Ads2', $newsletter.data_map.ad_code2.content), 'WE', array('Europe', eng, $newsletter.data_map.daily_news_stories_we|extract_embedded_objects(), 'SIAEurope'), 'RW', array('Asia Pacific/Latin America/Middle East & Africa', 'row', $newsletter.data_map.daily_news_stories_rw|extract_embedded_objects(), 'SIAEurope') )
    ads=hash('NA-ADS', array('NA-Ads', 'NA-Ads', $newsletter.data_map.ad_code.content))
}

