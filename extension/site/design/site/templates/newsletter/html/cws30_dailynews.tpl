{set-block variable=$topright}
    <a href={'/Newsletters/Newsletter-Subscriptions-for-Buyers-of-Staffing-Services'|ezurl(,'full')} style="color: white; text-decoration: none;">Subscribe Now</a> | <a href="http://twitter.com/#!/CW_Strategies" style="color: white; text-decoration: none;">Follow us on Twitter at @CW_Strategies</a>
{/set-block}

{set-block variable=$bottom}
    <p><a href={491|sitelink()}>CWS 3.0 Past Issues</a> | <a href="mailto:memberservices@staffingindustry.com">Contact Us</a></p>
    <b>If a colleague shared <em>CWS 3.0</em> with you and you'd like your own FREE weekly copy, please subscribe <a href="http://www2.staffingindustry.com/cws30" target="_blank">here</a>.</b>
    <p>CONTINGENT WORKFORCE STRATEGIES 3.0 is published weekly by Staffing Industry Analysts and is delivered to buyers of staffing services. For change of email address write to <a href="mailto:subservices@staffingindustry.com" target="_blank">subservices@staffingindustry.com</a> or call 1-800-950-9496 or 650-390-6200.</p>
    <p>To contact the editor, please e-mail <a href="mailto:smthomas@staffingindustry.com">Sharon Thomas</a> or call 650.390.6228.</p>
    <p>To advertise, contact <a href="mailto:htraeger@staffingindustry.com">Hugo Traeger</a> or call 650.390.6181.</p>
    <p>Copyright {currentdate()|datetime(custom, '%Y')} Crain Communications Inc</p>
{/set-block}

{def
    $newsletter = fetch('content', 'node', hash('node_id', $node_id))
}

{include
    uri="design:newsletter/parts/enewsletter.tpl"
    banner="CWS30_banner2.jpg"
    header="Top Stories"
    newsletter=$newsletter
    daily_news_node_id=195
    items=$newsletter.data_map.daily_news_stories_1|extract_embedded_objects()
}
