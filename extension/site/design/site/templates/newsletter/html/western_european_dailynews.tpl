{set-block variable=$bottom}
	<p>STAFFING INDUSTRY ANALYSTS EUROPEAN DAILY NEWS is published Monday through Friday by Staffing Industry Analysts and is delivered to Corporate Members. For change of email address, write to <a href="mailto:subservices@staffingindustry.com">subservices@staffingindustry.com</a> or call 1-800-950-9496 or 650-390-6200. If you no longer wish to receive the Staffing Industry Analysts European Daily News, please use the "Manage Your Subscription Preferences" link below. If you wish to stop receiving emails from Staffing Industry Analysts, please click on the "Unsubscribe From All SIA Emails" link instead. You will continue to have access to Daily News and Research on the website.</p>
	<p>To advertise, contact Richard Thorne at <a href="mailto:rthorne@staffingindustry.com">rthorne@staffingindustry.com</a> or call +44 (0) 207 194 7749.</p>
	<p>Copyright 2009-{currentdate()|datetime(custom, '%Y')} Crain Communications Inc</p>
{/set-block}

{def
	$newsletter = fetch('content', 'node', hash('node_id', $node_id))
}

{include
	uri="design:newsletter/parts/enewsletter.tpl"
	banner="EUdaily_banner1202.jpg"
	header="Top Stories - Europe"
	items=$newsletter.data_map.daily_news_stories_1|extract_embedded_objects()
}