{set-block variable=output}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>IT Report Newsletter</title>
<style type="text/css">
{literal}
.ExternalClass {
  width:100%;
}
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
  line-height:100%;
}
body {
  -webkit-text-size-adjust:none;
  -ms-text-size-adjust:none;
}
body, img, div, p, ul, li, span, strong, a {
  margin:0;
  padding:0;
}
table {
  border-spacing:0;
}
table td {
  border-collapse:collapse;
}
/****** END BUG FIXES ********/
p {
  margin:0;
  padding:0;
  margin-bottom:0;
}
h1, h2, h3, h4, h5, h6 {
  line-height:100%;
}
/****** END RESETTING DEFAULTS ********/
/****** EDITABLE STYLES - FOR YOUR TEMPLATE ********/
body, #body_style {
  width:100% !important;
  min-height:1000px;
  color:#000000;
  background:#d9dfe4;
  font-family:Arial, Helvetica, sans-serif;
  font-size:13px;
  line-height:1;
}
a {
  color:#000000;
  text-decoration:none;
}
a:link {
  color:#000000;
  text-decoration:none;
}
a:visited {
  color:#000000;
  text-decoration:none;
}
a:hover {
  color:#455560 !important;
  text-decoration:underline !important;
}
a[href^="tel"], a[href^="sms"] {
 text-decoration:none;
 color:#000000;
 pointer-events:none;
 cursor:default;
}
.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
 text-decoration:default;
 color:#000000 !important;
 pointer-events:auto;
 cursor:default;
}
p {
  margin-top:0;
  margin-bottom:0;
}
img {
  border:none;
  outline:none;
  text-decoration:none;
}
table {
  border-collapse:collapse;
  mso-table-lspace:0pt;
  mso-table-rspace:0pt;
}
{/literal}
</style>
</head>
{def  $text_width = 0}

<body style="width:100% !important; min-height:1000px; color:#000000; background-color:#d9dfe4; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:1;" alink="#000000" link="#000000" bgcolor="#d9dfe4" text="#000000" yahoo="fix">
<div id="body_style">
  <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
      <td valign="top"><table width="600" border="0" cellspacing="0" cellpadding="0" align="center">

          <!--head section-->

          <tr>
            <td valign="top"><table width="600" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top"><table width="600" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="8"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" alt="" height="1" width="1" /></td>
                      </tr>
                      <tr>
                        <td height="20" style="font-size:10px; color:#231f20; line-height:18px;"><span style="font-weight:bold;">{$newsletter.data_map.issue.content}</span> | <a href="http://www2.staffingindustry.com" style="color:#231f20; text-decoration:none;">Staffing Industry Analysts Home</a> | <a href="http://www2.staffingindustry.com/Research-Publications/Research-Topics/Staffing-Information-Technology" style="color:#231f20; text-decoration:none;">IT Topics</a> | <a href="http://staffingindustry.com/Newsletters/Subscribe" style="color:#231f20; text-decoration:none;">Subscribe Now</a> | <a href="http://www2.staffingindustry.com/About/Contact-Us" style="color:#231f20; text-decoration:none;">Contact Us</a></td>
                      </tr>
                      <tr>
                        <td height="4"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" alt="" height="1" width="1" /></td>
                      </tr>
                      <tr>
                        <td bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', $banner)|ezimage(no)}" alt="Staffing Industry Analysts I.T. Newsletter Banner" border="0" height="115" width="600" style="display:block;" /></td>
                      </tr>
                      <tr>
                        <td valign="top" bgcolor="rgb(255, 255, 255)" style="background-color:#ffffff;"><table width="600" border="0" cellspacing="0" cellpadding="0" style="background-color:#ffffff;" bgcolor="rgb(255, 255, 255)">
                            <tr>
                              <td valign="top" width="15" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" height="1" width="1" alt="" /></td>
                              <td valign="top" width="570" bgcolor="#ffffff"><table width="570" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                  <tr>
                                    <td height="20" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" height="1" width="1" alt="" /></td>
                                  </tr>
                                  <tr>
                                    <td valign="top" width="370" bgcolor="#ffffff"><table width="370" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td valign="top" width="342" bgcolor="#ffffff">
                                          {foreach $data as $k => $d}
                                          <table width="342" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                              <tr>
                                                <td valign="top" bgcolor="#ffffff"><h2 style="font-size:15px; line-height:15px; font-family:Arial, Helvetica, sans-serif; text-transform: uppercase; font-weight:bold; color:#578bac; padding:0 0 4px 0; border-bottom:1px solid #969fa5; margin:0;">{$d[0]}</h2></td>
                                              </tr>
                                              <tr>
                                                <td height="14" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" height="1" width="1" alt="" /></td>
                                              </tr>
                                              <tr>
                                                {foreach $d[2] as $key => $item}
                        {if eq($key, 0)}
                        <td valign="top" bgcolor="#ffffff">
                                                <table width="342" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                            {set $text_width = 249}
                            {if eq($item.class_identifier, 'article')}
                              {if $item.data_map.ml_image.content}
                                <td valign="top" width="80" bgcolor="#ffffff">{attribute_view_gui attribute=$item.data_map.ml_image.content.data_map.image href=$node|sitelink() image_class='newsletter'}</td>
                                <td width="10" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" height="1" width="1" alt="" /></td>
                              {elseif $item.data_map.image.content}
                                <td valign="top" width="80" bgcolor="#ffffff">{attribute_view_gui attribute=$item.data_map.image href=$node|sitelink() image_class='newsletter'}</td>
                                <td width="10" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" height="1" width="1" alt="" /></td>
                              {/if}
                            {elseif $item.data_map.image.content}
                              <td valign="top" width="80" bgcolor="#ffffff">{attribute_view_gui attribute=$item.data_map.image href=$node|sitelink() image_class='newsletter'}</td>
                              <td width="9" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" height="1" width="1" alt="" /></td>
                            {else}
                            {set $text_width = 342}
                            {/if}
                                                      <td width="{$text_width}" bgcolor="#ffffff" valign="top">
                                                        <a href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}" style="font-size:14px; line-height:14px; font-weight:bold; text-decoration:none; font-family:Arial, Helvetica, sans-serif; color:#111111;">
                              {switch match=$item.class_identifier}
                                {case match='news_item'}
                                    {$item.data_map.name.content|html_shorten(1000)}
                                {/case}
                                {case match='article'}
                                    {$item.data_map.title.content}
                                {/case}
                              {/switch}
                                                        </a>
                                                        <p style=" font-size:12px; line-height:16px; color:#000000; font-family:Arial, Helvetica, sans-serif; margin:0px; padding:0px;">
                              {switch match=$item.class_identifier}
                                {case match='news_item'}
                                    {$item.data_map.description.value.output.output_text|preg_replace('~<p>~','')|preg_replace('~</p>~','')|html_shorten(90,'... ')}
                                {/case}
                                {case match='article'}
                                    {$item.data_map.intro.value.output.output_text||preg_replace('~<p>~','')|preg_replace('~</p>~','')|preg_replace('~<ul>~','')|preg_replace('~<li>~','')|preg_replace('~</li>~',';<br />')|preg_replace('~</ul>~','')|html_shorten(90,'... ')}
                                {/case}
                              {/switch}
                             </p>
                                                      </td>
                                                    </tr>
                                                  </table>
                                                  </td>
                                              </tr>
                                              <tr style="display: none;">
                                                <td height="4px" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" height="1" width="1" alt="" /></td>
                                              </tr>
                                              <tr>
                                                <td style="text-align:right;" bgcolor="#ffffff"><a href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}" style="font-size:12px; text-decoration:none; margin:0px; padding:0px; line-height:20px; color:#111111; font-family:Arial, Helvetica, sans-serif; text-decoration:underline;">Read More</a></td>
                                              </tr>

                                              <tr>
                                                <td height="14" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" height="14" width="1" alt="" /></td>
                                              </tr>
                                                <td valign="top"><table width="342" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                                {else}
                                                    <tr>
                                                      <td valign="top" width="11" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'IT_Newsletter_bullet_6.gif')|ezimage(no)}" alt=" - " align="left" /></td>
                                                      <td width="330" bgcolor="#ffffff">
                              <a href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}" style="color:#111111; font-size:12px; line-height:16px; text-decoration:none; display: block; margin:0px; padding:0px 0px 4px 0px; font-family:Arial, Helvetica, sans-serif;">
                              {switch match=$item.class_identifier}
                                {case match='news_item'}
                                    {$item.data_map.name.content|html_shorten(1000)}
                                {/case}
                                {case match='article'}
                                    {$item.data_map.title.content}
                                {/case}
                              {/switch}
                              </a>
                                                      </td>
                                                    </tr>
                                                    {/if}
                                                    {/foreach}
                                                    <tr>
                                                      <td height="30" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" alt="" /></td>
                                                    </tr>
                                                  </table></td>
                                              </tr>
                                            </table>
                                            {/foreach}
                                            <table width="342" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:6px;" bgcolor="#ffffff">
                                              <tr>

                                                  <h2 style="font-size:15px; font-family:Arial, Helvetica, sans-serif; text-transform: uppercase; font-weight:bold; color:#578bac; padding:0 0 4px; border-bottom:1px solid #969fa5; margin:0;">QUESTIONS ABOUT IT STAFFING RESEARCH?</h2>

                                              </tr>
                                              <tr>
                                                <td height="14" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" alt=""  height="1" width="1" /></td>
                                              </tr>
                                              <tr>
                                                  <td style="vertical-align: top; width: 62px;">
                                                      <img style="margin-right: 10px; margin-bottom: 10px; float: left" src="http://www2.staffingindustry.com{concat('newsletter/', 'Andrew-Braswell-_HCSR_1403_large.jpg')|ezimage(no)}" alt="Andrew Braswell" />
                                                  </td>
                                                  <td style="vertical-align: top;">
                                                      <h3 class="small-header" style="font-size: 13px; margin: 0; margin-bottom: 3px;">Contact</h3>
                                                      <p style="margin: 2px 0; font-size: 13px; padding: 0;">Andrew Braswell, Senior Research Analyst</p>
                                                      <p style="margin: 2px 0; font-size: 13px; padding: 0;"><a style="color: #000000; text-decoration: none;" href="mailto:abraswell@staffingindustry.com">abraswell@staffingindustry.com</p>
                                                  </td>
                                              </tr>
                                              <tr>
                                                <td height="4"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" alt="" height="1" width="1" /></td>
                                              </tr>
                                            </table>
                                            <table>
                                              <tr>
                                                <td>
                                                  <span style="font-size:12.15px; line-height:17px; font-family:Arial, Helvetica, sans-serif; color:#000000; display:block;">Do you have IT-related news?</span> <span style="font-size:12.15px; line-height:17px; font-family:Arial, Helvetica, sans-serif; color:#000000; display:block;">Contact: Katherine Alvarez, <a href="mailto:kalvarez@staffingindustry.com" style="font-size:12px; line-height:17px; font-family:Arial, Helvetica, sans-serif; color:#111111;">kalvarez@staffingindustry.com</a></span>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td height="4"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" alt="" height="1" width="1" /></td>
                                              </tr>
                                            </table>

                                          <td width="28" valign="top" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" alt="" height="1" width="1" /></td>
                                        </tr>
                                      </table></td>
                                    <td valign="top" width="200" bgcolor="#ffffff" style="padding-top: 9px;">
                                    {$newsletter.data_map.ad_code.content}
                                    </td>
                                  </tr>
                                </table></td>
                              <td valign="top" width="15" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" alt="" height="1" width="1" /></td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr>
                        <td height="30" style="text-align:center; font-size:10.12px; color:#000000; line-height:12px; font-family:Arial, Helvetica, sans-serif;">Staffing Industry Analysts | 1975 W. El Camino Real, Ste. 304 | Mountain View, CA 94040</td>
                      </tr>
                      <tr>
                        <td height="28" style="font-size:9px; color:#000000; line-height:12px; font-family:Arial, Helvetica, sans-serif;"><a href={69807|sitelink()} style="color:#0000ff; text-decoration:underline;">ABOUT</a> | <a href="mailto:memberservices@staffingindustry.com" style="color:#0000ff; text-decoration:underline;">CONTACT</a> | <a href="http://www.staffingindustry.com/Newsletters/Newsletter-Subscriptions-for-Staffing-Firms" style="color:#0000ff; text-decoration:underline;">SUBSCRIBE NOW</a> </td>
                      </tr>
                      <tr>
                        <td height="4"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" height="1" width="1" alt="" /></td>
                      </tr>
                      <tr>
                        <td style=" font-size:9px; line-height:10px; color:#000000; font-family:Arial, Helvetica, sans-serif;"><p style="margin-bottom:13px; font-size:9px;">STAFFING INDUSTRY ANALYSTS' IT STAFFING REPORT is published once a month by Staffing Industry Analysts and is delivered to firms that provide IT staffing services. For change of email address write to <a href="mailto:subservices@staffingindustry.com" style="color:#0000ff; text-decoration:underline;">subservices@staffingindustry.com</a>. If you do not wish to receive the Staffing Industry Analysts IT Staffing Report, choose the appropriate option below. You will continue to have access to Daily News and Research on the website.</p>
                          <p style="margin-bottom:10px; font-size:9px;">Copyright 2009-{currentdate()|datetime(custom, '%Y')} Crain Communications Inc</p></td>
                      </tr>
                      <tr>
                        <td style="font-size:9px; color:#000000; line-height:12px; font-family:Arial, Helvetica, sans-serif;"><preferences>Manage Your Subscription Preferences</preferences> or <unsubscribe>Unsubscribe From All SIA Emails</unsubscribe></td>
                      </tr>
                      <tr>
                        <td height="15px"><img src="http://www2.staffingindustry.com{concat('newsletter/', 'spacer.gif')|ezimage(no)}" alt="" height="1" width="1" /></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
</body>
</html>
{/set-block}
{$output|fix_sitelink_siteaccess_block()}
