{set-block variable=output}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="margin:0;padding:0;" xmlns:v="urn:schemas-microsoft-com:vml"
	xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:w="urn:schemas-microsoft-com:office:word"
	xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
	xmlns="http://www.w3.org/TR/REC-html40">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<style type="text/css">
		html, body {ldelim}margin: 0;padding: 0;{rdelim}
		*{ldelim}font-family: arial, helvetica, sans-serif;{rdelim}
		h2 a {ldelim}color: #000;{rdelim}
		#date {ldelim}color: #455560;padding: 10px 0;font-size: 12px;line-height: 33px;margin-left: 14px;{rdelim}
		#header_table {ldelim}background: #fff;margin-left: 10px;border-collapse: collapse; border-width:0px;{rdelim}
		#header_td {ldelim}padding: 0px 4px 0px 3px;{rdelim}
		#header_h1 {ldelim}text-transform: uppercase;color: #000;line-height: 17px;font-size: 20px;margin:0px;padding:0px;{rdelim}
		.morenews {ldelim}font-size: 11px;text-align: right;margin-bottom: 15px;{rdelim}
		.morenews a {ldelim}color: #C41230;{rdelim}
		.newsitems {ldelim}list-style-type: disc;padding-left: 13px;{rdelim}
		.newsitems {ldelim}color: #455560;{rdelim}
		.newsitems li {ldelim}margin-bottom: 0;{rdelim}
		.newsitems li a {ldelim}color: #455560;font-size: 13px;{rdelim}
	</style>
</head>
<body>
<table width="650" cellpadding="0" style="margin: 5px; border: 1px solid #000; border-collapse: collapse; font-family: DIN Bold, Verdana; font-size: 12px">
	<tbody>
		<tr>
			<td colspan="2"><img src={concat('newsletter/', $banner)|ezimage()}></td>
		</tr>
		<tr style="font-size: 12px; font-weight: bold; padding: 10px; background-color: #455560 !important; color: #fff;">
			<td style="padding: 10px 0 10px 15px;">
				<a href="http://www2.staffingindustry.com" style="color: white; text-decoration: none;">www2.staffingindustry.com</a>
			</td>
			<td style="padding: 10px 15px 10px 0; text-align: right; color: white">
				{if $topright}
					{$topright}
				{else}
					<a href={190|sitelink()} style="color: white; text-decoration: none;">ABOUT</a> | <a href="mailto:memberservices@staffingindustry.com" style="color: white; text-decoration: none;">CONTACT</a>
				{/if}
			</td>
		</tr>
		<tr style="font-size: 10px; font-weight: bold; color: #455560">
			<td style="margin-left: 10px">
				<span style="color: #455560; margin: 0 25px; padding: 10px 5px; line-height: 33px;">{$newsletter.data_map.issue.content}</span>
			</td>
			<td style="text-align: right; padding-right: 25px">
				<span style="text-align: right; font-size: 12px;">Published by Staffing Industry Analysts</span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table style="padding: 10px; border-collapse: collapse;">
					<tbody>
						<tr valign="top">
							<td colspan="2">
								<table id="header_table" style="background: #fff;margin-left: 10px;border-collapse: collapse; border-width:0px;">
									<tbody>
										<tr valign="top">
											<td id="header_td" style="padding: 0px 4px 0px 3px;">
												<h1 style="text-transform: uppercase;color: #000;line-height: 17px;font-size: 20px;margin:0px;padding:0px;">{$header}</h1>
												<ul style="list-style-type: none; padding: 0; margin: 0">
												{foreach $items as $key => $item}
													<li style="margin-bottom: 1em">
														{if and($bottom|contains('CWS 3.0'), eq($key, $items|count()|dec))}
															<hr />
														{/if}
														{switch match=$item.class_identifier}
															{case match='news_item'}
																<h2 style="font-size: 14px; margin: 0; padding: 0">
																		<a href={$item|sitelink()} style="text-decoration: none; color: #00446a">{$item.data_map.name.content|html_shorten(1000)}</a>
																</h2>
																<span style="font: 12px Georgia">
																	{$item.data_map.description.content.output.output_text|html_shorten(150)|explode('<p>')|implode('<p style="margin-top: 0">')}
																</span>
															{/case}
															{case match='article'}
																<h2 style="font-size: 14px; margin: 0; padding: 0">
																	<a href={$item|sitelink()} style="text-decoration: none; color: #00446a">{$item.data_map.title.content}</a>
																</h2>
																<span style="font: 12px Georgia">
																	{$item.data_map.intro.content.output.output_text|explode('<p>')|implode('<p style="margin-top: 0">')}
																</span>
															{/case}
														{/switch}
													</li>
												{/foreach}
												</ul>
											</td>
										</tr>
										<tr valign="bottom">
											<td style="font-size: 9px">
												{$bottom}
												<p><preferences>Manage Your Subscription Preferences</preferences> or <unsubscribe>Unsubscribe From All SIA Emails</unsubscribe></p>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td valign="top" style="padding: 0 10px 10px 0">
								{$newsletter.data_map.ad_code.content}
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>
{/set-block}
{$output|fix_sitelink_siteaccess_block()}
