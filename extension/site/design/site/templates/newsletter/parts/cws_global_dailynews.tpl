{set-block variable=output}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="margin:0;padding:0;" xmlns:v="urn:schemas-microsoft-com:vml"
   xmlns:o="urn:schemas-microsoft-com:office:office"
   xmlns:w="urn:schemas-microsoft-com:office:word"
   xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
   xmlns="http://www.w3.org/TR/REC-html40">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <style type="text/css">
         html, body {ldelim}margin: 0;padding: 0; background-color: #67BFBA;{rdelim}
         *{ldelim}font-family: arial, helvetica, sans-serif;{rdelim}
         #masthead {ldelim}width: 100%;{rdelim}
         h2 a {ldelim}color: #000;{rdelim}
         h2 {ldelim}margin-bottom: 0px;{rdelim}
         #date {ldelim}color: #455560;padding: 10px 0;font-size: 12px;line-height: 33px;margin-left: 14px;{rdelim}
         #header_table {ldelim}background: #fff;margin-left: 10px;border-collapse: collapse; border-width:0px;{rdelim}
         #header_td {ldelim}padding: 0px 4px 0px 3px;{rdelim}
         #header_h1 {ldelim}text-transform: uppercase;color: #000;line-height: 17px;font-size: 20px;margin:0px;padding:0px;{rdelim}
         #body_table {ldelim}background: #fff;margin-left: 10px;border-collapse: collapse; border-width:0px;{rdelim}
         #body_td {ldelim}padding: 0px 4px 0px 3px;{rdelim}
         #body_h1 {ldelim}text-transform: uppercase;color: #455560;line-height: 13px;font-size: 12px;margin:0px;padding:0px;{rdelim}
         .morenews {ldelim}font-size: 11px;text-align: right;margin-bottom: 15px;{rdelim}
         .morenews a {ldelim}color: #C41230;{rdelim}
         .newsitems {ldelim}list-style-type: disc;padding-left: 13px;{rdelim}
         .newsitems {ldelim}color: #455560;{rdelim}
         .newsitems li {ldelim}margin-bottom: 0;{rdelim}
         .newsitems li a {ldelim}color: #455560;font-size: 13px;{rdelim}
         td p {ldelim}margin-bottom:0.5em;{rdelim}
         td p a {ldelim}color: #455560;{rdelim}
         td h3 a {ldelim}padding-bottom: 0px;{rdelim}
         tr.bold a {ldelim}font-weight:bold;{rdelim}
         .item-title td h3 {ldelim}margin: 0px !important;{rdelim}
         .item-description td p {ldelim}margin-top: 0px;{rdelim}
         .list-item td a {ldelim}line-height: 17px !important;{rdelim}
         .list-item td {ldelim}padding-bottom: 5px;{rdelim}
         .anchors a {ldelim}font-size: 14px; color: #000000;{rdelim}
         table table table {ldelim}position: relative; left: -2px;{rdelim}
         #research-block {ldelim}width: 415px; background: url("http://www2.staffingindustry.com/extension/site/design/site/images/newsletter/research-repeatable.gif") repeat scroll 0 0 #a60f29; border-bottom: 4px solid #000000; left:0px;{rdelim}
         #research-block td h2 {ldelim}background-color: #000000; width: 34%; margin: 0 10px 0px 5px !important; padding: 6px 4px 2px !important; border-bottom: none !important;{rdelim}
         #research-block td h2 a {ldelim}color: #ffffff;{rdelim}
         #research-block table {ldelim}position: relative; top: -2px;{rdelim}
         #research-block table td {ldelim}background-color: transparent !important;{rdelim}
         #research-block table td a {ldelim}color: #ffffff; padding: 0px !important;{rdelim}
         #research-block table {ldelim}margin-bottom: 0px !important;{rdelim}
      </style>
   </head>
   <body style="margin:8px;padding:0;">
      <table width="645" cellpadding="0" style="margin: 0px auto; border: none; border-collapse: collapse; font-family: DIN Bold, Verdana; font-size: 12px; background: #FFF">
         <tbody>
            <tr>
               <td colspan="2">
                  <img id="masthead" src={concat('newsletter/', $banner)|ezimage()} alt="Masthead banner image" />
               </td>
            </tr>
            <tr style="font-size: 12px; font-weight: bold; padding: 10px; background-color: #E5B53B; color: #455560;">
               <td colspan="2" style="padding-left: 10px">
                  <table width="645">
                     <tr style="font-size: 12px; font-weight: bold; padding: 10px; background-color: #E5B53B; color: #455560;">
                        <td style="padding: 10px 0 10px 2px;">
                           <a href="http://www2.staffingindustry.com" style="color: #455560; text-decoration: none;">www2.staffingindustry.com</a>
                        </td>
                        <td style="padding: 10px 15px 10px 0; text-align: right; color: #455560">
                           {if $topright}
                           {$topright}
                           {else}
                           Follow us on Twitter <a style="text-decoration: none; color: #455560" href="https://twitter.com/{$data[$type][3]}">@{$data[$type][3]}</a>
                           {/if}
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr style="font-size: 10px; color: #455560">
               <td colspan="2" style="padding-left: 7px">
                  <p style="font-size: 11px; padding: 14px 0 0; padding-left: 0;">
                     <span style="font-weight: bold">{$newsletter.data_map.issue.content}</span>
                  </p>
                  <div class="anchors">
                      {if $type|eq('RW')}<a href="#Nort">North America</a> | <a href="#Euro">Europe</a>{/if}
                      {if $type|eq('NA')}<a href="#Asia">Asia Pacific/Latin America/Middle East &amp; Africa</a> | <a href="#Euro">Europe</a>{/if}
                      {if $type|eq('WE')}<a href="#Nort">North America</a> | <a href="#Asia">Asia Pacific/Latin America/Middle East &amp; Africa</a>{/if}
                  </div>
               </td>
            </tr>
            <tr>
               <td height="10" colspan="2"></td>
            </tr>
            <tr>
               <td colspan="1" style="padding-left: 10px">
                  <table style="padding: 10px; border-collapse: collapse;">
                     <tbody>
                        <tr>
                           <td colspan="2" valign="top" style="padding: 0 10px 10px 0; height: 100%">
                              <table>
                                 <tbody>
                                    <tr style="padding: 0;">
                                       <td style="padding: 0">
                                          <h2 class="section-heads" style="color: #000; font-weight: bold; font-family: 'arial'; text-transform: uppercase; font-size: 14px; border-bottom: 3px solid #578BAC; padding: 0 0 3px 0;"><a name="{$data[$type][0]|shorten( 4, '' , 'right' )}">{$data[$type][0]}</a></h2>
                                       </td>
                                    </tr>
                                    <tr valign="top">
                                       <td>
                                         <table style="padding-bottom: 6px; margin-left: 0px; margin-bottom: 8px;">
                                         {foreach $data[$type][2] as $key => $item}
                                         {if eq($key, 0)}
                                            <tr class="item-title">
                                               <td valign="top" width="11" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com/extension/site/design/site/images/newsletter/IT_Newsletter_bullet_6-transparent.gif" alt=" - " align="left" style="background-color: tranparent;" /></td><td width="389" bgcolor="#ffffff">
                                                  <h3 class="small-header" style="font-size: 15px; margin: 0; margin-bottom: 3px;">
                                                     {switch match=$item.class_identifier}
                                                     {case match='news_item'}
                                                     <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none; color: #000000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.name.content|html_shorten(1000)}</a>
                                                     {/case}
                                                     {case match='article'}
                                                     <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none; color: #000000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.title.content}</a>
                                                     {/case}
                                                     {/switch}
                                                  </h3>
                                               </td>
                                            </tr>
                                            <tr class="item-description">
                                               <td valign="top" width="11" bgcolor="#ffffff"></td>
                                               <td width="389" style="margin: 0; font-size: 13px; line-height: 1.3; height:24px; overflow: hidden;">
                                                  {switch match=$item.class_identifier}
                                                  {case match='news_item'}
                                                  {$item.data_map.description.value.output.output_text|html_shorten(100,concat('... <a style="color: #C41230;" href="http://www2.staffingindustry.com', $item|sitelink("no"), '">Read More.</a>'))}
                                                  {/case}
                                                  {case match='article'}
                                                  {$item.data_map.intro.value.output.output_text|preg_replace('~
                                                  <ul>
                                                     ~','')|preg_replace('~
                                                     <li>~','')|preg_replace('~</li>
                                                     ~','<br />')|preg_replace('~
                                                  </ul>
                                                  ~','')|html_shorten(120,concat('... <a style="color: #C41230;" href="http://www2.staffingindustry.com', $item|sitelink("no"), '">Read More.</a>'))}
                                                  {/case}
                                                  {/switch}
                                               </td>
                                            </tr>
                                         {else}
                                         <tr class="list-item">
                                            <td valign="top" width="11" bgcolor="#ffffff">
                                                <img src="http://www2.staffingindustry.com/extension/site/design/site/images/newsletter/IT_Newsletter_bullet_6-transparent.gif" alt=" - " align="left" />
                                            </td>
                                            <td width="389" bgcolor="#ffffff">
                                                {switch match=$item.class_identifier}
                                                    {case match='news_item'}
                                                        <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none; color: #000000; font-weight:bold;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.name.content|html_shorten(1000)}</a>
                                                    {/case}
                                                    {case match='article'}
                                                        <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none; color: #000000; font-weight:bold;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.title.content}</a>
                                                    {/case}
                                                {/switch}
                                            </td>
                                         </tr>
                                         {/if}
                                         {/foreach}
                                         </table>
                                       </td>
                                    </tr>

                                    <tr valign="top">
                                      <td>
                                        <table id="research-block" style="border-spacing: 0px;border-top: 2px solid #000000; border-bottom: 0px">
                                        <tr style="padding: 0; background-color: #000000">
                                           <td style="padding: 0">
                                              <h2 class="section-heads" style="position: relative; top: -2px; margin-bottom: 10px; color: #000; font-weight: bold; font-family: 'arial'; text-transform: uppercase; font-size: 13px; text-align: center; border-bottom: 3px solid #578BAC; padding: 0 0 3px 0;"><a name="{$d[0]|shorten( 4, '' , 'right' )}">{$data['PC'][0]}</a></h2>
                                           </td>
                                        </tr>
                                        <tr class="bold" valign="top">
                                          <td>
                                            <table style="margin-left: 10px; margin-bottom: 8px;">
                                            {foreach $data['PC'][2] as $key => $item}
                                            {if eq($key, 0)}
                                            <tr>
                                              <td valign="top" width="11" bgcolor="#ffffff"><img src="http://www2.staffingindustry.com/extension/site/design/site/images/newsletter/IT_Newsletter_bullet_6-transparent-white.png" alt=" - " align="left" /></td><td width="389" bgcolor="#ffffff">
                                                {switch match=$item.class_identifier}
                                                {case match='news_item'}
                                                <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{if $item.data_map.short_name.has_content}{$item.data_map.short_name.content|html_shorten(1000)|wash}{else}{$item.data_map.name.content|html_shorten(1000)|wash}{/if}</a>
                                                {/case}
                                                {case match='article'}
                                                <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{if $item.data_map.short_title.has_content}{$item.data_map.short_title.content|wash}{else}{$item.data_map.title.content|wash}{/if}</a>
                                                {/case}
                                                {/switch}
                                              </td>
                                            </tr>
                                            {else}
                                            <tr>
                                               <td valign="top" width="11" bgcolor="#ffffff">
                                                 <img src="http://www2.staffingindustry.com/extension/site/design/site/images/newsletter/IT_Newsletter_bullet_6-transparent-white.png" alt=" - " align="left" />
                                               </td>
                                               <td width="389" bgcolor="#ffffff">
                                               {switch match=$item.class_identifier}
                                               {case match='news_item'}
                                               <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{if $item.data_map.short_name.has_content}{$item.data_map.short_name.content|html_shorten(1000)|wash}{else}{$item.data_map.name.content|html_shorten(1000)|wash}{/if}</a>
                                               {/case}
                                               {case match='article'}
                                               <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{if $item.data_map.short_title.has_content}{$item.data_map.short_title.content|wash}{else}{$item.data_map.title.content|wash}{/if}</a>
                                               {/case}
                                               {/switch}
                                               </td>
                                            </tr>
                                            {/if}
                                            {/foreach}
                                            </table>
                                          </td>
                                        </tr>
                                        </table>
                                      </td>
                                    </tr>

                                    {if $data['EU-ADS2'][2]|count|gt(0)}
                                        <tr class="{'EU-ADS2'|downcase}" valign="top">
                                            <td style="padding: 10px 0 25px 0;">{$data['EU-ADS2'][2]}</td>
                                        </tr>
                                    {/if}

                                    {foreach $data as $k => $d}
                                    {if $k|eq( 'PC' )}{continue}{/if}
                                    {if and($k|eq( 'UE' ), $data[$k][2]|count|gt(0))}
                                    <tr style="padding: 0;">
                                       <td style="padding: 0">
                                          <h2 class="section-heads" style="color: #000; font-weight: bold; font-family: 'arial'; text-transform: uppercase; font-size: 14px; border-bottom: 3px solid #578BAC; padding: 0 0 3px 0;"><a name="{$d[0]|shorten( 4, '' , 'right' )}">{$d[0]}</a></h2>
                                       </td>
                                    </tr>
                                    <tr valign="top">
                                       <td>
                                          {foreach $data[$k][2] as $key => $item}
                                          <table style="padding-bottom: 6px; margin-left: 0px; margin-bottom: 1px;">
                                             <tr>
                                                <td>
                                                   <h3 class="small-header" style="font-size: 15px; margin: 0; margin-bottom: 3px;">
                                                      {switch match=$item.class_identifier}
                                                      {case match='link'}
                                                      <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none; color: #000000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.name.content|html_shorten(1000)}</a>
                                                      {/case}
                                                      {case match='news_item'}
                                                      <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none; color: #000000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.name.content|html_shorten(1000)}</a>
                                                      {/case}
                                                      {case match='article'}
                                                      <a style="font-size: 14px; line-height:14px; display: block; margin:0px; padding:0px 0px 4px 0px;text-decoration: none; color: #000000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.title.content}</a>
                                                      {/case}
                                                      {/switch}
                                                   </h3>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td style="margin: 0; font-size: 13px; line-height: 1.3; height:24px; overflow: hidden;">
                                                   {switch match=$item.class_identifier}
                                                   {case match='link'}
                                                   {$item.data_map.description.value.output.output_text}
                                                   {/case}
                                                   {case match='news_item'}
                                                   {$item.data_map.description.value.output.output_text|html_shorten(120,'... ')}
                                                   {/case}
                                                   {case match='article'}
                                                   {$item.data_map.intro.value.output.output_text|preg_replace('~
                                                   <ul>
                                                      ~','')|preg_replace('~
                                                      <li>~','')|preg_replace('~</li>
                                                      ~','<br />')|preg_replace('~
                                                   </ul>
                                                   ~','')|html_shorten(120,'... ')}
                                                   {/case}
                                                   {/switch}
                                                </td>
                                             </tr>
                                          </table>
                                          {/foreach}
                                       </td>
                                    </tr>
                                    {elseif and($k|eq( 'EU-ADS2' ), $data[$k][2]|count|gt(0))}
                                    {continue}
                                    {else}
                                    {if or($k|eq($type), $d[2]|count|eq(0))}{continue}{/if}
                                    <tr style="padding: 0;">
                                       <td style="padding: 0">
                                          <h2 class="section-heads" style="color: #000; font-weight: bold; font-family: 'arial'; text-transform: uppercase; font-size: 14px; border-bottom: 3px solid #578BAC; padding: 0 0 3px 0;"><a name="{$d[0]|shorten( 4, '' , 'right' )}">{$d[0]}</a></h2>
                                       </td>
                                    </tr>
                                    <tr valign="top">
                                       <td>
                                          <table style="padding-bottom: 6px; margin-left: 0px; margin-bottom: 8px;">
                                          <tbody>
                                             {foreach $d[2] as $key => $item}
                                             <tr>
                                                <td valign="top" width="11" bgcolor="#ffffff">
                                                    <img src="http://www2.staffingindustry.com/extension/site/design/site/images/newsletter/IT_Newsletter_bullet_6-transparent.gif" alt=" - " align="left" />
                                                </td>
                                                <td width="389" bgcolor="#ffffff">
                                                {if and($bottom|contains('CWS 3.0'), eq($key, $items|count()|dec))}
                                                <hr />
                                                {/if}
                                                {switch match=$item.class_identifier}
                                                {case match='news_item'}
                                                <a style="color: #455560;font-size: 13px; line-height:13px; display: block; margin:0px; padding:0px 0px 4px 0px; text-decoration: none;" href={$item|sitelink()}>{$item.data_map.name.content|html_shorten(1000)}</a>
                                                {/case}
                                                {case match='article'}
                                                <a style="color: #455560;font-size: 13px; line-height:13px; display: block; margin:0px; padding:0px 0px 4px 0px; text-decoration: none;" href={$item|sitelink()}>{$item.data_map.title.content}</a>
                                                {/case}
                                                {/switch}
                                                </td>
                                             </tr>
                                             {/foreach}
                                          </tbody>
                                          </table>
                                          <p class="morenews" style="font-size: 11px;text-align: right;margin-bottom: 15px;">
                                            <a style="color: #C41230;" href="http://www2.staffingindustry.com/{$d[1]}/Research-Publications/Daily-News">More News...</a>
                                        </p>
                                       </td>
                                    </tr>
                                    {/if}
                                    {/foreach}
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr valign="bottom">
                           <td style="font-size: 9px; padding: 20px 15px 0 15px;">
                             {$bottom}
                             <p>
                                <preferences>Manage Your Subscription Preferences</preferences>
                                or
                                <unsubscribe>Unsubscribe from all SIA Emails</unsubscribe>
                             </p>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
               <td valign="top" style="padding: 0 10px 10px 0;">
                 {$ads['EU-ADS'][2]}
               </td>
            </tr>
         </tbody>
      </table>
   </body>
</html>
{/set-block}
{$output|fix_sitelink_siteaccess_block()}