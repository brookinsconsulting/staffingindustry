{set-block variable=output}
<!DOCTYPE html5>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<head>
    <title>IT Report Newsletter</title>
    <style type="text/css">
        {literal}
            html, body { background-color: #D9DFE4; font-family: helvetica, arial, sans-serif; }
            html *, body *, table, table * { border-collapse: collapse!important; border-spacing: 0!important; }
            img { max-width: 650px; }
            p { font-size: 14px; }
            h2, .section-heads { font-size: 15px; }
            h3, .small-header { font-size: 15px; }
            #main-table { font-family: helvetica, arial, sans-serif; width: 650px; }
            #inner-table { background-color: #FFFFFF; width: 650px; padding: 0; }
            #inner-header { height: 115px; }
            #table-body { width: 650px; }
            #left-column { padding: 15px; padding-top: 16px; width: 420px; }
            #right-column { width: 200px; padding-top: 15px; }
            a:hover { text-decoration: underline!important; color: #455560!important; }
        {/literal}
    </style>
</head>

<body style="font-family: helvetica, arial, sans-serif;">

<table id="main-table" cellspacing="0" cellpadding="0" style=" vertical-align: top; border-collapse: collapse; width: 650px; font-family: helvetica, arial, sans-serif;">
    <tr id="outer-header">
        <td style="padding-bottom: 10px;">
            <p style="font-size: 11px; padding: 20px 0 0; padding-left: 0;">
                <span style="font-weight: bold">{$newsletter.data_map.issue.content}</span> |
                <a href="http://www2.staffingindustry.com" style="color: #000000; text-decoration: none;">Staffing Industry Analysts Home</a> |
                <a href="http://www2.staffingindustry.com/Research-Publications/Research-Topics/Staffing-Information-Technology" style="color: #000000; text-decoration: none;">IT Topics</a> |
                <a target="new" href="http://staffingindustry.com/Newsletters/Subscribe" style="color: #000000; text-decoration: none;">Subscribe Now</a> |
                <a href="http://www2.staffingindustry.com/About/Contact-Us" style="color: #000000; text-decoration: none;">Contact Us</a>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <table cellspacing="0" border-collapse="collapse" id="inner-table" style="background-color: #FFFFFF; width: 650px; border-collapse: collapse;">
                <tr id="inner-header" style="height: 115px;">
                    <td colspan="2">
                        <img src="http://www2.staffingindustry.com{concat('newsletter/', $banner)|ezimage(no)}" alt="Staffing Industry Analysts I.T. Newsletter Banner" />
                    </td>
                </tr>
                <tr id="table-body" style="width: 650px;">
                    <td id="left-column" style="vertical-align: top; width: 370px; padding-top: 15px;">
                        {foreach $data as $k => $d}
                            <h2 class="section-heads" style="color: #578BAC; font-weight: bold; font-family: 'arial black'; text-transform: uppercase; font-size: 15px; border-bottom: 1px solid #578BAC; padding: 0 0 3px 0;">{$d[0]}</h2>
                            <ul style="margin: 0; padding: 0 0 14px 4px; clear: both;">
                                {foreach $d[2] as $key => $item}
                                    {if eq($key, 0)}
                                        <table style="padding-bottom: 6px; margin-left: -4px; margin-bottom: 8px;">
                                            <tr>
                                                {if eq($item.class_identifier, 'article')}
                                                    {if $item.data_map.ml_image.content}
                                                        <td rowspan="3" style="vertical-align: top;"><div style="float: left; margin-right: 10px; margin-bottom: 10px;">{attribute_view_gui attribute=$item.data_map.ml_image.content.data_map.image href=$node|sitelink() image_class='newsletter'}</div></td>
                                                    {elseif $item.data_map.image.content}
                                                        <td rowspan="3" style="vertical-align: top;"><div style="float: left; margin-right: 10px; margin-bottom: 10px;">{attribute_view_gui attribute=$item.data_map.image href=$node|sitelink() image_class='newsletter'}</div></td>
                                                    {/if}
                                                {elseif $item.data_map.image.content}
                                                    <td rowspan="3" style="vertical-align: top;"><div style="float: left; margin-right: 10px; margin-bottom: 10px;">{attribute_view_gui attribute=$item.data_map.image href=$node|sitelink() image_class='newsletter'}</div></td>
                                                {/if}
                                                <td>
                                                    <h3 class="small-header" style="font-size: 15px; margin: 0; margin-bottom: 3px;">
                                                        {switch match=$item.class_identifier}
                                                            {case match='news_item'}
                                                                    <a style="font-size: 14px; text-decoration: none; color: #000000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.name.content|html_shorten(1000)}</a>
                                                            {/case}
                                                            {case match='article'}
                                                                    <a style="font-size: 14px; text-decoration: none; color: #000000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.title.content}</a>
                                                            {/case}
                                                        {/switch}
                                                    </h3>
                                                </td>
                                            </tr>
                                            <tr>
                                            <td style="margin: 0; font-size: 13px; line-height: 1.3; height:24px; overflow: hidden;">
                                                {switch match=$item.class_identifier}
                                                    {case match='news_item'}
                                                            {$item.data_map.description.value.output.output_text|html_shorten(120,'... ')}
                                                    {/case}
                                                    {case match='article'}
                                                            {$item.data_map.intro.value.output.output_text|preg_replace('~<ul>~','')|preg_replace('~<li>~','')|preg_replace('~</li>~','<br />')|preg_replace('~</ul>~','')|html_shorten(120,'... ')}
                                                    {/case}
                                                {/switch}
                                            </td>
                                            </tr>
                                            <tr>
                                            <td style="margin: 0; font-size: 13px; text-align: right; padding-right: 10px; padding-top: 2px;">
                                                {switch match=$item.class_identifier}
                                                    {case match='news_item'}
                                                            <a style="color: 000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">Read More</a>
                                                    {/case}
                                                    {case match='article'}
                                                            <a style="color: 000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">Read More</a>
                                                    {/case}
                                                {/switch}
                                            </td>
                                            </tr>
                                        </table>
                                    {else}
                                        <li style="clear: both; margin-bottom: 4px; font-size: 13px; list-style-type: disc; position: relative; left: 16px;">
                                            {switch match=$item.class_identifier}
                                                {case match='news_item'}
                                                        <a style="font-size: 14px; text-decoration: none; color: #000000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.name.content|html_shorten(1000)}</a>
                                                {/case}
                                                {case match='article'}
                                                        <a style="font-size: 14px; text-decoration: none; color: #000000;" href="{concat('http://www2.staffingindustry.com', $item|sitelink('no'))}">{$item.data_map.title.content}</a>
                                                {/case}
                                            {/switch}

                                        </li>
                                    {/if}
                                {/foreach}
                            </ul>
                        {/foreach}
                        <h2 class="section-heads" style="font-weight: bold; font-family: 'arial black'; color: #578BAC; text-transform: uppercase; font-size: 15px; border-bottom: 1px solid #578BAC; padding: 0 0 3px 0;">Questions About IT Staffing Research?</h2>
                        <table>
                            <tr>
                                <td style="vertical-align: top; width: 62px;">
                                    <img style="margin-right: 10px; margin-bottom: 10px; float: left" src="http://www2.staffingindustry.com{concat('newsletter/', 'Andrew-Braswell-_HCSR_1403_large.jpg')|ezimage(no)}" alt="Andrew Braswell" />
                                </td>
                                <td style="vertical-align: top;">
                                    <h3 class="small-header" style="font-size: 13px; margin: 0; margin-bottom: 3px;">Contact</h3>
                                    <p style="margin: 2px 0; font-size: 13px; padding: 0;">Andrew Braswell, Senior Research Analyst</p>
                                    <p style="margin: 2px 0; font-size: 13px; padding: 0;"><a style="color: #000000; text-decoration: none;" href="mailto:abraswell@staffingindustry.com">abraswell@staffingindustry.com</p>
                                </td>
                            </tr>
                            <tr id="inner-footer">
                                <td colspan="2">
                                    <p style="margin: 2px 0; padding: 0px; font-size: 13px; text-align: left;">
                                        Do you have IT-related news?<br />
                                    </p>
                                    <p style="margin: 2px 0; padding: 0px; font-size: 13px; text-align: left;">
                                        Contact: Katherine Alvarez, <a style="color: #000000; text-decoration: none;" href="mailto:kalvarez@staffingindustry.com">kalvarez@staffingindustry.com</a>
                                    </p>
                                </td>
                            </tr>
                        </table>                    </td>
                    <td id="right-column" style="vertical-align: top; width: 200px; padding-top: 41px; padding-bottom: 18px;">
                        {$newsletter.data_map.ad_code.content}
                    </td>
                </tr>
            </table>
        </td>
        <tr id="outer-footer">
            <td style="padding-bottom: 20px;">
                <p style="padding: 20px 20px 0; font-size: 11px; text-align: center;">
                    Staffing Industry Analysts | 1975 W. El Camino Real, Ste. 304 | Mountain View, CA 94040
                </p>
            </td>
        </tr>
        <tr valign="bottom">
            <td style="font-size: 9px">
                {$bottom}
                <p style="font-size: 9px;"><preferences>Manage Your Subscription Preferences</preferences> or <unsubscribe>Unsubscribe From All SIA Emails</unsubscribe></p>
            </td>
        </tr>
    </tr>
</table>

</body>

</html>
{/set-block}
{$output|fix_sitelink_siteaccess_block()}
