{switch match=cartcurrency()}
    {case match='USD'}
        {def $add_to_cart_siteaccess = 'site'}
    {/case}
    {case match='EUR'}
        {def $add_to_cart_siteaccess = 'eng'}
    {/case}
{/switch}

    <div id="secondary-header">
    <div class="body-container">
        <nav id="social-media">
            <ul class="menu horizontal">
                <li>
                    <a class="social_media_icons" title="Facebook" target="_blank" href="https://www.facebook.com/companysia/"><img src="/extension/site/design/site/images/new-social/social-fb.png" /><img src="/extension/site/design/site/images/new-social/social-fb-hover.png"/></a>
                </li>
                <li>
                    <a class="social_media_icons" title="Twitter" href="{if $module_result.uri|contains('/eng')|not}https://twitter.com/SIAnalysts{else}http://twitter.com/siaeurope{/if}" target="_blank"><img src="/extension/site/design/site/images/new-social/social-twitter.png" /><img src="/extension/site/design/site/images/new-social/social-twitter-hover.png" /></a>
                </li>
                <li>
                    <a class="social_media_icons" title="LinkedIn" href="https://www.linkedin.com/company/staffing-industry-analysts" target="_blank"><img src="/extension/site/design/site/images/new-social/social-linkedin.png" /><img src="/extension/site/design/site/images/new-social/social-linkedin-hover.png"/></a>
                </li>
                <li>
                    <a class="social_media_icons" title="Youtube" href="https://www.youtube.com/channel/UC_vIj8e1cQ3a_ZHatFOcQKA" target="_blank"><img src="/extension/site/design/site/images/new-social/social-youtube.png" /><img src="/extension/site/design/site/images/new-social/social-youtube-hover.png"/></a>
                </li>
                <li class="menu-sep">|</li>
                <li class="feedback">
                    <a title="Website Feedback" href="http://www2.staffingindustry.com/site_member/Website-Feedback/">Website Feedback</a>
                </li>
                <li class="no-display">
                    {literal}
                    <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
                    <script type="IN/FollowCompany" data-counter="none" data-id="29944"></script>
                    {/literal}
                </li>
            </ul>
        </nav>
    </div>
</div>

<header role="banner"{if eq(cookie('usercontrolpanel'),'hide')} style="margin-top:-30px;"{/if}>
<div class="body-container">


    <a id="logo" href="/"><img title="{$logo_alt}" alt="{$logo_alt}" src={'images/SIA_HeaderLogo.jpg'|ezdesign()} width="120px" height="120px" /></a>

    {*include uri="design:parts/user_control_panel.tpl"*}
<!--    <ul id="user-pane" class="menu horizontal">
        {if $current_user.is_logged_in}
            <li><a href="/user/logout"><strong>Logout:</strong> {$current_user.contentobject.data_map.first_name.content|wash()} {$current_user.contentobject.data_map.last_name.content|wash()}</a></li>
        {else}
            <li class="site_login">
                <div id="login_form">
                <form method="post"  action={"/user/login/"|sitelink()} name="loginform">
                <input type="hidden" name="RedirectURI" value="/login/redirect/{"/content/dashboard"|base64_encode}" />
                <ul>
                    <li class="loginbox"><input placeholder='Username' class="halfbox" type="text" size="10" name="Login" id="id1" value="{$User:login|wash}" tabindex="1" />
                        <div id="login" class="no-display">
                            <div class="block">
                                <input placeholder='Password' class="halfbox" type="password" size="10" name="Password" id="id2" value="" tabindex="1" />
                            </div>
                        </div>
                    </li>
                    <li class="first"><a href="/user/login">Login / Sign Up</a><input class="no-display defaultbutton button" type="submit" name="LoginButton" value="{'Login'|i18n('design/ezwebin/user/login','Button')}" tabindex="1" /></li>
                </ul>
                </form>
                </div>
            </li>
            {*<li class="last"><a class="nav-link" href="/user/register">Register</a></li>*}
        {/if}
        {def $language_list=language_switcher($site.uri.original_uri)
             $a_t_n = $access_type.name|explode("_")[0]
             $current_language=$language_list[$a_t_n].text|wash()
             $ucp_hide=eq(cookie('usercontrolpanel'),'hide')
        }
        <li id="language-switcher">
            <div id="languages">
                <form id='lang_switch'>
                    {foreach $language_list as $siteaccess=>$language}
                        <input{if array('shop', 'xrowecommerce')|contains($pagedata.module_parameters.module_name)} disabled='true'{/if} type='radio' name='cur_lang' rel='{$requested_uri_string|trim('/')}' value='{$siteaccess}' {if eq($siteaccess,$a_t_n)} checked='true'{/if}/>
                        <label>{cond($language.text|contains('Rest')|not, $language.text|wash(), '<span>Asia Pacific</span>/<span>Latin America</span>/<span>Middle East &amp; Africa</span>')}</label>
                    {/foreach}
                </form>
            </div>
        </li>
    </ul>

    <div class="dashboard-link">
        <a href={"/content/dashboard"|sitelink()}>User Dashboard</a>
    </div>
-->

    <div id="site-tagline">
        <p>SIA is the Global Advisor on Staffing and Workforce Solutions</p>
    </div>

    <div class="searchblock">
        <div class="login_form">

            {default current_user=fetch('user','current_user')}
                {section show=$current_user.is_logged_in}
                    <a href="/user/logout" class="logoutbtn">Log out: {$current_user.contentobject.data_map.first_name.content|wash()} {$current_user.contentobject.data_map.last_name.content|wash()}</a>
                    <span id="dashboard" class="siausername"><a href="/content/dashboard">User Dashboard </a></span>
                {section-else}
                    <a href="/user/login" class="logoutbtn">Log in</a>
<!--                    <span id="overlaylaunch-inAbox">Log in</span> <span class="res_src_icon"></span>
                    {literal}
                        <script type="text/javascript">$('#overlaylaunch-inAbox').click(function() {$('#overlay-inAbox').hide().css('display', 'block');});</script>
                    {/literal} -->
                {/section}
            {/default}

        </div>

        <div id="search">
<!--            <a class="tips" href="/content/download/165059/6372186/file/WebsiteSearchTipsFnl_140416.pdf" alt="Search Tips">Search Tips</a> -->
            {include uri="design:parts/searchform.tpl" placeholder='Search'}
        </div>

<!--        <nav id="links">
            <ul class="menu vertical">
                <li class="cart"><a href={if $add_to_cart_siteaccess|ne('')}"/{$add_to_cart_siteaccess}/xrowecommerce/cart"{else}{"/xrowecommerce/cart"|sitelink()}{/if}><img alt="" src={'images/shopping-cart.png'|ezdesign()} width="18" height="14" />Cart ({$basket_count})</a></li>
                {*<li><a href={"/content/dashboard"|sitelink()}>User Dashboard</a></li>*}
                {*<li><a href="/contact">Contact Us</a></li>*}
            </ul>
        </nav> -->
    </div>

    {def
        $server_vars      = ezservervars()
        $current_url      = $requested_uri_string
        $current_language = $language_list[ $access_type.name|explode("_")[0] ].text|wash()
    }
    {if $server_vars['QUERY_STRING']}
        {set $current_url = concat( $current_url, '?', $server_vars['QUERY_STRING'] )}
    {/if}
    {def $language_list = language_switcher( $current_url )}
    <div class="frm1">
          <input type="submit" value="REGION">
          <div class="sel_box">
            <select id="region-selector" name="region-select" onChange="window.location.href=this.value">
{*
                <option value="/">NORTH AMERICA</option>
                <option value="/eng/">EUROPE</option>
                <option value="/row/">ASIA PACIFIC</option>
                <option value="/row/">LATIN AMERICA</option>
                <option value="/row/">MIDDLE EAST</option>
                <option value="/row/">AFRICA</option>
*}
                {foreach $language_list as $siteaccess => $language}
                    <option value="{$language.url}"{if eq( $current_language, $language.text )} selected="selected"{/if}>{if eq( $siteaccess, 'row' )}Asia Pacific/Latin America/Middle East & Africa{else}{$language.text}{/if}</option>
                {/foreach}
            </select>
          </div>
    </div>


{def $fetch_node=fetch('content','node',hash('node_id',216))
 $articles=fetch('content','list',hash('parent_node_id',216,
                                        'limit',5,
                                        'sort_by',$fetch_node.sort_array,
                                        'class_filter_type','include',
                                        'class_filter_array',array('folder')
                                        ))
 $show_image=false()
}

{*<img id="event-scrollable-demo" alt="" src={'images/temp/event-scrollable-demo.png'|ezdesign()} width="497" height="89" />*}
</div>

<nav id="global-menubar">
    {include uri="design:menu/menubar.tpl"
        root_node=fetch('content','node',hash('node_id',$indexpage))
        show_header=false()
        orientation='horizontal'
        include_root_node=cond($pagedata.homepage,false(),true())
        delimiter='<img alt="" src="/extension/site/design/site/images/global-divider.jpg" width="2" height="42">'
        submenu=true()
        attribute_filter=array('and', array('priority','between',array(1,10)), array( 'folder/hide_from', '!=', '1' ))
    }
</nav>

</header>
