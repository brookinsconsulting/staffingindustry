{def $meta_content = ''}
{if is_set($pagedata)|not}{def $pagedata = pagedata()}{/if}
{if and(module_params().module_name|eq('xrowecommerce'), module_params().function_name|eq('userregister'))}
    {def $title = 'Checkout - Staffing Industry Analysts Web Store'}
{elseif $pagedata.title|contains('/ Misc Marketing ')}
    {def $title = $pagedata.title|explode('/ Misc Marketing ')|implode()}
{else}
    {def $title = $pagedata.title}
{/if}
<title>{$title}</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
{if $site.redirect}<meta http-equiv="Refresh" content="{$site.redirect.timer}; URL={$site.redirect.location}" />{/if}
{foreach $site.meta as $key=>$item}
{if and(not($pagedata.is_error), array('description','keywords')|contains($key))}
{if and($pagedata.is_content,is_set($current_node.data_map[concat('meta_',$key)]), $current_node.data_map[concat('meta_',$key)].has_content)}
{*set-block variable='item'}{attribute_view_gui attribute=$current_node.data_map[concat('meta_',$key)]}{/set-block*}
{if eq($current_node.data_map[concat('meta_',$key)].data_type_string, 'ezkeyword')}
    {set $meta_content = $current_node.data_map[concat('meta_',$key)].content.keyword_string}
{else}
    {set $meta_content = $current_node.data_map[concat('meta_',$key)].content}
{/if}

{/if}
{/if}
<meta name="{$key|wash()}" content="{$meta_content|wash()|trim()}" />

{/foreach}
<meta name="MSSmartTagsPreventParsing" content="TRUE" />
<meta name="generator" content="eZ Publish" />

{if $pagedata.is_content}<link rel="canonical" href={$current_node.object.main_node|sitelink(,true())} />{/if}

{if first_set($enable_link,true())}{include uri="design:link.tpl" enable_help=first_set($enable_help,true()) enable_link=first_set($enable_link,true())}{/if}

<link rel="Shortcut Icon" href={'images/favicon.png'|ezdesign()} type="image/x-icon" />
<link rel="apple-touch-icon" href={'images/favicon-iphone.png'|ezdesign()}  type="image/x-icon" />
