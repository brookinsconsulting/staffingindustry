{set-block scope=global variable=cache_ttl}0{/set-block}
{* i set node=2 up here, but that's fake. the big array is complied below in * magic * *}
{def
	$uri="design:content/datatype/view/ezxmltags/listsubitems.tpl"
	$page="eznode://2"
	$header="My Top Articles"
	$depth="tree"
	$children_view=cond(
		ezpreference(concat('dashboard-block-', $block.identifier, '-children_view'))|ne(''),
		ezpreference(concat('dashboard-block-', $block.identifier, '-children_view')),
		"my_top_articles_line"
	)
	$limit=cond(
		ezpreference(concat('dashboard-block-', $block.identifier, '-limit'))|ne(''),
		ezpreference(concat('dashboard-block-', $block.identifier, '-limit')),
		"3"
	)
	$stylize='modular'
	$alltext='$none'
}

{include uri="design:parts/dashboard_customize_block.tpl"}

{* magic *}
{def	$topics = fetch('content', 'list', hash('parent_node_id', 70472))
	$selected_topic = false()
	$selected_topics = ezpreference('dashboard-my-top-articles')|explode(',')
}
{foreach $topics as $topic}
	{set $selected_topic = $selected_topics|contains($topic.node_id)}
{/foreach}
{* if nothing is checked (probably first load), get everything *}
{if $selected_topics|count|eq(0)}
	{set $selected_topics = array(70472)}
{/if}
{* end magic *}

{* List Sub Items - Custom Tag *}
{def	$node = fetch('content','node',hash('node_id',$page|explode('://')[1]))
	$ticker_limit=cond(eq($stylize,'ticker'),ezini('CustomAttribute_listsubitems_stylize','TickerLimit','ezoe_attributes.ini'),false())
	$dataNodes = fetch('content', 'tree', hash(
		'parent_node_id',$selected_topics,
		'limit', first_set(cond($ticker_limit,$ticker_limit,$limit),3),
		'class_filter_type', 'include',
		'class_filter_array', array('article'),
		'sort_by', array('attribute', false(), 'article/publish_date')
	))
	$has_header = and(is_set($header),ne($header,''))
	$rssexport=rssexport($node.node_id)
}
{def $rsssource = fetch(content, node, hash(node_id, $rssexport.source_node_id))}
<section class="{if eq($stylize,'advanced')}advanced{/if}{if ne($stylize,'basic')} module{/if} customtag custom-tag-listsubitems">
	<header><h1>{if or($has_header, eq($stylize,'custom'))}{cond(eq($header,'$content'),$content,$header)}{else}{$node.name|wash()}{/if}</h1>{if count($rssexport)}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezroot('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /></a>{/if}</header>
	<section class="customtag-content view-item-list">
	{foreach $dataNodes as $subitem}
		{node_view_gui view=$children_view content_node=$subitem htmlshorten=cond(eq($children_view,'listitem'), array(80,concat('... <a href="',$subitem|sitelink('no'),'">More</a>')), false() )}
		{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
	{/foreach}
{if ne($stylize,'basic')}
		{if is_set($alltext)}{if ne($alltext,'$none')}<a class="seeall" href={$node|sitelink()}>{$alltext}</a>{/if}{else}<a class="seeall" href={$node|sitelink()}>See All</a>{/if}
{/if}
	</section>
</section>
