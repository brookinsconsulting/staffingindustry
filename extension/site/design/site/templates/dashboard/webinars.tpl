{def
	$webinars = fetch('content', 'tree', hash(
		'parent_node_id', 2,
		'main_node_only', true(),
		'attribute_filter', array(array('xrow_product/webinar', '!=', ''))
	))
	$webinar_attendees = array()
	$webinar_id = false()
	$attendee_id = false()
}

{include uri="design:parts/dashboard_customize_block.tpl"}

<section class="module customtag custom-tag-listsubitems">
	<header><h1>Webinars</h1></header>
	<section class="customtag-content view-item-list">
	<ul>
	{foreach $webinars as $webinar}
		{if is_set($webinar.object|purchaser_list()[$current_user.contentobject_id])}
		<li>
			{set
				$webinar_attendees = webex_getattendees($webinar.data_map.webinar.content)
				$webinar_id = $webinar.data_map.webinar.content
			}
			{$webinar_attendees|debug($webinar_id)}
			<h2>{$webinar.name}</h2>
			{if $webinar_attendees|count|gt(0)}
				<ul class="webinar_attendees">
				{foreach $webinar_attendees as $key => $attendee}
					{set $attendee_id = $key|explode('_')[1]}
					{if eq(ezpreference(concat('attendee_', $attendee_id)), $attendee_id)}
						<li>{$attendee.person.name} &lt;{$attendee.person.email}&gt; (<a class="removeattendee" id="{$attendee_id}" href="#">remove</a>)</li>
					{/if}
				{/foreach}
				</ul>
			{else}
				<p>No attendees registered.</p>
			{/if}
			<p><a class="addattendee {$webinar_id}" rel="{$webinar_id}" href="#">Add attendee</a></p>
			<form style="display:none" class="addattendee {$webinar_id}">
				<input type="hidden" name="webinar_id" value="{$webinar_id}" />
				
				<label for="attendee_email_{$webinar_id}">Email</label>
				<input name="attendee_email" id="attendee_email_{$webinar_id}" />
				<br />

				<label for="attendee_name_{$webinar_id}">Name</label>
				<input name="attendee_name" id="attendee_name_{$webinar_id}" />
				<br />

				<label for="attendee_title_{$webinar_id}">Title</label>
				<input name="attendee_title" id="attendee_title_{$webinar_id}" />
				<br />

				<label for="attendee_company_{$webinar_id}">Company</label>
				<input name="attendee_company" id="attendee_company_{$webinar_id}" />
				<br />

				<input type="submit" value="Add" />
			</form>
		</li>
		{/if}
	{/foreach}
	</ul>
	</section>
</section>

<script>
{literal}
$(function(){
	$('a.addattendee').click(function(){
		var webinar_id = $(this).attr('rel');
		$(this).parent().siblings('form.addattendee.' + webinar_id).clone(true, true).dialog({
			modal: true,
			width: '60%'
		});
		return false;
	});

	$('a.removeattendee').click(function(){
		var url = '/webex/removeattendee';
		var data = 'attendee_id=' + $(this).attr('id');
		$.ajax({
			url: url,
			data: data,
			type: 'POST',
			async: false,
			success: function(){
				//window.location.reload();
				window.location.href = "/content/dashboard";
			},
			error: function(xhr){
				confirm(xhr.responseText);
			}
		});
		return false;
	});

	$('form.addattendee').submit(function(){
		var form = $(this);
		form.find('input[type=submit]').attr('disabled', 'disabled');
		form.find('input[type=submit]').val('Adding...');
		var url = '/webex/addattendee';
		var data = $(this).serialize();
		var webinar_id = $(this).find('input[name=webinar_id]').val();
		$.ajax({
			url: url,
			data: data,
			type: 'POST',
			success: function(response){
				var attendee_id = response;
				admin2ppAjaxSavePreference(
					'attendee_' + attendee_id,
					attendee_id,
					true
				)
			},
			error: function(xhr){
				form.find('input[type=submit]').removeAttr('disabled');
				form.find('input[type=submit]').val('Add');
				confirm(xhr.responseText);
			}
		});
		return false;
	});
});
{/literal}
</script>
