{def
	$search_link_text = false()
	$search_query_string = false()
}
{if ezpreference('dashboard-block-saved_searches')|eq('')}
	{def $savedsearches = false()}
{else}
	{def $savedsearches = ezpreference('dashboard-block-saved_searches')|explode('-')|implode('/')|explode(',')}
{/if}

{include uri="design:parts/dashboard_customize_block.tpl"}

<section class="module customtag custom-tag-listsubitems">
	<header><h1>Saved Searches</h1></header>
	<section class="customtag-content view-item-list">
	{if $savedsearches}
		<ul>
		{foreach $savedsearches as $search}
			{set $search = base64_decode($search)}
			{foreach $search|explode('?')[1]|explode('&') as $parameter}
				{if $parameter|explode('=')[0]|eq('SearchText')}
					{set $search_link_text = $parameter|explode('=')[1]}
				{/if}
			{/foreach}
			<li><a href={$search|sitelink()}>{$search_link_text}</a></li>
		{/foreach}
		</ul>
		<p><a href="#" onclick="admin2ppAjaxSavePreference('dashboard-block-saved_searches', '', true); return false;">Clear saved searches</a></p>
	{else}
		<p>You have no saved searches. Go to the <a href={'/content/search'|sitelink()}>search page</a> to save a search.</p>
	{/if}
	</section>
</section>
