{include uri="design:parts/dashboard_customize_block.tpl"}

{include
	uri="design:content/datatype/view/ezxmltags/listsubitems.tpl"
	page="eznode://70472"
	header="Topics"
	depth="list"
	children_view=cond(
		ezpreference(concat('dashboard-block-', $block.identifier, '-children_view'))|ne(''),
		ezpreference(concat('dashboard-block-', $block.identifier, '-children_view')),
		"listitem"
	)
	limit=cond(
		ezpreference(concat('dashboard-block-', $block.identifier, '-limit'))|ne(''),
		ezpreference(concat('dashboard-block-', $block.identifier, '-limit')),
		"10"
	)
	stylize='modular'
	alltext='More'
}
