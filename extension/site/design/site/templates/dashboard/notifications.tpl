{def
	$uri="design:content/datatype/view/ezxmltags/listsubitems.tpl"
	$page="eznode://40349"
	$header="Notifications from Staffing Industry Analysts"
	$depth="list"
	$children_view=cond(
		ezpreference(concat('dashboard-block-', $block.identifier, '-children_view'))|ne(''),
		ezpreference(concat('dashboard-block-', $block.identifier, '-children_view')),
		"embed-inline"
	)
	$limit=cond(
		ezpreference(concat('dashboard-block-', $block.identifier, '-limit'))|ne(''),
		ezpreference(concat('dashboard-block-', $block.identifier, '-limit')),
		"3"
	)
	$stylize='modular'
	$alltext='$none'
}

{include uri="design:parts/dashboard_customize_block.tpl"}

{* List Sub Items - Custom Tag *}
{def $node = fetch('content','node',hash('node_id',$page|explode('://')[1]))
	 $ticker_limit=cond(eq($stylize,'ticker'),ezini('CustomAttribute_listsubitems_stylize','TickerLimit','ezoe_attributes.ini'),false())
	 $dataNodes = fetch('content', 'tree', hash('parent_node_id',$node.node_id,
						'limit', first_set(cond($ticker_limit,$ticker_limit,$limit),3),
						'class_filter_type', 'exclude',
						'class_filter_array', array('folder'),
						'sort_by', $node.sort_array))
	 $has_header = and(is_set($header),ne($header,''))
	 $rssexport=rssexport($node.node_id)
}
{def $rsssource = fetch(content, node, hash(node_id, $rssexport.source_node_id))}
<section class="{if eq($stylize,'advanced')}advanced{/if}{if ne($stylize,'basic')} module{/if} customtag custom-tag-listsubitems">
	<header><h1>{if or($has_header, eq($stylize,'custom'))}{cond(eq($header,'$content'),$content,$header)}{else}{$node.name|wash()}{/if}</h1>{if count($rssexport)}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezroot('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /></a>{/if}</header>
	<section class="customtag-content view-item-list">
	{def	$current_user_object = fetch('content', 'object', hash('object_id', $current_user.contentobject_id))
		$subscriptions = fetch('content', 'list', hash(
			'parent_node_id', $current_user_object.main_node_id,
			'class_filter_type', 'include',
			'class_filter_array', array('subscription'),
			'language', array('eng-RW', 'eng-GB@euro', 'eng-US'),
			'limitation', array()
		))
	}
	{* the 'limitation' override passed to the subscription fetch should really not be necessary, but i couldn't figure out how to set up permissions so that was the case *}
	{foreach $subscriptions as $subscription}
		{if $subscription.data_map.end_date.has_content}
			{def	$days = $subscription.data_map.end_date.content.timestamp|sub(currentdate())|div(86400)|floor}
			{if $days|lt(60)}
				{*
				Your {$subscription.name} expires on {$subscription.data_map.end_date.content.timestamp|l10n('date')}.
				*}
				Your {$subscription.name} expires in {$days} days.
				<br />{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}
			{/if}
		{/if}
	{/foreach}
	{foreach $dataNodes as $subitem}
	{*
		{node_view_gui view=$children_view content_node=$subitem htmlshorten=cond(eq($children_view,'listitem'), array(80,concat('... <a href="',$subitem|sitelink('no'),'">More</a>')), false() )}
	*}
		{if $subitem.data_map.product.has_content}
			{if is_set($subitem.data_map.product.content|purchaser_list()[$current_user.contentobject_id])} {* has current_user purchased the related product? *}
				{if $subitem.data_map.purchased_link.has_content}
					<a href={$subitem.data_map.purchased_link.content}>{$subitem.data_map.purchased_message.content}</a>
				{else}
					{$subitem.data_map.purchased_message.content}
				{/if}
			{else}
				{if $subitem.data_map.link.has_content}
					<a href={$subitem.data_map.link.content}>{$subitem.data_map.message.content}</a>
				{else}
					{$subitem.data_map.message.content}
				{/if}
			{/if}
		{else}
			{if $subitem.data_map.link.has_content}
				<a href={$subitem.data_map.link.content}>{$subitem.data_map.message.content}</a>
			{else}
				{$subitem.data_map.message.content}
			{/if}
		{/if}
		{delimiter}<br />{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
	{/foreach}

{if ne($stylize,'basic')}
		{if is_set($alltext)}{if ne($alltext,'$none')}<a class="seeall" href={$node|sitelink()}>{$alltext}</a>{/if}{else}<a class="seeall" href={$node|sitelink()}>See All</a>{/if}
{/if}
	</section>
</section>
