<div class="shop shop-payment shop-payment-selectgateway">

{include uri="design:shop/basket_navigator.tpl" step='4'}
{$event|attribute(show,2,false())|debug('Event')}
{$product_item|attribute(show,2,false())|debug('Product')}
<h1>{'Select a payment method'|i18n('extension/xrowecommerce')}</h1>

{def $gateways = $event.allowed_gateways}
{if $gateways|gt(0)}
    <p>{'Please select your desired payment method below.'|i18n('extension/xrowecommerce')}</p>
<form method="post" action={"xrowecommerce/checkout"|ezurl}>
<input class="hide" style="display: hide;" type="submit" name="SelectButton"  value="{'Select'|i18n('extension/xrowecommerce')}" />
    <ul id="gateways">
    {foreach $gateways as $gateway}
    {if $gateway.Name|eq('Cybersource')}
        <li><input class="commerce_radiobutton" type="radio" name="SelectedGateway" value="{$gateway.value}" {run-once} checked="checked"  {/run-once}/><span>Pay by Credit Card</span>
        {if concat('gateway/', $gateway.value, '.png')|ezimage(no)|begins_with( '/extension' )}
        <img src={concat('gateway/', $gateway.value, '.png')|ezimage} />
        {/if}
        </li>
	{/if}
    {/foreach}
    </ul>
    <p>If you do not wish to use a credit card, please call us.<br />
    Within North America: 800-950-9496 or 650-390-6200<br />
    Outside North America: +44 (0)1462 442605<br /><br />

    We will be happy to help you.<p>
    <div id="buttonblock-bottom" class="buttonblock">
        <input id="cancel-button" class="button left-arrow2" type="submit" name="CancelButton" value="{'One step back'|i18n('extension/xrowecommerce')}" title="{'One step back'|i18n('extension/xrowecommerce')}"/>
        <input id="continue-button" class="button right-arrow2" type="submit" name="SelectButton" value="{'Continue'|i18n('extension/xrowecommerce')}" title="{'Continue'|i18n('extension/xrowecommerce')}"/>
    </div>
</form>
{else}
 <p>{'You do not have permission to use any of the available payment methods.'|i18n('extension/xrowecommerce')}</p>
 <div class="buttonblock">
    <input id="cancel-button" class="button" type="submit" name="CancelButton"  value="{'Cancel'|i18n('extension/xrowecommerce')}" />
    <div class="break"></div>
 </div>
{/if}

</div>
