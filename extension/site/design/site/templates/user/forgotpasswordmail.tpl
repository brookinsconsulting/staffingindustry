{let site_url=ezini("SiteSettings","SiteURL")}
{set-block scope=root variable=subject}{"%siteurl new password"|i18n("design/standard/user/forgotpassword",,hash('%siteurl',$site_url))}{/set-block}
{*
{"Your account information"|i18n('design/standard/user/forgotpassword')}
*}
{if not($password)}
{"Email"|i18n('design/standard/user/forgotpassword')}: {$user.email}
{/if}

{if $link}
<br /><br />
Click on the link below to generate your new password which will be sent to you in a separate email.
<br /><br />
{*
{"Click here to get new password"|i18n('design/standard/user/forgotpassword')}:
*}
{concat("user/forgotpassword/", $hash_key, '/')|ezurl(no,'full')}
{else}


{if $password}
{"New password"|i18n('design/standard/user/forgotpassword')}:<br />{$password}<br />
{/if}

{/if}

{/let}
