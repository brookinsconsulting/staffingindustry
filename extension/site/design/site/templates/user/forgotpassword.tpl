<div class="border-box">
<div class="border-tl"><div class="border-tr"><div class="border-tc"></div></div></div>
<div class="border-ml"><div class="border-mr"><div class="border-mc float-break">

<div class="user-forgotpassword">

{if $link}
<p>
	1. An email has been sent to the following address: {$email}.<br />
	2. Click on the link in the email to generate your new password which will be sent to you in a separate email.<br />
	3. If you are having any issues resetting your password please email <a href="mailto:memberservices@staffingindustry.com">memberservices@staffingindustry.com</a> or call us and we will be happy assist you (US 800-950-9496 or Europe +44 (0) 207 194 7759).
</p>
{else}
   {if $wrong_email}
   <div class="warning">
   <h2>{"There is no registered user with that email address."|i18n('design/ezwebin/user/forgotpassword')}</h2>
   </div>
   {/if}
   {if $generated}
   <p>
   {"Password was successfully generated and sent to: %1"|i18n('design/ezwebin/user/forgotpassword',,array($email))}
   </p>
   {else}
      {if $wrong_key}
      <div class="warning">
      <h2>{"The key is invalid or has been used. "|i18n('design/ezwebin/user/forgotpassword')}</h2>
      </div>
      {else}
      <form method="post" name="forgotpassword" action={"/user/forgotpassword/"|ezurl}>

      <div class="attribute-header">
      <h1 class="long">{"Have you forgotten your password?"|i18n('design/ezwebin/user/forgotpassword')}</h1>
      </div>

	<p>
		This is the first of a two-steps process:<br />
		1. Enter your email address below and a user confirmation email will be sent to you.<br />
		2. Click on the link in the confirmation email to generate a second email which contains your new password.<br />
        3. If you are having any issues resetting your password please email <a href="mailto:memberservices@staffingindustry.com">memberservices@staffingindustry.com</a> or call us and we will be happy assist you (US 800-950-9496 or Europe +44 (0) 207 194 7759).
	</p>

      <div class="block">
      <label for="email">{"Email"|i18n('design/ezwebin/user/forgotpassword')}:</label>
      <div class="labelbreak"></div>
      <input class="halfbox" type="text" name="UserEmail" size="40" value="{$wrong_email|wash}" />
      </div>

      <div class="buttonblock">
      <input class="button" type="submit" name="GenerateButton" value="Submit" />
      </div>
      </form>
      {/if}
   {/if}
{/if}

</div>

</div></div></div>
<div class="border-bl"><div class="border-br"><div class="border-bc"></div></div></div>
</div>