{if ezservervars()['REQUEST_URI']|contains('layout/set/overlay')|not}
	{include
		uri="design:content/datatype/view/ezxmltags/realmedia.tpl"
		position='Top'
	}
{else}
	{include
		uri="design:content/datatype/view/ezxmltags/realmedia.tpl"
		position='Position1'
	}
{/if}

<div class="user-login">

<form method="post"  action={"/user/login/"|sitelink()} name="loginform">

<div class="attribute-header">
    <h1 class="long">{"Login"|i18n("design/ezwebin/user/login")}</h1>
</div>

{if $User:warning.bad_login}
<div class="warning bad-login">
<h2>{"Could not login"|i18n("design/ezwebin/user/login")}</h2>
<ul>
    <li>{"A valid username and password is required to login."|i18n("design/ezwebin/user/login")}</li>
</ul>
</div>
{else}

{if $site_access.allowed|not}
<div class="warning">
<h2>{"Access not allowed"|i18n("design/ezwebin/user/login")}</h2>
<ul>
    <li>{"You are not allowed to access %1."|i18n("design/ezwebin/user/login",,array($site_access.name))}</li>
</ul>
</div>
{/if}

{/if}

<div class="block">
<label for="id1" class="invisible">{"Username"|i18n("design/ezwebin/user/login",'User name')}</label><div class="labelbreak"></div>
<input class="halfbox" type="text" size="10" name="Login" id="id1" value="" placeholder="Username" tabindex="1" />
</div>

<div class="block">
<label for="id2" class="invisible">{"Password"|i18n("design/ezwebin/user/login")}</label><div class="labelbreak"></div>
<input class="halfbox" type="password" size="10" name="Password" id="id2" value="" placeholder="Password" tabindex="1" />
</div>

{if ezini( 'Session', 'RememberMeTimeout' )}
<div class="block">
<input type="checkbox" tabindex="1" name="Cookie" id="id4" /><label for="id4" style="display:inline;">{"Remember me"|i18n("design/ezwebin/user/login")}</label>
</div>
{/if}

<div class="buttonblock">
<input class="defaultbutton button" type="submit" name="LoginButton" value="{'Login'|i18n('design/ezwebin/user/login','Button')}" tabindex="1" />
{if ezmodule( 'user/register' )}
    <input class="button" type="submit" name="RegisterButton" id="RegisterButton" value="{'Sign up'|i18n('design/ezwebin/user/login','Button')}" tabindex="1" />
{/if}
   <a href={'/user/forgotpassword'|ezurl} class="forgot"><input type="button" class="button" onclick="document.location.href='{'/user/forgotpassword'|ezurl(no)}'; return false;" value="{'Forgot your password?'|i18n( 'design/ezwebin/user/login' )}" /></a>
</div>

{*if ezini( 'SiteSettings', 'LoginPage' )|eq( 'custom' )}
    <p><a href={'/user/forgotpassword'|ezurl}>{'Forgot your password?'|i18n( 'design/ezwebin/user/login' )}</a></p>
{/if*}
{* <input type="hidden" name="RedirectURI" value="/login/redirect/{cond($User:redirect_uri, $User:redirect_uri, "/content/dashboard")|base64_encode}" /> *}
<input type="hidden" name="OriginalRedirectURI" value="{if ezhttp_hasvariable( 'OriginalRedirectURI', 'post' )}{ezhttp( 'OriginalRedirectURI', 'post' )|wash}{else}{$User:redirect_uri}{/if}" />
<input type="hidden" name="RedirectURI" value="{if $User:redirect_uri}{$User:redirect_uri}{else}/login/redirect/{'wp/login_redirect'|base64_encode}{/if}" />
{literal}
<script>

$(function(){
	if(window.self.location!==window.top.location){
		document.loginform.target = "_top";
		// There is a new extra slash after encoding "?" sign. Thats why we are using "|" instead of "?"
		var url = 'wp/login_redirect|redirect_url=' + window.top.location;
		$('input[name=RedirectURI]').val('/login/redirect/'+base64_encode(url));
	}
});

</script>
{/literal}

{if and( is_set( $User:post_data ), is_array( $User:post_data ) )}
  {foreach $User:post_data as $key => $postData}
     <input name="Last_{$key}" value="{$postData}" type="hidden" /><br/>
  {/foreach}
{/if}

</form>

</div>
