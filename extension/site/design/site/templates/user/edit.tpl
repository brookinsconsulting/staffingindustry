{def	$user=fetch('content','object',hash('object_id',$userID))
	$language=$user.initial_language.locale
	$tipafriend_access=fetch('user','has_access_to',hash(	'module','content',
								'function','tipafriend'))
	$current_user = fetch('user', 'current_user')
	$disable_template_cache = rand()
	$user_cids = array('client', 'user')
	$my_name = ''
	$my_CompanyName = ''
	$my_JobTitle = ''
	$my_Address1 = ''
	$my_Address2 = ''
	$my_City = ''
	$my_StateorProvince = ''
	$my_ZIPorPostalCode = ''
	$my_PhoneNumber = ''
}


{* for subusers, play by the parent's rules *}
{def $user_cids = array('client', 'user')}
{if $user_cids|contains($node.parent.class_identifier)}
	{set $current_user = fetchezuser($node.parent.contentobject_id)}
{/if}

{def
	$list_id = '49b6031764f7283bed4dacd81fe6cabb'
	$current_user_object = fetch('content', 'object', hash('object_id', $current_user.contentobject_id))
	$subscriptions = fetch('content', 'list', hash(
		'parent_node_id', $current_user_object.main_node_id,
		'class_filter_type', 'include',
		'class_filter_array', array('subscription'),
		'limitation', array()
	))
}

{set $my_name=$current_user_object.main_node.name
	 $my_PhoneNumber = $current_user_object.data_map.phone.content
	 $my_JobTitle = $current_user_object.data_map.job_title.content
}
{if $user_cids|contains($current_user_object.main_node.parent.class_identifier)}
	{set $current_user_object = fetchezuser($current_user_object.main_node.parent.contentobject_id)}
{/if}
{set $my_CompanyName=$current_user_object.data_map.company_name.content
	 $my_Address1 = $current_user_object.data_map.address1.content
	 $my_Address2 = $current_user_object.data_map.address2.content
	 $my_City = $current_user_object.data_map.city.content
	 $my_StateorProvince = $current_user_object.data_map.state.content
	 $my_ZIPorPostalCode = $current_user_object.data_map.zip_code.content
}
{if is_set($current_user_object.data_map.newsletter_country)}{set-block variable=my_Country}{attribute_view_gui attribute=$current_user_object.data_map.newsletter_country}{/set-block}{/if}
{if $my_PhoneNumber|eq('')}{set $my_PhoneNumber = $current_user_object.data_map.phone.content}{/if}


<script>
{literal}
$(function(){
	var url = '/subscriber/segments';
	var data = {
		"list_id" : "{/literal}{$list_id}{literal}",
		"email" : "{/literal}{$current_user.email}{literal}",
	}
	$.ajax({
		type: 'POST',
		url: url,
		data: data,
		success: function(result){
			result = $.parseJSON(result);
			$.each(result, function(segmentID, segmentName){
				if (segmentName.indexOf("Daily News") != -1) segmentName = 'Global Daily News';
				$('input[name="' + segmentName + '"]').attr('checked', 'checked');
			});
		},
	});

	$('#segments input[type=checkbox]').change(function(){
		$('#segments .feedback').hide();
		var url = '/subscriber/add';
		{/literal}
		var custom_fields = new Array({ldelim}
			"Key" : "CompanyName",
			"Value" : "{$my_CompanyName}"
		{rdelim},
		{ldelim}
			"Key" : "JobTitle",
			"Value" : "{$my_JobTitle}"
		{rdelim},
		{ldelim}
			"Key" : "Address1",
			"Value" : "{$my_Address1}"
		{rdelim},
		{ldelim}
			"Key" : "Address2",
			"Value" : "{$my_Address2}"
		{rdelim},
		{ldelim}
			"Key" : "City",
			"Value" : "{$my_City}"
		{rdelim},
		{ldelim}
			"Key" : "StateorProvince",
			"Value" : "{$my_StateorProvince}"
		{rdelim},
		{ldelim}
			"Key" : "ZIPorPostalCode",
			"Value" : "{$my_ZIPorPostalCode}"
		{rdelim},
		{ldelim}
			"Key" : "Country",
			"Value" : "{$my_Country|trim("\r\n")}"
		{rdelim},
		{ldelim}
			"Key" : "PhoneNumber",
			"Value" : "{$my_PhoneNumber}"
		{rdelim});
		{literal}
		var rb_newsletter = false;
		$('#segments input:checked').each(function(){
			if ($(this).hasClass('daily_news')) {
				custom_fields.push({
					"Key" : "Newsletters",
					"Value" : $(this).attr('name')
				});
			}
			if ($(this).hasClass('research_bulletin')) {
				custom_fields.push({
					"Key" : "Subscription Type",
					"Value" : $(this).attr('name')
				});
				if (!rb_newsletter) {
					custom_fields.push({
						"Key" : "Newsletters",
						"Value" : "Research Bulletin"
					});
					rb_newsletter = true;
				}
			}
		});
		if ($('#segments input:checked').length == 0) {
			custom_fields.push({
				"Key" : "Newsletters",
				"Value" : "blank"
			});
			custom_fields.push({
				"Key" : "Subscription Type",
				"Value" : "blank"
			});
		} else if ($('#segments input.research_bulletin:checked').length == 0) {
			custom_fields.push({
				"Key" : "Subscription Type",
				"Value" : "blank"
			});
		}
		var data = {
			"list_id" : "{/literal}{$list_id}{literal}",
			"email" : "{/literal}{$current_user.email}{literal}",
			"name" : "{/literal}{$my_name}{literal}",
			"custom_fields" : custom_fields
		}
		$.ajax({
			type: 'POST',
			url: url,
			data: data,
			success: function(result){
				result = $.parseJSON(result);
				if (result.http_status_code == 201) {
					var feedback = 'Update successful.';
					var color = 'green';
				} else {
					var feedback = result.response.Message;
					var color = 'red';
				}
				$('#segments .feedback').css('color', color).html(feedback).show().delay(3000).fadeOut(1000);
			},
		});
	});
});
{/literal}
</script>

<form action={concat($module.functions.edit.uri,"/",$userID)|ezurl} method="post" name="Edit">
<input type="hidden" name="ContentObjectLanguageCode" value="{$language}" />
	<div class="account-info">
		<div class="attribute-header">
			<h1 class="long">{"User profile"|i18n('extension/xrowecommerce')}</h1>
		</div>
	</div>
	<div class="payment-info">
		<table class="list">
			<tr>
				<th colspan="2">
					<h2>{'Account:'|i18n('extension/xrowecommerce')}</h2>
				</th>
			</tr>
			<tr>
				<td>
					<h3>{'Profile'|i18n('extension/xrowecommerce')}:</h3>
					<ul>
						<li>{"Username"|i18n('extension/xrowecommerce')}: {$userAccount.login|wash}</li>
						<li>{"Name"|i18n('extension/xrowecommerce')}: {attribute_view_gui attribute=$userAccount.contentobject.data_map.first_name} {attribute_view_gui attribute=$userAccount.contentobject.data_map.last_name}</li>
						<li>{'E-mail'|i18n('extension/xrowecommerce')}: {$userAccount.email|wash(email)}</li>
						<li><a title="{"Change Account Information"|i18n('extension/xrowecommerce')}" href="{concat("/content/edit/", $userAccount.contentobject_id, "/", "f", "/",$userAccount.contentobject.current_language)|ezroot('no')}">{"Change Account Information"|i18n('extension/xrowecommerce')} or Password</a></li>
					</ul>
				</td>
				<td id="segments">
{*
Eligibility rules: Daily News - anyone can subscribe, Research Bulletin - 4 segments, one for each Content Subscription type. If they are a North America Corporate Member, they can sign up for Research Bulletin - NA CM, etc. Users with more than one content subscription can sign up for more than one research bulletin. 
*}
					<h3>{'Subscriptions'|i18n('extension/xrowecommerce')}: <span class="feedback">&nbsp;</span></h3>
					{def
						$buyer = $current_user.groups|contains(461)
						$staffing = $current_user.groups|contains(453)
						$na_sub_node_id = 455
						$we_sub_node_id = 456
						$na_sub = false()
						$we_sub = false()
					}
					{foreach $subscriptions as $subscription}
						{if $subscription.data_map.product.content.main_node_id|eq($na_sub_node_id)}
							{set $na_sub = true()}
						{elseif $subscription.data_map.product.content.main_node_id|eq($we_sub_node_id)}
							{set $we_sub = true()}
						{/if}
					{/foreach}
					<ul>
						<li><input type="checkbox" class="cws30 daily_news" name="CWS30" value="" />CWS30</li>
						<li><input type="checkbox" class="daily_news" name="Global News Digest" value="" />Global News Digest</li>
						<li><input type="checkbox" class="daily_news" name="Global Daily News" value="" />Global Daily News</li>
						
						<li><input type="checkbox" class="daily_news" name="Global Daily News - CWS" value="" />Global Daily News - CWS</li>
						
						<li><input type="checkbox" class="it_staffing_report daily_news" name="IT Staffing Report" value="" />IT Staffing Report</li>
						
						<li><input type="checkbox" class="healthcare_staffing_report daily_news" name="Healthcare Staffing Report" value="" />Healthcare Staffing Report</li>
						
					{if $na_sub}
						<li><input type="checkbox" class="research_bulletin" name="Research Bulletin - NA CM Only" value="" />Research Bulletin - NA CM Only</li>
					{/if}
					{if and($na_sub,$buyer)}
						<li><input type="checkbox" class="research_bulletin" name="Research Bulletin - NA CWSC" value="" />Research Bulletin - NA CWSC</li>
					{/if}
					{if $we_sub}
						<li><input type="checkbox" class="research_bulletin" name="Research Bulletin - WE CM" value="" />Research Bulletin - WE CM</li>
					{/if}
					{if and($we_sub,$buyer)}
						<li><input type="checkbox" class="research_bulletin" name="Research Bulletin - WE CWSC" value="" />Research Bulletin - WE CWSC</li>
					{/if}
					</ul>
				</td>
			</tr>
			<tr>
				<th colspan="2"><h2>{"Personal Information"|i18n('extension/xrowecommerce')}</h2></th>
			</tr>
			<tr>
				<td>
					<h3>{"Account Settings"|i18n('extension/xrowecommerce')}</h3>
					<ul>
						<li><a title="{"Change billing and shipping address or your credit card information."|i18n('extension/xrowecommerce')}" href="{concat("/content/edit/", $userAccount.contentobject_id, "/", "f", "/",$userAccount.contentobject.current_language)|ezroot('no')}">{"Change billing and shipping address or creditcard."|i18n('extension/xrowecommerce')}</a></li>
						{*<li><a href="{'/user/password'|ezurl('no')}" title="{"Change password"|i18n('extension/xrowecommerce')}">{"Change password"|i18n('extension/xrowecommerce')}</a></li>*}
						<li>Account is <span class="enable_disable_info">{cond($user.data_map.user_account.content.is_enabled, 'en', 'dis')}abled</span>. <a href="#" class="{cond($user.data_map.user_account.content.is_enabled, 'dis', 'en')}able" rel="{$user.id}">{cond($user.data_map.user_account.content.is_enabled, 'dis', 'en')}able</a></li>
					</ul>
				</td>
				<td>
					<h3>{"Shop Features"|i18n('extension/xrowecommerce')}</h3>
					<ul>
						<li><a title="{"View recent and current orders."|i18n('extension/xrowecommerce')}" href={"/order/history"|ezurl}>{"Orders."|i18n('extension/xrowecommerce')}</a></li>
						<li><a title="{"View recurring orders."|i18n('extension/xrowecommerce')}" href={"/recurringorders/list"|ezurl}>{"Recurring orders."|i18n('extension/xrowecommerce')}</a></li>
						<li><a title="{"Edit or view wishlist."|i18n('extension/xrowecommerce')}" href={"/shop/wishlist"|ezurl}>{"Wishlist."|i18n('extension/xrowecommerce')}</a></li>
						<li><a title="{"Edit or view your notification settings."|i18n('extension/xrowecommerce')}" href={"notification/settings"|ezurl}>{"Notification settings."|i18n("design/ezwebin/user/edit")}</a></li>
					</ul>
				</td>
			</tr>
		</table>
	</div>
</form>

