{def $ca=$content_attributes_data_map
     $countries = fetch( 'content', 'country_list')
     $castring="ContentObjectAttribute_ezstring_data_text_"
     $country_default_ini="USA"
}
	{* left frame start *}
 <div class="shop-basket user_register">
	<div class="accountinfo">
		<h1>{'Account Information'|i18n('extension/xrowecommerce')}</h1>
		<p>{'Although your Login User Name must be kept the same, you can update your Email Address and/or change your Password by entering new information in the appropriate field.  When complete, click the "Update" button at the bottom of the page.'|i18n('extension/xrowecommerce')}</p>
		<p><span class="required">* {'Required field'|i18n('extension/xrowecommerce')}</span></p>
		{if ezini('Fields','company_name','xrowecommerce.ini').enabled|eq('true')}
		<div class="block">
			<label>{if ezini( 'Fields', 'company_name', 'xrowecommerce.ini' ).required|eq("true")}<span class="required">*</span>{/if}{'Company name'|i18n('extension/xrowecommerce')}</label>
			<div class="labelbreak"></div>
			<input type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.company_name.id}" value="{$ca.company_name.content|wash()}" />
			<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.company_name.id}" />
		</div>
		{/if}
		{if ezini('Fields','first_name','xrowecommerce.ini').enabled|eq('true')}
		<div class="block">
			<label><span class="required">*</span>{'First name'|i18n('extension/xrowecommerce')}</label>
			<div class="labelbreak"></div>
			<input type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.first_name.id}" value="{$ca.first_name.content|wash()}" />
			<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.first_name.id}" />
		</div>
		{if ezini('Fields','mi','xrowecommerce.ini').enabled|eq('true')}
		<div>
			<label>{'MI'|i18n('extension/xrowecommerce')}</label>
			<div class="labelbreak"></div>
			<input type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.s_mi.id}" size="2" value="{$ca.s_mi.content|wash()}" />
			<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_mi.id}" />
		</div>
		{/if}
		{if ezini('Fields','last_name','xrowecommerce.ini').enabled|eq('true')}
		<div class="block">
			<label><span class="required">*</span>{'Last name'|i18n('extension/xrowecommerce')}</label>
			<div class="labelbreak"></div>
			<input type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.last_name.id}" value="{$ca.last_name.content|wash()}" />
			<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.last_name.id}" />
		</div>
		{/if}
		<div class="block job_title">
            <label>Job Title</label>
            <div class="labelbreak"></div>
            <input type="text" id="last_name" name="ContentObjectAttribute_ezstring_data_text_{$ca.job_title.id}" value="{$ca.job_title.content|wash()}" />
            <input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.job_title.id}" />
        </div>
        <div class="block newsletter_country">
            <label><span class="required">*</span>{'Country'|i18n('extension/xrowecommerce')}</label>
            <div class="labelbreak"></div>
            <input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.newsletter_country.id}" />
			{default attribute_base=ContentObjectAttribute}
			{def $selected_id_array=cond($ca.newsletter_country.content.0|ne(''), $ca.newsletter_country.content, array(237))}
			<input type="hidden" name="{$attribute_base}_ezselect_selected_array_{$ca.newsletter_country.id}" value="" />
			<select onchange='matchcountries(this)' id="ezcoa-{$ca.newsletter_country.contentclassattribute_id}_{$ca.newsletter_country.contentclass_attribute_identifier}" class="ezcc-{$ca.newsletter_country.object.content_class.identifier} ezcca-{$ca.newsletter_country.object.content_class.identifier}_{$ca.newsletter_country.contentclass_attribute_identifier}" name="{$attribute_base}_ezselect_selected_array_{$ca.newsletter_country.id}[]" {if $ca.newsletter_country.class_content.is_multiselect}multiple{/if}>
			{section var=Options loop=$ca.newsletter_country.class_content.options}
			<option value="{$Options.item.id}" {if $selected_id_array|contains( $Options.item.id )}selected="selected"{/if}>{$Options.item.name|wash( xhtml )}</option>
			{/section}
			</select>
			{/default}
        </div>
		<div class="block">{attribute_edit_gui attribute=$ca.user_account}</div>
		<div class="break"></div>
	</div>

{* left frame end *}
	<div class="billing_shipping user_register">

{* right frame start *}
		<div class="billing">
			<h3>{'Billing Information'|i18n('extension/xrowecommerce')}</h3>
			<input type="hidden" name="MainNodeID" value="{$main_node_id}" />
			<div class="block">
				{if ezini('Fields','company_additional','xrowecommerce.ini').enabled|eq('true')}
				<div class="block">
					<label>{'Company additional information'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.company_additional.id}" value="{$ca.company_additional.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.company_additional.id}" />
				</div>
				{/if}
				{if ezini('Fields','tax_id','xrowecommerce.ini').enabled|eq('true')}
				<div>
					<label>{'VAT'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.tax_id.id}" value="{$ca.tax_id.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.tax_id.id}" />
				</div>
				{/if}

{def $redirect_if_discarded=ezhttp('RedirectIfDiscarded','session')|explode('/')
	$parent_node_id=$redirect_if_discarded|extract_right(1)|implode('/')|int()
	$parent_node=fetch('content','node',hash('node_id',$parent_node_id))
}

{def $ca_custom = cond($parent_node.class_identifier|eq('client'), $parent_node.data_map, $ca)}
				{if ezini('Fields','address1','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.address1.content|wash()}">
					<label>{'Address 1'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input class="box" type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.address1.id}" size="20" value="{$ca.address1.content|wash()}" title="{'Apartment, suite, unit, building, floor, etc.'|i18n('extension/xrowecommerce')}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.address1.id}" />
				</div>
				{/if}
				{if ezini('Fields','address2','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.address2.content|wash()}">
					<label>{'Address 2'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input class="box" type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.address2.id}" size="20" value="{$ca.address2.content|wash()}" title="{'Street address, P.O. box, company name, c/o'|i18n('extension/xrowecommerce')}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.address2.id}" />
				</div>
				{/if}
				{if ezini('Fields','tax_id','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.zip_code.content|wash()}">
					<label>{'Zip / Postcode'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" name="{$castring}{$ca.zip_code.id}" value="{$ca.zip_code.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.zip_code.id}" />
				</div>
				{/if}
				{if ezini('Fields','city','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.city.content|wash()}">
					<label>{'City / Town'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" name="{$castring}{$ca.city.id}" value="{$ca.city.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.city.id}" />
				</div>
				{/if}
				{if ezini('Fields','state','xrowecommerce.ini').enabled|eq('true')}
				<div alt="{$ca_custom.state.content|wash()}">
					<label>{'State / Province'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.state.id}" />
					{attribute_edit_gui attribute_base=$attribute_base attribute=$ca.state}
				</div>
				{/if}
				<div class="break"></div>
                {if ezini( 'Fields', 'country', 'xrowecommerce.ini' ).enabled|eq("true")}
                <div class="country">
                    <label>{if and(module_params()['module_name']|ne("user"), ezini( 'Fields', 'country', 'xrowecommerce.ini' ).required|eq("true"))}<span class="required">*</span>{/if}{'Country'|i18n('extension/xrowecommerce')}</label>
                    <div class="labelbreak"></div>
                    {def $country = $ca.country.content}
                    <input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.country.id}" />
                    <select name="{$castring}_{$ca.country.id}" id="country">
                    {foreach $countries as $country_list_item}
                        {if $country|ne( '' )}
                        {* Backwards compatability *}
                            <option value="{$country_list_item.Alpha3}" {if and( $country|ne(''), eq( $country, $country_list_item.Alpha3 ))} selected="selected"{/if}>{$country_list_item.Name|wash}</option>
                        {else}
                            <option {if $country_default_ini|eq( $country_list_item.Alpha3 )}selected="selected"{/if} value="{$country_list_item.Alpha3}">{$country_list_item.Name|wash}</option>
                        {/if}
                    {/foreach}
                    </select>
                </div>
                {/if}
				<div class="break"></div>
				{if ezini('Fields','phone','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.phone.content|wash()}">
					<label>{'Phone'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input class="box" type="text" name="{$castring}{$ca.phone.id}" value="{$ca.phone.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.phone.id}" />
				</div>
				{/if}
				<div class="break"></div>
				{if ezini('Fields','fax','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.fax.content|wash()}">
					<label>{'Fax'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input class="box" type="text" name="{$castring}{$ca.fax.id}" value="{$ca.fax.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.fax.id}" />
				</div>
				{/if}

				{include uri="design:content/edit_attribute_user_custom_left.tpl"}
			</div>
		</div>

		<div class="shipping">
			<h3>{'Shipping Information'|i18n('extension/xrowecommerce')}</h3>
			<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.shippingaddress.id}" />
			<label onclick="change();" class="shipping-checkbox" for="shipping-checkbox">
				<input class="shipping-checkbox" onclick="change();" id="shipping-checkbox"  name="ContentObjectAttribute_data_boolean_{$ca.shippingaddress.id}" value="" type="checkbox" {$ca.shippingaddress.data_int|choose( '', 'checked="checked"' )} />
				{'My billing and shipping addresses are identical.'|i18n('extension/xrowecommerce')}
			</label>
			<br /><br />
			<div class="block" id="shippinginfo">
				{if ezini('Fields','s_company_name','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.s_company_name.content|wash()}">
					<label>{'Company name'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" id="s_company_name" name="ContentObjectAttribute_ezstring_data_text_{$ca.s_company_name.id}" value="{$ca.s_company_name.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_company_name.id}" />
				</div>
				{/if}
				{if ezini('Fields','s_company_additional','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.s_company_additional.content|wash()}">
					<label>{'Form of company'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.s_company_additional.id}" value="{$ca.s_company_additional.content|wash()}" />
					<input type="hidden" id="s_company_name" name="ContentObjectAttribute_id[]" value="{$ca.s_company_additional.id}" />
				</div>
				{/if}
				{if ezini('Fields','s_first_name','xrowecommerce.ini').enabled|eq('true')}
				<div class="block">
					<label>{'First name'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.s_first_name.id}" value="{$ca.s_first_name.content|wash()}" />
					<input type="hidden" id="s_company_name" name="ContentObjectAttribute_id[]" value="{$ca.s_first_name.id}" />
				</div>
				{/if}
				{if ezini('Fields','mi','xrowecommerce.ini').enabled|eq('true')}
				<div class="block">
					<label>{'MI'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.s_mi.id}" size="2" value="{$ca.s_mi.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_mi.id}" />
				</div>
				{/if}
				{if ezini('Fields','s_last_name','xrowecommerce.ini').enabled|eq('true')}
				<div class="block">
					<label>{'Last name'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.s_last_name.id}" value="{$ca.s_last_name.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_last_name.id}" />
				</div>
				{/if}
				{if ezini('Fields','address1','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.s_address1.content|wash()}">
					<label>{'Address 1'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input class="box" type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.s_address1.id}" size="20" value="{$ca.s_address1.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_address1.id}" />
				</div>
				{/if}
				{if ezini('Fields','address2','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.s_address2.content|wash()}">
					<label>{'Address 2'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input class="box" type="text" name="ContentObjectAttribute_ezstring_data_text_{$ca.s_address2.id}" size="20" value="{$ca.s_address2.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_address2.id}" />
				</div>
				{/if}
				{if ezini('Fields','city','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.s_city.content|wash()}">
					<label>{'City / Town'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" name="{$castring}{$ca.s_city.id}" value="{$ca.s_city.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_city.id}" />
				</div>
				{/if}
				{if ezini('Fields','s_zip','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.s_zip_code.content|wash()}">
					<label>{'Zip / Postcode'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="text" name="{$castring}{$ca.s_zip_code.id}" value="{$ca.s_zip_code.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_zip_code.id}" />
				</div>
				{/if}
				{if ezini('Fields','state','xrowecommerce.ini').enabled|eq('true')}
				<div alt="{$ca_custom.s_state.content|wash()}">
					<label>{'State / Province'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_state.id}" />
					{attribute_edit_gui attribute_base=$attribute_base attribute=$ca.s_state}
				</div>
				{/if}
				<div class="break"></div>
                {if ezini( 'Fields', 's_country', 'xrowecommerce.ini' ).enabled|eq("true")}
                <div class="s_country">
                    <label>{if and(module_params()['module_name']|ne("user"), ezini( 'Fields', 's_country', 'xrowecommerce.ini' ).required|eq("true"))}<span class="required">*</span>{/if}{'Country'|i18n('extension/xrowecommerce')}</label>
                    <div class="labelbreak"></div>
                    {def $s_country = $ca.s_country.content}
                    <input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_country.id}" />
                    <select name="{$castring}_{$ca.s_country.id}" id="s_country">
                    {foreach $countries as $country_list_item}
                        {if $s_country|ne( '' )}
                        {* Backwards compatability *}
                            <option value="{$country_list_item.Alpha3}" {if and( $s_country|ne(''), eq( $s_country, $country_list_item.Alpha3 ))} selected="selected"{/if}>{$country_list_item.Name|wash}</option>
                        {else}
                            <option {if $country_default_ini|eq( $country_list_item.Alpha3 )}selected="selected"{/if} value="{$country_list_item.Alpha3}">{$country_list_item.Name|wash}</option>
                        {/if}
                    {/foreach}
                    </select>
                </div>
                <div class="break"></div>
                {/if}
				<div class="break"></div>
				{if ezini('Fields','phone','xrowecommerce.ini').enabled|eq('true')}
				<div class="block" alt="{$ca_custom.s_phone.content|wash()}">
					<label>{'Phone'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input class="box" type="text" name="{$castring}{$ca.s_phone.id}" value="{$ca.s_phone.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_phone.id}" />
				</div>
				{/if}
				{if ezini('Fields','fax','xrowecommerce.ini').enabled|eq('true')}
					{if eq(ezini( 'ShippingSettings', 'DisplayFax', 'xrowecommerce.ini' ), 'enabled' )}
						<div class="block" alt="{$ca_custom.s_fax.content|wash()}">
						<label>{'Fax'|i18n('extension/xrowecommerce')}</label>
						<div class="labelbreak"></div>
						<input class="box" type="text" name="{$castring}{$ca.s_fax.id}" value="{$ca.s_fax.content|wash()}" />
						<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_fax.id}" />
					</div>
					{/if}
				{/if}
				{if ezini('Fields','s_email','xrowecommerce.ini').enabled|eq('true')}
				<div class="block">
					<label>{'E-mail'|i18n('extension/xrowecommerce')}</label>
					<div class="labelbreak"></div>
					<input class="box" type="text" name="ContentObjectAttribute_data_text_{$ca.s_email.id}" value="{$ca.s_email.content|wash()}" />
					<input type="hidden" name="ContentObjectAttribute_id[]" value="{$ca.s_email.id}" />
				</div>
				<div class="break"></div>
				{include uri="design:content/edit_attribute_user_custom_right.tpl"}
			</div>
		</div>

{def $redirto = $redirect_if_discarded|extract_left(3)|implode('/')}
{if $redirto|eq('/')}{set $redirto = "/content/dashboard"}{/if}
<div class="buttonblock">
	<input class="button left-arrow2" type="submit" name="DiscardButton" value="{'Cancel'|i18n('extension/xrowecommerce')}" />
	<input class="button right-arrow" type="submit" name="PublishButton" value="{'Save'|i18n('extension/xrowecommerce', 'Save Button' )}" />
	<input type="hidden" name="DiscardConfirm" value="0" />
	<input type="hidden" name="RedirectURI" value="{$redirto}" />
	<input type="hidden" name="RedirectIfDiscarded" value="{$redirto}" />
</div>

{* right frame end *}

</div>

{literal}
	<script type='text/javascript'>
		$(function(){
			$(".billing_shipping div[alt]").each(function(){
				setme = $(this).attr('alt');
				$(this).find('input[type="text"], select').val(setme);
			})
		})
	</script>

    <script type="text/javascript">
		function matchcountries(selection){
			matchme = $(selection).find(':selected').text();
			matches = $("#country option:contains("+matchme+"), #s_country option:contains("+matchme+")");
			matches.each(function(){
				setme = $(this).parent();
				val = $(this).attr('value');
				setme.val(val);
			})
		}
    </script>
{/literal}