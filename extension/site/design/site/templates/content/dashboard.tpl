{ezscript_require(array(
	'admin2pp::jqueryui',
	'ezjsc::jqueryio',
	'node_tabs.js',
	'ezajaxrelations_jquery.js',
	'admin2pp_dragndrop_children.js',
	'admin2pp_utils.js',
	'admin2pp_preview.js',
	'admin2pp_dashboard_improved.js',
	'admin2pp_dashboard_feed_reader.js',
	'admin2pp_resize_object_info.js'
))}
{ezcss_require(array(
	'admin2pp.css',
	'admin2pp_improved_default.css'
))}
{set $blocks = fetch( 'admin2pp', 'dashboard_blocks', hash( 'active_only', true() ) )}
{def $right_blocks = array()}
{def $current_user = fetch('user', 'current_user')}

{include uri="design:admin2pp/dashboard_preferences_window.tpl"}

<div class="context-block content-dashboard">
	<h1 class="context-title">{'Dashboard'|i18n( 'design/admin/content/dashboard' )}</h1>
	<div class="block">
		<div class="left">
		{foreach $blocks as $block sequence array( 'left', 'right' ) as $position}
			{if $block.identifier|ne('')} {* for some reason empty blocks end up in here without this check *}
				{if $position|eq('left')}
					<div id="{$block.identifier}" class="dashboard-item">
						{if $block.template}
							{include uri=concat( 'design:', $block.template )}
						{else}
							{include uri=concat( 'design:dashboard/', $block.identifier, '.tpl' )}
						{/if}
					</div>
				{else}
					{append-block variable=$right_blocks}
					<div id="{$block.identifier}" class="dashboard-item">
						{if $block.template}
							{include uri=concat( 'design:', $block.template )}
								{else}
							{include uri=concat( 'design:dashboard/', $block.identifier, '.tpl' )}
						{/if}
					</div>
					{/append-block}
				{/if}
			{/if}
		{/foreach}
		</div>
		<div class="right">
		    {$right_blocks|implode('')}
		</div>
	</div>


</div>


<script type="text/javascript">
jQuery(document).ready(function()
{ldelim}

    var dashboard = new admin2ppDashboardBlocks();
    dashboard.init();
    dashboard.initSettings();

{rdelim});
</script>

{set-block variable='sidebar'}
	{include uri='design:menu/dashboard.tpl'}
{/set-block}

{pagedata_merge(hash('sidebar', $sidebar))}
