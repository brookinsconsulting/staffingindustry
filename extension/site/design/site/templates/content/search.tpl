{set-block scope=root variable=cache_ttl}0{/set-block}

{def $classes=classes()
     $hideclasses = array( 'user', 'client', 'newsletter' )
     $goodclasses = array()
}
{if ezhttp_hasvariable( 'ClassIdentifiers', 'get' )}
    {set $classes = array( ezhttp( 'ClassIdentifiers', 'get' )|wash )}
{/if}
{foreach $classes as $c}
	{if $hideclasses|contains($c)|not}{set $goodclasses = $goodclasses|append($c)}{/if}
{/foreach}

{ezscript_require(array(
	'admin2pp::jqueryui',
	'ezjsc::jqueryio',
	'node_tabs.js',
	'ezajaxrelations_jquery.js',
	'admin2pp_dragndrop_children.js',
	'admin2pp_utils.js',
	'admin2pp_preview.js',
	'admin2pp_dashboard_improved.js',
	'admin2pp_dashboard_feed_reader.js',
	'admin2pp_resize_object_info.js'
))}
{def 
	$savedsearches = ezpreference('dashboard-block-saved_searches')
}
{def
	$sections=fetch( 'content', 'section_list' )
	$secName = ''
}
{def $search=false()}
{if $use_template_search}
	{set $page_limit=10}
	{def $activeFacetParameters = array()}
	{if ezhttp_hasvariable('activeFacets','get')}
		{set $activeFacetParameters = ezhttp('activeFacets','get')}
	{/if}
	{def $dateFilter=0}
	{if ezhttp_hasvariable('dateFilter','get')}
		{set $dateFilter = ezhttp('dateFilter','get')}
		{switch match=$dateFilter}
		{case match=1}
			{def $dateFilterLabel="Last day"|i18n("design/standard/content/search")}
		{/case}
		{case match=2}
			{def $dateFilterLabel="Last week"|i18n("design/standard/content/search")}
		{/case}
		{case match=3}
			{def $dateFilterLabel="Last month"|i18n("design/standard/content/search")}
		{/case}
		{case match=4}
			{def $dateFilterLabel="Last three months"|i18n("design/standard/content/search")}
		{/case}
		{case match=5}
			{def $dateFilterLabel="Last year"|i18n("design/standard/content/search")}
		{/case}			
		{/switch}	
	{/if}
	{def $filterParameters = fetch('ezfind','filterParameters')
		 $defaultSearchFacets = fetch('ezfind','getDefaultSearchFacets')
		 $use_filterParameters = array()
	}

	{def $tmpparams = array()}
		{if is_array($filterParameters)}
		{foreach $filterParameters as $k => $f}
			{set $tmpparams = $tmpparams|merge(hash($k, concat('"',$f,'"')))}
		{/foreach}
	{/if}

	{if $tmpparams|count|eq(0)}
		{set $use_filterParameters = array('-meta_section_id_si:17', '-meta_section_id_si:3', '-meta_path_si:401')}
	{else}
		{set $use_filterParameters = $tmpparams|append('-meta_section_id_si:17')|append('-meta_section_id_si:3')|append('-meta_path_si:401')}
	{/if}
	
    {set $use_filterParameters = $use_filterParameters|append('-(meta_class_identifier_ms:file AND meta_path_si:43)')}
    {set $use_filterParameters = $use_filterParameters|append('-(meta_class_identifier_ms:folder AND meta_path_si:43)')}
	
	{set $search=fetch('ezfind','search',hash(
							'query', $search_text,
							'offset',$view_parameters.offset,
							'limit',$page_limit,
							'sort_by',hash('score','desc'),
							'facet',$defaultSearchFacets,
							'filter',$use_filterParameters,
							'class_id', $goodclasses,
							'publish_date',$dateFilter,
							'spell_check',array(true()),
							'subtree_array',cond(ezhttp_hasvariable('SubTreeArray','get'),array(ezhttp('SubTreeArray','get')),false())
							))}
	{set $search_result=$search['SearchResult']}
	{set $search_count=$search['SearchCount']}
	{def $search_extras=$search['SearchExtras']}
	{set $stop_word_array=$search['StopWordArray']}
	{set $search_data=$search}
	{*debug-log var=$search_extras.facet_fields msg='$search_extras.facet_fields'*}
{/if}

{def
    $baseURI = concat('/content/search?SearchText=',$search_text|urlencode)
    $contentFiltersBaseURI = $baseURI
}
{if ezhttp_hasvariable( 'SubTreeArray', 'get' )}
    {set $contentFilterSuffix = concat( $contentFilterSuffix, '&SubTreeArray=', ezhttp( 'SubTreeArray','get' )|wash )}
{/if}
{if ezhttp_hasvariable( 'ClassIdentifiers', 'get' )}
    {set $contentFilterSuffix = concat( $contentFilterSuffix, '&ClassIdentifiers=', ezhttp( 'ClassIdentifiers','get' )|wash )}
{/if}

{* Build the URI suffix,used throughout all URL generations in this page *}
{def $uriSuffix = ''}
{foreach $activeFacetParameters as $facetField => $facetValue}
	{set $uriSuffix = concat($uriSuffix,'&activeFacets[',$facetField,']=',$facetValue)}
{/foreach}

{foreach $filterParameters as $name => $value}
	{set $uriSuffix = concat($uriSuffix,'&filter[]=',$name,':',$value|urlencode)}
{/foreach}

{if gt($dateFilter,0)}
	{set $uriSuffix = concat($uriSuffix,'&dateFilter=',$dateFilter)}
        {set $contentFiltersBaseURI = concat($contentFiltersBaseURI,'&dateFilter=',$dateFilter)}
{/if}

{literal}
<script type="text/javascript">
	// toggle block
	function ezfToggleBlock(id){
		var value=(document.getElementById(id).style.display=='none')?'block':'none';
		ezfSetBlock(id,value);
		ezfSetCookie(id,value);
	}
	function ezfSetBlock(id,value){
		var el=document.getElementById(id);
		if(el!=null){
			el.style.display=value;
		}
	}
	function ezfTrim(str){
		if($.browser.version != 7.0){
			return str.replace(/^\s+|\s+$/g,'') ;
		}
	}

	function ezfGetCookie(name){
		if($.browser.version != 7.0){
			var cookieName='eZFind_' + name;
			var cookie=document.cookie;
			var cookieList=cookie.split(";");
			for(var idx in cookieList){
				if (typeof cookieList[idx] == 'string')
					cookie=cookieList[idx].split("=");
					if(ezfTrim(cookie[0]) == cookieName){
						return(cookie[1]);
					}
			}
			return 'none';
		}
	}

	function ezfSetCookie(name,value){
		var cookieName='eZFind_'+name;
		var expires=new Date();
		expires.setTime(expires.getTime()+(365*24*60*60*1000));
		document.cookie = cookieName+"="+value+"; expires="+expires+";";
	}

	function saveThisSearch(){
	// save current search to "saved searches" dashboard block
		$('#savethissearch').val('Saving...');
		$('#savethissearch').attr('disabled', 'disabled');

		var saved_searches = '{/literal}{$savedsearches}{literal}';
		var this_search = document.URL;

		if (saved_searches == '') {
			admin2ppAjaxSavePreference(
				'dashboard-block-saved_searches',
				base64_encode(this_search).replace('/', '-')
			);
		} else {
			admin2ppAjaxSavePreference(
				'dashboard-block-saved_searches',
				saved_searches + ',' + base64_encode(this_search).replace('/', '-')
			);
		}


		$('#savethissearch').val('Saved');
		$('#gotodashboard').fadeIn();
		return false;
	}
</script>
{/literal}

{if ezhttp_hasvariable( 'SubTreeArray', 'get' )}
    {set $uriSuffix = concat( $uriSuffix, '&SubTreeArray=', ezhttp( 'SubTreeArray','get' )|wash )}
{/if}
{if ezhttp_hasvariable( 'ClassIdentifiers', 'get' )}
    {set $uriSuffix = concat( $uriSuffix, '&ClassIdentifiers=', ezhttp( 'ClassIdentifiers','get' )|wash )}
{/if}


<div class="content-search search-results">
{include name=Navigator
	uri='design:navigator/google.tpl'
	page_uri='/content/search'
	page_uri_suffix=concat('?SearchText=',$search_text|urlencode,$search_timestamp|gt(0)|choose('',concat('&SearchTimestamp=',$search_timestamp)),$uriSuffix)
	item_count=$search_count
	view_parameters=$view_parameters
	item_limit=$page_limit}
{foreach $search_result as $result sequence array('bglight','bgdark') as $bgColor}
	{node_view_gui view='ezfind_line' bgColor=$bgColor use_url_translation=first_set($use_url_translation,false()) content_node=$result}
{/foreach}
{include name=Navigator
	uri='design:navigator/google.tpl'
	page_uri='/content/search'
	page_uri_suffix=concat('?SearchText=',$search_text|urlencode,$search_timestamp|gt(0)|choose('',concat('&SearchTimestamp=',$search_timestamp)),$uriSuffix)
	item_count=$search_count
	view_parameters=$view_parameters
	item_limit=$page_limit}
</div>

{set-block variable='searchbar'}
<div id="searchbar">
	<h1>{"Search Staffing Industry Analysts"|i18n("design/ezwebin/content/search")}</h1>
	{def
            $hidden = hash(
                'SubTreeArray',cond(
                    ezhttp_hasvariable('SubTreeArray','get'), ezhttp('SubTreeArray','get')|wash, ezini('NodeSettings','RootNode','content.ini')
                )
            )
        }
        {if ezhttp_hasvariable( 'ClassIdentifiers', 'get' )}
            {set $hidden = $hidden|merge( hash( 'ClassIdentifiers', ezhttp( 'ClassIdentifiers','get' )|wash ) )}
        {/if}
	{include uri="design:parts/searchpageform.tpl" search_text=$search_text|wash() hidden=$hidden}

{if $search_extras.spellcheck_collation}
	{def $spell_url=concat('/content/search/',$search_text|count_chars()|gt(0)|choose('',concat('?SearchText=',$search_extras.spellcheck_collation|urlencode)))|sitelink('no')}
	<p>Spell check suggestion: did you mean <strong>{concat("<a href=",$spell_url,">")}{$search_extras.spellcheck_collation}</a></strong>?</p>
{/if}
{if $stop_word_array}
	<p>{"The following words were excluded from the search"|i18n("design/base")}:{foreach $stop_word_array as $stopWord}{$stopWord.word|wash()}{delimiter},{/delimiter}{/foreach}</p>
{/if}
{if $search_count}
        <br /><br />
	<div class="feedback">
	<h2>Search for &#147;{$search_text|wash()}&#148; returned {$search_count} matches&nbsp;<b><input type="button" id="savethissearch" onclick="return saveThisSearch();" value="Save this search" style="top: -2px;" /></b>&nbsp;<a id="gotodashboard" style="display:none;" href={"/content/dashboard"|sitelink()}>Go to dashboard</a></h2>

	</div>
{else}
	<div class="warning">
	<h2>{'No results were found when searching for "%1".'|i18n("design/ezwebin/content/search",,array($search_text|wash))}</h2>
	{if $search_extras.hasError}{$search_extras.error|wash()}{/if}
	</div>
	<p>{'Search tips'|i18n('design/ezwebin/content/search')}</p>
	<ul>
		<li>{'Check spelling of keywords.'|i18n('design/ezwebin/content/search')}</li>
		<li>{'Try changing some keywords (eg, "car" instead of "cars").'|i18n('design/ezwebin/content/search')}</li>
		<li>{'Try searching with less specific keywords.'|i18n('design/ezwebin/content/search')}</li>
		<li>{'Reduce number of keywords to get more results.'|i18n('design/ezwebin/content/search')}</li>
	</ul>
{/if}
</div>
{/set-block}
{if and(eq(ezini('SearchSettings','DisplayFacets','site.ini'),'enabled'),$search_count)}
{set-block variable='facets'}
{def $facetData=''}
<section class="module infobox feedback" id="search-controls">
	<header><h1>Additional Filters</h1></header>
	<section class="infobox-content">
		{def $activeFacetsCount=0}
		<ul id="active-facets-list" class="menu vertical">
		{foreach $defaultSearchFacets as $key => $defaultFacet}
			{if array_keys( $activeFacetParameters )|contains( concat( $defaultFacet['field'], ':', $defaultFacet['name']) )}
				{def $facetData=$search_extras.facet_fields.$key}
				{foreach $facetData.nameList as $key2 => $facetName}
					{if eq( $activeFacetParameters[concat( $defaultFacet['field'], ':', $defaultFacet['name'] )], $facetName )}
						{def $activeFacetsCount=sum( $key, 1 )}
		
						{def $suffix=$uriSuffix|explode( concat( '&filter[]=', $facetData.queryLimit[$key2]|explode($key2)|implode(concat('"', $key2 ,'"')) ) )|implode( '' )|explode( concat( '&activeFacets[', $defaultFacet['field'], ':', $defaultFacet['name'], ']=', $facetName ) )|implode( '' )}
						{if $defaultFacet['field']|eq('section_id')}
							{foreach $sections as $sec}
								{if $sec.id|eq($facetName)}
									{if $sec.id|eq(1)}
										{set $secName = 'All Users'}
									{else}
										{set $secName = $sec.name}
									{/if}
									{break}
								{/if}
							{/foreach}
							<li><a href={concat( $baseURI, $suffix )|sitelink}>[x]</a> <strong>{$defaultFacet['name']}</strong>: {if $secName|contains('Article')}{$secName|explode('Article')|implode('Research')}{else}{$secName}{/if}</li>
						{else}
							<li><a href={concat( $baseURI, $suffix )|sitelink}>[x]</a> <strong>{$defaultFacet['name']}</strong>: {if $facetName|contains('Article')}{$facetName|explode('Article')|implode('Research')}{else}{$facetName}{/if}</li>
						{/if}
					{/if}
				{/foreach}
			{/if}
		{/foreach}
		{* handle date filter here, manually for now. Should be a facet later on *}
		{if gt( $dateFilter, 0 )}
			{set $activeFacetsCount=$activeFacetsCount|inc}
			{def $suffix=$uriSuffix|explode( concat( '&dateFilter=', $dateFilter ) )|implode( '' )}
			<li><a href={concat( $baseURI, $suffix )|sitelink}>[x]</a> <strong>{'Creation time'|i18n( 'extension/ezfind/facets' )}</strong>: {$dateFilterLabel}</li>
		{/if}
		{if ge( $activeFacetsCount, 2 )}
			<li><a href={$baseURI|sitelink}>[x]</a> <em>{'Clear all'|i18n( 'extension/ezfind/facets' )}</em></li>
		{/if}
		</ul>
		<ul id="facet-list" class="menu vertical">
                    <li>
                        <span><strong>{'Content types'|i18n( 'extension/ezfind/facets' )}</strong></span>
                        <ul>
                            {def $contentFilters = hash(
                                'research', hash(
                                    'label', 'Research',
                                    'parent_node_id', '70472',
                                    'classes', 'article'
                                ),
                                'editorial', hash(
                                    'label', 'Editorial',
                                    'parent_node_id', '212',
                                ),
                                'news', hash(
                                    'label', 'News',
                                    'parent_node_id', '2',
                                    'classes', 'news_item'
                                )
                            )}
                            {def
                                $contentFilterSuffix = ''
                                $isFilterEnabled = false()
                            }
                            
                            {foreach $contentFilters as $filterKey => $contentFilter}
                                {set $isFilterEnabled = true()}
                                {if ezhttp( 'SubTreeArray','get' )|ne( $contentFilter.parent_node_id )}
                                    {set $isFilterEnabled = false()}
                                {/if}
                                {if and( $contentFilter.classes, ezhttp( 'ClassIdentifiers','get' )|ne( $contentFilter.classes ) )}
                                    {set $isFilterEnabled = false()}
                                {/if}

                                {set $contentFilterSuffix = concat( '&SubTreeArray=', $contentFilter.parent_node_id )}
                                {if $contentFilter.classes}
                                    {set $contentFilterSuffix = concat( $contentFilterSuffix, '&ClassIdentifiers=', $contentFilter.classes )}
                                {/if}
                                <li>
                                    <a href={concat( $contentFiltersBaseURI, $contentFilterSuffix )|sitelink}>{$contentFilter.label|wash}</a>
                                    {if $isFilterEnabled}
                                        <a href={$contentFiltersBaseURI|sitelink}>{'(remove filter)'|i18n( 'extension/ezfind/facets' )}</a>
                                    {/if}
                                </li>
                            {/foreach}   
                            {undef $contentFilterSuffix}
                        </ul>
                    </li>
		{foreach $defaultSearchFacets as $key => $defaultFacet}
			{if and(array_keys( $activeFacetParameters )|contains( concat( $defaultFacet['field'], ':', $defaultFacet['name']) )|not, is_set($search_extras.facet_fields.$key.nameList), $search_extras.facet_fields.$key.nameList|count|gt(0))}
			<li>
				{set $facetData=$search_extras.facet_fields.$key}
				
				<span {*style="background-color: #F2F1ED"*}><strong>{$defaultFacet['name']}</strong></span>
				<ul>
		
					{foreach $facetData.nameList as $key2 => $facetName}
						{if ne( $key2, '' )}
							{if $defaultFacet['field']|eq('section_id')}
								{foreach $sections as $sec}
									{if $sec.id|eq($facetName)}
										{if $sec.id|eq(1)}
											{set $secName = 'All Users'}
										{else}
											{set $secName = $sec.name}
										{/if}
										{break}
									{/if}
								{/foreach}
								<li><a href={concat( $baseURI, '&filter[]=', $facetData.queryLimit[$key2]|wash|explode(" ")|implode("+"), '&activeFacets[', $defaultFacet['field']|wash|explode(" ")|implode("+"), ':', $defaultFacet['name']|wash|explode(" ")|implode("+"), ']=', $facetName|wash|explode(" ")|implode("+"), $uriSuffix|wash|explode(" ")|implode("+") )|sitelink()}>{if $secName|contains('Article')}{$secName|explode('Article')|implode('Research')}{else}{$secName}{/if}</a> ({$facetData.countList[$key2]})</li>
							{else}
								<li><a href={concat( $baseURI, '&filter[]=', $facetData.queryLimit[$key2]|wash|explode(" ")|implode("+"), '&activeFacets[', $defaultFacet['field']|wash|explode(" ")|implode("+"), ':', $defaultFacet['name']|wash|explode(" ")|implode("+"), ']=', $facetName|wash|explode(" ")|implode("+"), $uriSuffix|wash|explode(" ")|implode("+") )|sitelink()}>{if $facetName|contains('Article')}{$facetName|explode('Article')|implode('Research')}{else}{$facetName}{/if}</a> ({$facetData.countList[$key2]})</li>
							{/if}
						{/if}
					{/foreach}
				</ul>
			</li>
			{/if}
		{/foreach}
		{* date filtering here. Using a simple filter for now. Should use the date facets later on *}
		{* if eq( $dateFilter, 0 )}
			<li>
				<span *}{* style="background-color: #F2F1ED" *}{* ><strong>{'Creation time'|i18n( 'extension/ezfind/facets' )}</strong></span>
				<ul>
					<li><a href={concat( $baseURI, '&dateFilter=1', $uriSuffix )|urlencode|sitelink}>{"Last day"|i18n("design/standard/content/search")}</a></li>
					<li><a href={concat( $baseURI, '&dateFilter=2', $uriSuffix )|urlencode|sitelink}>{"Last week"|i18n("design/standard/content/search")}</a></li>
					<li><a href={concat( $baseURI, '&dateFilter=3', $uriSuffix )|urlencode|sitelink}>{"Last month"|i18n("design/standard/content/search")}</a></li>
					<li><a href={concat( $baseURI, '&dateFilter=4', $uriSuffix )|sitelink}>{"Last three months"|i18n("design/standard/content/search")}</a></li>
					<li><a href={concat( $baseURI, '&dateFilter=5', $uriSuffix )|sitelink}>{"Last year"|i18n("design/standard/content/search")}</a></li>
				</ul>
			</li>
		{/if *}
                    {def
                        $global_vars      = ezglobalvars()
                        $language_list    = language_switcher( $requested_uri_string )
                        $current_language = $language_list[ $global_vars.eZCurrentAccess.name|explode("_")[0] ].text|wash()
                        $region_title     = ''
                    }
                    <li>
                        <span><strong>{'Region'|i18n( 'extension/ezfind/facets' )}</strong></span>
                        <ul>
                        {foreach $language_list as $siteaccess => $language}
                            {set $region_title = $language.text}
                            {if eq( $siteaccess, 'row' )}
                                {set $region_title = 'Asia Pacific/Latin America/Middle East & Africa'}
                            {/if}
                            <li>{if eq( $current_language, $language.text )}<strong>{/if}<a href="{concat( $language.url, $contentFiltersBaseURI, $uriSuffix )|explode( '//' )|implode( '/' )}">{$region_title}</a>{if eq( $current_language, $language.text )}</strong>{/if}</li>
                        {/foreach}
                        </ul>
                    </li>
		</ul>
	</section>
</section>
{/set-block}
{/if}
{pagedata_merge(hash('sidebar',$facets,'searchbar',$searchbar))}
{literal}<script type="text/javascript">ezfSetBlock('ezfFacets',ezfGetCookie('ezfFacets'));ezfSetBlock('ezfHelp',ezfGetCookie('ezfHelp'));</script>{/literal}
