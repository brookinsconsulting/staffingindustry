{set-block scope=root variable=cache_ttl}0{/set-block}

{set $facet = $facet|explode('&quot;')|implode('"')}

{def $classes=array('article')
	 $page_limit=10
	 $activeFacetParameters = array()}

{def $use_filterParameters = 
			array( 'or',
			concat('submeta_topics___id_si:', 70472),
			concat('meta_path_si:', 70472)
			)
}

{if and($facet|contains(" "), $facet|contains('"')|not)}
	{set $facet = concat('"', $facet, '"')}
{/if}

{if $facet|eq('')}{set $facet="*:*"}{/if}

{$facet|debug('facet')}

{def $search=fetch('ezfind','search',hash(
						'query', $facet,
						'offset',$#view_parameters.offset,
						'limit',$page_limit,
						'sort_by',hash('published','desc'),
						'filter',$use_filterParameters,
						'class_id', $classes,
						'spell_check',array(true())
						))}
						
{def $search_result=$search['SearchResult']}
{def $search_count=$search['SearchCount']}
{def $search_extras=$search['SearchExtras']}
{def $stop_word_array=$search['StopWordArray']}
{def $search_data=$search}


<div class="content-search search-results">
{include name=Navigator
	uri='design:navigator/google.tpl'
	page_uri=$#node.url_alias|ezroot(no)
	page_uri_suffix=''
	item_count=$search_count
	view_parameters=$#view_parameters
	item_limit=$page_limit}
{foreach $search_result as $result sequence array('bglight','bgdark') as $bgColor}
	{node_view_gui view='ezfind_line' bgColor=$bgColor use_url_translation=first_set($use_url_translation,false()) content_node=$result}
{/foreach}
{include name=Navigator
	uri='design:navigator/google.tpl'
	page_uri=$#node.url_alias|ezroot(no)
	page_uri_suffix=''
	item_count=$search_count
	view_parameters=$#view_parameters
	item_limit=$page_limit}
</div>


