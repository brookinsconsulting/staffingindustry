{* Twitter Feed and Follow - Custom Tag *}
{def 	$users = first_set($userlist, 'execforum')
		$user_list = $users|explode(',')
		$siteaccess = siteaccess()
		$url = '/twitter/tweets/'
}
{if eq($siteaccess, 'site')}{set $siteaccess = ''}{else}{set $siteaccess = concat('/', $siteaccess)}{/if}
{set $url = concat($siteaccess, $url)}
<section class="module customtag custom-tag-twitterfeed">
	<header>
		<div class="column header-icon"><img alt="" src={'images/twitter-icon.png'|ezdesign()} /></div>
		<div class="column header-text"><h2>Twitter</h2></div>
	</header>
	<div class="customtag-content">
	{foreach $user_list as $user}
		{set $user = $user|trim()}
		<div id="{$user}-feed" class="feed-item">
			<div class="column">
			<a href="https://twitter.com/{$user}" target="_blank" class="twitter-follow-button" data-show-count="false" data-size="large" data-show-screen-name="false">Follow @{$user}</a>
			{literal}
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			{/literal}
			</div>
			<div class="column">
				<a href="https://twitter.com/{$user}" target="_blank" class="twitter_user">{$user}</a>
			</div>
			<script type="text/javascript">
				(function(user, url){literal}{
						$.ajax({
							'context':$('#' + user + '-feed'),
							'url':url + user + '/1',
							'dataType':'json',
							'success':function(data){
								if (data) {
									for(var i=0; i<data.length; i++){
										ago = "";
										// if (typeof data[i].created_at != Error) ago = $utils.timeAgo(data[i].created_at, new Date(), 4)
										// this.append('<div class="media_feed_post">'+data[i].text+'<br /><br />'+data[i].created_at+'</div>');
										this.append('<div class="media_feed_post">'+data[i].text+'</div>');									
									}
								}
							}
						})
				}{/literal})('{$user}', '{$url}');
			</script>

		</div>
	{/foreach}
	</div>
</section>



