{* List Recent Blog Posts - Custom Tag *}
{def $node = fetch('content','node',hash('node_id',210))
	 $page_limit = first_set($limit, 10)
	 $blog_post_count = fetch('content', 'tree_count', hash('parent_node_id',$node.node_id,
						'class_filter_type','include',
						'class_filter_array',array('blog_post')))
	 $has_header = and(is_set($header),ne($header,''))
	 $has_sub_header = and(is_set($sub_header),ne($sub_header,''))
}
<section class="custom-tag-recent_blogpostlist">
	{if or($has_header, $has_sub_header)}<header>{if $has_header}<h1>{$header}</h1>{/if}{if $has_sub_header}<h2>{$sub_header}</h2>{/if}</header>{/if}
	{foreach fetch('content', 'tree', hash('parent_node_id',$node.node_id,
						'limit', $page_limit,
						'offset', $#view_parameters.offset,
						'class_filter_type','include',
						'class_filter_array',array('blog_post'),
						'sort_by', array('published',false()))) as $blog}
		<section class="content-view-list class-blog-post">
			<h2><a href={$blog|sitelink()} title="{$blog.data_map.title.content|wash()}">{$blog.data_map.title.content|wash()}</a></h2>
			<span class="date">{$blog.data_map.publication_date.content.timestamp|datetime('custom','%F %j, %Y')}<span class="author"> - from {$blog.parent.name|wash()}</span></span>
			<div class="attribute-body">
			{attribute_view_gui attribute=$blog.data_map.body htmlshorten=array(180,concat('... <a href="',$blog|sitelink('no'),'">More</a>'))}
			</div>
			{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
		</section>
	{/foreach}
		{if gt($blog_post_count, $page_limit)}
		<div class="lineitem_controls">
			<div class="pager">
			{include name=Navigator
				uri='design:navigator/google.tpl'
				page_uri=$node.url_alias|sitelink(no)
				item_count=$blog_post_count
				view_parameters=$#view_parameters
				item_limit=$page_limit
				}
			</div>
		</div>
		{/if}
</section>
