<!-- Daily News - Custom Tag -->

{cache-block keys=array(first_set($limit,3))}

{def    $dataNodes_eng =fetch('site', 'daily_news', hash('researchNodeID', 195, 'language', 'eng-GB@euro', 'limit', first_set($limit,3)))
    $dataNodes_us =fetch('site', 'daily_news', hash('researchNodeID', 195, 'language', 'eng-US', 'limit', first_set($limit,3)))
    $dataNodes_row =fetch('site', 'daily_news', hash('researchNodeID', 195, 'language', 'eng-RW', 'limit', first_set($limit,3)))
}

<section class="module customtag custom-tag-daily-news vertical">
    <header>
        <h2>Daily News</h2>
        {include
            uri="design:content/datatype/view/ezxmltags/realmedia.tpl"
            position='TopRight'
        }
    </header>

    <section class='content_selector'><a href='#' {literal}onclick="toggleDailyNews(this);return false;"{/literal} class="items{if $dataNodes_us[1]|contains('site')} news-toggle{/if}">NA</a> | <a href='#' {literal}onclick="toggleDailyNews(this); return false;"{/literal} class="items{if $dataNodes_us[1]|contains('eng')} news-toggle{/if}">EUR</a><br><a href='#' {literal}onclick="toggleDailyNews(this); return false;"{/literal} class="items{if $dataNodes_us[1]|contains('row')} news-toggle{/if}">APAC/LATAM/MEA</a></section>

    <section class="customtag-content">
        <ul class="items NA{if $dataNodes_us[1]|contains('site')} news-toggle{/if}">
            {foreach $dataNodes_us[0] as $key=>$subitem}
                <li><a href={$subitem|sitelink}>{$subitem.name|wash|html_shorten(150)}</a></li>
            {/foreach}
        </ul>

        <ul class="items EU{if $dataNodes_us[1]|contains('eng')} news-toggle{/if}">
            {foreach $dataNodes_eng[0] as $key=>$subitem}
                <li><a href={$subitem|sitelink}>{$subitem.name|wash|html_shorten(150)}</a></li>
            {/foreach}
        </ul>
        <ul class="items ROW{if $dataNodes_us[1]|contains('row')} news-toggle{/if}">
            {foreach $dataNodes_row[0] as $key=>$subitem}
                <li><a href={$subitem|sitelink}>{$subitem.name|wash|html_shorten(150)}</a></li>
            {/foreach}
        </ul>
    </section>
</section>

{run-once}
<script type='text/javascript'>

{literal}
function toggleDailyNews(el) {
    var classObject = $(el).html();
    $(el).siblings().removeClass('news-toggle');
    $(el).toggleClass('news-toggle');
    $(el).parents('section.content_selector').siblings('section.customtag-content').find('ul').removeClass('news-toggle');
    $('.custom-tag-daily-news section.customtag-content .'+classObject).toggleClass('news-toggle');
}
{/literal}

</script>
{/run-once}

{/cache-block}
