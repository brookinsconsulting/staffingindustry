{* Latest Research Customtag *}
{def	$article_node = cond(ne($articlenode, ''), $articlenode|explode('eznode://'), false())
	$research_node = cond(ne($researchnode, ''), $researchnode|explode('eznode://'), false())
	$node_array = array()
	$item = ''
	$limit = cond(eq($limit, ''), 10, $limit)
}
{if $article_node}
	{def	$article = fetch('content', 'list', hash('parent_node_id', $article_node[1],
								'sort_by', array('priority', true()),
								'limit', $limit,
								'class_filter_type', 'include',
								'class_filter_array', array('article') ))}
	{foreach $article as $article_item}
		{set $node_array = $node_array|append($article_item.contentobject_id)}
	{/foreach}
{/if}
{if $research_node}
	{def	$research = fetch('content', 'tree', hash('parent_node_id', $research_node[1],
								'sort_by', array('attribute', false(), 'article/publish_date'),
								'limit', $limit,
								'class_filter_type', 'include',
								'class_filter_array', array('article') ))}
	{foreach $research as $research_item}
		{set $node_array = $node_array|append($research_item.contentobject_id)}
	{/foreach}
{/if}
{set $node_array = $node_array|unique()}
{set $node_array = $node_array|extract_left($limit)}
<div class="module customtag custom-tag-latest-research">
<h2>Latest Research</h2>

<ul class="vertical">
{foreach $node_array as $node}
	{set $item = fetch('content', 'object', hash('object_id', $node))}
	<li><a href={$item|sitelink()}>{if $item.data_map.headline.has_content}{$item.data_map.headline.content|wash()}{else}{$item.data_map.title.content|html_shorten(36)|wash()}{/if}</a></li>
{/foreach}
</ul>
<a title="" href="/Research-Publications/Research-Topics">More Research</a>
</div>
