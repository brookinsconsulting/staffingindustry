{* List Sub Items - Custom Tag *}
{def $node = fetch('content','node',hash('node_id',210))
	 $dataNodes = fetch('content', 'tree', hash('parent_node_id',$node.node_id,
						'limit', first_set($limit,3),
						'class_filter_type','include',
						'class_filter_array',array('blog_post'),
						'sort_by', array('published',false())))
	 $has_header = and(is_set($header),ne($header,''))
	 $has_sub_header = and(is_set($sub_header),ne($sub_header,''))
	 $rssexport=rssexport($node.node_id)
}
{def $rsssource = fetch(content, node, hash(node_id, $rssexport.source_node_id))}
<section class="module customtag custom-tag-listsubitems">
	<header>{if $has_header}<h1>{$header}</h1>{/if}{if $has_sub_header}<h2>{$sub_header}</h2>{/if}{if count($rssexport)}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezroot('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /></a>{/if}</header>
	<section class="customtag-content view-item-list">
	{foreach $dataNodes as $subitem}
		{node_view_gui view='listitem' content_node=$subitem htmlshorten=array(110,concat('... <a href="',$subitem|sitelink('no'),'">More</a>'))}
		{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
	{/foreach}
		<a class="seeall" href={$node|sitelink()}>See All</a>
	</section>
</section>