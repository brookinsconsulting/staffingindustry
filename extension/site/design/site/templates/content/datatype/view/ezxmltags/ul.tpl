{if $column_limit|gt(0)}

	{def $matchme = ''|pad(mul($column_limit,18), '<li[^<]*<\/li>[^<]*')}

	{def $content_r = $content|preg_replace(concat('/',$matchme|trim('[^<]*'),'/mus'), '$0<--!breakpoint-->')|explode('<--!breakpoint-->')
	}

	{foreach $content_r as $content_col}
	<div class="column"{if $column_width|gt(0)} style="width:{$column_width};"{/if}>
		<ul{if ne($classification|trim,'')} class="{$classification|wash}"{/if}>
			{$content_col|trim}
		</ul>
	</div>
	{/foreach}
	
{else}

	<ul{if ne($classification|trim,'')} class="{$classification|wash}"{/if}>
		{$content}
	</ul>
	
{/if}

