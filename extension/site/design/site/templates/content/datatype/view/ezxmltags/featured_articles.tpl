{* Featured Articles - Custom Tag *}
{def $articles=fetch('content','tree',hash('parent_node_id',2,
											'limit',10,
											'class_filter_type','include',
											'class_filter_array',array('article'),
											'attribute_filter',array(array(393,'=',1))
											))
}

<section class="module customtag custom-tag-scrollable featured-articles horizontal lock">
	<header>
	{def $current_user = fetch('user', 'current_user')}
	{if $current_user.groups|contains(42)}
		<a class="lock" title="Content available to subscribers <p><a href='/user/login'>Login</a> or <a class='nav-link' href='/site/Subscribe'>Subscribe</a> now.</p>" href="/user/login"><img alt="Subscriber&#39;s Only" src={'images/lock.png'|ezdesign()}></a>
	{/if}
	</header>
	<section class="customtag-content">
		<div class="navi"></div>
		<a class="prev browse left"></a>
		<div class="scrollable">
			<div class="items">
			{foreach $articles as $article}{node_view_gui view='embed' content_node=$article image_class='articlethumbnail'}{/foreach}
			</div>
		</div>
		<a class="next browse right"></a>
	</section>
</section>


