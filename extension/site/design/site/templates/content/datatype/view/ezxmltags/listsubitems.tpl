{* List Sub Items - Custom Tag *}
{def
	$node = fetch('content','node',hash('node_id',$page|explode('://')[1]))
	$ticker_limit=cond(eq($stylize,'ticker'),ezini('CustomAttribute_listsubitems_stylize','TickerLimit','ezoe_attributes.ini'),false())
	$has_header = and(is_set($header),ne($header,''))
	$has_sub_header = and(is_set($sub_header),ne($sub_header,''))
	$rssexport=rssexport($node.node_id)
	$dataNodesHash = array()
	$role_list = $current_user.role_id_list
}

{if $current_user|is_set|not}{def $current_user = fetch(user, current_user)}{/if}

{if $node.node_id|eq(195)}
	{set $ticker_limit = 28}
{/if}

{set $dataNodesHash = $dataNodesHash|merge(hash('parent_node_id', $node.node_id))}
{set $dataNodesHash = $dataNodesHash|merge(hash('limit', first_set(cond($ticker_limit,$ticker_limit,$limit),3)))}

{if array('event_calendar','multicalendar')|contains($node.class_identifier)}
	{set $dataNodesHash = $dataNodesHash|merge(hash('sort_by', array('attribute',true(),'event/from_time')))}
	{set $dataNodesHash = $dataNodesHash|merge(hash('class_filter_type','include'))}
	{set $dataNodesHash = $dataNodesHash|merge(hash('class_filter_array',array('event')))}
	{set $dataNodesHash = $dataNodesHash|merge(hash('attribute_filter',array( 'or', array('event/from_time','>=',currentdate()), array('event/to_time','>=',currentdate()))))}
{else}
	{set $dataNodesHash = $dataNodesHash|merge(hash('sort_by', $node.sort_array))}
	{set $dataNodesHash = $dataNodesHash|merge(hash('class_filter_type', 'exclude'))}
	{set $dataNodesHash = $dataNodesHash|merge(hash('class_filter_array', array(ad_zone, infobox, banner, image)))}
{/if}

{if and($role_list|contains(6), not($role_list|contains(8)))}
	{* for users who have the buyer role and not the staffing role *}
	{* only fetch objects in Standard (1), Buyer (7), Buyer + Staffing (9), and Buyer Products (11) *}
	{set $dataNodesHash = $dataNodesHash|merge(hash('limitation', array(hash('Section', array(1,7,9,11), ))))}
{/if}

{def
	$dataNodes = fetch('content', 'list', $dataNodesHash)
}

{*
{pagedata_set('included_node_id',$node.node_id,true())}
*}
{def $rsssource = fetch(content, node, hash(node_id, $rssexport.source_node_id))}
{if ne($stylize,'ticker')}
<section class="{if eq($stylize,'advanced')}advanced{/if}{if ne($stylize,'basic')} module{/if} customtag custom-tag-listsubitems">
	<header>{if or($has_header, eq($stylize,'custom'))}{cond(eq($header,'$content'),$content,concat('<h1>',$header,'</h1>', cond($has_sub_header,concat('<h2>',$sub_header,'</h2>'),'') ))}{else}<h1>{$node.name|wash()}</h1>{/if}{if count($rssexport)}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezurl('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /></a>{/if}</header>
	<section class="customtag-content view-item-list">
	{foreach $dataNodes as $subitem}
		{node_view_gui view=$children_view content_node=$subitem htmlshorten=cond(eq($children_view,'listitem'), array(80,concat('... <a href="',$subitem|sitelink('no'),'">More</a>')), false() )}
		{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
	{/foreach}
{if ne($stylize,'basic')}
		{if is_set($alltext)}{if ne($alltext,'$none')}<a class="seeall" href={$node|sitelink()}>{$alltext}</a>{/if}{else}<a class="seeall" href={$node|sitelink()}>See All</a>{/if}
{/if}
	</section>
</section>
{else}
{def $plimit=$limit}
<section class="module customtag custom-tag-scrollable vertical">
	<header>
		{if or($has_header, eq($stylize,'custom'))}{cond(eq($header,'$content'),$content,concat('<h1>',$header,'</h1>', cond($has_sub_header,concat('<h2>',$sub_header,'</h2>'),'') ))}{else}<h1>{$node.name|wash()}</h1>{/if}{if count($rssexport)}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezurl('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /></a>{/if}
		{include
			uri="design:content/datatype/view/ezxmltags/realmedia.tpl"
			position='TopRight'
		}
	</header>

	<section class="customtag-content">
		<a class="prev browse up"></a><hr class="line" />
			<div class="scrollable">
				<div class="items">
					<div>
						{foreach $dataNodes as $key=>$subitem}
							{if and(gt($key,1),lt($key,count($dataNodes)),eq(mod($key,$plimit),0))}</div><div>{/if}
							{node_view_gui view=$children_view content_node=$subitem}
						{/foreach}
					</div>
				</div>
			</div>
		<a class="next browse down"></a><hr class='line' />
		{if is_set($alltext)}{if ne($alltext,'$none')}<a class="seeall" href={$node|sitelink()}>{$alltext}</a>{/if}{else}<a class="seeall" href={$node|sitelink()}>See All</a>{/if}
	</section>
</section>
{/if}
