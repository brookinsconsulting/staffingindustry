{* Content Box - Custom Tag *}
{def $has_header = and(is_set($header),ne($header,''))
	 $has_sub_header = and(is_set($sub_header),ne($sub_header,''))
}
<section class="module {first_set($display_type,'customtag')}{if first_set($class,false())} {$class|implode(' ')}{/if}">
	{if or($has_header,$has_sub_header)}<header>{if $has_header}<h1>{$header}</h1>{/if}{if $has_sub_header}<h2>{$sub_header}</h2>{/if}</header>{/if}
	<section class="{first_set($display_type,'customtag')}-content">{$content}</section>
</section>
