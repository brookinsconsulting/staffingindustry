{* Article - Custom Tag *}
{def $node = fetch('content','node',hash('node_id',$page|explode('://')[1]))}

{if $children_view|eq('itemized')}
	{def $dataNodes = fetch('content', list, hash('parent_node_id',$node.node_id,
						'limit', 1,
						'class_filter_type','include',
						'class_filter_array',array('issue'),
						'sort_by', $node.sort_array))}

	{content_view_gui view=itemizedsubitems content_object=$dataNodes.0.object object_parameters=hash('limit', 5, 'offset', 0)}
{else}
{def $ticker_limit=cond(eq($stylize,'ticker'),ezini('CustomAttribute_listsubitems_stylize','TickerLimit','ezoe_attributes.ini'),false())
	 $dataNodes = fetch('content', $depth, hash('parent_node_id',$node.node_id,
						'limit', first_set(cond($ticker_limit,$ticker_limit,$limit),3),
						'class_filter_type','include',
						'class_filter_array',array('article'),
						'sort_by', array( 'attribute', false(), 'article/publish_date' )))
	 $has_header = and(is_set($header),ne($header,''))
}
<section class="{if eq($stylize,'ads')} advanced{/if}{if eq($stylize,'modular')} module{/if} customtag custom-tag-listsubitems">
	<header>{if or($has_header, eq($stylize,'custom'))}{cond(eq($header,'$content'),$content,concat('<h1>',$header,'</h1>'))}{else}<h1>{$node.name|wash()}</h1>{/if}</header>
	<section class="customtag-content view-item-list">
	{foreach $dataNodes as $key=>$subitem}
		{if and(ne($key,0),eq(mod($key,2),0))}
		<div class="separator content-view-embed class-image align-center advertisment">
			<div class="inline-ad">
				<div class="attribute-image Middle1" style="float:left;">
					<div class="caption">Advertisment</div>
					{include
						uri="design:content/datatype/view/ezxmltags/realmedia.tpl"
						position='Middle1'
					}
				</div>
				<div class="attribute-image Middle2" style="float:right;">
					<div class="caption">Advertisment</div>
					{include
						uri="design:content/datatype/view/ezxmltags/realmedia.tpl"
						position='Middle2'
					}
				</div>
			</div>
		</div>
		{/if}
		{node_view_gui view=$children_view content_node=$subitem htmlshorten=cond(eq($children_view,'listitem'), array(80,concat('... <a href="',$subitem|sitelink('no'),'">More</a>')), false() )}
		{delimiter}{if not(and(ne($key,0),eq(mod($key,2),0)))}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/if}{/delimiter}
	{/foreach}
	</section>
</section>
{/if}
