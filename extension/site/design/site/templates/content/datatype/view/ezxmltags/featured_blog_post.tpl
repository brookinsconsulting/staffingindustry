{* featured_blog_post custom tag *}

{def
	$featured_posts = fetch('content', 'tree', hash(
		'parent_node_id', 2,
		'class_filter_type', 'include',
		'class_filter_array', array('blog_post'),
		'limit', first_set($limit, 4),
		'sort_by', array('published', false())
	))
}
<section class="module customtag custom-tag-featured_blog_post">
	<header>
		<h2>Featured Blog</h2>
	</header>
	{foreach $featured_posts as $key => $post}
		{if $key|eq(0)}
			{node_view_gui content_node=$post view=embed}
		{else}
			{if $key|eq(1)}<ul class="vertical menu">{/if}
			<li><a href={$post|sitelink()} title="{$post.data_map.title.content|wash()}">{$post.data_map.title.content|wash()}</a></li>
		{/if}
	{/foreach}
	</ul>
</section>
