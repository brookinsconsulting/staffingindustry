{* GridView - Custom Tag *}
{if eq($width,'full')}{pagedata_merge(hash('sidebar',false(),'extrainfo',false()))}{/if}
{def $node_list=fetch('content','list',hash('parent_node_id',$#node.node_id,'class_filter_type','include','class_filter_array',array('folder'),'sort_by',$#node.sort_array))
	 $rowCount=ceil(div(count($node_list),4))
	 $sub_node_list=false()
}
<div class="column">
{foreach $node_list as $key=>$node_item}
{set $sub_node_list=fetch('content','list',hash('parent_node_id',$node_item.node_id,'class_filter_type','include','class_filter_array',array('folder'),'sort_by',$node_item.sort_array))}
{if and( gt($key,0), eq($key|mod($rowCount),0) )}
</div>
<div class="column">
{/if}
<div class="content-view-gridview class-{$node_item.class_identifier}">
	<h2><a href={$node_item|sitelink()}>{$node_item.name|wash()}</a></h2>
	{if count($sub_node_list)}
	<ul>
		{foreach $sub_node_list as $sub_node_item}
		<li><a href={$sub_node_item|sitelink()}>{$sub_node_item.name|wash()}</a></li>
		{/foreach}
	</ul>
	{else}
		{if and(first_set($node_item.data_map.short_description,false()),$node_item.data_map.short_description.has_content)}{attribute_view_gui attribute=$node_item.data_map.short_description}{/if}
	{/if}
</div>
{/foreach}
</div>