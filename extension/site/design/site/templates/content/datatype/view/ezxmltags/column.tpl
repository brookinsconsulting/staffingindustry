<!-- Topic - Embed /extension/site/design/site/templates/content/datatype/view/ezxmltags/column.tpl -->

{def $classes = ''}
{if is_set($class)}
	{if is_array($class)}
		{set $classes = $class|explode(' ')}
	{else}
		{set $classes = $class}
	{/if}
{/if}
<div{if $column_width|ne('')} style='width: {$column_width}'{/if} class="column{if ne($classes, '')} {$classes}{/if}">{$content}</div>