{set-block scope=root variable=cache_ttl}0{/set-block}

{def
	$node = fetch(content, node, hash(node_id, 195))
	$ticker_limit=ezini('CustomAttribute_listsubitems_stylize','TickerLimit','ezoe_attributes.ini')
	$has_header = and(is_set($header),ne($header,''))
	$has_sub_header = and(is_set($sub_header),ne($sub_header,''))
	$has_language = and(is_set($language),ne($language,''))
	$rssexport=rssexport($node.node_id)
	$dataNodesHash = array()
	$current_user = fetch(user, current_user)
	$role_list = $current_user.role_id_list
}

{set $facet = $facet|explode('&quot;')|implode('"')}

{def $classes=classes()
	 $hideclasses = array('user', 'client')
	 $goodclasses = array()
}

{foreach $classes as $c}
	{if $hideclasses|contains($c)|not}{set $goodclasses = $goodclasses|append($c)}{/if}
{/foreach}


{def
	$sections=fetch( 'content', 'section_list' )
	$secName = ''
}
{def $search=false()}

{def $activeFacetParameters = array()}

{if $has_language}
{def $use_filterParameters = array( 'or',
			             concat('submeta_topics___id_si:', 195, ' AND meta_available_language_codes_ms:', $language),
                                     concat('meta_path_si:', 195, ' AND meta_available_language_codes_ms:', $language)
                                  )}
{else}
{def $use_filterParameters = array( 'or',
			             concat('submeta_topics___id_si:', 195),
                                     concat('meta_path_si:', 195)
                                  )}
{/if}
						

{if and($facet|contains(" "), $facet|contains('"')|not)}
	{set $facet = concat('"', $facet, '"')}
{/if}

{if $facet|eq('')}{set $facet="*:*"}{/if}

{$facet|debug('facet')}

{set $search=fetch('ezfind','search',hash(
						'query', $facet,
						'offset',$#view_parameters.offset,
						'limit',first_set(cond($ticker_limit,$ticker_limit,$limit),3),
						'sort_by',hash('published','desc'),
						'filter',$use_filterParameters,
						'class_id', $goodclasses,
						'spell_check',array(true())
						))}
{def $search_result=$search['SearchResult']}
{def $search_count=$search['SearchCount']}
{def $search_extras=$search['SearchExtras']}
{def $stop_word_array=$search['StopWordArray']}
{def $search_data=$search}

{def $plimit=4}
<section class="module customtag custom-tag-scrollable vertical">
	<header>
		{if or($has_header, eq($stylize,'custom'))}{cond(eq($header,'$content'),$content,concat('<h1>',$header,'</h1>', cond($has_sub_header,concat('<h2 class="shrinkwrap">',$sub_header,'</h2>'),'') ))}{else}<h1>{$node.name|wash()}</h1>{/if}{if count($rssexport)}<a class="rss" href="{concat('/services/rss/', $rssexport.access_url)|ezurl('no')}" title="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed"><img src="{'images/rss.png'|ezdesign('no')}" alt="{$rsssource.name|wash()}-Staffing Industry Analysts RSS Feed" /></a>{/if}
		{include
			uri="design:content/datatype/view/ezxmltags/realmedia.tpl"
			position='TopRight'
		}
	</header>

	<section class="customtag-content">
		<a class="prev browse up"></a><hr class="line" />
			<div class="scrollable">
				<div class="items">
					<div>
						{foreach $search_result as $key=>$result}
							{if and(gt($key,1),lt($key,count($search_result)),eq(mod($key,$plimit),0))}</div><div>{/if}
							{node_view_gui view='listitem' content_node=$result}
						{/foreach}
					</div>
				</div>
			</div>
		<a class="next browse down"></a><hr class='line' />
	</section>
</section>


