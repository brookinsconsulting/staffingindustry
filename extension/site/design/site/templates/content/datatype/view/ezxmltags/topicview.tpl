{* TopicView - Custom Tag *}
{debug-accumulator id="custom/topicview"}

{ezscript_require(array(
	'admin2pp::jqueryui',
	'ezjsc::jqueryio',
	'node_tabs.js',
	'ezajaxrelations_jquery.js',
	'admin2pp_utils.js',
	'admin2pp_preview.js',
	'admin2pp_dashboard_improved.js',
	'admin2pp_dashboard_feed_reader.js'
))}

{literal}
	<script type="text/javascript">
	$(function(){
		$('.track_link a').click(function(){
				admin2ppAjaxSavePreference(
					'dashboard-block-my_top_articles-ssss' + $(this).attr('name'),
					$(this).attr('rel'),
					false
				);
				if ($(this).attr('rel') == '')	{
					$(this).html('track topic');
					$(this).attr('rel', '1');
				}else{
					$(this).html('stop tracking');
					$(this).attr('rel', '');
				}
				return false;
		});
		
		$('form.search-topics-form .submit_button').click(function(){
		    thisform = $('form.search-topics-form');
		    newaction = thisform.attr('action') + "/(searchterms)/" + $('#SearchText').val() + "-" + $('#SearchCategory').val();
    		thisform.attr('action', newaction);
		});
		
	});

	function admin2ppAjaxSavePreference( name, value, reload ) // need for this to wait after post, redefining from admin2pp_utils.js
	{
		var url = jQuery.ez.url.replace( 'ezjscore/', 'user/preferences/' ) + 'set_and_exit/' + name + '/' + value; 
		jQuery.post( url, {}, function(result){
			if (reload) {
				/* this prompts the user in firefox to resend data
				window.location.reload();
				*/
				window.location.href = reload;
			}
			return result;
		});
	}

	function valSearchForm(thisform)	{
		if (((document.location.href.indexOf("?") > 0) && ($('#SearchCategory').val() == '')) && (($('#SearchText').val() == '') || ($('#SearchText').val() == 'Optional Keyword Search...'))) {
			document.location.href = document.location.href.split("?")[0];
			return false;
		}
		if ((($('#SearchText').val() == '') || ($('#SearchText').val() == 'Optional Keyword Search...')) && ($('#SearchCategory').val() == ''))	{
			confirm('Please enter a search term, \nor select a category from the dropdown list.');
			return false;
		}
		/*
		if (($('#SearchText').val() == '') || ($('#SearchText').val() == 'Keyword search...'))	{
			confirm('Please enter a search term, \nor use the topic or article links instead of search.');
			return false;
		}
		
		*/
	}
	</script>
{/literal}

{def 	$current_user=fetch( 'user', 'current_user' )
		$role_list=$current_user.role_id_list
		$node_list=false()
	 	$rowCount=0
	 	$sub_node_list=false()
	 	$sub_node_list_count=0
		$search=false()
		$search_result=false()
		$search_extras=false()
		$stop_word_array=false()
		$search_data=false()
		$section=false()
		$search_count=0
		$search_timestamp=0
		$page_limit=10
		$search_text=''
		$search_category=''
		$search_subtree_array=''
		$rrs_count=0
		$rrs=false()
		$mysort = array('published')
		$activeFacetParameters = array()
		$savedsearches = ezpreference('dashboard-block-saved_searches')
		$baseURI=''
		$baseURIsuffix=''
		$column_position=''
		$category_node=''
		$dashboard_block_identifier = 'my_top_articles'
		$selected_topic = false()
		$track_this_topic = ''
		$cat_name_parts = array()
		$cat_name = ''
}

{if cond(ezhttp_hasvariable('SubTreeArray','get'),array(ezhttp('SubTreeArray','get')),false())}
	{if cond(ezhttp_hasvariable('SearchText','get'),array(ezhttp('SearchText','get')),false())}
		{if cond(ezhttp_hasvariable('SearchWord','get'),array(ezhttp('SearchWord','get')),false())}
			{if eq(ezhttp('SearchWord','get'), 'complete_phrase')}
				{set $search_text = concat('"', ezhttp('SearchText','get'), '"')}
			{else}
				{set $search_text = ezhttp('SearchText','get')}
			{/if}
		{else}
			{set $search_text = ezhttp('SearchText','get')}
		{/if}
	{/if}
	{if cond(ezhttp_hasvariable('SubTreeArray','get'),array(ezhttp('SubTreeArray','get')),false())}
		{set $search_subtree_array = ezhttp('SubTreeArray','get')}
	{/if}	
	{if cond(ezhttp_hasvariable('SearchCategory','get'),ne(ezhttp('SearchCategory','get'),''),false())}
		{set $search_category = ezhttp('SearchCategory','get')}	
	{else}
		{set $search_category = cond(ezhttp_hasvariable('SubTreeArray','get'),ezhttp('SubTreeArray','get'),false())}	
	{/if}
	{if cond(ezhttp_hasvariable('SearchTimestamp','get'),ne(ezhttp('SearchTimestamp','get'),''),false())}
		{set $search_timestamp = array(ezhttp('SearchTimestamp','get'))}	
	{/if}
	{set $category_node=fetch( 'content', 'node', hash( 'node_id', $search_category ) )}
	{if ezhttp_hasvariable('activeFacets','get')}
		{set $activeFacetParameters = ezhttp('activeFacets','get')}
	{/if}
	{def $dateFilter=0}
	{if ezhttp_hasvariable('dateFilter','get')}
		{set $dateFilter = ezhttp('dateFilter','get')}
		{switch match=$dateFilter}
		{case match=1}
			{def $dateFilterLabel="Last day"|i18n("design/standard/content/search")}
		{/case}
		{case match=2}
			{def $dateFilterLabel="Last week"|i18n("design/standard/content/search")}
		{/case}
		{case match=3}
			{def $dateFilterLabel="Last month"|i18n("design/standard/content/search")}
		{/case}
		{case match=4}
			{def $dateFilterLabel="Last three months"|i18n("design/standard/content/search")}
		{/case}
		{case match=5}
			{def $dateFilterLabel="Last year"|i18n("design/standard/content/search")}
		{/case}			
		{/switch}	
	{/if}
	
	
	{def $filterParameters = fetch('ezfind','filterParameters')
		$defaultSearchFacets = fetch('ezfind','getDefaultSearchFacets')}

	{set $cat_name_parts = $category_node.name|explode(':')}
	{set $cat_name = $cat_name_parts|reverse[0]}
	{set $filterParameters = 
				array( 'or',
				concat('submeta_topics___id_si:', $category_node.object.id),
				concat('submeta_new_topics___id_si:', $category_node.object.id),
				concat('meta_path_si:', $category_node.node_id)
				)
	}

	
	{def $custom_filters=array()}
	
	{if ezhttp_hasvariable('filter','get')}
		{def $at_filter = ezhttp('filter','get')}
		{set $filterParameters = array('and', $filterParameters)}
		{foreach $at_filter as $af}
		{set $filterParameters = $filterParameters|append(concat($af|explode(":")|implode(':"'), '"'))
			 $custom_filters = $custom_filters|append(concat($at_filter[0]|explode(":")|implode(':"'), '"'))}
		{/foreach}
	{/if}


	{if $search_text|eq('')}{set $search_text="*:*"}{/if}
								
	{set $search=fetch('ezfind','search',hash(
								'query',$search_text,
								'filter', $filterParameters,
								'offset',$#view_parameters.offset,
								'limit',$page_limit,
								'sort_by',hash( 'score','desc', 'published', 'desc' ),
								'facet',$defaultSearchFacets,
								'publish_date',$dateFilter,
								'class_id', array( 'article' ),
								'spell_check',array(true()),
								'subtree_array', array(70472)
								))}
								
	{set $search_result=$search['SearchResult']}
	{set $search_count=$search['SearchCount']}
	{set $search_extras=$search['SearchExtras']}
	{set $stop_word_array=$search['StopWordArray']}
	{set $search_data=$search}

	{set $baseURI = concat($#node.node_id|sitelink('no'),'?SubTreeArray=', ezhttp('SubTreeArray','get'))}
	{set $baseURIsuffix = concat('?SubTreeArray=', ezhttp('SubTreeArray','get'))}
	{set $baseURI=concat($baseURI,'&SearchText=',$search_text)}
	{set $baseURIsuffix=concat($baseURIsuffix, '&SearchText=', $search_text)}
	{if cond(ezhttp_hasvariable('SearchWord','get'),array(ezhttp('SearchWord','get')),false())}
		{set $baseURI = concat($baseURI,'&SearchWord=', ezhttp('SearchWord','get'))}
		{set $baseURIsuffix = concat($baseURIsuffix,'&SearchWord=', ezhttp('SearchWord','get'))}
	{/if}
	{if cond(ezhttp_hasvariable('SearchCategory','get'),array(ezhttp('SearchCategory','get')),false())}
		{set $baseURI = concat($baseURI,'&SearchCategory=', ezhttp('SearchCategory','get'))}
		{set $baseURIsuffix = concat($baseURIsuffix,'&SearchCategory=', ezhttp('SearchCategory','get'))}
	{/if}
	{if cond(ezhttp_hasvariable('SearchTimestamp','get'),array(ezhttp('SearchTimestamp','get')),false())}
		{set $baseURI = concat($baseURI,'&SearchTimestamp=', ezhttp('SearchTimestamp','get'))}
		{set $baseURIsuffix = concat($baseURIsuffix,'&SearchTimestamp=', ezhttp('SearchTimestamp','get'))}
	{/if}

	{def $uriSuffix = ''}
	{foreach $activeFacetParameters as $facetField => $facetValue}
		{set $uriSuffix = concat($uriSuffix,'&activeFacets[',$facetField,']=',$facetValue)}
	{/foreach}
	
	{foreach $custom_filters as $value}
		{set $uriSuffix = concat($uriSuffix,'&filter[]=',$value|explode(':"')|implode(':')|trim('"'))}
	{/foreach}

	{if gt($dateFilter,0)}
		{set $uriSuffix = concat($uriSuffix,'&dateFilter=',$dateFilter)}
	{/if}
	
	{literal}
	<script type="text/javascript">
		// toggle block
		function ezfToggleBlock(id){
			var value=(document.getElementById(id).style.display=='none')?'block':'none';
			ezfSetBlock(id,value);
			ezfSetCookie(id,value);
		}
		function ezfSetBlock(id,value){
			var el=document.getElementById(id);
			if(el!=null){
				el.style.display=value;
			}
		}
		function ezfTrim(str){
			if($.browser.version != 7.0){
				return str.replace(/^\s+|\s+$/g,'') ;
			}
		}

		function ezfGetCookie(name){
			if($.browser.version != 7.0){
				var cookieName='eZFind_' + name;
				var cookie=document.cookie;
				var cookieList=cookie.split(";");
				for(var idx in cookieList){
					if (typeof cookieList[idx] == 'string')
						cookie=cookieList[idx].split("=");
						if(ezfTrim(cookie[0]) == cookieName){
							return(cookie[1]);
						}
				}
				return 'none';
			}
		}


		function ezfSetCookie(name,value){
			var cookieName='eZFind_'+name;
			var expires=new Date();
			expires.setTime(expires.getTime()+(365*24*60*60*1000));
			document.cookie = cookieName+"="+value+"; expires="+expires+";";
		}
	
		function saveThisSearch(){
		// save current search to "saved searches" dashboard block
			$('#savethissearch').val('Saving...');
			$('#savethissearch').attr('disabled', 'disabled');
	
			var saved_searches = '{/literal}{$savedsearches}{literal}';
			var this_search = document.URL;
	
			if (saved_searches == '') {
				admin2ppAjaxSavePreference(
					'dashboard-block-saved_searches',
					base64_encode(this_search).replace('/', '-')
				);
			} else {
				admin2ppAjaxSavePreference(
					'dashboard-block-saved_searches',
					saved_searches + ',' + base64_encode(this_search).replace('/', '-')
				);
			}
	
	
			$('#savethissearch').val('Saved');
			// $('#gotodashboard').fadeIn();
			return false;
		}
	</script>
	{/literal}
{/if}

	<div id="topics"> {* this div is opened here but closed at the bottom of the template for framework compatibility *}
		{if $content|contains('topicview')|not}{$content}{/if}
		<div class="search-topics">
			<div id="search_help">
				<h3>Need help?</h3>
				<p><a href={40844|sitelink()}>Complete lists of SIA analysts</a> | 
				Customer Service <a href="mailto:memberservices@staffingindustry.com">email</a>, <br />800-950-9496 | 
				How do I get <a href={455|sitelink()}>access to this research</a>?
				<br /><a href="mailto:jnurthen@staffingindustry.com">John Nurthen</a>, Executive Director, Global Research 
				| <a href="#latest-research-list">Latest Research</a></p>
			</div>
			<h3>Select from the topics below to refine your search</h3>
			<form class="search-topics-form" onsubmit="return valSearchForm(this);" action={$#node.node_id|sitelink('no')}>
				<input type="hidden" id="SubTreeArray" name="SubTreeArray" value="{$#node.node_id}" />
				<label for="SearchCategory">1. </label>
				<select class="select_category" id="SearchCategory" name="SearchCategory">
					<option value="">Latest Research</option>
					{*if not($current_user.groups|contains('453'))*}
					{if not( and( or($role_list|contains(8), $role_list|contains(192), $role_list|contains(195)), not(or($role_list|contains(6), $role_list|contains(197), $role_list|contains(199) )) ) )}
						{set $node_list=fetch('content','list',hash('parent_node_id',$#node.node_id,'class_filter_type','include','class_filter_array',array('folder', 'topic'),'sort_by',$#node.sort_array, 'attribute_filter', array( 'and', array( 'section', '=', '7' ) )))}
						{foreach $node_list as $key=>$node_item}
							<option value="{$node_item.node_id}"{if cond(ezhttp_hasvariable('SearchCategory','get'),array(ezhttp('SearchCategory','get')),false())}{if eq(ezhttp('SearchCategory','get'), $node_item.node_id)} selected="selected"{/if}{/if}>{$node_item.name|wash()}</option>	
						{/foreach}
					{/if}
					{set $node_list=fetch('content','list',hash('parent_node_id',$#node.node_id,'class_filter_type','include','class_filter_array',array('folder', 'topic'),'sort_by',$#node.sort_array, 'attribute_filter', array( 'and', array( 'section', '!=', '7' ) )))}
					{foreach $node_list as $key=>$node_item}
						<option value="{$node_item.node_id}"{if cond(ezhttp_hasvariable('SearchCategory','get'),array(ezhttp('SearchCategory','get')),false())}{if eq(ezhttp('SearchCategory','get'), $node_item.node_id)} selected="selected"{/if}{/if}>{$node_item.name|wash()}</option>	
					{/foreach}
				</select>
				<label for="SearchText">2. </label>
				<input class="search-words" id="SearchText" name="SearchText" {if cond(ezhttp_hasvariable('SearchText','get'),ezhttp('SearchText','get')|ne(''),false())}value="{ezhttp('SearchText','get')}"{else}placeholder="Optional Keyword Search..." {/if} />
				<label>3. </label>
				<input type="submit" class="submit_button" value="Run Search"/>
			</form>
		</div>

		{if cond(ezhttp_hasvariable('SubTreeArray','get'),array(ezhttp('SubTreeArray','get')),false())}
			<h3 class="search-feedback">Search for {cond($search_text|eq('*:*'), 'everything', concat("'", $search_text,"'"))} within {$category_node.name} returned {$search_count} results. {*if $search_count}( <a href={$#node.node_id|sitelink()}>Return to Full Topic View</a> ){/if*}</h3>
			{if and($search_count, ne($search_text, ''))}
				<div id="searchbar">
					<p>
						<b><input type="button" id="savethissearch" onclick="return saveThisSearch();" value="Save this search" /></b>
						<a id="gotodashboard" style="display:none;" href={"/content/dashboard"|sitelink()}>Go to dashboard</a>
					</p>
				</div>
			{/if}
		{/if}
		

{if $search_count}

	<div class="content-search search-results">
		{include name=Navigator
			uri='design:navigator/google.tpl'
			page_uri=$#node.node_id|sitelink('no')
			page_uri_suffix=concat($baseURIsuffix,$search_timestamp|gt(0)|choose('',concat('&SearchTimestamp=',$search_timestamp)),$uriSuffix)
			item_count=$search_count
			view_parameters=$#view_parameters
			item_limit=$page_limit}
	
		{foreach $search_result as $result sequence array('bglight','bgdark') as $bgColor}
			{if $result.node_id}
				{node_view_gui view='ezfind_line' bgColor=$bgColor use_url_translation=first_set($use_url_translation,false()) content_node=$result}
			{/if}
		{/foreach}
		
		{include name=Navigator
			uri='design:navigator/google.tpl'
			page_uri=$#node.node_id|sitelink('no')
			page_uri_suffix=concat($baseURIsuffix,$search_timestamp|gt(0)|choose('',concat('&SearchTimestamp=',$search_timestamp)),$uriSuffix)
			item_count=$search_count
			view_parameters=$#view_parameters
			item_limit=$page_limit}
	</div>	

	{if and(eq(ezini('SearchSettings','DisplayFacets','site.ini'),'enabled'),$search_count, 1|eq(0))}

		{set-block variable='facets'}
			{def $facetData=''}
			<section class="module infobox feedback" id="search-controls">
				<header<h1>Filter Search Results</h1></header>
				<section class="infobox-content">
					{def $activeFacetsCount=0}
					<ul id="active-facets-list" class="menu vertical">

			          {foreach $defaultSearchFacets as $key => $defaultFacet}
			              {if $activeFacetParameters|array_keys|contains( concat( $defaultFacet['field'], ':', $defaultFacet['name']|explode(" ")|implode("%20")  ) )}
			                  {set $facetData=$search_extras.facet_fields.$key}
			                  {foreach $facetData.nameList as $key2 => $facetName}                  
			                      {if eq( $activeFacetParameters[concat( $defaultFacet['field'], ':', $defaultFacet['name'] )|explode(" ")|implode("%20")], $facetName )}
									  {undef $activeFacetsCount}
			                          {def $activeFacetsCount=sum( $key, 1 )}
			                          {def $suffix=$uriSuffix|explode( concat( '&filter[]=', $facetData.queryLimit[$key2]|wash ) )|implode( '' )|explode( concat( '&activeFacets[', $defaultFacet['field'], ':', $defaultFacet['name']|explode(" ")|implode("%20"), ']=', $facetName ) )|implode( '' )}
						              <li>
						                  <a href={concat( $baseURI, $suffix )|ezroot}>[x]</a> <strong>{$defaultFacet['name']}</strong>: {$facetName}
						              </li>                      
			                      {/if}
			                  {/foreach}
			              {/if}          
			          {/foreach}
					
					{* handle date filter here, manually for now. Should be a facet later on *}
					{if gt( $dateFilter, 0 )}
						{set $activeFacetsCount=$activeFacetsCount|inc}
						{def $suffix=$uriSuffix|explode( concat( '&dateFilter=', $dateFilter ) )|implode( '' )}
						<li><a href={concat( $baseURI, $suffix )|sitelink}>[x]</a> <strong>{'Creation time'|i18n( 'extension/ezfind/facets' )}</strong>: {$dateFilterLabel}</li>
					{/if}
					{if ge( $activeFacetsCount, 2 )}
						<li><a href={$baseURI|sitelink}>[x]</a> <em>{'Clear all'|i18n( 'extension/ezfind/facets' )}</em></li>
					{/if}
					</ul>
					<ul id="facet-list" class="menu vertical">
					{foreach $defaultSearchFacets as $key => $defaultFacet}
						{if array_keys( $activeFacetParameters )|contains( concat( $defaultFacet['field'], ':', $defaultFacet['name']) )|not}
						<li>
							{set $facetData=$search_extras.facet_fields.$key}
							<span {*style="background-color: #F2F1ED"*}><strong>{$defaultFacet['name']}</strong></span>
							<ul>
								{foreach $facetData.nameList as $key2 => $facetName}
									{if ne( $key2, '' )}
									{*<li><a href={concat( $baseURI, '&filter[]=', $facetData.queryLimit[$key2]|wash, '&activeFacets[', $defaultFacet['field'], ':', $defaultFacet['name'], ']=', $facetName, $uriSuffix )|sitelink()}>{$facetName}</a>({$facetData.countList[$key2]})</li>*}
									<li>
									<a href={concat( $baseURI, '&filter[]=', $facetData.queryLimit[$key2]|explode(' ')|implode('%20'), '&activeFacets[', $defaultFacet['field'], ':', $defaultFacet['name'], ']=', $facetName|explode(' ')|implode('%20'))}>
										{if eq($defaultFacet.name, 'Audience')}
											{set $section = fetch( 'section', 'object', hash( 'section_id', $facetName,))}
											{$section.name|wash}
										{else}
											{$facetName}
										{/if}
									</a>
									({$facetData.countList[$key2]})</li>
									{/if}
								{/foreach}
							</ul>
						</li>
						{/if}
					{/foreach}
					{* date filtering here. Using a simple filter for now. Should use the date facets later on *}
					{if eq( $dateFilter, 0 )}
						<li>
							<span {*style="background-color: #F2F1ED"*}><strong>{'Creation time'|i18n( 'extension/ezfind/facets' )}</strong></span>
							<ul>
								<li><a href={concat( $baseURI, '&dateFilter=1' )|sitelink}>{"Last day"|i18n("design/standard/content/search")}</a></li>
								<li><a href={concat( $baseURI, '&dateFilter=2' )|sitelink}>{"Last week"|i18n("design/standard/content/search")}</a></li>
								<li><a href={concat( $baseURI, '&dateFilter=3' )|sitelink}>{"Last month"|i18n("design/standard/content/search")}</a></li>
								<li><a href={concat( $baseURI, '&dateFilter=4' )|sitelink}>{"Last three months"|i18n("design/standard/content/search")}</a></li>
								<li><a href={concat( $baseURI, '&dateFilter=5' )|sitelink}>{"Last year"|i18n("design/standard/content/search")}</a></li>
							</ul>
						</li>
					{/if}
					</ul>
				</section>
			</section>

		{/set-block}

		{literal}<script type="text/javascript">ezfSetBlock('ezfFacets',ezfGetCookie('ezfFacets'));ezfSetBlock('ezfHelp',ezfGetCookie('ezfHelp'));</script>{/literal}
	{/if}

	{*pagedata_merge(hash('sidebar',$facets,'sidemenu',false()))*}
	{pagedata_merge(hash('sidebar',false(),'sidemenu',false()))}

{else} {* not($search_count) *}

	{if $current_user|is_buyer()}
		{def $dataNodesParams = hash('type', array('buyer'))}
	{elseif $current_user|is_staffing()}
		{def $dataNodesParams = hash('type', array('staffing'))}
	{else}
		{def $dataNodesParams = hash()}
	{/if}
	{def
		$dataNodes = fetch('site', 'research', cond(
			is_set($#view_parameters.offset),
			$dataNodesParams|merge(hash('offset', $#view_parameters.offset, 'researchNodeID', $#node.node_id )),
			$dataNodesParams
		))
		$dataNodesCount = fetch('site', 'research_count', $dataNodesParams|merge(hash('researchNodeID', $#node.node_id)))
		

	}
	
	<a name="latest-research-list"></a>
	<section class="customtag-content view-item-list">
		<h2 class="one-subhead">Latest Research</h2>
		{foreach $dataNodes as $key => $subitem}
			{node_view_gui view=line content_node=$subitem}
			{delimiter}{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}{/delimiter}
		{/foreach}
		{include
			name=Navigator
			uri='design:navigator/google.tpl'
			page_uri=$#node.node_id|sitelink('no')
			item_count=$dataNodesCount
			view_parameters=$#view_parameters
			item_limit=$page_limit
		}
	</section>

	{pagedata_merge(hash('sidebar',false(),'sidemenu',false()))}

{/if}

</div> {* /#topics *}

{pagedata_set('extrainfo',false())}

{/debug-accumulator}
