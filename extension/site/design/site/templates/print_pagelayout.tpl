{siteaccess_redirect()}

{def $role_list=$current_user.role_id_list}
<!DOCTYPE html>
<html lang="{$site.http_equiv.Content-language|wash()}" xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$site.http_equiv.Content-language|wash()}">

{if not(is_set($extra_cache_key))}{def $extra_cache_key=''}{/if}
{def $pagedata = pagedata()
	 $basket_is_empty = cond($current_user.is_logged_in,fetch(shop,basket).is_empty,1)
	 $basket_count = count(fetch(shop, basket).items)
	 $locales = fetch('content','translation_list')
	 $pagedesign = $pagedata.template_look
	 $indexpage = $pagedata.root_node_id
	 $content_info = first_set($module_result.content_info, false())
	 $persistent_infoboxes = array()
	 $cufon_fonts=cond(ezini_hasvariable('JavaScriptSettings','CufonFontsList','design.ini'),ezini('JavaScriptSettings','CufonFontsList','design.ini'),false())
	 $has_cufon=and($cufon_fonts,count($cufon_fonts))
}

<head>

<title>{$title}</title>
<meta charset="utf-8" />
<meta name="MSSmartTagsPreventParsing" content="TRUE" />
<meta name="generator" content="eZ Publish" />
<link rel="Shortcut Icon" href={'images/favicon.png'|ezdesign()} type="image/x-icon" />
<link rel="apple-touch-icon" href={'images/favicon-iphone.png'|ezdesign()}  type="image/x-icon" />

{include uri="design:page_head.tpl" enable_link=false()}
{include uri='design:page/head_style.tpl'}
{include uri='design:page/head_script.tpl'}
{literal}
<script type="text/javascript">
$(function(){
	$('aside, #global-menubar, footer,#path, .recent-updates, #user-control-panel, #toolbar, #extrainfo, .related-content, .share, .print, #debug, #search, #event-scrollable, #user-links, #social-media').remove();
	$('header').css({'background':'none'});
	$('.content-view-full.class-article #article-utility, .content-view-full.class-news-item #article-utility, .attribute-byline').css({'background':'none','border':'none'});
	$('.content-view-full #article-utility h2').css({'color':'#000000'});
});
</script>
{/literal}
</head>

{cache-block keys=array($module_result.uri, $basket_is_empty, $current_user.contentobject_id, $access_type.name, $extra_cache_key, ezgetvars())}

<body class="{$pagedata.site_classes|implode(' ')}">

<header role="banner"{if eq(cookie('usercontrolpanel'),'hide')} style="margin-top:-30px;"{/if}>
	<div class="document">
		<a id="logo" href="/"><img title="{$logo_alt}" alt="{$logo_alt}" src={'images/logo.png'|ezdesign()} width="150" height="120" /></a>
	</div>
</header>

	<section id="site-columns">
		<div class="document">
				<section class="{$pagedata.content_classes|implode(' ')}" id="site-main-content" role="main">
					{$module_result.content}
				</section>
		</div>
	</section>
	{* This comment will be replaced with actual debug report (if debug is on). *}
	<!--DEBUG_REPORT-->
{/cache-block}
{literal}
<script type="text/javascript">
$(document).ready(function() {
	window.print();
});
</script>
{/literal}
</body>

</html>
