{def $match = cond($siteaccess[name]|contains('row'), 2, cond($siteaccess[name]|contains('eng'), 1, 0))
	 $users = fetch(content, tree, hash(parent_node_id, 285, class_filter_type, 'include', class_filter_array, array('user'), attribute_filter, array('OR', array('user/region', '=', $match), array('user/region', 'like', concat($match, "-*")), array('user/region', 'like', concat("*-", $match, "-*")), array('user/region', 'like', concat("*-", $match))) ))|shuffle
	 $recent_research = array()
}

{foreach $users as $u max 3}
<h4><aside class="content-view-embed class-image line-image align-left image_no_overlay_no_border">
{if $u.data_map.image.has_content}
{attribute_view_gui attribute=$u.data_map.image image_class='product_tiny'}
{else}
{attribute_view_gui attribute=$u.data_map.ml_image.content.data_map.image image_class='product_tiny'}
{/if}
</aside>
<a href={$u|sitelink} >{$u.data_map.first_name.content|wash()} {$u.data_map.last_name.content|wash()}</a>
</h4>
{*set $recent_research = fetch('site', 'research', $recent_research_hash, 'limit', 1)}
<p><a href="{$recent_research.0.parent.url_alias|ezroot(no)}" >{$recent_research.0.parent.name|wash}</a></p>*}
{set-block variable=bio}
{attribute_view_gui attribute=$u.data_map.bio}
{/set-block}
{def $morelink = concat("<a href='", $u|sitelink(no), "'>...more</a>")}
{$bio|html_shorten(200, $morelink)}
{/foreach}

