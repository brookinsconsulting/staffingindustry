﻿{def	$taxpercent = mul( div(sub($order.total_inc_vat, $order.total_ex_vat), $order.total_ex_vat), 100)
	$percentage = mul( div(sub($order.total_inc_vat, $order.total_ex_vat), $order.total_ex_vat), 100)|l10n('number')
	$currency = fetch( 'shop', 'currency', hash( 'code', $order.productcollection.currency_code ) )
	$locale = false()
	$symbol = false()
}

{set-block scope=root variable=subject}{ezini( 'InvoiceSettings', 'CompanyName', 'xrowecommmerce.ini'  )} {"Order"|i18n("extension/xrowecommerce")} #{$order.order_nr}{/set-block}

{include uri="design:shop/orderemail/html/pre_text.tpl"}

<hr />

<p>{'Order ID'|i18n( 'extension/xrowecommerce' )}: {$order.order_nr}</p>

<p>{'Date'|i18n( 'extension/xrowecommerce' )}: {$order.created|l10n( 'datetime' )}</p>

{*
{shop_account_view_gui view=html order=$order}
*}
{* begin shop_account_view_gui paste *}
{def $shiplist=fetch( 'shipping', 'list_all_methods' )
     $gateways=fetch( 'xrowecommerce', 'list_all_gateways' )
     $country=false()
     $fetchStoredTransaction = ezini( 'eZAuthorizeSettings', 'StoreTransactionInformation', 'ezauthorize.ini' )
}

{* eZAuthorize + eZGPG - CC Storage Additions *}

<div class="shop-account">
    <table class="order_box">
	<br />
        <b>{"Customer"|i18n("extension/xrowecommerce")}</b>
	<br />
        {if and( eq(ezini( 'Fields', 'company_name', 'xrowecommerce.ini' ).enabled, 'true' ), $order.account_information.company_name)}
        <tr>
            <td>{'Company'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.company_name|wash}</td>
        </tr>
        {/if}
        {if and( eq(ezini( 'Fields', 'company_additional', 'xrowecommerce.ini' ).enabled, 'true' ), $order.account_information.company_additional)}
        <tr>
            <td>{'Company additional information'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.company_additional|wash}</td>
        </tr>
        {/if}
        {if and( eq(ezini( 'Fields', 'tax_id', 'xrowecommerce.ini' ).enabled, 'true' ), $order.account_information.tax_id)}
        <tr>
            <td>{'VAT'|i18n('extension/xrowecommerce')}:</td>
            <td>
                {$order.account_information.tax_id|wash} {if $order.account_information.tax_id_valid|eq('0')} ({'unconfirmed'|i18n('extension/xrowecommerce')}){/if}
            </td>
        </tr>
        {/if}
        <tr>
            <td>{'Name'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.first_name|wash}
            {$order.account_information.last_name|wash}</td>
        </tr>
        <tr>
            <td>{'Email'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.email|wash}</td>
        </tr>
    </table>


    {if eq($order.account_information.shipping,1)}
    <table id="combined-address-table" border="0" cellspacing="0"
        cellpadding="0" class="order_box">
	<br />
        <b>{"Billing & shipping address"|i18n("extension/xrowecommerce")|wash}</b>
	<br />
        <tr>
            <td>{'To'|i18n('extension/xrowecommerce')}:</td>
            <td>{if $order.account_information.company_name|wash}{$order.account_information.company_name|wash}
            {$order.account_information.company_additional|wash},{/if}
            {$order.account_information.first_name|wash}
            {$order.account_information.last_name|wash}</td>
        </tr>
        <tr>
            <td>{'Address'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.address1|wash}</td>
        </tr>
        {if gt(count($order.account_information.address2),0)}
        <tr>
            <td>&nbsp;</td>
            <td>{$order.account_information.address2|wash}</td>
        </tr>
        {/if}
        <tr>
            <td>{'Zip code'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.zip|wash}</td>
        </tr>
        <tr>
            <td>{'City'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.city|wash}</td>
        </tr>
        {if $order.account_information.state}
        <tr>
            <td>{'State'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.state|wash}</td>
        </tr>
        {/if}
        <tr>
            <td>{'Country'|i18n('extension/xrowecommerce')}:</td>
            <td>{set $country=fetch( 'content', 'country_list', hash( 'filter', 'Alpha3', 'value', $order.account_information.country ) )}{$country.Name|wash}</td>
        </tr>
        <tr>
            <td>{'Phone'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.phone|wash}</td>
        </tr>
        {if eq(ezini( 'Fields', 'fax', 'xrowecommerce.ini' ).enabled, 'true' )}
        <tr>
            <td>{'Fax'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.fax|wash}</td>
        </tr>
        {/if}
        {if eq(ezini( 'BasketInformation', 'DisplayShipping', 'xrowecommerce.ini' ), 'enabled' )}
        <tr>
            <td>{'Shipping'|i18n('extension/xrowecommerce')}:</td>
            <td>{if $shiplist} {foreach $shiplist as $method} {if $method.identifier|eq($order.account_information.shippingtype)}
            {$method.name} {/if} {/foreach} {/if}</td>
        </tr>
        {/if}
    </table>
    {else}
    <table id="billing-address-table" class="order_box" border="0"
        cellspacing="0" cellpadding="0">
	<br />
        <b>{"Billing Address"|i18n("extension/xrowecommerce")}</b>
	<br />
        <tr>
            <td>{'Name'|i18n('extension/xrowecommerce')}:</td>
            <td>{if $order.account_information.company_name|wash}{$order.account_information.company_name|wash}
            {$order.account_information.company_additional|wash}{else}{$order.account_information.first_name|wash}
            {$order.account_information.last_name|wash}{/if}</td>
        </tr>
        <tr>
            <td>{'Address'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.address1|wash}</td>
        </tr>
        {if gt(count($order.account_information.address2),0)}
        <tr>
            <td>&nbsp;</td>
            <td>{$order.account_information.address2|wash}</td>
        </tr>
        {/if}
        <tr>
            <td>{'Zip code'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.zip|wash}</td>
        </tr>
        <tr>
            <td>{'City'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.city|wash}</td>
        </tr>
        {if $order.account_information.state}
        <tr>
            <td>{'State'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.state|wash}</td>
        </tr>
        {/if}
        <tr>
            <td>{'Country'|i18n('extension/xrowecommerce')}:</td>
            <td>{set $country=fetch( 'content', 'country_list', hash( 'filter', 'Alpha3', 'value', $order.account_information.country ) )}{$country.Name|wash}</td>
        </tr>
        <tr>
            <td>{'Phone'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.phone|wash}</td>
        </tr>
        {if eq(ezini( 'Fields', 'fax', 'xrowecommerce.ini' ).enabled, 'true' )}
        <tr>
            <td>{'Fax'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.fax|wash}</td>
        </tr>
        {/if}
    {if eq(ezini( 'BasketInformation', 'DisplayShipping', 'xrowecommerce.ini' ), 'enabled' )}
        <tr>
            <td>{'Shipping'|i18n('extension/xrowecommerce')}:</td>
            <td>{foreach $shiplist as $metdod}
                    {if $method.identifier|eq($order.account_information.shippingtype)}
                        {$method.name|wash}
                    {/if}
                {/foreach}
            </td>
        </tr>
    {/if}
    </table>
    <table id="shipping-address-table" valign="top" class="order_box"
        border="0" cellspacing="0" cellpadding="0">
	<br />
        <b>{"Shipping Address"|i18n("extension/xrowecommerce")}</b>
	<br />
        <tr>
            <td>{'To'|i18n('extension/xrowecommerce')}:</td>
            <td>{if $order.account_information.s_company_name|wash}{$order.account_information.s_company_name|wash}
            {$order.account_information.s_company_additional|wash},{/if}
            {$order.account_information.s_first_name|wash}
            {$order.account_information.s_mi|wash}
            {$order.account_information.s_last_name|wash}</td>
        </tr>
        <tr>
            <td>{'Address'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.s_address1|wash}</td>
        </tr>
        {if gt(count($order.account_information.s_address2),0)}
        <tr>
            <td>&nbsp;</td>
            <td>{$order.account_information.s_address2|wash}</td>
        </tr>
        {/if}
        <tr>
            <td>{'Zip code'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.s_zip|wash}</td>
        </tr>
        <tr>
            <td>{'City'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.s_city|wash}</td>
        </tr>
        {if $order.account_information.s_state}
        <tr>
            <td>{'State'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.s_state|wash}</td>
        </tr>
        {/if}
        <tr>
            <td>{'Country'|i18n('extension/xrowecommerce')}:</td>
            <td>{set $country=fetch( 'content', 'country_list', hash( 'filter', 'Alpha3', 'value', $order.account_information.s_country ) )}{$country.Name|wash}</td>
        </tr>
        <tr>
            <td>{'Phone'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.s_phone|wash}</td>
        </tr>
        {if eq(ezini( 'Fields', 'Fax', 'xrowecommerce.ini' ).enabled, 'true' )}
            {if eq(ezini( 'ShippingSettings', 'DisplayFax', 'xrowecommerce.ini' ), 'enabled' )}
            <tr>
                <td>{'Fax'|i18n('extension/xrowecommerce')}:</td>
                <td>{$order.account_information.s_fax|wash}</td>
            </tr>
            {/if}
        {/if}
        <tr>
            <td>{'Email'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.s_email|wash}</td>
        </tr>
    </table>
    {/if}
    <table id="additional-orderinformation-table" class="order_box"
        border="0" cellspacing="0" cellpadding="0">
	<br />
        <b>{"Additional Order Information"|i18n("extension/xrowecommerce")}</b>
	<br />
        {if ezini( 'Fields', 'NoPartialDelivery', 'xrowecommerce.ini' ).enabled|eq('true')}
        <tr>
            <td>{'Partial delivery'|i18n('extension/xrowecommerce')}:</td>
            <td>{if $order.account_information.no_partial_delivery}
            {'No'|i18n('extension/xrowecommerce')} {else}
            {'Yes'|i18n('extension/xrowecommerce')} {/if}</td>
        </tr>
        {/if}
        {if ezini( 'BasketInformation', 'DisplayPaymentmethod', 'xrowecommerce.ini' )|eq( 'enabled' )}

            {* Enhancement - If credit card information is needed, include the number of the step in which the credit card information should be displayed. *}

            {if array( '' )|contains( $step )}
                {if $order.account_information.paymentmethod}
                <tr>
                    <td>{'Payment method'|i18n('extension/xrowecommerce')}:</td>
                    <td>{if $gateways|gt(0)} {foreach $gateways as $gateway}
                    {if $order.account_information.paymentmethod|eq($gateway.value)}{$gateway.Name|wash}{/if}
                    {/foreach} {/if}
        {if $order.account_information.type}
        {switch match=$order.account_information.type}
        {case in=array(1,2,3,4)}
        {$order.account_information.number|wash} {$order.account_information.month|wash} / {$order.account_information.year|wash}
        {/case}
        {case match='5'}
        {$order.account_information.accountnumber|wash} {$order.account_information.bankcode|wash}
        {/case}
        {/switch}
        {/if}
                    </td>
                </tr>
                {else}
                <tr>
                    <td>{'Payment method'|i18n('extension/xrowecommerce')}:</td>
                    <td>{'Unkown'|i18n('extension/xrowecommerce')}</td>
                </tr>
                {/if}
            {/if}
        {/if}
        {if and(ezini( 'Fields', 'Reference', 'xrowecommerce.ini' ).enabled|eq('true'), $order.account_information.reference)}
        <tr style="display: none;">
            <td>{'Reference'|i18n('extension/xrowecommerce')}:</td>
            <td>{$order.account_information.reference|wash}</td>
        </tr>
        {/if}
        {if and(ezini( 'Fields', 'Message', 'xrowecommerce.ini' ).enabled|eq('true'), $order.account_information.message)}
        <tr>
            <td>Please enter names and email of conference attendees here, if applicable:</td>
            <td>{$order.account_information.message|wash()|nl2br()}</td>
        </tr>
        {/if}
    </table>

    {if and( eq( $fetchStoredTransaction, true), ne( $order.account_information.ezauthorize_card_date, '') )} {def $key = ezini( 'eZGPGSettings', 'KeyID', 'ezgpg.ini' )}

    <table id="paymentinformation-table" class="order_box" cellspacing="0">
	<br />
        <b>{'Payment information'|i18n( 'extension/xrowecommerce' )}</b>
	<br />
        <tr>
            <td>{'Card Holder\'s Name'|i18n( 'extension/xrowecommerce' )}</td>
            <td>{ezgpg_decrypt($order.account_information.ezauthorize_card_name, $key)|wash}</td>
        </tr>
        <tr>
            <td>{'Last 4 Digits of Card Number'|i18n( 'extension/xrowecommerce' )}</td>
            <td>{ezgpg_decrypt_limit($order.account_information.ezauthorize_card_number, $key)|wash}</td>
        </tr>
        <tr>
            <td>{'Card Expiration Date'|i18n( 'extension/xrowecommerce' )}</td>
            <td>{$order.account_information.ezauthorize_card_date|wash}</td>
        </tr>
    </table>
{/if}
</div>
{* end shop_account_view_gui paste *}

<br />
<b>{"Product items"|i18n("extension/xrowecommerce")}</b>
<hr />

{if $currency}
    {set locale = $currency.locale
         symbol = $currency.symbol}
{/if}
{foreach $order.product_items as $product_item}
	{include uri="design:shop/orderemail/html/product_cell_view.tpl"}
{/foreach}

<hr />

{foreach $order.order_items as $OrderItem}
	<hr />
	<p>{$OrderItem.description}:  {$OrderItem.price_inc_vat|l10n( 'currency', $locale, $symbol )}</p>
 
{/foreach}

{if $taxpercent|eq(0)|not}

	<hr />
	<p>{'Tax'|i18n('extension/xrowecommerce')}: {sub($order.total_inc_vat, $order.total_ex_vat)|l10n( 'currency', $locale, $symbol )}</p>
	 
{/if}
<hr />
<p>{'Total'|i18n( 'extension/xrowecommerce' )}: {$order.total_inc_vat|l10n( 'currency', $locale, $symbol )}</p>
<hr /> 
{include uri="design:shop/orderemail/html/post_text.tpl"}

{undef $currency $locale $symbol}
