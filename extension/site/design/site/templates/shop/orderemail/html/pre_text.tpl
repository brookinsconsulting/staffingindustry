﻿<html>
<head>
<title>{'Order confirmation email from'|i18n( 'extension/xrowecommerce' )} {ezini( 'InvoiceSettings', 'CompanyName', 'xrowecommmerce.ini'  )}</title>
<style type="text/css">
{literal}
table tr td, table tr th
{
    text-align: left;
}
{/literal}
</style>
</head>
<body>
<p>Dear {$order.account_information.first_name|wash},</p>
<p>Thank you for your order.</p>
<p>Please review the details of your order confirmation below and print a copy for your records. <b>Do not reply to this email.</b></p>
{foreach array($order.product_items) as $item}
	{def $item_node = $item[0]['item_object'].contentobject}
	{def $file_attribute = $item_node.file}

	{foreach array($item_node.data_map.file, $item_node.data_map.file1) as $file_attribute}
		{if and($file_attribute.has_content, $file_attribute.content.can_read)}
			<p>You may download your file by clicking on the following link below:<br />
			{def
				$download_link = concat(
					'/content/download/',
					$file_attribute.content.data_map.file.contentobject_id,
					'/',
					$file_attribute.content.data_map.file.id,'/version/',
					$file_attribute.content.data_map.file.version,
					'/file/',
					$file_attribute.content.data_map.file.content.original_filename
				)
			}
			<a href={$download_link|sitelink(true(), true())}>Download {$file_attribute.content.name}</a>
			<br />
			</p>
			{undef $download_link}
		{/if}
	{/foreach}
	{undef $item_node $file_attribute}
{/foreach}
<p>If you have any questions, please call or email Member Services. You may reach us at <a href="mailto:memberservices@staffingindustry.com">memberservices@staffingindustry.com</a>, or call 800-950-9496 or 650-390-6200.</p>
<p>We appreciate your business and look forward to serving you again in the near future.</p>
