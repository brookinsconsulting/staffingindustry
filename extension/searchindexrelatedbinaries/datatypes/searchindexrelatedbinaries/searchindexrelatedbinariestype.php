<?php
//
// Definition of ezvimeovideoType class
//
// SOFTWARE NAME: eZ Vimeo
// SOFTWARE RELEASE: 1.0-0
// COPYRIGHT NOTICE: Copyright (C) 2011 ThinkCreative
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.
//
//

class searchIndexRelatedBinariesType extends eZDataType
{
	const DATA_TYPE_STRING = 'searchindexrelatedbinaries';

    /*!
     Construction of the class, note that the second parameter in eZDataType 
     is the actual name showed in the datatype dropdown list.
    */
	function __construct()
	{
	    parent::__construct( self::DATA_TYPE_STRING, 'Index Related Binaries' );
	}

    /*!
      Validates the input and returns true if the input was
      valid for this datatype.
    */
    function validateObjectAttributeHTTPInput( $http, $base, $objectAttribute )
    {
        return eZInputValidator::STATE_ACCEPTED;
    }


    /*!
    */
    function fetchObjectAttributeHTTPInput( $http, $base, $contentObjectAttribute )
    {
        if ( $http->hasPostVariable( $base . "_data_boolean_" . $contentObjectAttribute->attribute( "id" ) ))
        {
            $data = $http->postVariable( $base . "_data_boolean_" . $contentObjectAttribute->attribute( "id" ) );
            if ( isset( $data ) && $data !== '0' && $data !== 'false' )
                $data = 1;
            else
                $data = 0;
        }
        else
        {
            $data = 0;
        }
        $contentObjectAttribute->setAttribute( "data_int", $data );
        return true;
    }

    /*!
     Store the content. Since the content has been stored in function 
     fetchObjectAttributeHTTPInput(), this function is with empty code.
    */
    function storeObjectAttribute( $objectattribute )
    {
    }

    /*!
     Returns the meta data used for storing search indices.
    */
    function metaData( $contentObjectAttribute )
    {
        $clean = "";
        if ($contentObjectAttribute->attribute( "data_int" ) == 1) {
            $parent_ob_id = $contentObjectAttribute->attribute('contentobject_id');

            $parent_ob = eZContentObject::fetch($parent_ob_id);
            $related_objects = $parent_ob->relatedContentObjectArray();

            $metaData = "";

            foreach ($related_objects as $relation) {
                foreach ($relation->contentObjectAttributes() as $contentObjectAttribute) {

                    $binaryFile = $contentObjectAttribute->content();


                    if ( $binaryFile instanceof eZBinaryFile )
                    {
                        $metaData .= $binaryFile->metaData();
                    }

                }
            }
            $clean = mb_convert_encoding($metaData, 'UTF-8', 'UTF-8');
        }
        return $clean;
    }

    /*!
     Returns the text.
    */
    function title( $objectAttribute, $name = null)
    {
        return $this->metaData( $objectAttribute );
    }

    function isIndexable()
    {
        return true;
    }

    function sortKey( $objectAttribute )
    {
        return $this->metaData( $objectAttribute );
    }
  
    function sortKeyType()
    {
        return 'int';
    }

    function hasObjectAttributeContent( $contentObjectAttribute )
    {
        return trim( $contentObjectAttribute->attribute( 'data_int' ) ) != '';
    }

    /*!
     Returns the content.
    */
    function objectAttributeContent( $contentObjectAttribute )
    {
        return $contentObjectAttribute->attribute( "data_int" );
    }

    function objectDisplayInformation( $objectAttribute, $mergeInfo = false )
    {
        $info = array( 'edit' => array( 'grouped_input' => true ),
                       'collection' => array( 'grouped_input' => true ) );
        return eZDataType::objectDisplayInformation( $objectAttribute, $info );
    }


}

eZDataType::register( searchIndexRelatedBinariesType::DATA_TYPE_STRING, 'searchIndexRelatedBinariesType' );
