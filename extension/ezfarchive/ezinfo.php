<?php
/**
 * File containing ezfarchive info
 *
 * @copyright Copyright (C) 1999-2010 eZ Systems AS. All rights reserved.
 * @license http://ez.no/software/proprietary_license_options/ez_proprietary_use_license_v1_0
 * @version 1.0.0
 * @package ez_network
 */

class ezfarchiveInfo
{
    static function info()
    {
        return array( 'Name' => 'eZ Archive extension',
                      'Version' => '1.0.0',
                      'Copyright' => 'Copyright (C) 1999-2010 eZ Systems AS',
                      'License'   => 'eZ Proprietary Use License v1.0',
                     );
    }
}

?>
