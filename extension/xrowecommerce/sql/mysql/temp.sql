INSERT INTO `ezworkflow_group_link` (`group_id`, `group_name`, `workflow_id`, `workflow_version`) VALUES
(2, 'Shop', 1, 0),
(2, 'Shop', 2, 0),
(2, 'Shop', 3, 0);

INSERT INTO `ezworkflow_event` (`data_int1`, `data_int2`, `data_int3`, `data_int4`, `data_text1`, `data_text2`, `data_text3`, `data_text4`, `data_text5`, `description`, `id`, `placement`, `version`, `workflow_id`, `workflow_type_string`) VALUES
(0, 0, 0, 0, '', '', 'a:1:{i:0;s:10:"fixedprice";}', '', '', '', 1, 1, 0, 1, 'event_ezshippinginterface'),
(0, 0, 0, 0, '', '', '-1', '1', '', '', 2, 1, 0, 2, 'event_xrowpaymentgateway');


INSERT INTO `ezworkflow` (`created`, `creator_id`, `id`, `is_enabled`, `modified`, `modifier_id`, `name`, `version`, `workflow_type_string`) VALUES
(1277261442, 14, 1, 1, 1277261442, 14, 'Before Confirmorder', 0, 'group_ezserial'),
(1277261442, 14, 2, 1, 1277261442, 14, 'Before Checkout', 0, 'group_ezserial'),
(1277261442, 14, 3, 1, 1277261442, 14, 'Aftersale', 0, 'group_ezserial');

INSERT INTO `eztrigger` (`connect_type`, `function_name`, `id`, `module_name`, `name`, `workflow_id`) VALUES
('b', 'confirmorder', 1, 'shop', 'pre_confirmorder', 1),
('b', 'checkout', 2, 'shop', 'pre_checkout', 2),
('a', 'checkout', 3, 'shop', 'post_checkout', 3);
