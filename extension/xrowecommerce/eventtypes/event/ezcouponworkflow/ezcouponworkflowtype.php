<?php

class eZCouponWorkflowType extends eZWorkflowEventType
{
	const WORKFLOW_TYPE_STRING = 'ezcouponworkflow';
	const STATE_CANCEL = 2;
	const STATE_INVALID_CODE = 3;
	const STATE_VALID_CODE = 1;
	const BASE = 'event_ezcoupon';

    /*!
     Constructor
    */
    function eZCouponWorkflowType()
    {
        $this->eZWorkflowEventType( eZCouponWorkflowType::WORKFLOW_TYPE_STRING, ezpI18n::tr( 'kernel/workflow/event', "Coupon" ) );
        $this->setTriggerTypes( array( 'shop' => array( 'confirmorder' => array ( 'before' ) ) ) );
    }

    function execute( $process, $event )
    {

	    $parameters = $process->attribute( 'parameter_list' );
        $orderID = $parameters['order_id'];

        $order = eZOrder::fetch( $orderID );
        $orderItems = $order->attribute( 'order_items' );

		//$shippingInfo = eZShippingManager::getShippingInfo( $order->productCollection()->ID );
		
		//eZDebug::writeDebug($shippingInfo, 'shippingInfo');
		
		$play_with = $order->totalIncVAT(); // + $shippingInfo['cost_inc'];

		$order_av = $order->accountInformation();
		
		$pre_http = eZHTTPTool::instance();
		
		if ($order_av['coupon_code']) {
		
			$var = eZCouponWorkflowType::BASE . "_code_" . $event->attribute( "id" );
			$select = eZCouponWorkflowType::BASE . "_SelectButton_" . $event->attribute( "id" );
			if (!$pre_http->hasPostVariable( $select )) {
			$_POST[$var] = $order_av['coupon_code'];
			$_POST[$select] =1;
			}
		} else {
			return eZWorkflowEventType::STATUS_ACCEPTED;
		}

        $http = eZHTTPTool::instance();
        $this->fetchInput( $http, eZCouponWorkflowType::BASE, $event, $process );
        if( $process->attribute( 'event_state' ) == eZCouponWorkflowType::STATE_CANCEL )
        {
            return eZWorkflowEventType::STATUS_ACCEPTED;
        }
        if( $process->attribute( 'event_state' ) != eZCouponWorkflowType::STATE_VALID_CODE )
        {
            $process->Template = array();
            $process->Template['templateName'] = 'design:workflow/coupon.tpl';
            $process->Template['templateVars'] = array ( 'process' => $process, 'event' => $event, 'base'=> eZCouponWorkflowType::BASE );

            return eZWorkflowType::STATUS_FETCH_TEMPLATE_REPEAT;

        }
        $ini = eZINI::instance( 'xrowcoupon.ini' );


        $coupon = new xrowCoupon( $event->attribute( "data_text1" ) );

        $attributes = $coupon->fetchAttributes();

		foreach ($attributes as $atkey => $attribute) {
			
			if ($play_with <= 0) continue;

			$dm = $attribute->object()->dataMap();



			$prod_nodes = array();
			if ($dm['product']->hasContent()) {
				foreach ($dm['product']->content()->assignedNodes(false) as $na) {
					$prod_nodes[] = $na['node_id'];
				}
			}

			$totalVAT = ($order->totalIncVAT() - $order->totalExVAT());
			$totalExVAT = $order->totalExVAT();
			if ($totalVAT > 0) {
		    	$vat_rate = $totalVAT / $totalExVAT * 100;
			} else {
				$vat_rate = 0;
			}


	        $data = $attribute->content();

	        $description = $ini->variable( "CouponSettings", "Description" ) . " " . $event->attribute( "data_text1" );
	
			$db = eZDB::instance();
			$q = "select count(ezorder_item.description) as cc from ezorder_item, ezorder_status_history where ezorder_item.order_id = ezorder_status_history.order_id and  ezorder_item.description='$description' and ezorder_status_history.status_id in (2,3)";

			$reults = $db->arrayQuery($q);
			$max_used = $reults[0]['cc'];
			$max_usage = $dm['max_usage']->content();
			$still_good = true;
			if ($max_usage > 0 && ($max_usage-$max_used < 0)) $still_good = false;
	

	        $addShipping = true;
	        foreach ( array_keys( $orderItems ) as $key )
	        {

	            $orderItem =& $orderItems[$key];

	            if ( $orderItem->attribute( 'type' ) == "shippingcost" )
	            {
	                $shippingvalue = $orderItem->attribute('price');
	            }
	        }

	        if ( $addShipping )
	        {

	            $price = 0;
				$p_store = false;

	            if ( $data['discount_type'] == ezCouponType::DISCOUNT_TYPE_FLAT )
	            {
	                $price = $data['discount'] * -1;
					$p_store = $price;
					if ($play_with + $price < 0) $price = 0 - $play_with;
	            }
	            elseif ( $data['discount_type'] == ezCouponType::DISCOUNT_TYPE_FREE_SHIPPING )
	            {
	                $price = $shippingvalue * -1;
					$p_store = $price;
					if ($play_with + $price < 0) $price = 0 - $play_with;
	            }
	            elseif ( $data['discount_type'] == ezCouponType::DISCOUNT_TYPE_PERCENT_PRODUCT )
	            {

					foreach ($order->productItems() as $pi) {
						if (in_array("".$pi['node_id'], $prod_nodes)) {
							$free_id = $pi['item_object']->contentObject()->ID;
							$use_q = $pi['item_count'];
							//if (array_key_exists('ob'.$free_id,$shippingInfo['free'])) {
							//	$use_q = $use_q - $shippingInfo['free']['ob'.$free_id];
							//}
							$price = $pi['price_ex_vat'] * $data['discount'] / 100 * -1 * $use_q;
							$p_store = $price;
							$vat_rate = $pi['vat_value'];
							if ($play_with + $price < 0) $price = 0 - $play_with;
							break;
						}
					}
	            }
	            elseif ( $data['discount_type'] == ezCouponType::DISCOUNT_TYPE_FLAT_PRODUCT )
	            {
					foreach ($order->productItems() as $pi) {
						if (in_array("".$pi['node_id'], $prod_nodes)) {
							$free_id = $pi['item_object']->contentObject()->ID;
							$use_q = $pi['item_count'];
							//if (array_key_exists('ob'.$free_id,$shippingInfo['free'])) {
							//	$use_q = $use_q - $shippingInfo['free']['ob'.$free_id];
							//}
							$price = $data['discount'] * -1 * $use_q;
							$p_store = $price;
							$vat_rate = $pi['vat_value'];
							if ($play_with + $price < 0) $price = 0 - $play_with;
							break;
						}
					}
	            }
	            else
	            {
	                //$price = ($order->attribute( 'product_total_ex_vat' ) + $shippingInfo['cost']) * $data['discount'] / 100 * -1;
					$price = $order->attribute( 'product_total_ex_vat' ) * $data['discount'] / 100 * -1;
					$p_store = $price;
					if ($play_with + $price < 0) $price = 0 - $play_with;
	            }
	
	            // Remove any existing order coupon before appending a new item
				if ($atkey ==0) {
					$list = eZOrderItem::fetchListByType( $orderID, 'coupon' );
					if( count( $list ) > 0 )
					{
						foreach ( $list as $item ) {
							if (strpos($item->attribute('description'), 'Automatic discounts:') != 0) {
								$item->remove();
							}
						}
					}
				}
				
				if ($still_good && $price != 0 || ($p_store === 0 && $still_good)) {
		            $orderItem = new eZOrderItem( array( 'order_id' => $orderID,
		                                                 'description' => $description,
		                                                 'price' => $price,
		                                                 'type' => 'coupon',
		                                                 'is_vat_inc' => false,
		                                                 'vat_value' => $vat_rate )
		                                          );
		            $orderItem->store();
				}
	        }

		}

        return eZWorkflowEventType::STATUS_ACCEPTED;
    }


    function fetchInput( &$http, $base, &$event, &$process )
    {
	
        $var = $base . "_code_" . $event->attribute( "id" );
        $cancel = $base . "_CancelButton_" . $event->attribute( "id" );
        $select = $base . "_SelectButton_" . $event->attribute( "id" );

        if ( $http->hasPostVariable( $cancel ) and $http->postVariable( $cancel ) )
        {
            $process->setAttribute( 'event_state', eZCouponWorkflowType::STATE_CANCEL );
            return;
        }
        if ( $http->hasPostVariable( $var ) and $http->hasPostVariable( $select ) and count( $http->postVariable( $var ) ) > 0 )
        {
            $coupon = new xrowCoupon( $http->postVariable( $var ) );
            $event->setAttribute( "data_text1", $coupon->code );
            if ( $coupon->isValid() )
            {
                $process->setAttribute( 'event_state', eZCouponWorkflowType::STATE_VALID_CODE );
                return;
            }
            else
            {
                $process->setAttribute( 'event_state', eZCouponWorkflowType::STATE_INVALID_CODE );
                return;
            }
        }
        $parameters = $process->attribute( 'parameter_list' );
        $orderID = $parameters['order_id'];
        $order = eZOrder::fetch( $orderID );
        if ( is_object( $order ) )
        {
        	$doc = new DOMDocument( '1.0', 'utf-8' );
            $root = $doc->createElement( "data_text_1" );
            $doc->appendChild( $root );
        	
           // $xml = new eZXML();
           //$xmlDoc = $order->attribute( 'data_text_1' );
            

            if( $xmlDoc != null )
            {
                //$dom = $xml->domTree( $xmlDoc );
                //$id = $dom->elementsByName( "coupon_code" );
                
                $coupon_codeNode = $doc->createElement( "coupon_code", $id );
                $root->appendChild( $coupon_codeNode );
                
                if ( is_object( $id[0] ) )
                {
                    $code = $id[0]->textContent();
                    // we have an empty code this mean a coupon has been supplied at the user register page, so we cancle here
                    if ( !$code )
                    {
                        $process->setAttribute( 'event_state', eZCouponWorkflowType::STATE_CANCEL );
                        return;
                    }
                    $coupon = new xrowCoupon( $id[0]->textContent() );
                    if ( $coupon->isValid() )
                    {
                        $process->setAttribute( 'event_state', eZCouponWorkflowType::STATE_VALID_CODE );
                        $event->setAttribute( "data_text1", $coupon->code );
                        return;
                    }
                    else
                    {
                        $process->setAttribute( 'event_state', eZCouponWorkflowType::STATE_INVALID_CODE );
                        return;
                    }
                }
            }
        }
    }
}

eZWorkflowEventType::registerEventType( eZCouponWorkflowType::WORKFLOW_TYPE_STRING, "ezcouponworkflowtype" );

?>
