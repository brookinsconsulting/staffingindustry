<?php
//
// Definition of Order history class
//

/*! \file history.php
*/

$OrderID = $Params['OrderID'];

$module =& $Params['Module'];

$order = eZOrder::fetch( $OrderID );

$tpl = eZTemplate::factory();
$tpl->setVariable( "order", $order );

$content = $tpl->fetch( "design:shop/order_details.tpl" );

echo $content;

eZExecution::cleanExit();

?>