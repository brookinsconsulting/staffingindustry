[InputAttributes]
BillingAttributes[]
BillingAttributes[]=cctype
BillingAttributes[]=ccnumber
BillingAttributes[]=month
BillingAttributes[]=year
BillingAttributes[]=cvNumber
CustomerAttributes[]
CustomerAttributes[]=addressline1
CustomerAttributes[]=addressline2
CustomerAttributes[]=addressline3
CustomerAttributes[]=city
CustomerAttributes[]=county
CustomerAttributes[]=state
CustomerAttributes[]=postalcode
CustomerAttributes[]=country
CustomerAttributes[]=contactfirstname
CustomerAttributes[]=contactlastname
#CustomerAttributes[]=contacttitle
CustomerAttributes[]=emailaddress
CustomerAttributes[]=phonenumber
CustomerAttributes[]=phoneextension
CustomerAttributes[]=faxnumber

[cctype]
identifier=CreditCardType
name=Payment Method
type=select
required=yes
data[]
data[001]=Visa
data[002]=MasterCard
data[003]=American Express
#data[004]=Discover
#data[005]=Diners Club
#data[006]=Carte Blanche
#data[007]=JCB

[ccnumber]
identifier=ccnumber
name=Credit Card Number
type=cc_number
required=yes

[year]
identifier=CCExpireYear
name=Expiration Year
type=year
required=yes

[month]
identifier=CCExpireMonth
name=Expiration Month
type=month
required=yes

[cvNumber]
identifier=cvNumber
name=Card Security Code
type=integer
required=yes

[addressline1]
identifier=AddressLine1
name=Address Line 1
type=text
required=yes

[addressline2]
identifier=AddressLine2
name=Address Line 2
type=text
required=no

[addressline3]
identifier=AddressLine3
name=Address Line 3
type=text
required=no

[city]
identifier=CityName
name=City
type=text
required=yes

[county]
identifier=CountyName
name=County
type=text
required=no

[state]
identifier=StateCode
name=State
type=usastate
required=no

[postalcode]
identifier=PostalCode
name=Postal Code
type=integer
required=yes

[country]
identifier=CountryCode
name=Country
type=country_code
required=yes

[contactfirstname]
identifier=FirstName
name=First Name
type=text
required=yes

[contactlastname]
identifier=LastName
name=Last Name
type=text
required=yes

[contacttitle]
identifier=ContactTitle
name=Contact Title
type=text
required=yes

[emailaddress]
identifier=EmailAddress
name=Email Address
type=email
required=yes

[phonenumber]
identifier=PhoneNumber
name=Phone Number
type=text
required=yes

[phoneextension]
identifier=PhoneExtension
name=Phone Extension
type=text
required=no

[faxnumber]
identifier=FaxNumber
name=Fax Number
type=text
required=no

