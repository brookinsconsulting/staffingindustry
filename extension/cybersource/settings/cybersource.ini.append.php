﻿<?php /*

[StaticSettings]
merchantID=staffing2011
transactionKey=mOGjX7pQJL6tLjk6N91KR4+FnAn+qTCs9L9+3FRK44zZw0K91QcAHXAOYxchaaLwL3MU4TPd61lZ6G/sew443EBufQSIfldBS9zKoQtzOlbxvvtFS+1ltEsZoR4+zyhFnBJfLkWrIqnYHOATtxoesOm9EyyBoTCdrQoCD82I4jR2Xb2Qv7N2ihOIXkQYm9rRVf7gDzLFThmpP0VFpANDDukjvitgxdntQU0VVCT8OIMaJ/jaJiet0failGqo3xTsOfshpozyAyMHeoX6Y0LqKhHTEsrp1VVS4g4p+2TKigq0nEWJx5Gu59arUya9YFJ10rPK0RVjHVOtksppk1ueww==
#test mode
#wsdlUrl=https://ics2wstest.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.57.wsdl
#live mode
wsdlUrl=https://ics2ws.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.57.wsdl

[ReasonCodes]
reasonCode[]
reasonCode[100]=Successful transaction
reasonCode[101]=The request is missing one or more required fields. Possible action: See the reply missing fields and resend the request with the complete information.
reasonCode[102]=One or more fields in the request contains invalid data. Possible action: See the reply invalid fields and resend the request with the correct information.
reasonCode[110]=Only a partial amount was approved.
reasonCode[150]=Error: General system failure. 
reasonCode[151]=Error: The request was received but there was a server timeout. 
reasonCode[152]=Error: The request was received, but a service did not finish running in time. 
reasonCode[200]=The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the AVS check.
reasonCode[201]=The issuing bank has questions about the request.
reasonCode[202]=Expired card. Possible action: Request a different card or other form of payment.
reasonCode[203]=General decline of the card. No other information provided by the issuing bank. Possible action: Request a different card or other form of payment.
reasonCode[204]=Insufficient funds in the account. Possible action: Request a different card or other form of payment.
reasonCode[205]=Stolen or lost card. Possible action: Refer the transaction to your customer support center for manual review.
reasonCode[207]=Issuing bank unavailable. Possible action: Wait a few minutes and resend the request.
reasonCode[208]=Inactive card or card not authorized for card-not-present transactions. Possible action: Request a different card or other form of payment.
reasonCode[209]=American Express Card Identification Digits (CID) did not match. Possible action: Request a different card or other form of payment.
reasonCode[210]=The card has reached the credit limit. Possible action: Request a different card or other form of payment.
reasonCode[211]=Invalid card verification number. Possible action: Request a different card or other form of payment.
reasonCode[220]=The processor declined the request based on a general issue with the customer's account. 
reasonCode[221]=The customer matched an entry on the processor's negative file. 
reasonCode[222]=The customer's bank account is frozen. 
reasonCode[230]=The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the CVN check.
reasonCode[231]=Invalid account number. Possible action: Request a different card or other form of payment.
reasonCode[232]=The card type is not accepted by the payment processor. 
reasonCode[233]=General decline by the processor. Possible action: Request a different card or other form of payment.
reasonCode[234]=There is a problem with your CyberSource merchant configuration. 
reasonCode[236]=Processor failure. Possible action: Wait a few minutes and resend the request.
reasonCode[240]=The card type sent is invalid or does not correlate with the card number. Possible action: Confirm that the card type correlates with the card number specified in the request, then resend the request. 
reasonCode[250]=Error: The request was received, but there was a timeout at the payment processor.
reasonCode[undef]=Your transaction couldn't be completed, please contact us.

[CreditCardCodes]
creditCardCode[]
creditCardCode[001]=Visa
creditCardCode[002]=MasterCard
creditCardCode[003]=American Express
creditCardCode[004]=Discover
creditCardCode[005]=Diners Club
creditCardCode[006]=Carte Blanche
creditCardCode[007]=JCB

*/ ?>
