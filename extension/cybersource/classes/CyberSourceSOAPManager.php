<?php

/**
 * This file contains the definition for CyberSourceSOAPManager class
 *  
 */

/**
 * 
 *  @author Jairo Riascos Mu�oz
 *  @desc Encapsulates operations over Cybersource Services
 */

class CyberSourceSOAPManager extends SoapClient{
	
	var $MERCHANT_ID;
	var $TRANSACTION_KEY;
	var $WSDL_URL;
		
	function __construct($options = null) {
		$ini = eZINI::instance('cybersource.ini');
		$this-> MERCHANT_ID  = $ini->variable('StaticSettings','merchantID');		
		$this-> TRANSACTION_KEY  = $ini->variable('StaticSettings','transactionKey');
		$this-> WSDL_URL  = $ini->variable('StaticSettings','wsdlUrl');
		$wsdl = $this->WSDL_URL;
		parent::__construct($wsdl);
   }
   
   function __doRequest($request, $location, $action, $version) 
	{
		$user = $this->MERCHANT_ID;
		$password = $this->TRANSACTION_KEY;
		
		$soapHeader = "<SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"><wsse:Security SOAP-ENV:mustUnderstand=\"1\"><wsse:UsernameToken><wsse:Username>$user</wsse:Username><wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">$password</wsse:Password></wsse:UsernameToken></wsse:Security></SOAP-ENV:Header>";
		
		$requestDOM = new DOMDocument('1.0');
		$soapHeaderDOM = new DOMDocument('1.0');
		
		try {
			$requestDOM->loadXML($request);
			$soapHeaderDOM->loadXML($soapHeader);
			$node = $requestDOM->importNode($soapHeaderDOM->firstChild, true);
			$requestDOM->firstChild->insertBefore($node, $requestDOM->firstChild->firstChild);			
			$request = $requestDOM->saveXML();
		} 
		catch (DOMException $e) {
			die( 'Error adding UsernameToken: ' . $e->code);
		}
		return parent::__doRequest($request, $location, $action, $version);
	}
	
	public function execute($request)
	{
		try
		{
			$reply = $this->runTransaction($request);
			return $reply;			
		} 
		catch (SoapFault $exception) 
		{
			var_dump(get_class($exception));
			var_dump($exception);
		}		
	}
	
	public function buildMerchantRefId($eventId)
	{
		$date = new eZDate();
		$MerchantRefId = 'SPCS-'.$date->year().$date->month().$date->day(). '-' .$eventId;
		return $MerchantRefId;
	}	
	
	static function getEventIdFromMerchantId($merchantRefId)
	{
		$idParts = explode('-', $merchantRefId);
		return $idParts[2];		
	}	

}   

?>