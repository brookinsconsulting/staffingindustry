<?php
class SimpleOrderMananger{

	var $config;
	
	function __construct()
	{
		$this->config = cybs_load_config( 'extension/cybersource/settings/cybs.ini' );		
	}
		
	public function handleRequest($params)
	{
		printf( "CREDIT CARD AUTHORIZATION REQUEST: \n%s\n",
			$this->serializeArray( $params ) );
			return $params;
	}
	public function execute($params){
	
		//send request serialized
		$reply = array();
		$status = cybs_run_transaction( $this->config, $params, $reply );		
		
		if ($status==0)
		{
			printf( "CREDIT CARD AUTHORIZATION REPLY: \n%s\n",
			$this->serializeArray( $reply ) );
			
			$decision = $reply['decision'];
			if (strtoupper( $decision ) == 'ACCEPT')
			{
				// return the requestID
				$captureResponse=$this->runCapture( $this->config, $reply['requestID'] );
				if ($captureResponse['responseCode']==1){
					//TODO: run hooks
					return true;
				}
				else{
					return false;
				}
			}
		}	
		else
		{
			$this->handleError( $status, $request, $reply );
			return( '' );
		}		
		
		return array('status'=> $status,
						'reply'=>$reply);
		
	
	}
	public function handleResponse($params, $hooks, $referenceParams)
	{
		
	
	}	

	private function serializeArray( $arr )
	{
		$content = '';
		while (list( $key, $val ) = each( $arr ))
		{
			$content = $content . $key . ' => ' . $val . "\n";
		}
		return( $content );
	}

	
	private function runCapture( $config, $authRequestID )
	{
		$request = array();
		
		$request['ccCaptureService_run'] = 'true';

		// we will let the CyberSource PHP extension get the merchantID from the
		// $config array and insert it into $request.

		// so that you can efficiently track the order in the CyberSource
		// reports and transaction search screens, you should use the same
		// merchantReferenceCode for the auth and subsequent captures and
		// credits.
		$request['merchantReferenceCode'] = 'MRC-14344';

		// reference the requestID returned by the previous auth.
		$request['ccCaptureService_authRequestID'] = $authRequestID;
		
		// this sample assumes only the first item has been shipped.
		$request['purchaseTotals_currency'] = 'USD';
		$request['item_0_unitPrice'] = '12.34';

		// add more fields here per your business needs
		
		printf( "FOLLOW-ON CAPTURE REQUEST: \n%s\n",
			$this->serializeArray( $request ) );

		// send request now
		$reply = array();
		$status = cybs_run_transaction( $config, $request, $reply );
					
		if ($status == CYBS_S_OK)
		{
			printf( "FOLLOW-ON CAPTURE REPLY: \n%s\n",
				$this->serializeArray( $reply ) );
				return array('responseCode'=>1,
								'reply'=>$reply);
		}			         		
		else
		{
			$this->handleError( $status, $request, $reply );
			return array('responseCode'=>0,
								'reply'=>null);
		}			         		
	}
	
	
	private function handleError( $status, $request, $reply )
	{
		echo "RunTransaction Status: $status\n";
		

		switch ($status)
		{
			case CYBS_S_PHP_PARAM_ERROR:
				printf( "Please check the parameters passed to cybs_run_transaction for correctness.\n" );
				break;
			
			case CYBS_S_PRE_SEND_ERROR:
				printf(	"The following error occurred before the request could be sent:\n%s\n",
						 $reply[CYBS_SK_ERROR_INFO] );
				break;
			
			case CYBS_S_SEND_ERROR:
				printf( "The following error occurred while sending the request:\n%s\n",
						 $reply[CYBS_SK_ERROR_INFO] );
				break;

			case CYBS_S_RECEIVE_ERROR:
				printf( "The following error occurred while waiting for or retrieving the reply:\n%s\n",
						 $reply[CYBS_SK_ERROR_INFO] );
				handleCriticalError( $status, $request, $reply );
				break;

			case CYBS_S_POST_RECEIVE_ERROR:
				printf(	"The following error occurred after receiving and during processing of the reply:\n%s\n",
						 $reply[CYBS_SK_ERROR_INFO] );
				handleCriticalError( $status, $request, $reply );
				break;		

			case CYBS_S_CRITICAL_SERVER_FAULT:
				printf( "The server returned a CriticalServerError fault:\n%s\n",
						getFaultContent( $reply ) );
				handleCriticalError( $status, $request, $reply );
				break;
			
			case CYBS_S_SERVER_FAULT:
				printf( "The server returned a ServerError fault:\n%s\n",
						getFaultContent( $reply ) );
				break;

			case CYBS_S_OTHER_FAULT:
				printf( "The server returned a fault:\n%s\n",
						getFaultContent( $reply ) );
				break;
	 
			case CYBS_S_HTTP_ERROR:
				printf(	"An HTTP error occurred:\n%s\nResponse Body:\n%s\n",
						 $reply[CYBS_SK_ERROR_INFO], $reply[CYBS_SK_RAW_REPLY] );
				break;
		}
	}
}
	
?>