<?php
	
/**
 * This file contains the definition for SimpleOrderSOAPManager class
 *  
 */

/**
 * 
 *  @author Jairo Riascos Mu�oz
 *  @desc Encapsulates operations over Cybersource payment tokenization 
 */

class SimpleOrderSOAPManager extends CyberSourceSOAPManager{
	
	public function handleRequest($params, $process=null)
	{
		/* Retrieving customer data */
		
		$processParams = $process->attribute( 'parameter_list' );
		$orderID = $processParams['order_id'];
		$buildMerchantRefId = $this->buildMerchantRefId($orderID);
		$request = new stdClass();		
		$request->merchantID = $this->MERCHANT_ID;		
		$request->merchantReferenceCode = $buildMerchantRefId;
		$request->clientLibrary = "PHP";
		$request->clientLibraryVersion = phpversion();
		$request->clientEnvironment = php_uname();
		
		$ccAuthService = new stdClass();
		$ccAuthService->run = "true";
		$request->ccAuthService = $ccAuthService;
		
		$ccCaptureService = new stdClass();
		$ccCaptureService->run = "true";
		$request->ccCaptureService = $ccCaptureService;
		
		$purchaseTotals = new stdClass();
		$purchaseTotals->currency = $params['currency_code'];

		$purchaseTotals->grandTotalAmount = $params['total'];

		$request->purchaseTotals = $purchaseTotals;
		
		
		
		

		$itemObjectData = array();
		foreach($params['items'] as $item)
		{
			$itemObj = new stdClass();			
			$itemObj->unitPrice = $item['unit_price'];
			$itemObj->quantity = $item['quantity'];
			$itemObj->id = $item['id'];
			$itemObj->productName = $item['name'];				
			$itemObj->productSKU = $item['sku'];
			$itemObj->taxAmount = $item['tax'];
			$itemObjectData[] = $itemObj;
		}

		$request->item = $itemObjectData;
		
		$billTo = new stdClass();
		$billTo->firstName = $params['first_name'];
		$billTo->lastName = $params['last_name'];
		$billTo->street1 = $params['address1'];
		$billTo->street2 = $params['address2'];
		$billTo->street3 = '';
		$billTo->city = $params['city'];
		//$billTo->state = $params['state'];		
		$billTo->state = 'CA';
		$billTo->postalCode = $params['zip'];
		$billTo->country = $params['country'];
		$billTo->email = $params['email'];
	//	$billTo->phoneNumber = $params['phone'];		
		$billTo->ipAddress = eZSys::serverVariable( 'REMOTE_ADDR');
		$request->billTo = $billTo;
		
		$card = new stdClass();
		$card->accountNumber = $params['CardNumber'];
		$card->expirationMonth = $params['ExpirationMonth'];
		$card->expirationYear = $params['ExpirationYear'];
	//	$card->cvNumber = $params['SecurityNumber'];		
	//	$card->cardType = $params['CardType']; 
		$request->card = $card;		

		return $request;		 	 
	}		

	public function handleResponse($handleRequest, $response)
	{
		$eventWriter = new EventWriter();
		$eventParams = array( 'EventTransactionId'=> $handleRequest['userInfo']['eventId'],
									'decision'=> $response->decision,
									'reasonCode' => $response->reasonCode,							  
									'CCRequestId' => $response->requestID									
							    );
		$completeTransaction = $eventWriter->regUpdateIncreaseBalance($eventParams);				
		return  $completeTransaction;
		//TODO manage error messages
	}
}
	
?>