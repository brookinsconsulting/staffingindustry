<?php
	
/**
 * This file contains the definition for SimpleOrderSOAPManager class
 *  
 */

/**
 * 
 *  @author Jairo Riascos Mu�oz
 *  @desc Encapsulates operations over Cybersource payment tokenization 
 */

class SimpleOrderSOAPManagerTokenized extends CyberSourceSOAPManager{
	
	public function handleRequest($amount)
	{
		/* Retrieving customer data */
		$customerServices = new CustomerServices();
		$response = $customerServices->getCurrentCustomer();
		if($response->responseCode != SimplePortResponse::OK)
		{
			return $response->responseCode;
		}
		$customer = $response->responseData;
		$customerData = $customer->toArray();
		$userId = $customer->representativeUserId();
		$customerId = $customerData['Account']['CustomerId'];		
		
		$eventWriter = new EventWriter();
		$eventParams = array('UserId' => $userId,
									'CustomerId' => $customerId, 
									'EventSummary' => 'Cybersource Simple Order', 
									'Amount' => $amount, 
									'TotalAmount' => $amount);
		$event = $eventWriter-> regIncreaseBalance($eventParams);
						
		$buildMerchantRefId = $this->buildMerchantRefId($event->responseData);
		
		$userInfo = array('customerId' => $customerId,
								'userId' => $userId,
								'eventId' => $event->responseData);
			
		//TODO eventos -> get subscription ID and amount
		$request = new stdClass();		
		$request->merchantID = $this->MERCHANT_ID;		
		$request->merchantReferenceCode = $buildMerchantRefId;
		$request->clientLibrary = "PHP";
		$request->clientLibraryVersion = phpversion();
		$request->clientEnvironment = php_uname();
		
		$ccAuthService = new stdClass();
		$ccAuthService->run = "true";
		$request->ccAuthService = $ccAuthService;
		
		$ccCaptureService = new stdClass();
		$ccCaptureService->run = "true";
		$request->ccCaptureService = $ccCaptureService;
				
		$recurringSubscriptionInfo = new stdClass();
		$recurringSubscriptionInfo->subscriptionID = $customerData['Billing']['CCSubscriptionId'];
		$request->recurringSubscriptionInfo = $recurringSubscriptionInfo;

		$purchaseTotals = new stdClass();
		$purchaseTotals->currency = "USD";
		$request->purchaseTotals = $purchaseTotals;

		$item0 = new stdClass();
		$item0->unitPrice = $amount;
		$item0->quantity = "1";
		$request->item = array($item0);

		return array('request' => $request,
						'userInfo' => $userInfo);		 	 
	}		

	public function handleResponse($handleRequest, $response)
	{
		$eventWriter = new EventWriter();
		$eventParams = array( 'EventTransactionId'=> $handleRequest['userInfo']['eventId'],
									'decision'=> $response->decision,
									'reasonCode' => $response->reasonCode,							  
									'CCRequestId' => $response->requestID									
							    );
		$completeTransaction = $eventWriter->regUpdateIncreaseBalance($eventParams);				
		return  $completeTransaction;
		//TODO manage error messages
	}
}
	
?>