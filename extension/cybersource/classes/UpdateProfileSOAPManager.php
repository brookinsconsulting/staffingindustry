<?php

/**
 * This file contains the definition for UpdateProfileSOAPManager class
 *  
 */

/**
 * 
 *  @author Jairo Riascos Mu�oz
 *  @desc Encapsulates operations over Cybersource update profile + add founds 
 */

class UpdateProfileSOAPManager extends CyberSourceSOAPManager{
	
	public function handleRequest($params)
	{
		/* Retrieving customer data */
		$customerServices = new CustomerServices();
		$response = $customerServices->getCurrentCustomer();
		if($response->responseCode != SimplePortResponse::OK)
		{
			return $response->responseCode;
		}
		$customer = $response->responseData;
		$customerData = $customer->toArray();		
		$userId = $customer->representativeUserId();
		$customerId = $customerData['Account']['CustomerId'];		
			
		/* writing start update profile event */	
		
		$eventWriter = new EventWriter();
		$eventParams = array('UserId' => $userId,
									'CustomerId' => $customerId, 
									'EventSummary' => 'Cybersource Update Profile'	);
		$event = $eventWriter-> regCPUpdateStart($eventParams);
				
		$buildMerchantRefId = $this->buildMerchantRefId($event->responseData);
		
		$userInfo = array('customerId' => $customerId,
								'userId' => $userId,
								'eventId' => $event->responseData);
					
		$request = new stdClass();		
		$request->merchantID = $this->MERCHANT_ID;
		$request->merchantReferenceCode = $buildMerchantRefId;
		$request->clientLibrary = "PHP";
		$request->clientLibraryVersion = phpversion();
		$request->clientEnvironment = php_uname();
		
		$paySubscriptionUpdateService = new stdClass();
		$paySubscriptionUpdateService->run = "true";
		$request->paySubscriptionUpdateService = $paySubscriptionUpdateService;
		
		$billTo = new stdClass();
		$billTo->firstName = $params['CustomerAttributes']['FirstName']['value'];		
		$billTo->lastName = $params['CustomerAttributes']['LastName']['value'];
		$billTo->street1 = $params['CustomerAttributes']['AddressLine1']['value'];
		$billTo->street2 = $params['CustomerAttributes']['AddressLine2']['value'];
		$billTo->street3 = $params['CustomerAttributes']['AddressLine3']['value'];
		$billTo->city = $params['CustomerAttributes']['CityName']['value'];
		$billTo->state = $params['CustomerAttributes']['StateCode']['value'];		
		$billTo->postalCode = $params['CustomerAttributes']['PostalCode']['value'];
		$billTo->country = $params['CustomerAttributes']['CountryCode']['value'];
		$billTo->email = $params['CustomerAttributes']['EmailAddress']['value'];
		$billTo->phoneNumber = $params['CustomerAttributes']['PhoneNumber']['value'];		
		$billTo->ipAddress = eZSys::serverVariable( 'REMOTE_ADDR');
		$request->billTo = $billTo;
		
		$card = new stdClass();
		$card->accountNumber = $params['BillingAttributes']['ccnumber']['value'];
		$card->expirationMonth = $params['BillingAttributes']['CCExpireMonth']['value'];
		$card->expirationYear = $params['BillingAttributes']['CCExpireYear']['value'];
		$card->cardType = $params['BillingAttributes']['CreditCardType']['value'];
		$request->card = $card;
				
		$recurringSubscriptionInfo = new stdClass();
		$recurringSubscriptionInfo->subscriptionID = $customerData['Billing']['CCSubscriptionId'];
		$request->recurringSubscriptionInfo = $recurringSubscriptionInfo;
		
		//	echo"REQUEST DATA:\n";
		//print_r($customerData);
		//	print_r($request);
			
		return array('request' => $request,
						'userInfo' => $userInfo);		 
	}		
	
	public function handleResponse($handleRequest, $response, $customFields)
	{
		$eventResponse = $this->writeEvent($handleRequest, $response, $customFields);	
		return $eventResponse;
	}

	private function writeEvent($handleRequest, $response, $customFields)
	{
		$eventData = array( 'EventTransactionId'=> $handleRequest['userInfo']['eventId'],
									'decision'=> $response->decision,
									'reasonCode' => $response->reasonCode,
									'UserId' => $handleRequest['userInfo']['userId'],
									'CustomerId' => $handleRequest['userInfo']['customerId'],
									'CCSubscriptionId' => $response->paySubscriptionUpdateReply->subscriptionID,   
								   'CreditCardType' => $handleRequest['request']->card->cardType,
								   'CCLast4' => substr( $handleRequest['request']->card->accountNumber, -4),
								   'CCExpireMonth' => $handleRequest['request']->card->expirationMonth,
									'CCExpireYear' => $handleRequest['request']->card->expirationYear,
								   'CCExpireDate' => $handleRequest['request']->card->expirationYear . '-' . $handleRequest['request']->card->expirationMonth . '-01',
									'CCRequestId' => $response->requestID,
									'AddressLine1' => $handleRequest['request']->billTo->street1, 
								   'AddressLine2' => $handleRequest['request']->billTo->street2,
								   'AddressLine3' => $handleRequest['request']->billTo->street3, 
								   'CityName' => $handleRequest['request']->billTo->city, 
								   'StateCode' => $handleRequest['request']->billTo->state, 
								   'PostalCode' => $handleRequest['request']->billTo->postalCode, 
								   'CountryCode' => $handleRequest['request']->billTo->country, 
								   'ContactName' => $handleRequest['request']->billTo->firstName .' ' . $handleRequest['request']->billTo->lastName, 
								   'EmailAddress' => $handleRequest['request']->billTo->email, 
								   'PhoneNumber' => $handleRequest['request']->billTo->phoneNumber,
								   'CountyName' => $customFields['CountyName'], 
								   'PhoneExtension' => $customFields['PhoneExtension'], 
								   'FaxNumber' => $customFields['FaxNumber'],
		);
	
		$eventWriter = new EventWriter();
		$completeRegistration = $eventWriter->regCPUpdateComplete($eventData);
		eZDebug::writeNotice($completeRegistration, 'CS Customer Profile event registration response');
		return $completeRegistration;	
	}	
}
	
?>