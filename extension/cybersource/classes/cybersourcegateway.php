<?php


class CybersourceGateway extends eZPaymentGateway
{
	const GATEWAY_TYPE = "Cybersource";
	const CYBERSOURCE_SHOW_FORM = 1;
	const CYBERSOURCE_EXECUTE = 2;
	const AUTOMATIC_STATUS = false;
       
    /*!
     Constructor
    */
	function CybersourceGateway()
	{
	
	}
    
	
	function execute( $process, $event )
	{
		
		
	
		
		
		$processParams = $process->attribute( 'parameter_list' );
		$http = eZHTTPTool::instance();
		$orderID = $processParams['order_id'];
		$order = eZOrder::fetch( $orderID );
		$xmlstring = $order->attribute( 'data_text_1' );

		
		if ( $xmlstring != null )
		{
			$doc = new DOMDocument( );
			$doc->loadXML( $xmlstring );

			if ($doc->getElementsByTagName('paymentmethod')->length < 1) {
				$root = $doc->documentElement;
				$invoice = $doc->createElement( xrowECommerce::ACCOUNT_KEY_PAYMENTMETHOD, self::GATEWAY_TYPE );
				$root->appendChild( $invoice );
			}

			$order->setAttribute( 'data_text_1', $doc->saveXML() );
			$order->store();
		}         
		
		$processParameters = $process->attribute( 'parameter_list' );
		$processID =  $process->attribute( 'id' );
		

		if ( $http->hasPostVariable( 'validate' ) )
		{
			
			$errors = $this->validateForm( $process );
			if ( !$errors ) 
			{
				$process->setAttribute( 'event_state', self::CYBERSOURCE_EXECUTE );
			}
			else
			{
				return $this->loadForm( $process, $errors);
			}
		}

		switch ( $process->attribute( 'event_state' ) )
		{
			case self::CYBERSOURCE_SHOW_FORM:
			{
				// cybersource form
				return $this->loadForm( $process );
			}
			break;
			case self::CYBERSOURCE_EXECUTE:
			{
				// Cybersource request
				
				$billingParams = $this->getCybersourceParams($process);
				$inputParams = $_POST;
				$rawParams = array_merge($billingParams, $inputParams);												 				
				$cs = new SimpleOrderSOAPManager();		
				$request = $cs->handleRequest($rawParams, $process);						
				// TODO : validate request and return if some error exist eg: ini errors
				$response = $cs->execute($request);			
				if($response->reasonCode == 100)
				{
					return eZWorkflowEventType::STATUS_ACCEPTED;
				}
				else
				{
					$errors = array($response->decision . ": " . $this->getReasonCodeMessage($response->reasonCode)  );
					return $this->loadForm( $process, $errors);
				}
			}
			break;
		}                          
	}
	
	
    function loadForm( &$process, $errors = 0 )
    {
        $http = eZHTTPTool::instance();

        // get parameters
        $processParams = $process->attribute( 'parameter_list' );

        // load ini
        $ini = eZINI::instance( 'ezauthorize.ini' );

        // regen posted form values
        if ( $http->hasPostVariable( 'validate' ) and
             $ini->variable( 'eZAuthorizeSettings', 'RepostVariablesOnError' ) )
        {
            $tplVars['cardname'] = trim( $http->postVariable( 'CardName' ) );
            $tplVars['cardtype'] = strtolower( $http->postVariable( 'CardType' ) );
            $tplVars['cardnumber'] = $http->postVariable( 'CardNumber' );
            $tplVars['expirationmonth'] = $http->postVariable( 'ExpirationMonth' );
            $tplVars['expirationyear'] = $http->postVariable( 'ExpirationYear' );
            $tplVars['securitynumber'] = $http->postVariable( 'SecurityNumber' );
            $tplVars['amount'] = '';
        }
        else
        {
            // set form values to blank
            $tplVars['cardname'] = '';
            $tplVars['cardtype'] = '';
            $tplVars['cardnumber'] = '';
            $tplVars['expirationmonth'] = '';
            $tplVars['expirationyear'] = '';
            $tplVars['securitynumber'] = '';
            $tplVars['amount'] = '';
        }

        $tplVars['s_display_help'] = $ini->variable( 'eZAuthorizeSettings', 'DisplayHelp' );
        $tplVars['errors'] = $errors;
        $tplVars['order_id'] = $processParams['order_id'];
      //  print_r(self::traceExecution());
       // die();

        $process->Template=array
        (
            'templateName' => 'design:workflow/eventtype/result/' . 'cybersource_form.tpl',
            'templateVars' => $tplVars,
            'path' => array( array( 'url' => false,
                                    'text' =>  'Payment Information') ) 

        );
        return eZWorkflowEventType::STATUS_FETCH_TEMPLATE_REPEAT;
    }	
    
    
    function useForm()
    {
        if ( eZSys::isShellExecution() )
            return false;
        else 
            return true; 
    }
    
    function validateForm( &$process )
    {
        $http = eZHTTPTool::instance();
        $errors = array();

        if ( trim( $http->postVariable( 'CardNumber' ) ) == '' )
        {
            $errors[] = 'You must enter a card number.';
        }
        elseif( strlen( trim( $http->postVariable( 'CardNumber' ) ) ) > 49 )
        {
            $errors[] = 'Your card number should be under 50 characters.';
        }

        if ( trim( $http->postVariable( 'CardName' ) ) == '' )
        {
            $errors[] = 'You must enter a card name.';
        }
        elseif( strlen( trim( $http->postVariable( 'CardName' ) ) ) > 79 )
        {
            $errors[] = 'Your card name should be under 80 characters.';
        }

        if ( trim( $http->postVariable( 'ExpirationMonth' ) ) == '' )
        {
            $errors[] = 'You must select an expiration month.';
        }

        if ( trim( $http->postVariable( 'ExpirationYear' ) ) == '' )
        {
            $errors[] = 'You must select an expiration year.';
        }

        return $errors;
    }    
    
    
    function getCybersourceParams($process)
    {
    	$processParams = $process->attribute( 'parameter_list' );
		$http = eZHTTPTool::instance();
		$orderID = $processParams['order_id'];
		$order = eZOrder::fetch( $orderID );
		$total =  $order->totalIncVAT();
		$currencyCode = $order->currencyCode();		
		$xmlstring = $order->attribute( 'data_text_1' );
		$billingArray = $this->billingXMLToArray($xmlstring);
		$billingArray['total'] = $total;
		$billingArray['currency_code'] = $currencyCode;
		$productItems = $order->productItems();
		$itemArray = array();
		foreach($productItems as $item)
		{
			$itemDataMap = $item['item_object']->ContentObject->dataMap();
			$itemArray[$item['id']] = array (
				'unit_price' => $item['price_inc_vat'],
				'quantity' => $item['item_count'],
				'id' => $item['id'],
				'name' => $item['object_name'],
				'sku' => $itemDataMap['product_id']->content(),
				'tax' => $item['vat_value']
			);
		}
		$billingArray['items'] = $itemArray;	
		return $billingArray;						
    }
    
    function getReasonCodeMessage($id)
    {
    	$ini = eZINI::instance('cybersource.ini');
		$reasonCodes  = $ini->variable('ReasonCodes','reasonCode');
		if($reasonCodes[$id] != "")
		{
			return $reasonCodes[$id];
		}
		else
			return "Undescribed gateway error, code $id";
		
    }
    
    
    function billingXMLToArray($xmlString)
    {    
    	if ( $xmlString != null )
        {
        	$dom = new DOMDocument( '1.0', 'utf-8' );
  	    	$billingData = array();   
        	$success = $dom->loadXML( $xmlString );
        	$billingNodes = $dom->getElementsByTagName('shop_account');             	     	
        	$billingDataNode =  $billingNodes->item(0);
        	$XMLElements =  $billingDataNode->childNodes;
        	for($i=0; $i < $XMLElements->length; $i++)
        	{
        		$itemName = $XMLElements->item($i)->nodeName;
        		$item = $XMLElements->item($i)->textContent;
        		if($itemName != 'shop_account')
        		{
        			$billingData[$itemName] = $item;	
        		}       			       			 
        	}
        	return $billingData;    	
        }   	
    } 
	
}

$gateway = new eZPaymentGatewayType();
$gateway->registerGateway( CybersourceGateway::GATEWAY_TYPE, "cybersourcegateway", "Cybersource" );

?>
