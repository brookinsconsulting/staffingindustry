<?php

/**
 * This file contains the definition for RenewCustomerProfileSOAPManager class
 *  
 */

/**
 * 
 *  @author Jairo Riascos Mu�oz
 *  @desc Encapsulates operations over Cybersource Renew Subscription 
 */

class RenewCustomerProfileSOAPManager extends CyberSourceSOAPManager{

	public function handleRequest()
	{
		/* Retrieving customer data */
		$customerServices = new CustomerServices();
		$response = $customerServices->getCurrentCustomer();
		if($response->responseCode != SimplePortResponse::OK)
		{
			return $response->responseCode;
		}
		$customer = $response->responseData;
		$customerData = $customer->toArray();
		$userId = $customer->representativeUserId();
		$customerId = $customerData['Account']['CustomerId'];		
		
		/* retrieving renew fee */	

		$renewFee = BillingManager::getRenewalFee();
				
		$eventWriter = new EventWriter();
		$eventParams = array('UserId' => $userId,
									'CustomerId' => $customerId, 
									'EventSummary' => 'Cybersource Renew Customer Profile', 
									'Amount' => $renewFee, 
									'TotalAmount' => $renewFee);
				
		$event = $eventWriter-> regRenewCPRegistrationStart($eventParams);						
		$buildMerchantRefId = $this->buildMerchantRefId($event->responseData);
				
		$userInfo = array('customerId' => $customerId,
								'userId' => $userId,
								'eventId' => $event->responseData);			
		
		$request = new stdClass();		
		$request->merchantID = $this->MERCHANT_ID;		
		$request->merchantReferenceCode = $buildMerchantRefId;
		$request->clientLibrary = "PHP";
		$request->clientLibraryVersion = phpversion();
		$request->clientEnvironment = php_uname();
		
		$ccAuthService = new stdClass();
		$ccAuthService->run = "true";
		$request->ccAuthService = $ccAuthService;
		
		$ccCaptureService = new stdClass();
		$ccCaptureService->run = "true";
		$request->ccCaptureService = $ccCaptureService;
				
		$recurringSubscriptionInfo = new stdClass();
		$recurringSubscriptionInfo->subscriptionID = $customerData['Billing']['CCSubscriptionId'];
		$request->recurringSubscriptionInfo = $recurringSubscriptionInfo;

		$purchaseTotals = new stdClass();
		$purchaseTotals->currency = "USD";
		$request->purchaseTotals = $purchaseTotals;

		$item0 = new stdClass();
		$item0->unitPrice = $renewFee;
		$item0->quantity = "1";
		$request->item = array($item0);

		return array('request' => $request,
						'userInfo' => $userInfo);		 	 
	}		

	public function handleResponse($handleRequest, $response)
	{
		$eventWriter = new EventWriter();
		$eventParams = array( 'EventTransactionId'=> $handleRequest['userInfo']['eventId'],
									'decision'=> $response->decision,
									'reasonCode' => $response->reasonCode,
									'UserId' => $handleRequest['userInfo']['userId'],
									'CustomerId' => $handleRequest['userInfo']['customerId'],							  
									'CCRequestId' => $response->requestID									
							    );
							   
		$completeTransaction = $eventWriter->regRenewCPRegistrationComplete($eventParams);
		
		if( $response->decision == 'ACCEPT'){
			$billinManager = new BillingManager();
			$renewData = array('CustomerId' => $handleRequest['userInfo']['customerId']);
			$renewAccount = $billinManager->renewAccount($renewData);			
		}					
		return  $completeTransaction;		
	}
}
	
?>