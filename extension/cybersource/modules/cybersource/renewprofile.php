<?php

/**
 * This view loads billing information from the customer, validating this data at type level and 
 * setting the order for renew cybersource customer profile, external gateway unsuccessful responses will be redirected to this view for allowing user to retry  
 * 
 * @author  Jairo Riascos Mu�oz
 * @version 0.01, 11/11/10
 */

include_once( "kernel/common/template.php" );

$module = $Params['Module'];
$tpl = templateInit();
$http = eZHTTPTool::instance();
$ini = eZINI::instance( 'cybersource.ini' );
$renewalFee = BillingManager::getRenewalFee();
	
if ( $module->isCurrentAction( 'Proceed' ) )
{
	$billingServices = new BillingServices();
	$response = $billingServices->renewCustomerProfile();
		
	if ( $response->reasonCode <> 100 )
	{
		
		$tpl->setVariable( 'missing_fields', $response->missingField);
		$tpl->setVariable( 'invalid_fields', $response->invalidField);
		$tpl->setVariable( 'reason_code', $response->reasonCode);
		$tpl->setVariable( 'has_CS_validation_errors', true);		
		$tpl->setVariable( 'renewalFee', $renewalFee);
		$Result = array();
		$Result['content'] = $tpl->fetch( "design:cybersource/renewprofile/renew_profile.tpl" );
		$Result['path'] = array ( array ('url' => 'cybersource/renewprofile', 'text' => "Renew Profile Form") );
	}
	else
	{
		$customerServices = new CustomerServices();
		$customerResponse = $customerServices->getCurrentCustomer();
		if($customerResponse->responseCode != SimplePortResponse::OK)
		{	
			return $module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
		}
		$customer = $customerResponse->responseData;
		$customerData = $customer->toArray();		
		
		$eventId = CustomerProfileSOAPManager::getEventIdFromMerchantId($response->merchantReferenceCode);
		$eventData =  EventManager::buildAccountEvent($eventId)->toArray();
				
		$tpl->setVariable( 'customerData', $customerData);
		$tpl->setVariable( 'eventData', $eventData);			
		$tpl->setVariable( 'renewalFee', $renewalFee);	
		$Result = array();
		$Result['content'] = $tpl->fetch( "design:cybersource/renewprofile/renew_profile_confirm.tpl" );
		$Result['path'] = array ( array ('url' => 'cybersource/renewprofile', 'text' => "Cybersource Renew Profile Confirmation") );			
	}
}
else
{
	$customerServices = new CustomerServices();
	$response = $customerServices->getCurrentCustomer();
	
	if($response->responseCode != SimplePortResponse::OK)
	{
		return $module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
	}
	$customer = $response->responseData;
	$customerData = $customer->toArray();
	$userData = $customer->representativeUser()->toArray();
	
	if($customerData['Account']['Status'] == 'ACTIVE' || $customerData['Account']['Status'] == 'INACTIVE')
	{
		$tpl->setVariable( 'customerData', $customerData);
		$tpl->setVariable( 'userData', $userData);
		$tpl->setVariable( 'renewalFee', $renewalFee);
		$Result = array();
		$Result['content'] = $tpl->fetch( "design:cybersource/renewprofile/renew_profile.tpl" );
		$Result['path'] = array ( array ('url' => 'cybersource/renewprofile', 'text' => "Renew Profile Form") );		
	}
	else
	{
			$tpl->setVariable( 'customer_status', $customerData['Account']['Status']);
			$tpl->setVariable( 'customer_data', $customerData);
			$tpl->setVariable( 'renewalFee', $renewalFee);
			$Result = array();
			$Result['content'] = $tpl->fetch( "design:cybersource/renewprofile/renew_profile_access.tpl" );
			$Result['path'] = array ( array ('url' => 'cybersource/renewprofile', 'text' => "Renew Profile not aviable") );	
	}
}

?>
