<?php

/**
 * This view complete the information of the customer in the billing information form 
 * 
 * @author  Javier D�az Garz�n
 * @version 0.01, 12/10/10
 */

require_once( "kernel/common/template.php" );

function attributeArray()
{
	return array('AddressLine1','AddressLine2','AddressLine3','CityName','CountyName','StateCode','PostalCode','CountryCode','ContactName','ContactTitle','EmailAddress','PhoneNumber','PhoneExtension','FaxNumber');		 	
}

$http = eZHTTPTool::instance();
$module = $Params['Module'];

$customerServices = new CustomerServices();
$customer = $customerServices->getCurrentCustomer();

if($customer->responseCode != SimplePortResponse::OK)
{
	return $module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
}

$customerData = $customer->responseData->toArray();
$profileData = $customerData[Profile]; 
$attributeKeyArray = attributeArray();
$response = array();
$attribute_traslation = '';

foreach ($profileData as $key => $value)
{
	if(in_array($key,$attributeKeyArray))
	{
		$response[$key] = $value;
	}		
}

echo json_encode($response);

$Result['pagelayout'] = true;
eZDB::checkTransactionCounter();
eZExecution::cleanExit();

?>