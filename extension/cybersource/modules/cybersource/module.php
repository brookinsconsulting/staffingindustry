<?php

$Module = array( 'name' => 'cybersource', 'variable_params' => false );

$ViewList = array();

						
$ViewList['billingform'] = array('functions' => array( 'billingform' ),
								 'script' => 'billingform.php',
								 'ui_component' => 'customermanagement',
								 'default_navigation_part' => 'ezsimpleportnavigationpart',
								 'params' => array ( ),
								 'single_post_actions' => array( 'ProceedButton' => 'Proceed',
								 								 'CancelButton' => 'Cancel')
								);		

$ViewList['getcustomerdata'] = array('functions' => array( 'getcustomerdata' ),
								 'script' => 'getcustomerdata.php',
								 'params' => array() );	

$ViewList['updateprofile'] = array('functions' => array( 'updateprofile' ),
												'script' => 'updateprofile.php',
												'ui_component' => 'customermanagement',
												'default_navigation_part' => 'ezsimpleportnavigationpart',
												'params' => array ( ),
												'single_post_actions' => array( 'ProceedButton' => 'Proceed',
																							'CancelButton' => 'Cancel')
												);	
												
$ViewList['renewprofile'] = array('functions' => array( 'renewprofile' ),
												'script' => 'renewprofile.php',
												'ui_component' => 'customermanagement',
												'default_navigation_part' => 'ezsimpleportnavigationpart',
												'params' => array ( ),
												'single_post_actions' => array( 'ProceedButton' => 'Proceed',
																							'CancelButton' => 'Cancel')
												);													
									

$SiteAccess = array('name'=> 'SiteAccess',
					'values'=> array(),
					'path' => 'classes/',
					'file' => 'ezsiteaccess.php',
					'class' => 'eZSiteAccess',
					'function' => 'siteAccessList',
					'parameter' => array()
					);	

$FunctionList['billingform'] = array( 'SiteAccess' => $SiteAccess );
$FunctionList['getcustomerdata'] = array( 'SiteAccess' => $SiteAccess );
$FunctionList['updateprofile'] = array( 'SiteAccess' => $SiteAccess );
$FunctionList['renewprofile'] = array( 'SiteAccess' => $SiteAccess );

?>


