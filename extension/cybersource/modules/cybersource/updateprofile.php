<?php

/**
 * This view loads and recieves billing and shipping information from the customer, validating this data at type level and 
 * setting the order, external gateway unsuccessful responses will be redirected to this view for allowing user to retry  
 * 
 * @author  Jairo Riascos Mu�oz
 * @version 0.01, 12/10/10
 */

include_once( "kernel/common/template.php" );

$module = $Params['Module'];
$tpl = templateInit();
$http = eZHTTPTool::instance();
$ini = eZINI::instance( 'cybersource.ini' );
$inputObject = new AplInputObject("cybersourcebillinginput.ini");
	
if ( $module->isCurrentAction( 'Proceed' ) )
{
	$validAttributes = $inputObject->validateAttributesInput();
	$attributes = $inputObject->attributesToArray();	
	if($validAttributes)
	{	
		$billingServices = new BillingServices(); 
		$response = $billingServices->updateCustomerProfile($attributes);
		
		if ( $response->reasonCode <> 100 )
		{
			//TODO error message handler
			$tpl->setVariable( 'fields', $attributes);	
			$tpl->setVariable( 'reason_code', $response->reasonCode);
			$tpl->setVariable( 'missing_fields', $response->missingField);
			$tpl->setVariable( 'invalid_fields', $response->invalidField);
			$tpl->setVariable( 'has_CS_validation_errors', true);		
			
			$Result = array();
			$Result['content'] = $tpl->fetch( "design:cybersource/updateprofile/update_form.tpl" );
			$Result['path'] = array ( array ('url' => 'cybersource/updateprofile', 'text' => "Update Profile Form") );
			
		}
		else
		{
			$customerServices = new CustomerServices();
			$customerResponse = $customerServices->getCurrentCustomer();
			if($customerResponse->responseCode != SimplePortResponse::OK)
			{	
				return $module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
			}
			$customer = $customerResponse->responseData;
			$customerData = $customer->toArray();		
			
			$eventId = CustomerProfileSOAPManager::getEventIdFromMerchantId($response->merchantReferenceCode);
			$eventData =  EventManager::buildAccountEvent($eventId)->toArray();
			
			$tpl->setVariable( 'customerData', $customerData);
			$tpl->setVariable( 'eventData', $eventData);			
				
			$Result = array();
			$Result['content'] = $tpl->fetch( "design:cybersource/updateprofile/update_form_confirm.tpl" );
			$Result['path'] = array ( array ('url' => 'cybersource/updateprofile', 'text' => "Cybersource Update Profile Confirmation") );			
		}		
	}
	// Attributes don't pass a type validation
	else
	{
		$attributes = $inputObject->attributesToArray();	
		$validationErrors = ($validAttributes)?0:1;
		$tpl->setVariable( 'fields', $attributes);	
		$tpl->setVariable( 'has_validation_errors', $validationErrors);	
				
		$Result = array();
		$Result['content'] = $tpl->fetch( "design:cybersource/updateprofile/update_form.tpl" );
		$Result['path'] = array ( array ('url' => 'cybersource/updateprofile', 'text' => "Update Profile Form") );
	}	
}
else
{
	$customerServices = new CustomerServices();
	$response = $customerServices->getCurrentCustomer();
	
	if($response->responseCode != SimplePortResponse::OK)
	{
		return $module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );
	}
	$customer = $response->responseData;
	$customerData = $customer->toArray();
	
	if($customerData['Account']['Status'] == 'ACTIVE')
	{
		$attributes = $inputObject->attributeMetadataArray();
		$tpl->setVariable( 'fields', $attributes);
		$Result = array();
		$Result['content'] = $tpl->fetch( "design:cybersource/updateprofile/update_form.tpl" );
		$Result['path'] = array ( array ('url' => 'cybersource/updateprofile', 'text' => "Update Profile Form") );		
	}
	else
	{
			$tpl->setVariable( 'customer_status', $customerData['Account']['Status']);
			$tpl->setVariable( 'customer_data', $customerData);
			$Result = array();
			$Result['content'] = $tpl->fetch( "design:cybersource/updateprofile/update_form_access.tpl" );
			$Result['path'] = array ( array ('url' => 'cybersource/updateprofile', 'text' => "Update Form not aviable") );	
	}
}

?>
