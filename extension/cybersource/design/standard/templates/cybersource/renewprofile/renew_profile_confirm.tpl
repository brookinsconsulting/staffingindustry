{* <apldoc>
Shows the cybersource transaction confirmation.
</apldoc> *}
{def $eventType = ''
	$billingIni = ezini('cctype','data','cybersourcebillinginput.ini')
}
{if $eventData.EventTypeName|eq('UpdateCSProfile')}
	{set $eventType = 'Update Billing Profile'}
{else}	
	{set $eventType =  explode_by_capital_letter($eventData.EventTypeName)}
{/if}

<div id="confirm-order">
	<div class="keep-informed">
		<h1>Renew Profile Successful</h1>
		<i>Operation succesful. $ {$eventData.Billing.TotalAmount} has been deducted from your current billing profile.</i>
		<h2>Operation Detail</h2>
		<ul>
			<li><strong>Type:</strong> {$eventType}</li>
			{if $eventData.Billing.CybersourceRequestId}
				<li><strong>Transaction Reference Code:</strong> {$eventData.Billing.CybersourceRequestId}</li>
			{/if}
			<li><strong>Status:</strong> {$eventData.Status}</li>
			<li><strong>Amount:</strong> $ {$eventData.Billing.TotalAmount}</li>
			<li><strong>Transaction Summary:</strong> {$eventData.EventSummary}</li>
			<li><strong>Transaction Date:</strong> {$eventData.CreateDate}</li>			
		</ul>
		<h2>Customer Information</h2>
		<ul>
			<li><strong>First Name:</strong> {$customerData.Profile.FirstName}</li>
			<li><strong>Last Name:</strong> {$customerData.Profile.LastName}</li>
			<li><strong>Email Address:</strong> {$customerData.Profile.EmailAddress}</li>
			<li><strong>Phone Number:</strong> {$customerData.Profile.PhoneNumber}</li>
			{if $customerData.Profile.PhoneExtension|ne('')}
				<li><strong>Phone Extension:</strong> {$customerData.Profile.PhoneExtension}</li>
			{/if}
		</ul>	
		<strong>Your account expires on :	{$customerData.Account.EzRegExpireDate|datetime('custom', '%M/%d/%Y')}</strong>
	</div>	
</div>