{* <apldoc>
Shows the administrator customer management section, the options aviable from here are:
<ul>
<li>Set user as Valid</li>
<li>Send LOA Countersign</li>
</ul> 
</apldoc> *}
{def $userStatus = $customerData.Account.Status
	$userName = $customerData.Profile.CustomerName
	$userEmail = $customerData.Profile.EmailAddress
	$userID = $customerData.Account.CustomerId
	$balance = $customerData.Account.CurrentBalanceAmount
	$amounts = ezini('IncreaseBalaceSettings','AmountOption','oms.ini')
	$creditCardCodes = ezini('CreditCardCodes','creditCardCode','cybersource.ini')
}
{if $has_CS_validation_errors}
	<div class="warning">
		<h5>{if $reasonCodes[$reason_code]}{$reasonCodes[$reason_code]|explode('.')|implode('.<br />')}{else}{$reasonCodes[undef]|explode('.')|implode('.<br />')}{/if}</h5>
		{*Show attribute before view of wrapper*}
		{include uri='design:wrappers/view_wrapper_before.tpl'}
		{if $missing_fields}
			{if $missing_fields|is_array()}
				<ul>
					{foreach $missing_fields as $missing_field}
						<li>{explode_by_capital_letter($missing_field|explode('c:')|implode())}</li>
						{*<li>{$missing_field}</li>*}
					{/foreach}
				</ul>
			{else}
				{explode_by_capital_letter($missing_fields|explode('c:')|implode())}
				{*$missing_fields*}
			{/if}
		{/if}
		{if $invalid_fields}
			{if $invalid_fields|is_array()}
				<ul>
					{foreach $invalid_fields as $invalid_field}
						<li>{explode_by_capital_letter($invalid_field|explode('c:')|implode())}</li>
						{*<li>{$invalid_field}</li>*}
					{/foreach}
				</ul>
			{else}
				{explode_by_capital_letter($invalid_fields|explode('c:')|implode())}
				{*$invalid_fields*}
			{/if}		
		{/if}
	</div>
{/if}
<div class="add_founds">
	<h1>Renew Your Profile</h1>
	<h2>{$userName}</h2> 
	<strong>Email:</strong> {$userEmail} <br/>
	<div id="customer_balance_box">
		<h3>Your Current Billing Information</h3>
		<ul>
			<li>Card Holder: {$customerData.Billing.ContactName}</li>
			<li>Card Type: {$creditCardCodes[$customerData.Billing.CreditCardType]}</li>
			<li>Expiration Year: {$customerData.Billing.CCExpireYear}</li>
			<li>Expiration Month: {$customerData.Billing.CCExpireMonth}</li>
			<li>Credit Card last four digits: {$customerData.Billing.CCLast4}</li>		
		</ul>
		<i> You can deduct the renew profile fee (${$renewalFee}) from your Current Billing Account.</i> <br/>
		<form id="balance_discount"  method="post" action="/cybersource/renewprofile">
			<input type="submit" class="button" id="ProceedButton" name="ProceedButton" value="Proceed"></input>		
		</form>
		<i> or Update your billing information, click <a href={'cybersource/updateprofile'|ezurl()}>here</a></i> <br/>		
	</div>
</div>
{*Show attribute after view of wrapper*}
{include uri='design:wrappers/view_wrapper_after.tpl'}