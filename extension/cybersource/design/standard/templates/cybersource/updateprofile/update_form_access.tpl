{* <apldoc>
Shows the cybersource transaction confirmation.
</apldoc> *}
{*Show attribute before view of wrapper*}
{include uri='design:wrappers/view_wrapper_before.tpl'}<br />

<h2>Update Profile not aviable</h2>
{if eq($customer_status, 'NEW')}
	Your account is in process of validation, soon you will be notificated to  <b>{$customer_data.Profile.EmailAddress} </b>  for proceeding with your registration process
{else}
	You are not allowed to do a profile update, contact us for more details
{/if}

{*Show attribute after view of wrapper*}
{include uri='design:wrappers/view_wrapper_after.tpl'}