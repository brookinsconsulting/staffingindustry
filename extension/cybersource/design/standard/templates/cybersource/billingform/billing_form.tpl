{* <apldoc>
Shows the form of billing information.
</apldoc> *}

<div class="simple_port_wrapper">
{def $reasonCodes = ezini('ReasonCodes','reasonCode','cybersource.ini')
}
<h2>Billing Information Form</h2>
{*Show attribute before view of wrapper*}
{include uri='design:wrappers/view_wrapper_before.tpl'}

{if $has_validation_errors}
<div class="warning">
Please complete the highlighted fields
</div>
{/if}
{if $has_CS_validation_errors}
	<div class="warning">
		<h5>{if $reasonCodes[$reason_code]}{$reasonCodes[$reason_code]|explode('.')|implode('.<br />')}{else}{$reasonCodes[undef]|explode('.')|implode('.<br />')}{/if}</h5>
		{if $missing_fields}
			{if $missing_fields|is_array()}
				<ul>
					{foreach $missing_fields as $missing_field}
						<li>{explode_by_capital_letter($missing_field|explode('c:')|implode())}</li>
						{*<li>{$missing_field}</li>*}
					{/foreach}
				</ul>
			{else}
				{explode_by_capital_letter($missing_fields|explode('c:')|implode())}
				{*$missing_fields*}
			{/if}
		{/if}
		{if $invalid_fields}
			{if $invalid_fields|is_array()}
				<ul>
					{foreach $invalid_fields as $invalid_field}
						<li>{explode_by_capital_letter($invalid_field|explode('c:')|implode())}</li>
						{*<li>{$invalid_field}</li>*}
					{/foreach}
				</ul>
			{else}
				{explode_by_capital_letter($invalid_fields|explode('c:')|implode())}
				{*$invalid_fields*}
			{/if}		
		{/if}
	</div>
{/if}
	
<div id="confirm-order">
  <div class="keep-informed">
  <h2>Subscription Information</h2>
  <label> Setup Fee: </label> ${$setupFee}
  <label> Subscription Time: </label> One Year
  <form id="billingRegForm" class="omsForm" method="post" action="/cybersource/billingform">	
      <span class="required"><span class="required_field">*</span> Indicates a required field</span>
      <h2>Enter credit card information</h2>
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.BillingAttributes.CreditCardType res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.BillingAttributes.ccnumber res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.BillingAttributes.CCExpireMonth res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.BillingAttributes.CCExpireYear res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.BillingAttributes.cvNumber res=$response}
      <br/><br/> <a id="get_data">Complete the next information with customer data</a> <br/><br/>
      <h2>Enter credit card billing information</h2>
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.AddressLine1 res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.AddressLine2 res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.AddressLine3 res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.CityName res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.CountyName res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.StateCode res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.PostalCode res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.CountryCode res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.FirstName res=$response}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.LastName res=$response}
      {*include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.ContactTitle res=$response*}
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.EmailAddress res=$response}
      <span class="info_update">We value your <a href="http://www.neustar.biz/about-neustar/privacy-statement" target="blank">privacy</a></span>
      <div class="break"></div>
      <span class="info_update">Your email address will be your user name.</span>
      <div class="break"></div>
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.PhoneNumber res=$response}
      <span class="info_update">Format: 703-555-1212</span>
      <div class="break"></div>
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.PhoneExtension res=$response}
      <span class="info_update">Format: numbers only</span>
      <div class="break"></div>
      {include uri='design:attributeinput/attribute_view.tpl' attribute=$fields.CustomerAttributes.FaxNumber res=$response}
      <span class="info_update">Format: 703-555-1212</span>
      <div class="break"></div>
      <input type="hidden" class="input_hidden" name="product_id" value="{$product_id}"></input>
      <input type="submit" class="proceed_button_billing" name="ProceedButton" value="Proceed"></input>
      <input type="submit" class="registration_button_billing" name="CancelButton" value="Cancel"></input>	
  </form>	
  </div>	
</div>

{include uri='design:formevents.tpl' type='aplinput'}

</div>
{literal}
<script type="text/javascript">
	jQuery(document).ready(function(){	
		var infox;
		$("#get_data").click(function(){			
			$.post('/cybersource/getcustomerdata',function(data){
				infox = JSON.parse(data);
				for ( var i in infox){
					var identifier = '#'+ i;
					$(identifier).attr("value", infox[i]); 
				}
			});
		});

	});				
</script>
{/literal}
{*Show attribute after view of wrapper*}
{include uri='design:wrappers/view_wrapper_after.tpl'}		