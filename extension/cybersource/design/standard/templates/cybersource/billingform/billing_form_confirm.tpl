{* <apldoc>
Shows the cybersource transaction confirmation.
</apldoc> *}
{def $eventType = explode_by_capital_letter($eventData.EventTypeName)
	$billingIni = ezini('cctype','data','cybersourcebillinginput.ini')
}
<div id="confirm-order">
	<div class="keep-informed">
		<h1>Order Detail</h1>
		<ul>
			<li><strong>Type:</strong> {$eventType}</li>
			{if $eventData.Billing.CybersourceRequestId}
				<li><strong>Transaction Reference Code:</strong> {$eventData.Billing.CybersourceRequestId}</li>
			{/if}
			<li><strong>Status:</strong> {$eventData.Status}</li>
			<li><strong>Transaction Summary:</strong> {$eventData.EventSummary}</li>
			<li><strong>Date:</strong> {$eventData.CreateDate}</li>
			<li><strong>Amount:</strong> $ {$eventData.Billing.UnitAmount}</li>
		</ul>
		<h2>Customer Information</h2>
		<ul>
			<li><strong>First Name:</strong> {$customerData.Profile.FirstName}</li>
			<li><strong>Last Name:</strong> {$customerData.Profile.LastName}</li>
			<li><strong>Email Address:</strong> {$customerData.Profile.EmailAddress}</li>
			<li><strong>Phone Number:</strong> {$customerData.Profile.PhoneNumber}</li>
			{if $customerData.Profile.PhoneExtension|ne('')}
				<li><strong>Phone Extension:</strong> {$customerData.Profile.PhoneExtension}</li>
			{/if}
		</ul>	
		<h2>Billing Information</h2>
		<ul>
			<li><strong>Contact Name:</strong> {$customerData.Billing.ContactName}</li>
			<li><strong>Credit Card Type:</strong> {$billingIni[$customerData.Billing.CreditCardType]}</li>
			<li><strong>Credit Card Last Four Digits:</strong> {$customerData.Billing.CCLast4}</li>
			<li><strong>Expiration Year:</strong> {$customerData.Billing.CCExpireYear}</li>
			<li><strong>Expiration Month:</strong> {$customerData.Billing.CCExpireMonth}</li>
			<li><strong>Country:</strong> {$customerData.Billing.CountryCode}</li>
			<li><strong>State:</strong> {$customerData.Billing.StateCode}</li>
			<li><strong>City:</strong> {$customerData.Billing.CityName}</li>
			<li><strong>Address:</strong> {$customerData.Billing.AddressLine1}</li>
			<li><strong>Postal Code:</strong> {$customerData.Billing.PostalCode}</li>
			<li><strong>Email:</strong> {$customerData.Billing.EmailAddress}</li>
			<li><strong>Phone:</strong> {$customerData.Billing.PhoneNumber}</li>
			{if $customerData.Billing.PhoneExtension|ne('')}
				<li><strong>Phone Extension:</strong> {$customerData.Billing.PhoneExtension}</li>
			{/if}
		</ul>
	</div>	
</div>