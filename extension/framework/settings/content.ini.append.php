<?php /* #?ini charset="utf-8"?

[ChildrenNodeList]
ExcludedClasses[]
ExcludedClasses[]=banner
ExcludedClasses[]-link

[CustomTagSettings]
AvailableCustomTags[]=contentbox
AvailableCustomTags[]=listsubitems
AvailableCustomTags[]=tabbox
AvailableCustomTags[]=column

[contentbox]
CustomAttributes[]=header

[listsubitems]
CustomAttributes[]=page
CustomAttributes[]=header
CustomAttributes[]=children_view
CustomAttributes[]=limit
CustomAttributes[]=stylize
CustomAttributes[]=alltext

[tabbox]
CustomAttributes[]=page
CustomAttributes[]=header
CustomAttributes[]=limit

[article]
SummaryInFullView=disabled

[folder]
SummaryInFullView=disabled

[link]
AvailableClasses[]=button

[ul]
AvailableClasses[]
AvailableClasses[]=menu vertical
AvailableClasses[]=menu horizontal

[literal]
AvailableClasses[]=html


[SiteMapSettings]
Limit=10
Columns=3
Rows=3
# columns|rows
UseCount=columns
# include|exclude
ClassFilterType=exclude
ClassFilter[]
ClassFilter[]=banner
ClassFilter[]=footer
ClassFilter[]=image
ClassFilter[]=infobox


*/ ?>