<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
DesignExtensions[]=framework

[StylesheetSettings]
#SiteCSS=
#ClassesCSS=
CSSFileList[]
CSSFileList[]=html5.css
CSSFileList[]=jquery.tools.css
CSSFileList[]=content.css


[JavaScriptSettings]
LibraryScripts[]
;LibraryScripts[]=http://code.jquery.com/jquery-1.5.1.min.js
;LibraryScripts[]=http://cdn.jquerytools.org/1.2.5/all/jquery.tools.min.js
;LibraryScripts[]=http://files.thinkcreative.com/jquery.tools_1.2.5_all.min.js
JavaScriptList[]
JavaScriptList[]=jquery.placeholder.js
FrontendJavaScriptList[]=excanvas.js
FrontendJavaScriptList[]=jquery.bt.js
FrontendJavaScriptList[]=jquery.cookie.js
;JavaScriptList[]=ezjsc::yui3
;JavaScriptList[]=ezjsc::yui3io
;CufonYUI=http://files.thinkcreative.com/cufon-yui.js
#CufonFontsList[]



*/ ?>
