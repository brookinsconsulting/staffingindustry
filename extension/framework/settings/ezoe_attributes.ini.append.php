<?php /* #?ini charset="utf8"?

[CustomAttribute_listsubitems_page]
Name=Source Page
Type=link
LinkType[]
LinkType[eznode://]=Node ID

[CustomAttribute_listsubitems_header]
Title=Lets you define the text to be displayed at the top of the box.
Name=Header Text
Type=text

[CustomAttribute_listsubitems_children_view]
Title=The view to use to render the children
Type=select
Selection[]
Selection[line]=Line
Selection[embed]=Embed
Selection[listitem]=List Item
Default=listitem

[CustomAttribute_listsubitems_limit]
Title=Lets you define the maximum number of items to display in the box.
Name=Page Limit
Type=int
Default=5

[CustomAttribute_listsubitems_stylize]
Title=Gives the custom tag a stylized appearance
Name=Stylize Box
Type=select
Selection[]
Selection[basic]=Basic
Selection[modular]=Basic w/ Content Box
Selection[advanced]=Advanced
Selection[custom]=Custom Header
Selection[ticker]=Ticker
Default=modular
TickerLimit=20

[CustomAttribute_listsubitems_alltext]
Title=Text will appear as link to page that displays all items
Name=All Link Text


[CustomAttribute_tabbox_page]
Name=Source Page
Type=link
LinkType[]
LinkType[eznode://]=Node ID

[CustomAttribute_tabbox_header]
Title=Lets you define the text to be displayed at the top of the box.
Name=Header Text
Type=text

[CustomAttribute_tabbox_limit]
Title=Lets you define the maximum number of items to display in the box.
Name=Pane Limit
Type=int
Default=5


*/ ?>