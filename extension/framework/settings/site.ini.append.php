<?php /* #?ini charset="utf-8"?

[ContentSettings]
ViewCaching=enabled

[FileSettings]
TemporaryPermissions=0777
StorageDirPermissions=0777
StorageFilePermissions=0777
LogFilePermissions=0777

[GoogleSettings]
TrackingCode=

[SearchSettings]
DelayedIndexing=enabled
DisplayFacets=enabled

[SiteSettings]
MetaDataArray[]
MetaDataArray[keywords]=
MetaDataArray[description]=

[TemplateSettings]
ExtensionAutoloadPath[]=framework
ShowUsedTemplates=enabled
TemplateCompile=enabled
TemplateCache=enabled

[RegionalSettings]
TextTranslation=disabled

[RoleSettings] 
PolicyOmitList[]=website/updates

[DebugSettings]
DebugOutput=enabled
#DebugByUser=enabled
DebugUserIDList[]
DebugUserIDList[]=10
DebugUserIDList[]=14
DebugUserIDList[]=172
DebugUserIDList[]=173
DebugUserIDList[]=174
DebugUserIDList[]=175
DebugUserIDList[]=176
DebugUserIDList[]=177
DebugUserIDList[]=178
DebugUserIDList[]=179

*/ ?>