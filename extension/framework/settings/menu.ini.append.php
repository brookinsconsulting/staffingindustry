<?php /* #?ini charset="utf-8"?

[MenuContentSettings]
# Default setting as found in settings/menu.ini is commented out
TopIdentifierList[]
TopIdentifierList[]=folder
TopIdentifierList[]=feedback_form
TopIdentifierList[]=gallery
TopIdentifierList[]=forum
TopIdentifierList[]=documentation_page
TopIdentifierList[]=forums
TopIdentifierList[]=event_calendar
TopIdentifierList[]=multicalendar
TopIdentifierList[]=link
TopIdentifierList[]=blog
TopIdentifierList[]=frontpage

# Default setting as found in settings/menu.ini is commented out
LeftIdentifierList[]
#LeftIdentifierList[]=article
#LeftIdentifierList[]=article_mainpage
LeftIdentifierList[]=folder
LeftIdentifierList[]=feedback_form
LeftIdentifierList[]=gallery
LeftIdentifierList[]=forum
LeftIdentifierList[]=documentation_page
LeftIdentifierList[]=forums
LeftIdentifierList[]=event_calendar
LeftIdentifierList[]=multicalendar
LeftIdentifierList[]=link
LeftIdentifierList[]=blog
LeftIdentifierList[]=frontpage

# Classes to use in extra menu (infobox)
ExtraIdentifierList[]
ExtraIdentifierList[]=infobox

[MenuSettings]
HideSideMenuClasses[]
HideSideMenuClasses[]=frontpage
HideSideMenuClasses[]=survey
SideMenuPosition=left


[NavigationPart]
Part[ezcontentnavigationpart]=Content Structure
Part[ezmedianavigationpart]=Media Library
Part[ezusernavigationpart]=User Accounts
Part[ezmynavigationpart]=My Account
Part[websitenavigationpart]=Site

[TopAdminMenu]
Tabs[]=website

[Topmenu_website]
URL[]
URL[default]=website/updates
NavigationPartIdentifier=websitenavigationpart
Name=Site Info
Tooltip=Information Specific to the Website
Enabled[]
Enabled[default]=true
Enabled[browse]=false
Enabled[edit]=false
Shown[]
Shown[default]=true
Shown[navigation]=true
Shown[browse]=true

*/ ?>