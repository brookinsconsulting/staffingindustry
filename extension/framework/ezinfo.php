<?php

class FrameworkInfo
{
	static function info(){
		return array(
			'Name'=>"Framework",
			'Version'=>'Version 0.9.0 Beta',
			'Copyright'=>"Copyright (C) 2010.",
			'License'=>"GNU General Public License v2.0",
			'Dependencies'=>array('moduletools','sitelink')
			);
	}
}

?>