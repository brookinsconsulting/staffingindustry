$placeholder=(function($){
	var $default='';
	var $lib={
		'clear':function(obj){
			if(obj.val()===$default){
				obj.val('');
			}
		},
		'exists':function(){
			return('placeholder' in document.createElement('input'))
		}
	};
	$.fn.placeholder=function(){
		if($lib.exists()){return this;}
		this.each(function(){
			var obj = $(this);
			$default=obj.attr('placeholder')?obj.attr('placeholder'):obj.val();
			obj.val($default).bind('focus',function(){
				$lib.clear(obj);
			}).bind('blur',function(){
				if(obj.val()==''){
					obj.val($default);
				}
			}).parents('form').bind('submit',function(){
				$lib.clear(obj);
			});
			$('body').bind('unload',function(){
				$lib.clear(obj);
			});
		});
	}
	if(!$lib.exists()){
		$(function(){
			$('input[placeholder]').placeholder();
		});
	}
	return{
		'exists':$lib.exists()
	};

})(jQuery);