{if $has_cufon}<script type="text/javascript">Cufon.now();</script>{/if}
{if first_set($include,false())}{include uri=$include}{/if}
{if $pagedesign.data_map.footer_script.has_content}
<script language="javascript" type="text/javascript">
    {$pagedesign.data_map.footer_script.content}
</script>
{/if}
