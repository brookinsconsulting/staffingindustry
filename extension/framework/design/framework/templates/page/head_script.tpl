{def $libscripts=ezini('JavaScriptSettings','LibraryScripts','design.ini')
	 $cufon_yui=ezini('JavaScriptSettings','CufonYUI','design.ini')
	 $ini_scripts=cond(is_unset($load_script_list),ezini('JavaScriptSettings','JavaScriptList','design.ini')|merge(ezini('JavaScriptSettings','FrontendJavaScriptList','design.ini')),false())
	 $javascript_list=$libscripts
}
{if $ini_scripts}{set $javascript_list = $javascript_list|merge($ini_scripts)}{/if}
{if $has_cufon}{set $javascript_list=array($cufon_yui)|merge($javascript_list|merge($cufon_fonts))}{/if}
{ezscript_load($javascript_list)}

{if and(ezini_hasvariable('GoogleSettings','TrackingCode','site.ini'),ne(ezini('GoogleSettings','TrackingCode','site.ini'),''))}
<script type="text/javascript">
var _gaq=_gaq||[];_gaq.push(["_setAccount", "{ezini('GoogleSettings','TrackingCode','site.ini')}"]);_gaq.push(["_trackPageview"]);
{literal}(function(){var ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.src=('https:'==document.location.protocol?'https://ssl':'http://www')+'.google-analytics.com/ga.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})();{/literal}
</script>
{/if}<!--[if lt IE 9]><script type="text/javascript" src={'javascript/ie/html5.js'|ezdesign()} charset="utf-8"></script><![endif]-->