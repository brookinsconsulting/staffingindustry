{def $classes = ezini('MenuContentSettings', 'LeftIdentifierList', 'menu.ini')
	 $left_menu_items = 0
	 $useNode = false()
	 $checkNode = $current_node
}
{while and( is_set($checkNode.parent), not($useNode), not(cond( or($useNode,not($useNode)), $useNode, $classes|contains($useNode.class_identifier) )) )}
	{set $left_menu_items = fetch('content', 'list_count', hash('parent_node_id', $checkNode.node_id,
																'class_filter_type', 'include',
																'class_filter_array', $classes,
																'attribute_filter',array('and', array('priority','between',array(0,998)))
																))
		 $useNode=cond($left_menu_items,$checkNode,false())
		 $checkNode=$checkNode.parent	 
	}
	{if or(is_object($checkNode)|not, $checkNode.parent|eq($checkNode))}{break}{/if}
{/while}
<nav id="sidemenu" class="module">
{include uri="design:menu/menubar.tpl"
	root_node=$useNode
	show_header=first_set($show_header,false())
	link_header=true()
	attribute_filter=array('and', array('priority','between',array(0,998)))
}
{if first_set($return,false())}
	{include uri="design:content/datatype/view/ezxmltags/separator.tpl"}
	{if ne($useNode.node_id,ezini('NodeSettings', 'RootNode', 'content.ini'))}
	<div class="previous-link"><a href={$useNode.parent|sitelink()}>Return to: {$useNode.parent.name|wash()}</a></div>
	{else}
	<div class="previous-link"><a href={$useNode|sitelink()}>Return to: {$useNode.name|wash()}</a></div>
	{/if}
{/if}
</nav>