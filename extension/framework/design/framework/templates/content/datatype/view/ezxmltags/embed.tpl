{def $alignment = $object_parameters.align
     $myclass = $object.class_identifier
	 $element=cond(and($alignment,ne($alignment,'center')),'aside','div')
	 $show_image=and(cond(is_set($hide_image),not($hide_image),true()), cond(is_set($object.data_map.image),$object.data_map.image.has_content,false()))
}
{if $myclass|eq('large_file')}{set $myclass = 'file' }{/if}
<{$element}{if is_set($object_parameters.id)} id="{$object_parameters.id}"{/if} class="content-view-embed class-{$myclass}{if $show_image} line-image{/if}{if $alignment} align-{$alignment}{else} noalign{/if}{if ne($classification|trim,'')} {$classification|wash}{/if}">
{content_view_gui view=$view link_parameters=$link_parameters object_parameters=$object_parameters content_object=$object classification=$classification}
</{$element}>