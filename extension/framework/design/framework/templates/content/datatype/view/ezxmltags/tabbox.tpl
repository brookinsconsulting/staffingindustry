{def $node = fetch('content','node',hash('node_id',$page|explode('://')[1]))
	 $data_nodes = fetch('content', 'list', hash('parent_node_id',$node.node_id,
						'limit', first_set($limit,3),
						'sort_by', $node.sort_array))
	 $has_header = and(is_set($header),ne($header,''))
	 $tab_names=array()
	 $pane_nodes=array()
}
{foreach $data_nodes as $pane_node}
{set $tab_names=$tab_names|append($pane_node.name|wash())
	 $pane_nodes=$pane_nodes|append(fetch('content','list',hash('parent_node_id',$pane_node.node_id,'limit',1,'sort_by',$pane_node.sort_array)))
}
{/foreach}
<section class="customtag custom-tag-tabbox">
	<header>
		<h1>{if $has_header}{$header}{else}{$node.name|wash()}{/if}</h1>
	</header>
	<section class="customtag-content">
		<ul class="tabs">
		{foreach $tab_names as $key=>$name}
			<li><a href={$data_nodes[$key]|sitelink()}>{$name}</a></li>
		{/foreach}
		</ul>
		<div class="panes">
			{foreach $pane_nodes as $pane}
			<div class="pane-item">{node_view_gui view='embed' content_node=$pane[0]}</div>
			{/foreach}
		</div>
	</section>
</section>