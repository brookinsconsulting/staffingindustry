{def $classes = ''}
{if is_set($class)}
	{if is_array($class)}
		{set $classes = $class|explode(' ')}
	{else}
		{set $classes = $class}
	{/if}
{/if}
<div class="column{if ne($classes, '')} {$classes}{/if}">{$content}</div>