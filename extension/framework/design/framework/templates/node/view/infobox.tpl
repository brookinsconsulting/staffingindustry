<aside class="module infobox">
	{if not($node.data_map.hide_header.data_int)}<header><h1>{$node.name|wash()}</h1></header>{/if}
	{if $node.data_map.image.has_content}<div class="frame">{attribute_view_gui attribute=$node.data_map.image image_class='infoboximage' css_class='attribute-image'}</div>{/if}
	<section class="infobox-content">
		{attribute_view_gui attribute=$node.object.data_map.content}
		{if $node.data_map.external_link.has_content}
			<div class="infobox-link"><a href="{$node.data_map.external_link.content}">{$node.data_map.external_link.data_text|wash()}</a></div>
		{/if}
	</section>
{if or($node.object.can_edit, $node.object.can_remove)}
	<div class="controls">
		<form action={"/content/action"|ezroot()} method="post">
		{if $node.object.can_edit}
			<input type="image" name="EditButton" src={"edit-infobox-ico.gif"|ezimage()} alt="Edit" />
			<input type="hidden" name="ContentObjectLanguageCode" value="{$node.object.current_language}" />
		{/if}
		{if $node.object.can_remove}
			<input type="image" name="ActionRemove" src={"trash-infobox-ico.gif"|ezimage()} alt="Remove" />
		{/if}
			<input type="hidden" name="ContentObjectID" value="{$node.object.id}" />
			<input type="hidden" name="NodeID" value="{$node.node_id}" />
			<input type="hidden" name="ContentNodeID" value="{$node.node_id}" />
		</form>
	</div>
{/if}
</aside>