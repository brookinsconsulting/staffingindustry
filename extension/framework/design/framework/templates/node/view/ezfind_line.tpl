{def $show_image=and(cond(is_set($hide_image),not($hide_image),true()), cond(is_set($node.data_map.image),$node.data_map.image.has_content,false()))}
<div class="content-view-line class-{$node.class_identifier}{if $show_image} line-image{/if} {$bgColor}" style="background-position:{$node.score_percent|wash()|int()}% 0;">
{if $show_image}
	{set-block variable='left_column'}
		<div class="attribute-image">{attribute_view_gui attribute=$node.data_map.image image_class='search_result'}</div>
	{/set-block}
{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$left_column class='search_result'}
{/if}

{set-block variable='right_column'}
	<h2><a href={$node|sitelink()}>{$node.name|wash()}</a></h2>
	{$node.highlight}
	<div><em><a href={$node|sitelink()}>/{$node.url_alias|shorten(70, '...', 'middle')|wash}</a></em></div>
	<span class="score-percent">Relevance: {$node.score_percent|wash}%</span>
{/set-block}
	{if $show_image}
		{include uri="design:content/datatype/view/ezxmltags/column.tpl" content=$right_column class='r-search_result' position='right'}
	{else}
		{$right_column}
	{/if}
</div>
