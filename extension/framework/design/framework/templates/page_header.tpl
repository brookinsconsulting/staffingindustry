<header role="banner">
	<a id="logo" href="/"><img title="{$logo_alt}" alt="{$logo_alt}" src={'images/logo.png'|ezdesign()} width="{$logo_width|int()}" height="{$logo_height|int()}" /></a>
	<nav id="utility">
		{include uri="design:menu/menubar.tpl"
			root_node=fetch('content','node',hash('node_id',$indexpage))
			show_header=false()
			orientation='horizontal'
			delimiter='|'
			attribute_filter=array('and', array('priority','between',array(100,110)))
			menu=hash('prepend',hash(
					'Login','/user/login'
				))
		}
	</nav>
	<div id="search">
		{include uri="design:parts/searchform.tpl" placeholder='Search'}
	</div>
</header>