<form action={"/content/search"|sitelink()} method="get">
{if first_set($hidden,false())}
{foreach $hidden as $key=>$value}
	<input type="hidden" name="{$key}" value="{$value}" />
{/foreach}
{/if}
	<fieldset>
		<input class="searchtext" type="text" name="SearchText"{if first_set($placeholder,false())} placeholder="{$placeholder|wash()}"{/if} />
		<input class="searchbutton" type="image" name="SearchButton" alt="Search" src={'images/search_02.png'|ezdesign()} />
	</fieldset>
</form>