{def $related_content=fetch('ezfind','moreLikeThis',hash('query_type', 'nid',
														'query', $node.node_id,
														'limit', 5,
														'class_id',array('article','news_item','blog_post')
))
	 $wrapper=cond(first_set($section,false()),'section','div')
}
{if count($related_content['SearchResult'])}
{def $threshold=15}
<{$wrapper}{if first_set('class',false())} class="{$class|implode(' ')}"{/if}>
	{if eq($wrapper,'section')}<header><h1>{first_set($header,'Related Content')}</h1></header>{else}<h1>{first_set($header,'Related Content')}</h1>{/if}
	<ul>
	{foreach $related_content['SearchResult'] as $related_object max 5}
	{if gt($related_object.score_percent,$threshold)}
		<li><a href={$related_object|sitelink()} title="{$related_object.name|wash()}">{$related_object.name|wash()}</a><!--{$related_object.score_percent}--></li>
	{/if}
	{/foreach}
	</ul>
</{$wrapper}>
{/if}