{if and(or(and(is_set($infoboxes_only), not($infoboxes_only)),is_unset($infoboxes_only)), and(is_set($pagedata.persistent_variable.extrainfo), $pagedata.persistent_variable.extrainfo) )}
	{$pagedata.persistent_variable.extrainfo}
{/if}
{if count($infoboxes)}
	{foreach $infoboxes as $infobox}{node_view_gui content_node=$infobox view='infobox'}{/foreach}
{/if}
