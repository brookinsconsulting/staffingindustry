{ezcss_require('website.css')}

{def $attrcounter=0
	 $attribute_categorys=ezini('ClassAttributeSettings', 'CategoryList', 'content.ini')
	 $attribute_default_category=ezini('ClassAttributeSettings', 'DefaultCategory', 'content.ini')
	 $ignore_datatype_list=ezini('GeneralSettings','IgnoreDataTypeList','website.ini')
	 $valid_filter_datatypes=array('ezboolean','ezdate','ezdatetime','ezemail','ezinteger','ezobjectrelation','ezselection','ezstring','eztime')
	 $filter_operators=array('=','!=','<','>','<=','>=','in','between','not_between','like')
}

{if is_set($script_id)}
<h2 style="padding: 20px; border: 1px solid #ddd; background-color: #eee">Your file is being generated and will be emailed to {$email} when complete. You can monitor its progress <a href={concat('/scriptmonitor/view/', $script_id)|sitelink()}>here</a>.</h2>
{/if}
<form id="website-users" action={concat('/website/users/',$class_identifier)|sitelink()} method="post">
<input type="hidden" name="format" value="csv" />
<div class="maincontentheader"><h1>{$ModuleView.name|wash()}</h1></div>

	<p>For a full csv of user data, download this file: <a href='/var/storage/SIA_Unames.csv'>SIA_Unames.csv</a> (last updated {'var/storage/SIA_Unames.csv'|getmoddate|datetime("custom", "%m/%d/%Y")})</p>
	<p>For only active users, download this file: <a href='/var/storage/active_users.csv'>active_users.csv</a> (last updated {'var/storage/active_users.csv'|getmoddate|datetime("custom", "%m/%d/%Y")})</p>

	<p>This form will provide a comma-separated value (CSV) file of the users that match your specified criteria. Please note the following points in using this form:</p>
	<ul>
		<li>The form <strong>is</strong> case-sensitive.</li>
		<li>In order for a item to be used in the query, the checkbox <strong>must be</strong> checked.</li>
		<li>When using the &#147;like&#148; option, an asterisk (*) <strong>must be</strong> used.</li>
	</ul>

	<fieldset class="ezcca-attributes-group-object-properties ezcca-attributes-default-group">
	<legend>Object Properties</legend>
	<div class="ezcca-collapsible-fieldset-content">

		<div class="block">
			<div class="attribute-label">
				<label><input name="object_properties[]" type="checkbox" value="owner" />Creator UserID</label>
				<select name="object_properties-owner-filter" size="1">
					<option value="LIKE">LIKE</option>
					<option value="LIKE *...*">LIKE *...*</option>
					<option value="NOT LIKE">NOT LIKE</option>
					<option value="=">=</option>
					<option value="!=">!=</option>
					<option value="= ''">= ''</option>
					<option value="!= ''">!= ''</option>
					<option value="IS NULL">IS NULL</option>
					<option value="IS NOT NULL">IS NOT NULL</option>
				</select>
			</div>
		<input name="object_properties-owner-query" type="text" size="75" />
		</div>

		<div class="block">
			<div class="attribute-label">
				<label><input name="object_properties[]" type="checkbox" value="email" />User Email</label>
{*
				<select name="object_properties-email-filter" size="1">
				{foreach $filter_operators as $key=>$filter}
					<option value="{$filter}">{$filter|wash()}</options>
				{/foreach}
				</select>
*}
				<select name="object_properties-email-filter" size="1">
					<option value="LIKE">LIKE</option>
					<option value="LIKE *...*">LIKE *...*</option>
					<option value="NOT LIKE">NOT LIKE</option>
					<option value="=">=</option>
					<option value="!=">!=</option>
					<option value="= ''">= ''</option>
					<option value="!= ''">!= ''</option>
					<option value="IS NULL">IS NULL</option>
					<option value="IS NOT NULL">IS NOT NULL</option>
				</select>
			</div>
		<input name="object_properties-email-query" type="text" size="75" />
		</div>

		<div class="block datetime">
			<div class="attribute-label">
				<label><input name="object_properties[]" type="checkbox" value="published" />Date Created</label>
				<select name="object_properties-published-filter" size="1">
				{foreach $filter_operators as $key=>$filter}
					<option value="{$filter}">{$filter|wash()}</options>
				{/foreach}
				</select>
			</div>
			<span>From: <input name="object_properties-published-query[]" type="text" size="30" /> To: <input name="object_properties-published-query[]" type="text" size="30" /></span>
		</div>

		<div class="block">
			<div class="attribute-label">
				<label><input name="object_properties[]" type="checkbox" value="roles" />User Roles</label>
				<input class="text-center" type="text" name="object_properties-roles-filter" size="15" value="in" readonly="readonly" />
{*
				<select name="object_properties-roles-filter" size="1">
				{foreach $filter_operators as $key=>$filter}
					<option value="{$filter}">{$filter|wash()}</options>
				{/foreach}
				</select>
*}
			</div>
			<select name="object_properties-roles-query" size="1">
			{foreach fetch('website','roleslist') as $role}
				<option value="{$role.id}">{$role.name|wash()}</option>
			{/foreach}
			</select>
		</div>

		<div class="block">
			<div class="attribute-label">
				<label><input name="object_properties[]" type="checkbox" value="direct" />Direct User</label>
			</div>
			Finds the {$class_identifier} items that are direct subitems of an user group.
		</div>

	</div>
	</fieldset>

{foreach $grouped_data_map as $attribute_group => $content_attributes_grouped}
{*if $attribute_group|ne($attribute_default_category)*}
	<fieldset class="ezcca-collapsible ezcca-attributes-group-{$attribute_group|wash}{if $attribute_group|eq($attribute_default_category)} ezcca-attributes-default-group{/if}">
	<legend><a href="JavaScript:void(0);">{$attribute_categorys[$attribute_group]}</a></legend>
	<div class="ezcca-collapsible-fieldset-content">
{*/if*}
{foreach $content_attributes_grouped as $attribute_identifier => $attribute}
{if $ignore_datatype_list|contains($attribute.data_type_string)}{continue}{/if}
	{*include uri=concat('content/datatype/filter/',$attribute.data_type_string,'.tpl')|include_template() attribute=$attribute*}
	{include uri=concat('design:content/datatype/filter/',$attribute.data_type_string,'.tpl')
		filterable=$valid_filter_datatypes|contains($attribute.data_type_string)
	}
	{*include uri="design:content/datatype/filter.tpl" attribute=$attribute*}
{/foreach}
{*if $attribute_group|ne($attribute_default_category)*}
	</div>
	</fieldset>
{*/if*}
{/foreach}

<div class="block buttonblock">
<input class="button" type="submit" name="UserAttributesButton" value="Generate Report">
</div>

{*
{foreach $class.data_map as $key=>$value}
{if and(gt($attrcounter,0),eq($attrcounter|mod(7),0))}{/if}
	<div>
	{ *<label><input type="checkbox" name="UserAttributes[]" value="{$key}" />{$value.name|wash()}</label>* }
	<h3>{$value.name|wash()} - {$key}</h3>
	</div>
{/foreach}
*}
</form>

{run-once}
<script type="text/javascript">
{literal}
jQuery(function($){
	$('fieldset.ezcca-collapsible legend a').click(function(){
		var container = $(this.parentNode.parentNode),
			inner = container.find('div.ezcca-collapsible-fieldset-content');
		if(container.hasClass('ezcca-collapsed')){
			container.removeClass('ezcca-collapsed');
			inner.slideDown(150);
		}else{
			inner.slideUp(150,function(){
				$(this.parentNode).addClass('ezcca-collapsed');
			});
		}
	});
	// Collapse by default, unless the group has at least one attribute with label.message-error
	$('fieldset.ezcca-collapsible').each(function(){
		var item=$(this);
		if (item.find('label.message-error').length==0 && !item.hasClass('ezcca-attributes-default-group')){
			item.addClass('ezcca-collapsed').find('div.ezcca-collapsible-fieldset-content').hide();
		}
	});
	
	$('#website-users .datetime input[type=text]').datepicker();

});
{/literal}
</script>
{/run-once}

{*
<br /><hr /><br />


<form id="website-users" action={concat('/website/users/',$class_identifier)|sitelink()} method="post" onsubmit="return false;">
<div class="block headings"><h2 class="first">Field</h2><h2>Value</h2></div>
<div class="block">
<label><input type="checkbox" />Oxycon Module SID</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />Email Address</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />Primary Contact</label><input type="checkbox" />
</div>
<div class="block">
<label><input type="checkbox" />North American Corporate Membership</label><input type="checkbox" />
</div>
<div class="block datetime">
<label><input type="checkbox" />North America Corporate Membership End Date</label><span>From: <input type="text" size="30" /> To: <input type="text" size="30" /></span>
</div>
<div class="block">
<label><input type="checkbox" />Western European Corporate Membership</label><input type="checkbox" />
</div>
<div class="block datetime">
<label><input type="checkbox" />Western European Corporate Membership End Date</label><span>From: <input type="text" size="30" /> To: <input type="text" size="30" /></span>
</div>
<div class="block">
<label><input type="checkbox" />North American Membership</label><input type="checkbox" />
</div>
<div class="block datetime">
<label><input type="checkbox" />North America Membership End Date</label><span>From: <input type="text" size="30" /> To: <input type="text" size="30" /></span>
</div>
<div class="block">
<label><input type="checkbox" />Western European Membership</label><input type="checkbox" />
</div>
<div class="block datetime">
<label><input type="checkbox" />Western European Membership End Date</label><span>From: <input type="text" size="30" /> To: <input type="text" size="30" /></span>
</div>
<div class="block">
<label><input type="checkbox" />First Name</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />Middle</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />Last Name</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />Job Title</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />Company Address</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />Address 2</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />City</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />State/Province</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />Postal ZIP</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />Country</label><input type="text" size="75" />
</div>
<div class="block">
<label><input type="checkbox" />Phone</label><input type="text" size="75" />
</div>

<div class="block buttonblock">
<input class="button" type="button" name="UserAttributesButton" value="Generate Report">
</div>
</form>
{literal}
<script type="text/javascript">
$(function(){
	$('#website-users .datetime input[type=text]').datepicker();
});
</script>
{/literal}
*}

