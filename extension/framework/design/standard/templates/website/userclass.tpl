<div class="maincontentheader"><h1>{$ModuleView.name|wash()}</h1></div>
{if count($class_list)}
<p>Please select one of the following User Classes to view:</p>
<ul>
{foreach $class_list as $class}
	<li>{if eq($class.identifier|wash(),'client')}<a href="/site_admin/website/users/{$class.identifier|wash()}">{/if}{$class.name|wash()}{if eq($class.identifier|wash(),'client')}</a>{/if}</li>
{/foreach}
</ul>
{/if}