<div class="block">
	<div class="attribute-label">
		<label><input name="object_attributes[]" type="checkbox" value="{$attribute.identifier|wash()}" />{$attribute.name|wash()}</label>
		{if $filterable}
		<select name="object_attributes-{$attribute.identifier|wash()}-filter" size="1">
		{foreach $filter_operators as $key=>$filter}
			<option value="{$filter}">{$filter|wash()}</options>
		{/foreach}
		</select>
		{/if}
	</div>
<input name="object_attributes-{$attribute.identifier|wash()}-query" type="text" size="75" />
</div>