<div class="block">
	<div class="attribute-label">
		<label><input name="object_attributes[]" type="checkbox"{if $attribute.content.is_multiselect} disabled="disabled"{/if} value="{$attribute.identifier|wash()}" />{$attribute.name|wash()}</label>
	{if not($attribute.content.is_multiselect)}
		{if and($filterable)}
		<select name="object_attributes-{$attribute.identifier|wash()}-filter" size="1">
		{foreach $filter_operators as $key=>$filter}
			<option value="{$filter}">{$filter|wash()}</options>
		{/foreach}
		</select>
		{/if}
	{/if}
	</div>
	{if not($attribute.content.is_multiselect)}
		{if count($attribute.content.options)}
		<select name="object_attributes-{$attribute.identifier|wash()}-query" size="1">
		{foreach $attribute.content.options as $option}
			<option value="{$option.id}">{$option.name|wash()}</option>
		{/foreach}
		</select>
		{/if}
	{else}
	<h3>The system does not allow filtering on this attribute.</h3>
	{/if}
</div>