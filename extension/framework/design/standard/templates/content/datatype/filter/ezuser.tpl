<div class="block">
	<div class="attribute-label">
		<label><input type="checkbox" />{$attribute.name|wash()}</label>
		{if $filterable}
		<select name="" size="1">
		{foreach $filter_operators as $key=>$filter}
			<option value="{$filter}">{$filter|wash()}</options>
		{/foreach}
		</select>
		{/if}
	</div>
<input type="text" size="75" />
</div>
<div class="block">
	{$attribute|attribute('show',2)}
</div>