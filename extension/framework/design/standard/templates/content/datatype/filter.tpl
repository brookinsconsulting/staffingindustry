{def $valid_filter_datatypes=array('ezboolean','ezdate','ezdatetime','ezemail','ezinteger','ezobjectrelation','ezselection','ezstring','eztime')
	 $filter_operators=array('','=','!=','<','>','<=','>=','in','between','not_between','like')
}
{include uri=concat('design:content/datatype/filter/',$attribute.data_type_string,'.tpl')
	filterable=$valid_filter_datatypes|contains($attribute.data_type_string)
}