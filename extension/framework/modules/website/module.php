<?php

$ModuleTools = ModuleTools::instance();

$Module=array(
	'name'=>'Site Information',
	'variable_params'=>true,
	'default_navigation_part'=>'websitenavigationpart',
	'left_menu_heading'=>'Site Information'
);/*,
	'left_menu'=>'design:website/menu.tpl'
*/
$ViewList = array();

$ViewList['updates'] = array(
	'name'=>'Recent Site Updates',
	'script'=>'updates.php',
	'params'=>array('SortBy','Class'),
	'unordered_params'=>array('offset'=>'Offset','subtree'=>'SubTree','featured'=>'Featured'),
	'default_navigation_part'=>$Module['default_navigation_part'],
	'left_menu'=>true
);

$ViewList['userclass'] = array(
	'name'=>'View Users Information',
	'script'=>'userclass.php',
	'params'=>array(),
	'unordered_params'=>array(),
	'default_navigation_part'=>$Module['default_navigation_part'],
	'left_menu'=>true
);

$ViewList['users'] = array(
	'name'=>'View Users Information',
	'script'=>'users.php',
	'params'=>array('ClassIdentifier'),
	'unordered_params'=>array(),
	'default_navigation_part'=>$Module['default_navigation_part'],
	'left_menu'=>false
);

?>