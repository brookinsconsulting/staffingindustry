<?php

$FunctionList = array(); 

$FunctionList['roleslist'] = array(
			'name' => 'Roles List',
			'call_method' => array('include_file'=>'websitefunctions.php', 'class'=>'WebsiteFunctions', 'method'=>'rolesList'),
			'parameter_type' => 'standard',
			'parameters' => array(
				)
);

$FunctionList['updates'] = array(
			'name' => 'Recent Updates',
			'call_method' => array('include_file'=>'websitefunctions.php', 'class'=>'WebsiteFunctions', 'method'=>'updates'),
			'parameter_type' => 'standard',
			'parameters' => array(
				array('name'=>'parent_node_id', 'type'=>'integer', 'required'=>false, 'default'=>2),
				array('name'=>'class_array', 'type'=>'array', 'required'=>false, 'default'=>false),
				array('name'=>'limit', 'type'=>'mixed', 'required'=>false, 'default'=>10),
				array('name'=>'offset', 'type'=>'integer', 'required'=>false, 'default'=>0)
				)
);



?>