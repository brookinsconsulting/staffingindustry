<?php

$ModuleTools = ModuleTools::instance($Module);
//eZDebug::writeDebug($ClassIdentifier,'ClassIdentifier');

$UserClass=eZContentClass::fetchByIdentifier($ClassIdentifier);
//eZDebug::writeDebug($UserClass,'UserClass');
$DataMap=$UserClass->dataMap();
$GroupedDataMap=createGroupedDataMap($DataMap);
$db=eZDB::instance();

if(count($_POST)){
	$currentUser = eZUser::currentUser();
	$currentUserID = $currentUser->id();
	$email = $currentUser->Email;
	$timestamp = time();
	$fh = fopen("var/userexport/$timestamp", 'w') or die("can't open file");
	fwrite($fh, serialize($_POST));
	fclose($fh);

	$script = eZScheduledScript::create(
		'userexport.php',
		"extension/site/bin/php/userexport.php --timestamp=$timestamp --email='$email'"
	);
	$script->store();

	$result = $db->arrayQuery("SELECT id FROM `ezscheduled_script` WHERE user_id = $currentUserID ORDER BY id DESC");
	$scriptid = $result[0]['id'];

	return $ModuleTools->result(array(
		'grouped_data_map'=>$GroupedDataMap,
		'class'=>$UserClass,
		'class_identifier'=>$ClassIdentifier,
		'script_id'=>$scriptid,
		'email'=>$email
	));

}else{
	return $ModuleTools->result(array(
		'grouped_data_map'=>$GroupedDataMap,
		'class'=>$UserClass,
		'class_identifier'=>$ClassIdentifier
	));
}



function createGroupedDataMap( $contentObjectAttributes )
{
$groupedDataMap  = array();
$contentINI      = eZINI::instance( 'content.ini' );
$categorys       = $contentINI->variable( 'ClassAttributeSettings', 'CategoryList' );
$defaultCategory = $contentINI->variable( 'ClassAttributeSettings', 'DefaultCategory' );
foreach( $contentObjectAttributes as $attribute )
{
$classAttribute      = $attribute;
$attributeCategory   = $classAttribute->attribute('category');
$attributeIdentifier = $classAttribute->attribute( 'identifier' );
if ( !isset( $categorys[ $attributeCategory ] ) || !$attributeCategory )
$attributeCategory = $defaultCategory;

if ( !isset( $groupedDataMap[ $attributeCategory ] ) )
$groupedDataMap[ $attributeCategory ] = array();

$groupedDataMap[ $attributeCategory ][$attributeIdentifier] = $attribute;

}
return $groupedDataMap;
}




?>
