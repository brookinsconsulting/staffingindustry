<?php

	class WebsiteFunctions
{

	function rolesList(){
		return array('result'=>eZRole::fetchByOffset($offset=0, $limit=50, $asObject=true, $ignoreTemp=true));
	}

	function updates($parent_node_id=2,$class_array=false,$limit=10,$offset=0){
/*
		$DB=eZDB::instance();
		eZDebug::writeDebug(func_get_args(),'function arguments');

$sortingInfo = eZContentObjectTreeNode::createSortingSQLStrings(array('published',false), 'ezcontentobject_tree', false);
$classCondition = eZContentObjectTreeNode::createClassFilteringSQLString('include', $class_array);
$attributeFilter = eZContentObjectTreeNode::createAttributeFilterSQLStrings($filterAttributes=false, $sortingInfo);
eZContentObjectTreeNode::createExtendedAttributeFilterSQLStrings( $ExtendedAttributeFilter=false );
$mainNodeOnlyCond = eZContentObjectTreeNode::createMainNodeConditionSQLString($mainNodeOnly=false);
$pathStringCond     = '';
$notEqParentString  = '';
// If the node(s) doesn't exist we return null.
eZContentObjectTreeNode::createPathConditionAndNotEqParentSQLStrings($pathStringCond, $notEqParentString, $parent_node_id, false, false);
$useVersionName = true;
$versionNameTables = eZContentObjectTreeNode::createVersionNameTablesSQLString($useVersionName);
$versionNameTargets = eZContentObjectTreeNode::createVersionNameTargetsSQLString($useVersionName);
$versionNameJoins = eZContentObjectTreeNode::createVersionNameJoinsSQLString($useVersionName, false);
$languageFilter = ' AND ' . eZContentLanguage::languagesSQLFilter('ezcontentobject');
$objectNameFilterSQL = eZContentObjectTreeNode::createObjectNameFilterConditionSQLString( $objectNameFilter=false );
$limitation = ( isset( $params['Limitation']  ) && is_array( $params['Limitation']  ) ) ? $params['Limitation']: false;
$limitationList = eZContentObjectTreeNode::getLimitationList( $limitation );
$sqlPermissionChecking = eZContentObjectTreeNode::createPermissionCheckingSQL( $limitationList );
// Determine whether we should show invisible nodes.
$showInvisibleNodesCond = eZContentObjectTreeNode::createShowInvisibleSQLString( !$ignoreVisibility=false );

$query = "SELECT DISTINCT
			ezcontentobject.*,
			ezcontentobject_tree.*,
			ezcontentclass.serialized_name_list as class_serialized_name_list,
			ezcontentclass.identifier as class_identifier,
			ezcontentclass.is_container as is_container
			$versionNameTargets
			$sortingInfo[attributeTargetSQL]
		FROM
			ezcontentobject_tree,
			ezcontentobject,ezcontentclass
			$versionNameTables
			$sortingInfo[attributeFromSQL]
			$attributeFilter[from]
			$sqlPermissionChecking[from]
		WHERE
			$pathStringCond
			$sortingInfo[attributeWhereSQL]
			$attributeFilter[where]
			ezcontentclass.version=0 AND
			$notEqParentString
			ezcontentobject_tree.contentobject_id = ezcontentobject.id  AND
			ezcontentclass.id = ezcontentobject.contentclass_id AND
			$mainNodeOnlyCond
			$classCondition
			$versionNameJoins
			$showInvisibleNodesCond
			$sqlPermissionChecking[where]
			$objectNameFilterSQL
			$languageFilter";
if( $sortingInfo['sortingFields']){$query .= " ORDER BY $sortingInfo[sortingFields]";}
$server = count( $sqlPermissionChecking['temp_tables'] ) > 0 ? eZDBInterface::SERVER_SLAVE : false;

eZDebug::writeNotice($query,'Website Updates Query');

$nodeListArray = $DB->arrayQuery( $query, array(), $server );
//$retNodeList = eZContentObjectTreeNode::makeObjectsArray($nodeListArray);
//eZContentObject::fillNodeListAttributes($retNodeList);


		eZDebug::writeDebug(count($nodeListArray),'nodeListArray');
//		foreach($nodeListArray as $node)



//		$DB->dropTempTableList($sqlPermissionChecking['temp_tables']);
*/
		return array('result'=>array());
	}

}
?>