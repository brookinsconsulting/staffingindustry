<?php
$ModuleTools = ModuleTools::instance($Module);
$RootNodeID=SiteUtils::configSetting('NodeSettings','RootNode','content.ini');
$Limit=10;
$Parameters=array(
	'SortBy'=>array($SortBy?array($SortBy,false):array('modified',false),false),
	'Depth'=>100
);
$ViewParameters=array(
	'offset'=>isset($Params['Offset'])?$Params['Offset']:null,
	'subtree'=>isset($Params['SubTree'])?$Params['SubTree']:null,
	'featured'=>isset($Params['Featured'])?$Params['Featured']:null
	);

if($Class){
	$Parameters=array_merge($Parameters,array('ClassFilterType'=>'include','ClassFilterArray'=>explode(',',$Class)));
	if($Class=='article'){
		if(isset($Params['Featured']) && $Params['Featured']!== false){
				$Parameters=array_merge($Parameters,array('AttributeFilter'=>array(
					array('article/featured', '=', ($Params['Featured']=='yes')?true:false)
				)));
		}
		$Parameters['SortBy']=array('attribute',false,'article/publish_date');
	}
}
if(isset($Params['Offset'])){
	$Parameters=array_merge($Parameters,array('Offset'=>$Params['Offset']));
}
if(isset($Params['SubTree'])){
	$RootNodeID=$Params['SubTree'];
}

$Params['Limit']=10;
$Parameters=array_merge($Parameters,array('Limit'=>$Params['Limit']));
$Nodes=eZContentObjectTreeNode::subTreeByNodeID($Parameters,$RootNodeID);
$Params['Limit']=null;
$Parameters=array_merge($Parameters,array('Limit'=>$Params['Limit']));
$NodesCount=eZContentObjectTreeNode::subTreeCountByNodeID($Parameters,$RootNodeID);

$ModuleTools->setTemplateVariable('nodes',$Nodes);
$ModuleTools->setTemplateVariable('nodes_count',$NodesCount);
$ModuleTools->setTemplateVariable('view_parameters',$ViewParameters);
$ModuleTools->setTemplateVariable('limit',$Limit);
$ModuleTools->setTemplateVariable('url_alias','/website/updates'.($SortBy?"/$SortBy":'').($Class?"/$Class":''));

return $ModuleTools->result();
?>