<?php

class productiononlyOperator
{
	var $Operators;

	function __construct(){
		$this->Operators=array('production_only');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'production_only'=>array()
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){

		return true;
	}
}

?>