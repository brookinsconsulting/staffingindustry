<?php

class html_shortenOperator
{
	var $Operators;

	function html_shortenOperator($name="html_shorten"){
	$this->Operators = array( $name );
	}

	// Returns the template operators.
	function operatorList(){
	return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'html_shorten' => array(
				'wc_limit' => array('type'=>'numeric','required'=>false,'default'=>50),
				'append' => array('type'=>'string','required'=>false,'default'=>'...')
			)
		);
	}

	function modify($tpl, &$operatorName, &$operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue, &$namedParameters){
	
		if (mb_detect_encoding($operatorValue) != 'ISO-8859-1' && mb_detect_encoding($operatorValue) != 'UTF-8') {
			$operatorValue = iconv(mb_detect_encoding($operatorValue), "ISO-8859-1//TRANSLIT", $operatorValue);
		}

		$save_temp = $operatorValue;
		$save_temp2 = $operatorValue;
		preg_match_all("/<[^>]*?>/", $save_temp, $tag_matches);
		if($namedParameters['wc_limit']==0){
			if(count($tag_matches[0])){
				$save_temp3 = preg_replace("/<p>(&nbsp;|\s)*?<\/p>/", "", $save_temp2);
				$out = preg_replace("/(<[^>]*?>)(\s*)?$/", $namedParameters['append'].'$1$2', $save_temp3);
			}else{
				$out = preg_replace("/(\S)(\s*?)$/", '$1'.$namedParameters['append'].'$2', $save_temp2);
			}
			$operatorValue = $out;
			return true;
		}else{
			$resultstring='';
			$opentags = array();
			$max_length = $namedParameters['wc_limit'];
			$endstring = $namedParameters['append'];
			$cropped=false;

			foreach($tag_matches[0] as $key=>$this_tag_match){
				$splitarray = preg_split('/'.preg_quote($this_tag_match,'/').'/',$save_temp,2);
				$tagtype = preg_replace("/ .*\/?>/", ">", $this_tag_match);
				$tagtypeopen = preg_replace("/\//", "", $tagtype);
				if(strlen($splitarray[0])  > $max_length && !$cropped){
					$word_end = strpos($splitarray[0], ' ', $max_length);
					if(!$word_end){$word_end = strlen($splitarray[0]);}
					$resultstring .= substr($splitarray[0], 0, $word_end);
					$cropped=true;
					$resultstring .= $endstring;
				}else{
					if(!$cropped){
						$resultstring .= $splitarray[0];
						if(strpos($tagtype, '/') === false){
							$directional = 1;
						}else{
							$directional = -1;
						}
						$tagtypeopen = preg_replace("/\//", "", $tagtype);
						if(array_key_exists($tagtypeopen, $opentags) ){
							$opentags[$tagtypeopen] = $opentags[$tagtypeopen] + $directional;
						}else{
							$opentags[$tagtypeopen] = $directional;
						}
					}
					$max_length = $max_length - strlen($splitarray[0]);
				}
				if(!$cropped){
					$resultstring .= $this_tag_match;
				} else {
					if(strpos($tagtype, '/') == 1 && array_key_exists($tagtypeopen, $opentags) && $opentags[$tagtypeopen] > 0){
						$resultstring .= $tagtype;
						$opentags[$tagtypeopen] = $opentags[$tagtypeopen] -1;
					}
				}
				$save_temp = $splitarray[1];
			}

			if(count($tag_matches[0]) == 0){
				if(strlen($save_temp2)  > $max_length){
					$word_end = strpos($save_temp2, ' ', $max_length);
					if ($word_end) {
					    $resultstring .= substr($save_temp2, 0, $word_end);
    					$cropped=true;
    					$resultstring .= $endstring;
					} else {
						$cropped=false;
						$resultstring = $save_temp2;
					}
				}else{
					$resultstring = $save_temp2;
				}
			}

			$operatorValue = $resultstring;
		}
	}
}

?>
