<?php

	class debugOperator
{
	var $Operators;

	function debugOperator(){
		$this->Operators = array('debug', 'kill_debug', 'classes', 'shuffle');
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
        	return true;
	}

	function namedParameterList(){
	        return array(
			'debug' => array('label' => array('type'=>'string', 'required'=>false, 'default'=>'')),
			'kill_debug' => array('active' => array('type'=>'mixed', 'required'=>false, 'default'=>1))
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
	      switch ($operatorName) {
		case 'kill_debug':{
			$GLOBALS['eZDebugEnabled'] = !(bool)$namedParameters['active'];
			$operatorValue = '';
			return true;
			break;
		}
		case 'debug':{
			eZDebug::writeDebug($operatorValue, (isset($namedParameters['label'])?$namedParameters['label']:''));
			$operatorValue = '';
			return true;
			break;
		}
		case 'shuffle':{
			shuffle($operatorValue);
			return true;
			break;
		}
		case 'classes':{
			$db = eZDB::instance();
			$q = "select identifier from ezcontentclass";
			$out = array();
			$result = $db->arrayQuery($q);
			foreach ($result as $r) {
				$out[] = $r['identifier'];
			}
			$operatorValue = $out;
			return true;
			break;
		}
	   }
	}
}

?>
