<?php

	class CookieOperator
{

	static protected $Settings = false;
	static protected $AcceptCookies = true;
	static protected $CookieExclusions = array();

	var $Operators;

	function CookieOperator(){
		$this->Operators = array('cookie','has_cookie','delete_cookie');

		self::$Settings = eZINI::instance('acceptcookies.ini');
		if(self::$Settings){
			$CookieName = self::$Settings->variable('GeneralSettings', 'CookieName');
			self::$CookieExclusions = self::$Settings->variable('GeneralSettings', 'CookieExclusions');
			if(isset($_COOKIE[$CookieName])){
				self::$AcceptCookies = json_decode($_COOKIE[$CookieName]);
			}
		}
	}

	function &operatorList(){
		return $this->Operators;
	}

	function namedParameterPerOperator(){
		return true;
	}

	function namedParameterList(){
		return array(
			'cookie' => array(
				'name' => array('type'=>'string', 'required'=>true, 'default'=>''),
				'value' => array('type'=>'string', 'required'=>false, 'default'=>''),
				'expire' => array('type'=>'integer', 'required'=>false, 'default'=>0),
				'path' => array('type'=>'string', 'required'=>false, 'default'=>''),
				'domain' => array('type'=>'string', 'required'=>false, 'default'=>'.' . $_SERVER["HTTP_HOST"]),
				'secure' => array('type'=>'boolean', 'required'=>false, 'default'=>false),
				'httponly' => array('type'=>'boolean', 'required'=>false, 'default'=>false)
				),
			'has_cookie' => array(
				'name' => array('type'=>'string', 'required'=>true, 'default'=>''),
				'create' => array('type'=>'boolean', 'required'=>false, 'default'=>false)
				),
			'delete_cookie' => array(
				'name' => array('type'=>'string', 'required'=>true, 'default'=>'')
				)
		);
	}

	function modify(&$tpl, &$operatorName, &$operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters){
		switch($operatorName){
			case 'has_cookie':{
				$operatorValue=self::hasCookie($namedParameters['name'],$namedParameters['create']);
				break;
			}
			case 'delete_cookie':{
				call_user_func_array('self::setCookie',array($namedParameters['name'],'',time()-3600));
				$operatorValue='';
				break;
			}
			default:{
				$result=call_user_func_array('self::cookie',$namedParameters);
				$operatorValue=$result['has_value']?$result['value']:'';
			}
		}
		return true;
	}

	static function cookie(){
		if($count=func_num_args() && $name=func_get_arg(0)){
			$value=func_get_arg(1);
			if(gettype($name)=='string' && empty($value) && self::hasCookie(func_get_arg(0))){
				return array('has_value'=>true,'value'=>$_COOKIE[$name]);
			}
			return array('has_value'=>false,'value'=>call_user_func_array('self::setCookie',func_get_args()));
		}
		return array('has_value'=>false,'value'=>false);
	}

	static function hasCookie($name='',$create=false){
		if($create){
			return isset($_COOKIE[$name])?$_COOKIE[$name]:self::setCookie($name,'null');
		}
		return isset($_COOKIE[$name]);
	}

	static function setCookie(){
		if(self::$AcceptCookies || in_array(func_get_arg(0), self::$CookieExclusions)){
			return call_user_func_array('setcookie',func_get_args());
		}
		return false;
	}
}

?>
