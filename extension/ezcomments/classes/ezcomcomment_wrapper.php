<?php
/**
 * File containing ezcomComment class
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license http://ez.no/licenses/gnu_gpl GNU GPLv2
 *
 */

/**
 * ezcomComment persistent object class definition
 *
 */
class ezcomComment_wrapper extends ezcomComment
{
	
	public static function definition()
    {
        static $def = array( 'fields' => array( 'id' => array( 'name' => 'ID',
                                                               'datatype' => 'integer',
                                                               'default' => 0,
                                                               'required' => true ),
                                                'language_id' => array( 'name' => 'LanguageID',
                                                                        'datatype' => 'integer',
                                                                        'default' => 0,
                                                                        'required' => true ),
                                                'created' => array( 'name' => 'Created',
                                                                    'datatype' => 'integer',
                                                                    'default' => 0,
                                                                    'required' => true ),
                                                'modified' => array( 'name' => 'Modified',
                                                                     'datatype' => 'integer',
                                                                     'default' => 0,
                                                                     'required' => true ),
                                                'user_id' => array( 'name' => 'UserID',
                                                                    'datatype' => 'integer',
                                                                    'default' => 0,
                                                                    'required' => true ),
                                                'session_key' => array( 'name' => 'SessionKey',
                                                                        'datatype' => 'string',
                                                                        'default' => '',
                                                                        'required' => true ),
                                                'ip' => array( 'name' => 'IPAddress',
                                                               'datatype' => 'string',
                                                               'default' => '',
                                                               'required' => true ),
                                                'contentobject_id' => array( 'name' => 'ContentObjectID',
                                                                             'datatype' => 'integer',
                                                                             'default' => 0,
                                                                             'required' => true ),
                                                'parent_comment_id' => array( 'name' => 'ParentCommentID',
                                                                              'datatype' => 'integer',
                                                                              'default' => 0,
                                                                              'required' => true ),
                                                'name' => array( 'name' => 'Name',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => true ),
                                                'email' => array( 'name' => 'EMail',
                                                                  'datatype' => 'string',
                                                                  'default' => '',
                                                                  'required' => true ),
                                                'url' => array( 'name' => 'URL',
                                                                'datatype' => 'string',
                                                                'default' => '',
                                                                'required' => true ),
                                                'text' => array( 'name' => 'Text',
                                                                 'datatype' => 'string',
                                                                 'default' => '',
                                                                 'required' => true ),
                                                'status' => array( 'name' => 'Status',
                                                                   'datatype' => 'integer',
                                                                   'default' => 0,
                                                                   'required' => true ),
                                                'title' => array( 'name' => 'Title',
                                                                  'datatype' => 'string',
                                                                  'default' => '',
                                                                  'required' => true ) ),
                             'keys' => array( 'id' ),
                             'function_attributes' => array(
                                                            'contentobject' => 'object',
															'url_alias' => 'url_alias',
															'contentObjectAttributes' => 'contentObjectAttributes',
 															'class_identifier' => 'class_identifier',
															'class_name' => 'class_name',
															'data_map' => 'data_map',
															'object' => 'object',
															'assigned_nodes' => 'assigned_nodes',
															'can_remove' => 'can_remove',
															'version' => 'object',
															'current' => 'object',
															'main_node' => 'get_main_node',
															'published' => 'published'),
                             'increment_key' => 'id',
                             'class_name' => 'ezcomComment_wrapper',
                             'name' => 'ezcomment' );
        return $def;
    }

	public function assigned_nodes() {
		return array();
	}
	
	public function purge() {
		return true;
	}

	public function can_remove() {
		return true;
	}
	
	public function get_main_node() {
		$ob = eZContentObject::fetch($this->attribute('contentobject_id'));		
		return $ob->mainNode();
	}

	public function url_alias() {
		$ob = eZContentObject::fetch($this->attribute('contentobject_id'));		
		return $ob->mainNode()->urlAlias();
	}

	public function contentObjectAttributes() {
		$def = self::definition();
		$out = array();
		foreach (array_keys($def['fields']) as $f) {
			$out[] = new ezcomComment_wrapper_attribute($f, $this->attribute($f));
		}
		return $out;
	}

	public function class_identifier() {
		return 'ezcomment';
	}
	
	public function class_name() {
		return 'ezcomment';
	}
	
	public function object() {
		return $this;
	}
	
	public function published() {
		return $this->attribute('created');
	}
	
	public function data_map() {
		$dm = eZUser::currentUser()->contentObject()->dataMap();
		$at = array_shift($dm);
		$dome = clone($at);
		$dome->setAttribute('data_text', $this->attribute('text'));
		return array('text' => $dome);
	}

    public static function fetchByContentObjectIDList( $objectIDList = null, $userID = null, $languageCode = null, $status = null, $sorts = null, $offset = null, $length = null, $extraCondition = array() )
    {
        $cond = array();

        // object id list
        if( $objectIDList !== null && !is_array( $objectIDList ) )
        {
            return null;
        }
        if( is_array( $objectIDList ) )
        {
            $cond['contentobject_id'] = array( $objectIDList );
        }

        // user id
        if( $userID !== null )
        {
            $cond['user_id'] = $userID;
        }

        // language id
        if( $languageCode === null )
        {
            $ini = eZINI::instance();
            $languageCode = $ini->variable( 'RegionalSettings' , 'ContentObjectLocale' );
        }
		if ($languageCode != 'all') {
	        $languageID = eZContentLanguage::fetchByLocale( $languageCode )->attribute( 'id' );
	        $cond['language_id'] = $languageID;
		}

        // status
        if( $status !== null )
        {
            $cond['status'] = $status;
        }
        $cond = array_merge( $cond, $extraCondition );

        $limit = array( 'offset' => $offset, 'length' => $length );

        $result = eZPersistentObject::fetchObjectList( self::definition(), null, $cond, $sorts, $limit );
        return $result;
    }

    static function fetch( $id )
    {
        $cond = array( 'id' => $id );
        $return = eZPersistentObject::fetchObject( self::definition(), null, $cond );
        return $return;
    }

}

class  ezcomComment_wrapper_attribute {
	
	var $attribut_name;
	var $attribute_value;
	
	function __construct($f, $v = '') {
		$this->attribute_name = $f;
		$this->attribute_value = $v;
	}
	
	function attribute($type) {
		if ($type == 'content') return $this->attribute_value;
		if ($type == 'contentclass_attribute_identifier') return $this->attribute_name;
		return 'text';
	}
	
	function hasContent() {
		return true;
	}
	
}

?>
