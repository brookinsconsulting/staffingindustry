<?php

$sys = eZSys::instance();
$access_r = $sys->AccessPath;
$access = $access_r['siteaccess']['name'];

if ( !$isQuiet )
    $cli->output( "Generating Sitemap...\n"  );

    
// Get a reference to eZINI. append.php will be added automatically.
$ini = eZINI::instance( 'site.ini' );
$googlesitemapsINI = eZINI::instance( 'googlesitemaps.ini' );

$depth_r = $googlesitemapsINI->variable('NodeSettings','Node_Depth_Priority');

// Settings variables
if ( $googlesitemapsINI->hasVariable( 'all2eGoogleSitemapSettings', 'SitemapRootNodeID' ) &&
	 $googlesitemapsINI->hasVariable( 'all2eGoogleSitemapSettings', 'Filename' ) &&
	 $googlesitemapsINI->hasVariable( 'all2eGoogleSitemapSettings', 'Path' ) &&
	 $googlesitemapsINI->hasVariable( 'Classes', 'ClassFilterType' ) &&
	 $googlesitemapsINI->hasVariable( 'Classes', 'ClassFilterArray' ) &&
	 $ini->hasVariable( 'SiteSettings','SiteURL' )
	 )
{
    $sitemapRootNodeID = $googlesitemapsINI->variable( 'all2eGoogleSitemapSettings','SitemapRootNodeID' );
    
	$sitemapFilename = $googlesitemapsINI->variable( 'all2eGoogleSitemapSettings','Filename' );
    $sitemapPath = $googlesitemapsINI->variable( 'all2eGoogleSitemapSettings','Path' );
    $xmlDataFile = $sitemapPath . $sitemapFilename;
    
    $classFilterType = $googlesitemapsINI->variable( 'Classes','ClassFilterType' );
    $classFilterArray = $googlesitemapsINI->variable( 'Classes','ClassFilterArray' );
    
    $siteURL = $ini->variable( 'SiteSettings','SiteURL' );
	
	if (!file_exists($sitemapPath)) mkdir($sitemapPath);
}
else
{
    eZDebug::writeError( 'Missing INI Variables in configuration block GeneralSettings.' );
    return;
}

// Get the Sitemap's root node
$rootNode = eZContentObjectTreeNode::fetch( $sitemapRootNodeID );

if (!is_object($rootNode)) {
	eZDebug::writeError( 'Invalid SitemapRootNodeID in configuration block GeneralSettings.' );
	return;
}

// Fetch the content tree
$nodeArray =& $rootNode->subTree( array( 'ClassFilterType' => $classFilterType,
                                         'ClassFilterArray' => $classFilterArray ) );
                                         
                                         
array_unshift($nodeArray , $rootNode);

$xmlRoot = "urlset";
$xmlNode = "url";

// Define XML Child Nodes
$xmlSubNodes = array("loc","lastmod","changefreq","priority");

// Create the DOMnode
$dom = new DOMDocument("1.0","UTF-8");

// Create DOM-Root (urlset)
$root = $dom->createElement($xmlRoot);
$root->setAttribute( "xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9" );
$root = $dom->appendChild($root);

// Generate XML data
$namedParameters=array('parameters'=>array('quotes'=>false));

foreach ($nodeArray as $node_k => $subTreeNode) {
	if (!is_object($subTreeNode)) continue; 

	$linkme = clone $subTreeNode;
	$trash = SiteLinkOperator::sitelink($linkme, $namedParameters);
	$urlAlias = $linkme;
	if (strpos($urlAlias, 'http') !== 0) $urlAlias = "http://" .$ini->variable( 'SiteSettings','SiteURL' ) . $access . "/" . $subTreeNode->urlAlias() . "/?cookies=disabled";
	if (strpos($urlAlias, 'Test-Area') > 0) continue;

	$urlAlias = preg_replace("/\/\/\//", "//", $urlAlias);
	$object = $subTreeNode->object();
	$depth = $subTreeNode->attribute( 'depth' );
	$modified = date("c" , $object->attribute( 'modified' ));	
	
	if (empty($urlAlias)) continue;
	
	// Create new url element
	$node = $dom->createElement($xmlNode);
	// append to root node
	$node = $root->appendChild($node);
	
	// create new url subnode
	$subNode = $dom->createElement($xmlSubNodes[0]);
	$subNode = $node->appendChild($subNode);
	// set text node with data
	$date = $dom->createTextNode($urlAlias);
	$date = $subNode->appendChild($date); 

	// create modified subnode
	$subNode = $dom->createElement($xmlSubNodes[1]);
	$subNode = $node->appendChild($subNode);	
	// set data
	$lastmod = $dom->createTextNode($modified);
	$lastmod = $subNode->appendChild($lastmod);
	
	$depth = $subTreeNode->attribute('depth') -1;
	if ($depth > (count($depth_r) - 1)) $depth = count($depth_r) - 1;
	
	// create priority subnode
	$subNode = $dom->createElement($xmlSubNodes[3]);
	$subNode = $node->appendChild($subNode);	
	// set data
	$lastmod = $dom->createTextNode($depth_r[$depth]);
	$lastmod = $subNode->appendChild($lastmod);
	
}

// write XML Sitemap to file
$dom->save($xmlDataFile);

if ( !$isQuiet )
    $cli->output( "Sitemap has been generated!\n" );

?>
