<?php

class AddNewGroupRolesType extends eZWorkflowEventType
{

	const ID="addnewgrouproles";

	function __construct()
	{
		$this->eZWorkflowEventType( AddNewGroupRolesType::ID, "Add New Group Roles" );
		$this->setTriggerTypes( array(
            'shop' => array(
                'checkout' => array(
                    'after'
                )
            )
        ) );
	}

	function execute( $process, $event )
	{
		$parameters = $process->attribute( 'parameter_list' );

		$order = eZOrder::fetch($parameters['order_id']);

		foreach ($order->productCollection()->itemList() as $prod) {
			$dmap = eZContentObject::fetch($prod->ContentObjectID)->fetchDataMap();
			if (array_key_exists('related_roles', $dmap)) {
				$roles_r = explode(",", $dmap['related_roles']->content());
				if (is_array($roles_r) && $roles_r[0] !="") {
					foreach ($roles_r as $rk => $role) {
						$r_i = trim($role) + 0;
						$role = eZRole::fetch( $r_i );
						$db = eZDB::instance();
						$db->begin();
						$role->assignToUser( $parameters['user_id'] );
						eZRole::expireCache();
						$db->commit();
						eZUser::cleanupCache();
					}
				}
			}

		}

		return eZWorkflowType::STATUS_ACCEPTED;
	}
}

eZWorkflowEventType::registerEventType(AddNewGroupRolesType::ID, "addnewgrouprolestype" );

?> 