<?php

class MoveUserAfterRegType extends eZWorkflowEventType
{
	const ID = "moveuserafterreg";

	function __construct()
	{
		$this->eZWorkflowEventType( MoveUserAfterRegType::ID, "Move User After Registration" );
		$this->setTriggerTypes( array( 'content' => array( 'publish' => array( 'after' ) ) ) );
	}

	function execute($process, $event){
		$parameters = $process->attribute( 'parameter_list' );
		$objectID = $parameters['object_id'];
		$object = eZContentObject::fetch( $parameters['object_id'] );
		$cur_node = $object->mainNodeID();
		$data = $object->attribute( 'data_map' );
		$type = $data['type']->content();
		if (!is_array($type)) return eZWorkflowType::STATUS_ACCEPTED;
		$goodtomove = false;
		foreach ($object->assignedNodes(false) as $ans) {
			if ($ans['parent_node_id'] == 12) $goodtomove = true;
		} 
		if($goodtomove){
			if ($type[0] == 1) {
				eZContentObjectTreeNodeOperations::move( $cur_node, 4336 );
				eZContentObject::fixReverseRelations( $objectID, 'move' );
				eZContentCacheManager::clearAllContentCache();
				eZUser::cleanupCache();
			} else {	
				eZContentObjectTreeNodeOperations::move( $cur_node, 440 );
				eZContentObject::fixReverseRelations( $objectID, 'move' );
				eZContentCacheManager::clearAllContentCache();
				eZUser::cleanupCache();			
			}
		}
		return eZWorkflowType::STATUS_ACCEPTED;
	}
}
eZWorkflowEventType::registerEventType( MoveUserAfterRegType::ID, "moveuserafterregtype" );

?> 
