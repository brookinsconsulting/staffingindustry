<?php
/**
 * File containing the ezpMultivariateTestScenario class.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package extension
 */

class ezpMultivariateTestScenario extends eZPersistentObject
{
    /**
     * Constructor
     *
     * @param array $row
     */
    public function __construct( $row = array() )
    {
        parent::__construct( $row );
    }

    /**
     * Return the definition for the object
     *
     * @static
     * @return array
     */
    public static function definition()
    {
        return array( 'fields' => array( 'id' => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => '0',
                                                        'required' => true ),
                                         'node_id' => array( 'name' => 'NodeID',
                                                             'datatype' => 'integer',
                                                             'default' => '0',
                                                             'required' => true ),
                                         'name' => array( 'name' => 'Name',
                                                          'datatype' => 'string',
                                                          'default' => '',
                                                          'required' => false ),
                                         'is_enabled' => array( 'name' => 'IsEnabled',
                                                                'datatype' => 'integer',
                                                                'default' => '0',
                                                                'required' => false ),
                                         'created' => array( 'name' => 'Created',
                                                             'datatype' => 'integer',
                                                             'default' => '0',
                                                             'required' => false ) ),
                      'keys' => array( 'id' ),
                      'increment_key' => 'id',
                      'function_attributes' => array( 'node' => 'fetchNode',
                                                      'test_item_list' => 'fetchTestItemList',
                                                      'random_test_item' => 'getRandomTestItem',
                                                      'source_probability' => 'getSourceProbability' ),
                      'class_name' => 'ezpMultivariateTestScenario',
                      'sort' => array( 'id' => 'asc' ),
                      'name' => 'ezmultivariate_test_scenario' );
    }

    /**
     * Fetches multivariate test scenario by id
     *
     * @static
     * @param integer $id
     * @param bool $asObject
     * @return ezpMultivariateTestScenario|null
     */
    public static function fetch( $id, $asObject = true )
    {
        return parent::fetchObject( self::definition(), null, array( 'id' => $id ), $asObject );
    }

    /**
     * Fetches multivariate test scenario by node id
     *
     * @static
     * @param integer $nodeId
     * @param bool $asObject
     * @return ezpMultivariateTestScenario|null
     */
    public static function fetchByNodeID( $nodeId, $asObject = true )
    {
        return parent::fetchObject( self::definition(), null, array( 'node_id' => $nodeId ), $asObject );
    }

    /**
     * Fetches list of scenarios by node id
     *
     * @static
     * @param integer $nodeId
     * @param bool $asObject
     * @return array|null
     */
    public static function fetchListByNodeID( $nodeId, $asObject = true )
    {
        return parent::fetchObjectList( self::definition(), null, array( 'node_id' => $nodeId ), null, null, $asObject );
    }

    /**
     * Fetches currently enabled test scenario
     *
     * @static
     * @param integer $nodeId
     * @param bool $asObject
     * @return ezpMultivariateTestScenario|null
     */
    public static function fetchEnabledByNodeID( $nodeId, $asObject = true )
    {
        return parent::fetchObject( self::definition(), null, array( 'node_id' => $nodeId, 'is_enabled' => 1 ), $asObject );
    }

    /**
     * Helper method to fetch random test item
     *
     * @return ezpMultivariateTestItem
     */
    public function getRandomTestItem()
    {
        $probability = 0;

        $testItemList[] = new ezpMultivariateTestItem( array( 'object_id' => $this->attribute( 'node' )->attribute( 'contentobject_id' ),
                                                              'probability' => $this->attribute( 'source_probability' ),
                                                              'scenario_id' => $this->attribute( 'id' ) ) );
        $testItemList = array_merge( $testItemList, $this->attribute( 'test_item_list' ) ) ;
        foreach ( $testItemList  as $testItem )
            $probability += $testItem->attribute( 'probability' );

        $random = round( mt_rand( 0, $probability ) );

        $max = $i = 0;
        do
            $max += $testItemList[$i++]->attribute( 'probability' );
        while( $random > $max );

        return $testItemList[$i-1];
    }

    /**
     * Helper method to get a node
     *
     * @return eZContentObjectTreeNode
     */
    public function getRandomNode()
    {
        $contentObject = $this->getRandomTestItem()->attribute( 'content_object' );
        if ( $contentObject instanceof eZContentObject )
            return $contentObject->attribute( 'main_node' );
    }

    /**
     * Helper method to fetch a node
     *
     * @return eZContentObjectTreeNode
     */
    public function fetchNode()
    {
        return eZContentObjectTreeNode::fetch( $this->attribute( 'node_id' ) );
    }

    /**
     * Helper method to fetch collection with multivariate test items
     *
     * @return array|null
     */
    public function fetchTestItemList()
    {
        return ezpMultivariateTestItem::fetchListByScenarioID( $this->attribute( 'id' ) );
    }

    /**
     * Searches for test scenario for given node
     *
     * @static
     * @param eZContentObjectTreeNode $node
     * @return ezpMultivariateTestScenario|null
     */
    public static function findScenarioByNode( eZContentObjectTreeNode $node )
    {
        $nodeId = $node->attribute( 'node_id' );
        $scenario = self::fetchByNodeID( $nodeId );

        if ( !$scenario instanceof ezpMultivariateTestScenario )
        {
            $testItem = ezpMultivariateTestItem::fetchByObjectID( $node->attribute( 'contentobject_id' ) );

            if ( $testItem instanceof ezpMultivariateTestItem )
                $scenario = $testItem->attribute( 'scenario' );
        }

        return $scenario;
    }

    /**
     * Helper method to calculate source page probability
     *
     * @return int
     */
    public function getSourceProbability()
    {
        $probability = 100;

        foreach( $this->attribute( 'test_item_list' ) as $testItem )
            $probability -= $testItem->attribute( 'probability' );

        return $probability;
    }
}
