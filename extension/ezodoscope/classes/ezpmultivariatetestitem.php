<?php
/**
 * File containing the ezpMultivariateTestItem class.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package extension
 */

class ezpMultivariateTestItem extends eZPersistentObject
{
    /**
     * Constructor
     *
     * @param array $row
     */
    public function __construct( $row = array() )
    {
        parent::__construct( $row );
    }

    /**
     * Return the definition for the object
     *
     * @static
     * @return array
     */
    public static function definition()
    {
        return array( 'fields' => array( 'id' => array( 'name' => 'ID',
                                                        'datatype' => 'integer',
                                                        'default' => '0',
                                                        'required' => true ),
                                         'object_id' => array( 'name' => 'ObjectID',
                                                               'datatype' => 'integer',
                                                               'default' => '0',
                                                               'required' => true ),
                                         'probability' => array( 'name' => 'Probability',
                                                                 'datatype' => 'integer',
                                                                 'default' => '0',
                                                                 'required' => false ),
                                         'scenario_id' => array( 'name' => 'ScenarioID',
                                                                 'datatype' => 'integer',
                                                                 'default' => '0',
                                                                 'required' => false,
                                                                 'foreign_class' => 'ezpMultivariateTestScenario',
                                                                 'foreign_attribute' => 'id',
                                                                 'multiplicity' => '1..*') ),
                      'keys' => array( 'id', 'scenario_id' ),
                      'increment_key' => 'id',
                      'function_attributes' => array( 'content_object' => 'fetchContentObject',
                                                      'scenario' => 'fetchScenario' ),
                      'class_name' => 'ezpMultivariateTestItem',
                      'sort' => array( 'id' => 'asc' ),
                      'name' => 'ezmultivariate_test_item' );
    }

    /**
     * Fetches multivariate test item by id
     *
     * @static
     * @param integer $id
     * @param bool $asObject
     * @return ezpMultivariateTestItem|null
     */
    public static function fetch( $id, $asObject = true )
    {
        return parent::fetchObject( self::definition(), null, array( 'id' => $id ), $asObject );
    }

    /**
     * Fetches test item by scenario and object ID
     *
     * @static
     * @param integer $scenarioId
     * @param integer $objectId
     * @param bool $asObject
     * @return ezpMultivariateTestItem|null
     */
    public static function fetchByScenarioAndObjectID( $scenarioId, $objectId, $asObject = true )
    {
        return parent::fetchObject( self::definition(), null, array( 'scenario_id' => $scenarioId, 'object_id' => $objectId ), $asObject );
    }

    /**
     * Fetches test item by object ID
     *
     * @static
     * @param integer $objectId
     * @param bool $asObject
     */
    public static function fetchByObjectID( $objectId, $asObject = true )
    {
        return parent::fetchObject( self::definition(), null, array( 'object_id' => $objectId ), $asObject );
    }

    /**
     * Fetches list of test items by scenario id
     *
     * @static
     * @param $scenarioId
     * @param bool $asObject
     * @return array|null
     */
    public static function fetchListByScenarioID( $scenarioId, $asObject = true )
    {
        return parent::fetchObjectList( self::definition(), null, array( 'scenario_id' => $scenarioId ), null, null, $asObject );
    }

    /**
     * Helper method to fetch content object
     *
     * @return eZContentObject|null
     */
    public function fetchContentObject()
    {
        return eZContentObject::fetch( $this->attribute( 'object_id' ) );
    }

    /**
     * Helper method to fetch multivariate test scenario object
     *
     * @return ezpMultivariateTestItem|null
     */
    public function fetchScenario()
    {
        return ezpMultivariateTestScenario::fetch( $this->attribute( 'scenario_id' ) );
    }
}
