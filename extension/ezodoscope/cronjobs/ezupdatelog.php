<?php

/**
 * File containing the ezodoscope_updatelog CLI script for parsing the webserver log files and uploading a ziped version of the tracked information
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package ezodoscope
 */

if ( !$isQuiet )
{
    $cli->output( "Starting..." );
}

try
{
    $ini = eZINI::instance( 'odoscope.ini' );
    $db = eZDB::instance();

    $rows = $db->arrayQuery( "SELECT * FROM ezsite_data WHERE name LIKE 'odoscope_key'" );

    if ( count( $rows ) == 0 || !$rows )
    {
        $script->shutdown( 1, 'odoscope is not activated.' );
    }

    foreach ( $rows as $row )
    {
        $oscKey = $row['value'];
    }

    $logFile = $ini->variable( 'AccessLogSettings', 'Path' );

    if ( !$logFile )
    {
        $script->shutdown( 1, 'Please fill in the path of the web access log file in odoscope.ini' );
    }

    $logFileHandler = fopen( $logFile, 'r' );

    if ( $logFileHandler )
    {

        $cli->output( 'access.log file found at: ' . $logFile );
        $cli->output( 'Now analyzing...' );

        $timestamp = date( '_Ymd_Gis' );

        $cacheDir = eZSys::varDirectory() . '/cache/ezodoscope';
        eZDir::mkdir( $cacheDir );

        $outputFilePath = $cacheDir . '/osc' . $timestamp . '.txt';
        $outputFileHandler = fopen( $outputFilePath, "w" );

        if ( $outputFileHandler == false )
        {
            $script->shutdown( 1, 'Error writing file: ' . $outputFilePath );
        }

        $regex = '/(?<datetime>\d{2}\x2F\w{3}\x2F\d{4}:\d{2}:\d{2}:\d{2}\x20[^\x5D]*)/';

        $lastLogUpdate = $db->arrayQuery( "SELECT * FROM ezsite_data WHERE name LIKE 'last_log_update'" );

        if ( count( $lastLogUpdate ) == 0 || !$rows )
        {
            $lastLogUpdateTimestamp = 0;
        }
        else
        {
            $lastLogUpdateTimestamp = $lastLogUpdate[0]['value'];
        }

        $newLastLogUpdateTimestamp = $lastLogUpdateTimestamp;

        $logUpdated = false;

        $line = '';

        while ( !feof( $logFileHandler ) )
        {
            $line = fgets( $logFileHandler, 4096 );

            if ( stristr( $line, "osc.gif?" ) !== false )
            {
                preg_match( $regex, $line, $match );

                if ( $match && $match[0] != '' )
                {
                    if ( strtotime( $match[0] ) > $lastLogUpdateTimestamp )
                    {
                        $newLastLogUpdateTimestamp = strtotime( $match[0] );

                        // Replace last octet of the client IP address with .0
                        if ( $ini->variable( 'PrivacySettings', 'IPAddressBlurring' ) == 'enabled' )
                        {
                            preg_match_all( '/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $line, $matches );

                            foreach ( $matches[0] as $ip )
                            {
                                $line = str_replace( $ip, substr( $ip, 0, strrpos( $ip, '.' ) ) . '.0', $line );
                            }
                        }

                        fwrite( $outputFileHandler, $line );

                        $logUpdated = true;
                    }
                }
            }
        }

        fclose( $logFileHandler );
        fclose( $outputFileHandler );

        if ( $logUpdated == false )
        {
            $script->shutdown( 1, 'Nothing to update. Sending not necessary.' );
        }

        $compressedFileName = 'osc' . $timestamp . '.txt.bz2';
        $compressedFilePath = $cacheDir . '/' . $compressedFileName;

        if ( ( $data = file_get_contents( $outputFilePath ) ) != false )
        {
            if ( file_put_contents( 'compress.bzip2://' . $compressedFilePath, $data ) == false )
            {
                $script->shutdown( 1, 'Can not create a bzip2 file: ' . $compressedFilePath );
            }
        }
        else
        {
            $script->shutdown( 1, 'Can not read data source file: ' . $outputFilePath );
        }

        unlink( $outputFilePath );

        $cli->output( "Uploading log-file..." );

        $httpRequest = new ezpHttpRequest( "https://ez.odoscope.com/Account/eZUploadLogs", HttpRequest::METH_POST );
        $httpRequest->addPostFile( 'uploadfile', $compressedFilePath );
        $httpRequest->addPostFields( array( 'sitekey' => $oscKey ) );
        $result = $httpRequest->send();

        if ( $result->getResponseCode() != 200 )
        {
            $script->shutdown( 1, 'Sending log failed.' );
        }
    }
    else
    {
        $script->shutdown( 1, 'Access-Log not available. Wrong path?' );
    }

    $db->begin();
    $db->query( "DELETE FROM ezsite_data WHERE name LIKE 'last_log_update'" );
    $db->query( "INSERT INTO ezsite_data ( name, value ) VALUES ( 'last_log_update', '$newLastLogUpdateTimestamp' )" );
    $db->commit();

}
catch ( Exception $e )
{
    if ( $e instanceof HttpException )
    {
        if ( isset( $e->innerException ) )
        {
            $script->shutdown( 1, 'Error: ' . $e->innerException->getMessage() );
        }
    }
    else
    {
        $script->shutdown( 1, 'Error: ' . $e->getMessage() );
    }
}

if ( !$isQuiet )
{
    $cli->output( "Done." );
}

?>
