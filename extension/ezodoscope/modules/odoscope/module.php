<?php

/**
 * File containing the eZ Publish odoscope module implementation.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package ezodoscope
 */

$module = array( 'name' => 'odoscope' );
$ViewList = array();
$ViewList['activate'] = array( 'script' => 'activate.php',
							    'default_navigation_part' => 'odoscopenavigationpart',
                               'functions' => array( 'activate' ));
$ViewList['view'] = array( 'script' => 'view.php',
							    'default_navigation_part' => 'odoscopenavigationpart',
                               'functions' => array( 'view' ));
 
$FunctionList = array(); 
$FunctionList['activate'] = array(); 
$FunctionList['view'] = array(); 
?>