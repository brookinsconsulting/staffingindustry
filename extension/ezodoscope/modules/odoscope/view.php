<?php

/**
 * File containing the eZ Publish odoscope view implementation.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package ezodoscope
 */


$tpl = eZTemplate::factory();
$db = eZDB::instance();

$rows = $db->arrayQuery( "SELECT * FROM ezsite_data WHERE name LIKE 'odoscope_key'" );
$rowsUsr = $db->arrayQuery( "SELECT * FROM ezsite_data WHERE name LIKE 'odoscope_name'" );

if ( count( $rows ) == 0 && count( $rowsUsr ) == 0 )
{
    $schema = $_SERVER['SERVER_PORT'] == '443' ? 'https' : 'http';
    $host = strlen( $_SERVER['HTTP_HOST'] ) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
    $uri = str_replace( 'odoscope/view', 'odoscope/activate', $_SERVER['REQUEST_URI'] );

    header( "Location: " . $schema . "://" . $host . $uri );
}
else
{
    $postData = array(
        'userName' => $rowsUsr[0]['value'],
        'siteKey' => $rows[0]['value']
    );

    $httpRequest = new ezpHttpRequest( 'https://ez.odoscope.com/Account/eZLogOn', HttpRequest::METH_POST );
    $httpRequest->addPostFields( $postData );
    $httpRequest->addHeaders( array( 'User-Agent' => $_SERVER['HTTP_USER_AGENT'] ) );

    try
    {
        $result = $httpRequest->send();

        if ( stristr( $result->getBody(), 'error' ) === false )
        {
            $auth = true;
            $tpl->setVariable( 'user_name', $postData['userName'] );
            $tpl->setVariable( 'key', $result->getBody() );
        }
        else
        {
            $auth = $result->getBody();
        }
    }
    catch ( HttpException $e )
    {
        $auth = $e->getMessage();
    }

    $tpl->setVariable( 'auth', $auth );
}

$tpl->setVariable( 'persistent_variable', false );

$Result = array();
$Result['content'] = $tpl->fetch( 'design:odoscope/view.tpl' );
$Result['path'] = array(
    array(
        'url' => 'odoscope/view',
        'text' => 'odoscope'
    )
);

$contentInfoArray['persistent_variable'] = false;
if ( $tpl->variable( 'persistent_variable' ) !== false )
    $contentInfoArray['persistent_variable'] = $tpl->variable( 'persistent_variable' );

$Result['content_info'] = $contentInfoArray;


?>
