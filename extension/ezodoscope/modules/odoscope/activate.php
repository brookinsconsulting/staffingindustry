<?php

/**
 * File containing the eZ Publish odoscope activate implementation.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package ezodoscope
 */

$tpl = eZTemplate::factory();
$http = eZHTTPTool::instance();
$db = eZDB::instance();

$emptyFields = false;
$hasError = false;
$errorText = '';
if ( $http->hasPostVariable( 'send' ) )
{
    if ( $http->postVariable( 'sitename' ) != '' && $http->postVariable( 'ezsitekey' ) != '' )
    {
        $current_user = eZUser::currentuser();

        $guid = uniqid( md5( $http->postVariable( 'sitename' ) ), true );

        $postData = array(
            "userName" => $guid,
            "eMail" => $current_user->Email,
            "siteName" => $db->escapeString( $http->postVariable( 'sitename' ) ),
            "ezInstallationKey" => $db->escapeString( $http->postVariable( 'ezsitekey' ) )
        );

        $httpRequest = new ezpHttpRequest( 'https://ez.odoscope.com/Account/eZRegisterSite', HttpRequest::METH_POST );
        $httpRequest->addPostFields( $postData );
        $httpRequest->addHeaders( array( 'User-Agent' => $_SERVER['HTTP_USER_AGENT'] ) );

        try
        {
            $result = $httpRequest->send();
            $message = $result->getBody();

            if ( stristr( $message, 'error' ) === false )
            {
                $rows = $db->arrayQuery( "SELECT * FROM ezsite_data WHERE name LIKE 'odoscope_key'" );
                $rowsUsr = $db->arrayQuery( "SELECT * FROM ezsite_data WHERE name LIKE 'odoscope_name'" );

                if ( count( $rows ) == 0 && count( $rowsUsr ) == 0 )
                {
                    $db->begin();

                    $db->query( "INSERT INTO ezsite_data ( name, value ) VALUES ( 'odoscope_key', '{$message}' )" );
                    $db->query( "INSERT INTO ezsite_data ( name, value ) VALUES ( 'odoscope_name', '$guid' )" );

                    $db->commit();

                    $tpl->setVariable( 'insertSuccess', 1 );

                }
                else
                {
                    $tpl->setVariable( 'insertSuccess', 0 );
                }
            }
            else
            {
                $errorText = $message;
                $hasError = true;
            }
        }
        catch ( HttpException $e )
        {
            $errorText = $e->getMessage();
            $hasError = true;
        }
    }
    else
    {
        $emptyFields = true;
        $hasError = true;
    }

    if ( $hasError )
    {
        $tpl->setVariable( 'error', 1 );
        $tpl->setVariable( 'errorText', $errorText );
        $tpl->setVariable( 'emptyField', ( $emptyFields == true ) ? 1 : 0 );
        $tpl->setVariable( 'sitename', $http->postVariable( 'sitename' ) );
        $tpl->setVariable( 'ezsitekey', $http->postVariable( 'ezsitekey' ) );
    }
}
elseif ( $http->hasPostVariable( 'reactivate' ) )
{
    $tpl->setVariable( 'reactivate', 1 );
    $Result = array();
    $Result['content'] = $tpl->fetch( 'design:odoscope/activate.tpl' );
    $Result['left_menu'] = "design:odoscope/leftmenu.tpl";

    $Result['path'] = array(
        array(
            'url' => 'odoscope/activate',
            'text' => 'odoscope'
        )
    );
}
elseif ( $http->hasPostVariable( 'doreactivate' ) )
{
    if ( $http->postVariable( 'sitename' ) != '' && $http->postVariable( 'ezsitekey' ) != '' )
    {
        $current_user = eZUser::currentuser();

        $guid = uniqid( md5( $http->postVariable( 'sitename' ) ), true );

        $postData = array(
            "userName" => $guid,
            "eMail" => $current_user->Email,
            "siteName" => $db->escapeString( $http->postVariable( 'sitename' ) ),
            "ezInstallationKey" => $db->escapeString( $http->postVariable( 'ezsitekey' ) )
        );

        $httpRequest = new ezpHttpRequest( 'https://ez.odoscope.com/Account/eZRegisterSite', HttpRequest::METH_POST );
        $httpRequest->addPostFields( $postData );
        $httpRequest->addHeaders( array( 'User-Agent' => $_SERVER['HTTP_USER_AGENT'] ) );

        try
        {
            $result = $httpRequest->send();
            $message = $result->getBody();

            if ( stristr( $message, 'error' ) === false )
            {
                $db->begin();

                $db->query( "DELETE FROM ezsite_data WHERE name LIKE 'odoscope_key'" );
                $db->query( "DELETE FROM ezsite_data WHERE name LIKE 'odoscope_name'" );
                $db->query( "INSERT INTO ezsite_data ( name, value ) VALUES ( 'odoscope_key', '{$message}' )" );
                $db->query( "INSERT INTO ezsite_data ( name, value ) VALUES ( 'odoscope_name', '$guid' )" );

                $db->commit();

                $tpl->setVariable( 'insertSuccess', 1 );
            }
            else
            {
                $errorText = $message;
                $hasError = true;
            }
        }
        catch ( HttpException $e )
        {
            $errorText = $e->getMessage();
            $hasError = true;
        }
    }
    else
    {
        $emptyFields = true;
        $hasError = true;
    }

    if ( $hasError )
    {
        $tpl->setVariable( 'error_reactivate', 1 );
        $tpl->setVariable( 'errorText', $errorText );
        $tpl->setVariable( 'reactivate_emptyField', ( $emptyFields == true ) ? 1 : 0 );
        $tpl->setVariable( 'sitename', $http->postVariable( 'sitename' ) );
        $tpl->setVariable( 'ezsitekey', $http->postVariable( 'ezsitekey' ) );
    }
}
else
{
    $rows = $db->arrayQuery( "SELECT * FROM ezsite_data WHERE name LIKE 'odoscope_key'" );

    if ( count( $rows ) == 0 )
    {
        $tpl->setVariable( 'activated', 0 );
    }
    else
    {
        $tpl->setVariable( 'activated', 1 );
    }
}

$tpl->setVariable( 'persistent_variable', false );

$Result = array();
$Result['content'] = $tpl->fetch( 'design:odoscope/activate.tpl' );
$Result['path'] = array(
    array(
        'url' => 'odoscope/activate',
        'text' => 'odoscope'
    )
);

$contentInfoArray['persistent_variable'] = false;
if ( $tpl->variable( 'persistent_variable' ) !== false )
    $contentInfoArray['persistent_variable'] = $tpl->variable( 'persistent_variable' );

$Result['content_info'] = $contentInfoArray;

?>
