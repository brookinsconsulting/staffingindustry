<?php
/**
 * File containing the scenario view definition.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package extension
 */

$scenarioId = isset( $Params['ID'] ) ? $Params['ID'] : null;

if ( !$scenarioId )
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );

$scenario = ezpMultivariateTestScenario::fetch( $scenarioId );

$tpl = eZTemplate::factory();

$tpl->setVariable( 'scenario', $scenario );

$Result = array();
$Result['content'] = $tpl->fetch( 'design:multivariatetest/view_scenario.tpl' );
$Result['path'] = array( array( 'url' => false,
                                'text' => ezpI18n::tr( 'extension/ezodoscope', 'Details: %name', null, array( '%name' => $scenario->attribute( 'name' ) ) ) ) );
