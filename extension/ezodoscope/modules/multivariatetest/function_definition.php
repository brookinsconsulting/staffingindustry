<?php
/**
 * File containing the fetch functions definition.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package extension
 */

$FunctionList = array();

$FunctionList['scenario'] = array( 'name' => 'scenario',
                                   'operation_types' => array( 'read' ),
                                   'call_method' => array( 'include_file' => 'extension/ezodoscope/modules/multivariatetest/functioncollection.php',
                                                           'class' => 'ezpMultivariateTestFunctionCollection',
                                                           'method' => 'fetchScenario' ),
                                   'parameter_type' => 'standard',
                                   'parameters' => array( array( 'name' => 'scenario_id',
                                                                 'type' => 'integer',
                                                                 'required' => true ) ) );

$FunctionList['scenario_list'] = array( 'name' => 'scenario_list',
                                        'operation_types' => array( 'read' ),
                                        'call_method' => array( 'include_file' => 'extension/ezodoscope/modules/multivariatetest/functioncollection.php',
                                                                'class' => 'ezpMultivariateTestFunctionCollection',
                                                                'method' => 'fetchScenarioListByNodeID' ),
                                        'parameter_type' => 'standard',
                                        'parameters' => array( array( 'name' => 'node_id',
                                                                      'type' => 'integer',
                                                                      'required' => true ) ) );
