<?php
/**
 * File containing the action view definition.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package extension
 */

$module = $Params['Module'];

if ( $module->isCurrentAction( 'CreateTestScenario' ) )
{
    $nodeID = $module->hasActionParameter( 'NodeID' ) ? $module->actionParameter( 'NodeID' ) : 0;

    $testScenario = new ezpMultivariateTestScenario();
    $testScenario->setAttribute( 'node_id', $nodeID );
    $testScenario->setAttribute( 'created', time() );
    $testScenario->store();

    return $module->redirectToView( 'edit', array( $testScenario->attribute( 'id' ) ) );
}

if ( $module->isCurrentAction( 'RemoveTestScenario' ) )
{
    $nodeID = $module->hasActionParameter( 'NodeID' ) ? $module->actionParameter( 'NodeID' ) : 0;
    $testScenarioIDArray = $module->hasActionParameter( 'TestScenarioIDArray' ) ? $module->actionParameter( 'TestScenarioIDArray' ) : array();

    $db = eZDB::instance();
    $db->begin();

    foreach ( $testScenarioIDArray as $testScenarioID )
    {
        $testScenario = ezpMultivariateTestScenario::fetch( $testScenarioID );

        foreach( $testScenario->attribute( 'test_item_list' ) as $testItem )
            $testItem->remove();

        $testScenario->remove();
    }

    $db->commit();

    $node = eZContentObjectTreeNode::fetch( $nodeID );
    $object = $node->object();

    eZContentCacheManager::clearContentCacheIfNeeded( $object->attribute( 'id' ) );

    return $module->redirectTo( $node->attribute( 'url_alias' ) );
}

if ( $module->isCurrentAction( 'EnableTestScenario' ) )
{
    $nodeID = $module->hasActionParameter( 'NodeID' ) ? $module->actionParameter( 'NodeID' ) : 0;
    $testScenarioID = $module->hasActionParameter( 'TestScenarioStatusCheck' ) ? $module->actionParameter( 'TestScenarioStatusCheck' ) : 0;

    $db = eZDB::instance();
    $db->begin();

    $currentTestScenario = ezpMultivariateTestScenario::fetchEnabledByNodeID( $nodeID );
    if ( $currentTestScenario instanceof ezpMultivariateTestScenario )
    {
        $currentTestScenario->setAttribute( 'is_enabled', 0 );
        $currentTestScenario->store();
    }

    $testScenario = ezpMultivariateTestScenario::fetch( $testScenarioID );
    if ( $testScenario instanceof ezpMultivariateTestScenario )
    {
        $testScenario->setAttribute( 'is_enabled', 1 );
        $testScenario->store();
    }

    $db->commit();

    $node = eZContentObjectTreeNode::fetch( $nodeID );
    $object = $node->object();

    eZContentCacheManager::clearContentCacheIfNeeded( $object->attribute( 'id' ) );

    return $module->redirectTo( $node->attribute( 'url_alias' ) );
}
