<?php
/**
 * File containing the edit view definition.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package extension
 */

$module = $Params['Module'];
$testScenarioID = isset( $Params['ID'] ) ? $Params['ID'] : null;

if ( !$testScenarioID )
    return $Module->handleError( eZError::KERNEL_NOT_AVAILABLE, 'kernel' );

$testScenario = ezpMultivariateTestScenario::fetch( $testScenarioID );
$node = eZContentObjectTreeNode::fetch( $testScenario->attribute( 'node_id' ) );
$object = $node->object();

$http = eZHTTPTool::instance();

if ( $module->isCurrentAction( 'CancelTestScenario' ) )
{
    return $module->redirectTo( $node->attribute( 'url_alias' ) );
}

if ( $module->isCurrentAction( 'StoreTestScenario' ) )
{
    $name = $http->hasPostVariable( 'TestScenarioName' ) ? $http->postVariable( 'TestScenarioName' ) : '';
    $isEnabled = $http->hasPostVariable( 'EnableTestScenario' ) ? (int)$http->postVariable( 'EnableTestScenario' ) : 0;
    $testItemProbabilityArray = $http->hasPostVariable( 'TestItemProbabilityArray' ) ? $http->postVariable( 'TestItemProbabilityArray' ) : array();

    $testScenario->setAttribute( 'name', $name );
    $testScenario->setAttribute( 'is_enabled', $isEnabled );
    $testScenario->store();

    foreach( $testItemProbabilityArray as $testItemID => $testItemProbability )
    {
        $testItem = ezpMultivariateTestItem::fetch( $testItemID );
        $testItem->setAttribute( 'probability', abs( $testItemProbability ) );
        $testItem->store();
    }

    eZContentCacheManager::clearContentCacheIfNeeded( $object->attribute( 'id' ) );

    return $module->redirectTo( $node->attribute( 'url_alias' ) );
}

if ( $module->isCurrentAction( 'RemoveTestItem' ) )
{
    $testItemIDArray = $module->hasActionParameter( 'TestItemIDArray' ) ? $module->actionParameter( 'TestItemIDArray' ) : array();

    foreach( $testItemIDArray as $testItemID )
    {
        $testItem = ezpMultivariateTestItem::fetch( $testItemID );
        $testItem->remove();
    }
}

if ( $module->isCurrentAction( 'AddTestItem' ) )
{
    if ( $module->hasActionParameter( 'SelectedObjectIDArray' ) )
    {
        $objectIDArray = $module->actionParameter( 'SelectedObjectIDArray' );

        foreach ( $objectIDArray as $objectID )
        {
            $testItem = new ezpMultivariateTestItem();
            $testItem->setAttribute( 'object_id', $objectID );
            $testItem->setAttribute( 'probability', 0 );
            $testItem->setAttribute( 'scenario_id', $testScenarioID );
            $testItem->store();
        }
    }
}

if ( $module->isCurrentAction( 'AddTestItemBrowse' ) )
{
    // Save current state before switching into the browse mode
    $name = $http->hasPostVariable( 'TestScenarioName' ) ? $http->postVariable( 'TestScenarioName' ) : '';
    $isEnabled = $http->hasPostVariable( 'EnableTestScenario' ) ? (int)$http->postVariable( 'EnableTestScenario' ) : 0;

    $testScenario->setAttribute( 'name', $name );
    $testScenario->setAttribute( 'is_enabled', $isEnabled );
    $testScenario->store();

    return eZContentBrowse::browse( array( 'class_array' => null,
                                           'action_name' => 'AddTestItem',
                                           'browse_custom_action' => array( 'name' => 'AddTestItemButton',
                                                                            'value' => '' ),
                                           'from_page' => '/multivariatetest/edit/' . $testScenarioID,
                                           'cancel_page' => '/multivariatetest/edit/' . $testScenarioID,
                                           'persistent_data' => array( 'HasObjectInput' => 0 ) ), $module );
}

$tpl = eZTemplate::factory();

$tpl->setVariable( 'scenario', $testScenario );

$Result = array();
$Result['content'] = $tpl->fetch( 'design:multivariatetest/edit_scenario.tpl' );
$Result['path'] = array( array( 'url' => false,
                                'text' => ezpI18n::tr( 'extension/ezodoscope', 'Edit Test Scenario' ) ) );

