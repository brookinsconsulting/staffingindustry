<?php
/**
 * File containing the multivariatetest module definition.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package extension
 */

$Module = array( 'name' => 'Multivariatetest', "variable_params" => true );

$ViewList = array();
$ViewList['action'] = array( 'script' => 'action.php',
                             'single_post_actions' => array( 'CreateTestScenarioButton' => 'CreateTestScenario',
                                                             'EnableTestScenarioButton' => 'EnableTestScenario',
                                                             'RemoveTestScenarioButton' => 'RemoveTestScenario' ),
                             'post_action_parameters' => array( 'CreateTestScenario' => array( 'NodeID' => 'NodeID' ),
                                                                'EnableTestScenario' => array( 'NodeID' => 'NodeID',
                                                                                               'TestScenarioStatusCheck' => 'TestScenarioStatusCheck' ),
                                                                'RemoveTestScenario' => array( 'NodeID' => 'NodeID',
                                                                                               'TestScenarioIDArray' => 'TestScenarioIDArray' ) ) );
$ViewList['scenario'] = array( 'script' => 'scenario.php',
                               'params' => array( 'ID' ) );
$ViewList['edit'] = array( 'script' => 'edit.php',
                           'single_post_actions' => array( 'StoreTestScenarioButton' => 'StoreTestScenario',
                                                           'CancelTestScenarioButton' => 'CancelTestScenario',
                                                           'RemoveTestItemButton' => 'RemoveTestItem',
                                                           'AddTestItemButton' => 'AddTestItem',
                                                           'AddTestItemBrowseButton' => 'AddTestItemBrowse' ),
                           'post_action_parameters' => array( 'AddTestItem' => array( 'SelectedObjectIDArray' => 'SelectedObjectIDArray' ),
                                                              'RemoveTestItem' => array( 'TestItemIDArray' => 'TestItemIDArray' ) ),
                           'params' => array( 'ID' ) );
