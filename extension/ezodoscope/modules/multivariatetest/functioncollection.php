<?php
/**
 * File containing the ezpMultivariateTestFunctionCollection class.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package extension
 */

class ezpMultivariateTestFunctionCollection
{
    /**
     * Fetches scenario by ID
     *
     * @param integer $scenarioId
     * @return array
     */
    public function fetchScenario( $scenarioId )
    {
        $result = array( 'result' => ezpMultivariateTestScenario::fetch( $scenarioId ) );
        return $result;
    }

    /**
     * Fetches list of secnarios by node ID
     *
     * @param integer $nodeId
     * @return array
     */
    public function fetchScenarioListByNodeID( $nodeId )
    {
        $result = array( 'result' => ezpMultivariateTestScenario::fetchListByNodeID( $nodeId ) );
        return $result;
    }
}
