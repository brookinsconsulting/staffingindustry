<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>extension/ezodoscope</name>
    <message>
        <source>Multivariate test scenario: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content to test against</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Probability</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Node ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Variants associated with this test scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Toggle selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Object ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select test item for removal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove selected test items from the list above.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add new test item into this test scenario.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Validation warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please verify probability values for the test items. Total count can not exceed 100 limit for all selected test items.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Store</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Store this scenario.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit this scenario.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Test scenarios</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create a new test scenario.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select test scenario for removal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit &lt;%test_scenario&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove selected test scenarios from the list above.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change status of selected test scenario.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ezodoscope/admin/activate</name>
    <message>
        <source>odoscope succesfully activated.</source>
        <translation>odoscope succesfully activated.</translation>
    </message>
    <message>
        <source>Please fill in all fields.</source>
        <translation>Please fill in all fields.</translation>
    </message>
    <message>
        <source>Site name</source>
        <translation>Site name</translation>
    </message>
    <message>
        <source>eZ installation key</source>
        <translation>eZ installation key</translation>
    </message>
    <message>
        <source>Reactivate</source>
        <translation>Reactivate</translation>
    </message>
    <message>
        <source>Activate</source>
        <translation>Activate</translation>
    </message>
    <message>
        <source>To use odoscope, please activate it first.</source>
        <translation>To use odoscope, please activate it first.</translation>
    </message>
    <message>
        <source>odoscope is activated.</source>
        <translation>odoscope is activated.</translation>
    </message>
    <message>
        <source>If you want to reactivate odoscope, click on the button below.</source>
        <translation>If you want to reactivate odoscope, click on the button below.</translation>
    </message>
</context>
<context>
    <name>ezodoscope/admin/leftmenu</name>
    <message>
        <source>Activation</source>
        <translation>Activation</translation>
    </message>
    <message>
        <source>odoscope Web Viewer</source>
        <translation>odoscope Web Viewer</translation>
    </message>
</context>
<context>
    <name>ezodoscope/admin/view</name>
    <message>
        <source>odoscope</source>
        <translation>odoscope</translation>
    </message>
</context>
</TS>
