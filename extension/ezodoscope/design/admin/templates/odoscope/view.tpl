{set scope=global persistent_variable=hash('left_menu', false())}
<div class="odoscope">

{if $auth|eq(1)}
    {def $loginUrl = 'https://ez.odoscope.com/Account/eZLoggedIn'}

    <iframe style="border: none;" src="{concat($loginUrl, '/', $user_name, '/', $key)}"
            width="100%" height="700px" name="odoscope">
    </iframe>
{else}
    <div class="message-error">
        <h2><span class="time">[{currentdate()|l10n( shortdatetime )}]</span>{$auth}</h2>
    </div>
{/if}

</div>

