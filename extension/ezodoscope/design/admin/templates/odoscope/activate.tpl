{set scope=global persistent_variable=hash('left_menu', false())}
<div class="odoscope">

    <div class="context-block">

        <div class="box-header"><div class="box-tc"><div class="box-ml"><div class="box-mr"><div class="box-tl"><div class="box-tr">

            <h1 class="context-title">odoscope</h1>

        <div class="header-mainline"></div>

        </div></div></div></div></div></div>

        <div class="box-bc"><div class="box-ml"><div class="box-mr"><div class="box-bl"><div class="box-br"><div class="box-content">
			
			
			{if $insertSuccess|eq(1)}
				<div class="message-feedback">
					<h2><span class="time">[{currentdate()|l10n( shortdatetime )}]</span>{"odoscope succesfully activated."|i18n('ezodoscope/admin/activate')}</h2>
				</div>
		
			{elseif or($reactivate|eq(1), $reactivate_emptyField|eq(1), $error_reactivate|eq(1))}
			
				{if $reactivate_emptyField|eq(1)}
					<div class="message-error">
						<h2><span class="time">[{currentdate()|l10n( shortdatetime )}]</span>{"Please fill in all fields."|i18n('ezodoscope/admin/activate')}</h2>
					</div>
				{/if}
				{if $error_reactivate|eq(1)}
					<div class="message-error">
						<h2><span class="time">[{currentdate()|l10n( shortdatetime )}]</span>{$errorText}</h2>
					</div>
				{/if}
			
				<form method="post">
						<div class="info">
							Now you can reactivate odoscope.
						</div>
						<table class="list" cellspacing="0">
							<tr class="bgdark">
								<td>{"Site name"|i18n('ezodoscope/admin/activate')}:</td>
								<td class="odoscope-text-field"><input name="sitename" class="odoscope-text-field" type="text" {if $sitename|ne('')}value="{$sitename}"{/if} /></td>
							</tr>
							<tr class="bglight">
								<td>{"eZ installation key"|i18n('ezodoscope/admin/activate')}:</td>
								<td class="odoscope-text-field"><input name="ezsitekey" class="odoscope-text-field" type="text" {if $ezsitekey|ne('')}value="{$ezsitekey}"{/if} /></td>
							</tr>
						</table>
						<input type="submit" title="" value="{"Reactivate"|i18n('ezodoscope/admin/activate')}" name="doreactivate" class="button">
					</form>
			
			
			{else}
				{if $emptyField|eq(1)}
					<div class="message-error">
						<h2><span class="time">[{currentdate()|l10n( shortdatetime )}]</span>{"Please fill in all fields."|i18n('ezodoscope/admin/activate')}</h2>
					</div>
				{elseif $error|eq(1)}
					<div class="message-error">
						<h2><span class="time">[{currentdate()|l10n( shortdatetime )}]</span>{$errorText}</h2>
					</div>
				{/if}
				
				
				{if $activated|eq(0)}

					<form method="post">
						<div class="info">
						{"To use odoscope, please activate it first."|i18n('ezodoscope/admin/activate')}							
						</div>
						<table class="list" cellspacing="0">
							<tr class="bgdark">
								<td>{"Site name"|i18n('ezodoscope/admin/activate')}:</td>
								<td class="odoscope-text-field"><input name="sitename" class="odoscope-text-field" type="text" {if $sitename|ne('')}value="{$sitename}"{/if} /></td>
							</tr>
							<tr class="bglight">
								<td>{"eZ installation key"|i18n('ezodoscope/admin/activate')}:</td>
								<td class="odoscope-text-field"><input name="ezsitekey" class="odoscope-text-field" type="text" {if $ezsitekey|ne('')}value="{$ezsitekey}"{/if} /></td>
							</tr>
						</table>
						<input type="submit" title="" value="{"Activate"|i18n('ezodoscope/admin/activate')}" name="send" class="button">
					</form>
				{else}
					<div class="message-feedback">
						<h2><span class="time">[{currentdate()|l10n( shortdatetime )}]</span>{"odoscope is activated."|i18n('ezodoscope/admin/activate')}</h2>
					</div>
					<div>{"If you want to reactivate odoscope, click on the button below."|i18n('ezodoscope/admin/activate')}</div>
					<form method="post">
						<input type="submit" title="" value="{"Reactivate"|i18n('ezodoscope/admin/activate')}" name="reactivate" class="button">
					</form>
				{/if}
			{/if}
        </div></div></div></div></div></div>

    </div>

</div>

