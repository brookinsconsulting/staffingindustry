{ezscript_require( array( 'ezjsc::yui3', 'ezmultivariateteaserhandler.js' ) )}

{def $test_items = fetch( 'content', 'list', hash( 'parent_node_id', $node.node_id ) )}

<div class="content-view-line">
    <div class="class-multivarite-test-container float-break">
    {foreach $test_items as $test_item}
        <div id="teaser-object-{$test_item.contentobject_id}" style="display: none">
        {node_view_gui view='block_item' content_node=$test_item}
        </div>
    {/foreach}
    </div>

    <script>
    var test_items = [
    {foreach $test_items as $test_item}
        {ldelim} probability: Math.abs( parseInt( {$test_item.priority} ) ), name: '{$test_item.name|wash()}', object_id: {$test_item.contentobject_id} {rdelim} {delimiter},{/delimiter}
    {/foreach}
    ];
    var nodeID = {$node.node_id};
    {literal}
    YUI().use('node', 'event', function( Y ) {
        var test = new eZMultivariateTeaserHandler( { test_items: test_items,
                                                      test_item_container: '#teaser-object-',
                                                      test_scenario: 'MultivariateTestScenario#' + nodeID
                                                      }, Y );
        test.run();
    } );
    {/literal}
    </script>
</div>

{undef $test_items}
