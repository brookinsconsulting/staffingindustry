<form name="multivariatetests" action="{concat( '/multivariatetest/edit/', $scenario.id )|ezurl( 'no' )}" method="post">
<div class="context-block">

{* DESIGN: Header START *}<div class="box-header"><div class="box-ml">

<h1 class="context-title">{'Multivariate test scenario: '|i18n( 'extension/ezodoscope' )}{$scenario.name|wash}</h1>

{* DESIGN: Mainline *}<div class="header-mainline"></div>

{* DESIGN: Header END *}</div></div>

{* DESIGN: Content START *}<div class="box-ml"><div class="box-mr"><div class="box-content">

<div class="context-attributes">

<div class="block">
<label>{'Name'|i18n( 'extension/ezodoscope' )}:</label>
<input type="text" name="TestScenarioName" value="{$scenario.name|wash()}" class="halfbox" />
</div>

<div class="block">
<label>{'Created'|i18n( 'extension/ezodoscope' )}:</label>
{$scenario.created|l10n( 'shortdatetime' )}
</div>

<div class="block">
<label>{'ID'|i18n( 'extension/ezodoscope' )}:</label>
{$scenario.id}
</div>

<div class="block">
<label>{'Status'|i18n( 'extension/ezodoscope' )}:</label>
<input type="checkbox" name="EnableTestScenario" value="1"{if $scenario.is_enabled} checked="checked"{/if} />{$scenario.is_enabled|choose( 'Disabled'|i18n( 'extension/ezodoscope' ), 'Enabled'|i18n( 'extension/ezodoscope' ) )}
</div>

<div class="block">
<label>{'Content to test against'|i18n( 'extension/ezodoscope' )}: {$scenario.node.name|wash()}</label>
{'Probability'|i18n( 'extension/ezodoscope' )}: <span class="source-probability">{$scenario.source_probability}</span>%<br />
{'Location'|i18n( 'extension/ezodoscope' )}: {$scenario.node.parent.name|wash()} <br />
{'Node ID'|i18n( 'extension/ezodoscope' )}: {$scenario.node.node_id}
</div>

<div class="block">
    <h3>{'Variants associated with this test scenario'|i18n( 'extension/ezodoscope' )}</h3>
    <table class="list" cellspacing="0">
    <tr>
        <th class="tight"><img src={'toggle-button-16x16.gif'|ezimage} width="16" height="16" alt="{'Toggle selection'|i18n( 'extension/ezodoscope' )}" onclick="ezjs_toggleCheckboxes( document.multivariatetests, 'TestItemIDArray[]' ); return false;"/></th>
        <th>{'Name'|i18n( 'extension/ezodoscope' )}</th>
        <th>{'Probability'|i18n( 'extension/ezodoscope' )} (%)</th>
        <th>{'Object ID'|i18n( 'extension/ezodoscope' )}</th>
        <th>{'Node ID'|i18n( 'extension/ezodoscope' )}</th>
    </tr>
    <tr>
        <td><input type="checkbox" value="" name="TestItemIDArray[]" title="{'Select test item for removal.'|i18n( 'extension/ezodoscope' )}" disabled="disabled" /></td>
        <td><a href="{$scenario.node.url_alias|ezurl( 'no' )}" title="{$scenario.node.name|wash()}">{$scenario.node.name|wash()}</a> <strong>({'Source page'|i18n( 'extension/ezodoscope' )})</strong></td>
        <td><span class="source-probability">{$scenario.source_probability}</span></td>
        <td>{$scenario.node.object.id}</td>
        <td>{$scenario.node.node_id}</td>
    </tr>
    {foreach $scenario.test_item_list as $test_item}
    <tr>
        <td><input type="checkbox" value="{$test_item.id}" name="TestItemIDArray[]" title="{'Select test item for removal.'|i18n( 'extension/ezodoscope' )}" /></td>
        <td><a href="{$test_item.content_object.main_node.url_alias|ezurl( 'no' )}" title="{$test_item.content_object.name|wash()}">{$test_item.content_object.name|wash()}</a></td>
        <td><input type="text" name="TestItemProbabilityArray[{$test_item.id}]" value="{$test_item.probability}" /></td>
        <td>{$test_item.content_object.id}</td>
        <td>{$test_item.content_object.main_node.node_id}</td>
    </tr>
    {/foreach}
    </table>
    <div class="block">
        <div class="button-left">
            <input class="button" type="submit" name="RemoveTestItemButton" value="{'Remove selected'|i18n( 'extension/ezodoscope' )}" title="{'Remove selected test items from the list above.'|i18n( 'extension/ezodoscope' )}" />
        </div>
        <div class="button-right">
            <input class="button" type="submit" name="AddTestItemBrowseButton" value="{'Add'|i18n( 'extension/ezodoscope' )}" title="{'Add new test item into this test scenario.'|i18n( 'extension/ezodoscope' )}" />
        </div>
        <div class="break"></div>
    </div>
    {ezscript_require( array( 'ezjsc::yui3', 'ezmodalwindow.js' ) )}
    <div id="testing-modal-window" class="modal-window" style="display:none;">
        <h2><a href="#" class="window-close">{'Close'|i18n( 'extension/ezodoscope' )}</a><span></span></h2>
        <div class="window-content">Test Content</div>
    </div>
    <script>
        var strings = {ldelim} title: '{'Validation warning'|i18n( 'extension/ezodoscope' )}',
                               message: '{'Please verify probability values for the test items. Total count can not exceed 100 limit for all selected test items.'|i18n( 'extension/ezodoscope' )}' {rdelim};
        {literal}
        YUI(YUI3_config).use( 'event', 'ezmodalwindow', function( Y ) {
            var modal = new Y.eZ.ModalWindow( { window: '#testing-modal-window',
                                                centered: true,
                                                width: 400 }, Y );
            var testItemInputNodes = Y.all( 'input[name^=TestItemProbabilityArray]' );

            Y.one( 'input[name=StoreTestScenarioButton]' ).on( 'click', function( e ) {
                var probability = 0;
                testItemInputNodes.each( function( node ) {
                    probability += parseInt( node.get( 'value' ) );
                } );
                if ( probability > 100 ) {
                    modal.setTitle( strings.title );
                    modal.setContent( strings.message );
                    modal.open();
                    e.preventDefault();
                }
            });
            testItemInputNodes.on( 'change', function( e ) {
                var probability = 100;
                this.each( function( node ) {
                    probability -= parseInt( node.get( 'value' ) );
                });
                var sourceProbNode = Y.all( '.source-probability' );
                if ( probability <= 0 ) {
                    sourceProbNode.setContent( 0 );
                } else {
                    sourceProbNode.setContent( probability );
                }
            })
        })
        {/literal}
    </script>
</div>

</div>

{* DESIGN: Content END *}</div></div></div>

<div class="controlbar">
{* DESIGN: Control bar START *}<div class="box-bc"><div class="box-ml">
<div class="block">
<input class="button" type="submit" name="StoreTestScenarioButton" value="{'Store'|i18n( 'extension/ezodoscope' )}" title="{'Store this scenario.'|i18n( 'extension/ezodoscope' )}" />
<input class="button" type="submit" name="CancelTestScenarioButton" value="{'Cancel'|i18n( 'extension/ezodoscope' )}" title="{'Cancel'|i18n( 'extension/ezodoscope' )}" />
</div>
{* DESIGN: Control bar END *}</div></div>
</div>

</div>
</form>
