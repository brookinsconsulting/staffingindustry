<form action="{concat( '/multivariatetest/edit/', $scenario.id )|ezurl( 'no' )}" method="post">
<div class="context-block">

{* DESIGN: Header START *}<div class="box-header"><div class="box-ml">

<h1 class="context-title">{'Multivariate test scenario: '|i18n( 'extension/ezodoscope' )}{$scenario.name|wash}</h1>

{* DESIGN: Mainline *}<div class="header-mainline"></div>

{* DESIGN: Header END *}</div></div>

{* DESIGN: Content START *}<div class="box-ml"><div class="box-mr"><div class="box-content">

<div class="context-attributes">

<div class="block">
<label>{'Name'|i18n( 'extension/ezodoscope' )}:</label>
{$scenario.name|wash}
</div>

<div class="block">
<label>{'Created'|i18n( 'extension/ezodoscope' )}:</label>
{$scenario.created|l10n( 'shortdatetime' )}
</div>

<div class="block">
<label>{'ID'|i18n( 'extension/ezodoscope' )}:</label>
{$scenario.id}
</div>

<div class="block">
<label>{'Status'|i18n( 'extension/ezodoscope' )}:</label>
{$scenario.is_enabled|choose( 'Disabled'|i18n( 'extension/ezodoscope' ), 'Enabled'|i18n( 'extension/ezodoscope' ) )}
</div>

<div class="block">
<label>{'Content to test against'|i18n( 'extension/ezodoscope' )}: {$scenario.node.name|wash()}</label>
{'Probability'|i18n( 'extension/ezodoscope' )}: {$scenario.source_probability}%<br />
{'Location'|i18n( 'extension/ezodoscope' )}: {$scenario.node.parent.name|wash()} <br />
{'Node ID'|i18n( 'extension/ezodoscope' )}: {$scenario.node.node_id}
</div>

<div class="block">
    <h3>{'Variants associated with this test scenario'|i18n( 'extension/ezodoscope' )}</h3>
    <table class="list" cellspacing="0">
    <tr>
        <th>{'Name'|i18n( 'extension/ezodoscope' )}</th>
        <th>{'Probability'|i18n( 'extension/ezodoscope' )} (%)</th>
        <th>{'Object ID'|i18n( 'extension/ezodoscope' )}</th>
        <th>{'Node ID'|i18n( 'extension/ezodoscope' )}</th>
    </tr>
    <tr class="bglight">
        <td><a href="{$scenario.node.url_alias|ezurl( 'no' )}" title="{$scenario.node.name|wash()}">{$scenario.node.name|wash()}</a> <strong>({'Source page'|i18n( 'extension/ezodoscope' )})</strong></td>
        <td>{$scenario.source_probability}</td>
        <td>{$scenario.node.object.id}</td>
        <td>{$scenario.node.node_id}</td>
    </tr>
    {foreach $scenario.test_item_list as $test_item sequence array( 'bgdark', 'bglight' ) as $style}
    <tr class="{$style}">
        <td><a href="{$test_item.content_object.main_node.url_alias|ezurl( 'no' )}" title="{$test_item.content_object.name|wash()}">{$test_item.content_object.name|wash()}</a></td>
        <td>{$test_item.probability}</td>
        <td>{$test_item.content_object.id}</td>
        <td>{$test_item.content_object.main_node.node_id}</td>
    </tr>
    {/foreach}
    </table>
</div>

</div>

{* DESIGN: Content END *}</div></div></div>

<div class="controlbar">
{* DESIGN: Control bar START *}<div class="box-bc"><div class="box-ml">
<div class="block">
<input class="button" type="submit" name="EditScenarioButton" value="{'Edit'|i18n( 'extension/ezodoscope' )}" title="{'Edit this scenario.'|i18n( 'extension/ezodoscope' )}" />
</div>
{* DESIGN: Control bar END *}</div></div>
</div>

</div>
</form>
