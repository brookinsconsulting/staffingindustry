<form name="multivariatetests" method="post" action={'multivariatetest/action'|ezurl}>
<input type="hidden" name="NodeID" value="{$node.node_id}" />
<div class="block">    
    <fieldset>
        <legend>{'Test scenarios'|i18n( 'extension/ezodoscope' )}</legend>
        
        <p>{'Create a new test scenario.'|i18n( 'extension/ezodoscope' )}</p>

        <input  id="ezasi-create-test-scenario" class="button" type="submit" name="CreateTestScenarioButton" value="{'Create'|i18n( 'extension/ezodoscope' )}" title="{'Create a new test scenario.'|i18n( 'extension/ezodoscope' )}" />

        {def $test_scenarios = fetch( 'multivariatetest', 'scenario_list', hash( 'node_id', $node.node_id ) )}

        {if $test_scenarios}
        <div class="block">
            <table class="list" cellspacing="0">
            <tr>
                <th class="tight"><img src={'toggle-button-16x16.gif'|ezimage} width="16" height="16" alt="{'Toggle selection'|i18n( 'extension/ezodoscope' )}" onclick="ezjs_toggleCheckboxes( document.multivariatetests, 'TestScenarioIDArray[]' ); return false;"/></th>
                <th>{'Name'|i18n( 'extension/ezodoscope' )}</th>
                <th>{'Pages'|i18n( 'extension/ezodoscope' )} ({'Probability'|i18n( 'extension/ezodoscope' )} %)</th>
                <th>{'Created'|i18n( 'extension/ezodoscope' )}</th>
                <th>{'Status'|i18n( 'extension/ezodoscope' )}</th>
                <th class="tight"></th>
            </tr>
            {foreach $test_scenarios as $test_scenario sequence array( 'bglight', 'bgdark' ) as $style}
            <tr class="{$style}">
                <td><input type="checkbox" value="{$test_scenario.id}" name="TestScenarioIDArray[]" title="{'Select test scenario for removal.'|i18n( 'extension/ezodoscope' )}" /></td>
                <td><a href="{concat( '/multivariatetest/scenario/', $test_scenario.id )|ezurl( 'no' )}" title="{$test_scenario.name|wash()}">{$test_scenario.name|wash()}</a></td>
                <td>
                    {$test_scenario.node.name|wash()} ({$test_scenario.source_probability}) ,
                    {foreach $test_scenario.test_item_list as $test_item}
                    <a href="{$test_item.content_object.main_node.url_alias|ezurl('no')}" title="{$test_item.content_object.name|wash()}">{$test_item.content_object.name|wash()} ({$test_item.probability})</a>
                    {delimiter},{/delimiter}
                    {/foreach}
                </td>
                <td>{$test_scenario.created|l10n( 'shortdatetime' )}</td>
                <td>{$test_scenario.is_enabled|choose( 'Disabled'|i18n( 'extension/ezodoscope' ), 'Enabled'|i18n( 'extension/ezodoscope' ) )}<input type="radio" name="TestScenarioStatusCheck" value="{$test_scenario.id}"{if $test_scenario.is_enabled}checked="checked"{/if} /></td>
                <td><a href="{concat( '/multivariatetest/edit/', $test_scenario.id )|ezurl( 'no' )}" title="{'Edit <%test_scenario>.'|i18n( 'extension/ezodoscope',, hash( '%test_scenario', $test_scenario.name ) )|wash}"><img src={'edit.gif'|ezimage} width="16" height="16" alt="{'Edit'|i18n( 'extension/ezodoscope' )}" /></a></td>
            </tr>
            {/foreach}
            </table>
            <div class="block">
                <div class="button-left">
                    <input class="button" type="submit" name="RemoveTestScenarioButton" value="{'Remove selected'|i18n( 'extension/ezodoscope' )}" title="{'Remove selected test scenarios from the list above.'|i18n( 'extension/ezodoscope' )}" />
                </div>
                <div class="button-right">
                    <input class="button" type="submit" name="EnableTestScenarioButton" value="{'Set'|i18n( 'extension/ezodoscope' )}" title="{'Change status of selected test scenario.'|i18n( 'extension/ezodoscope' )}" />
                </div>
                <div class="break"></div>
            </div>
        </div>
        {/if}

        {undef $test_scenarios}
    </fieldset>
</div>
</form>
