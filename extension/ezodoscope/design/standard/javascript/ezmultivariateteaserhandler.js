/**
 * Constructor of eZMultivariateTeaserHandler class
 *
 * @param config Configuration object containing the following keys
 *               - 'test_items' [required] Array with test item objects { probability: 50, object_id: 14 }
 *               - 'test_item_container' [required] HTML id attribute prefix e.g #teaser-object- applied to teaser
 *                                                  container.
 * @param Y YUI3 object instance
 */
function eZMultivariateTeaserHandler( config, Y ) {
    this.Y = Y;
    this.config = config;
}

/**
 * Based on the test items probability calcuation display a teaser
 *
 */
eZMultivariateTeaserHandler.prototype.run  = function() {
    var probability = 0, Y = this.Y, test_items = this.config.test_items;

    for ( var i = 0; i < test_items.length; i++  ) {
        probability += test_items[i].probability;
    }

    var random = Math.floor( Math.random() * ( probability + 1 ) );
    var max = 0, i = 0;

    do
        max += test_items[i++].probability;
    while( random > max );

    var testItem = test_items[i-1];
    Y.one( this.config.test_item_container + testItem.object_id ).show();

    if ( window.osc )
        osc.evt( 'MultivariateTesting', 'Teaser_' + this.config.test_scenario + '_' + testItem.name  );
}
