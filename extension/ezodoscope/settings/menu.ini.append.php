<?php /* #?ini charset="utf8"?

[NavigationPart]
Part[odoscopenavigationpart]=odoscope

[TopAdminMenu]
Tabs[]=odoscope

[Topmenu_odoscope]
NavigationPartIdentifier=odoscopenavigationpart
Name=odoscope
Tooltip=odoscope tool

URL[]
URL[default]=odoscope/view

Enabled[]
Enabled[default]=true
Enabled[browse]=false
Enabled[edit]=false

Shown[]
Shown[default]=true
Shown[edit]=true
Shown[navigation]=true
Shown[browse]=true

*/ ?>