CREATE TABLE IF NOT EXISTS `ezmultivariate_test_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `probability` int(11) NOT NULL,
  `scenario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ezmultivariate_test_item_object_id` (`object_id`),
  KEY `ezmultivariate_test_item_node_id` (`scenario_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ezmultivariate_test_scenario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_enabled` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ezmultivariate_test_scenario_node_id` (`node_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
