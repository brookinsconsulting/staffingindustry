CREATE SEQUENCE ezmultivariate_test_item_s
    START 1
    INCREMENT 1
    MAXVALUE 9223372036854775807
    MINVALUE 1
    CACHE 1;

CREATE TABLE ezmultivariate_test_item (
  id integer DEFAULT nextval('ezmultivariate_test_item_s'::text) NOT NULL,
  object_id integer DEFAULT 0 NOT NULL,
  probability integer DEFAULT 0 NOT NULL,
  scenario_id integer DEFAULT 0 NOT NULL
);

ALTER TABLE ONLY ezmultivariate_test_item
    ADD CONSTRAINT ezmultivariate_test_item_pkey PRIMARY KEY (id);

CREATE INDEX ezmultivariate_test_item_object_id ON ezmultivariate_test_item USING btree (object_id);
CREATE INDEX ezmultivariate_test_item_scenario_id ON ezmultivariate_test_item USING btree (scenario_id);

CREATE SEQUENCE ezmultivariate_test_scenario_s
    START 1
    INCREMENT 1
    MAXVALUE 9223372036854775807
    MINVALUE 1
    CACHE 1;

CREATE TABLE ezmultivariate_test_scenario (
  id integer DEFAULT nextval('ezmultivariate_test_scenario_s'::text) NOT NULL,
  node_id integer DEFAULT 0 NOT NULL,
  name VARCHAR(255) NULL,
  is_enabled integer DEFAULT 0 NOT NULL,
  created integer DEFAULT 0 NOT NULL
);

ALTER TABLE ONLY ezmultivariate_test_scenario
    ADD CONSTRAINT ezmultivariate_test_scenario_pkey PRIMARY KEY (id);

CREATE INDEX ezmultivariate_test_scenario_node_id ON ezmultivariate_test_scenario USING btree (node_id);
