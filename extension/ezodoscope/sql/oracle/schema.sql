CREATE SEQUENCE s_multivariate_test_item;

CREATE TABLE ezmultivariate_test_item (
  id integer NOT NULL,
  object_id integer DEFAULT 0 NOT NULL,
  probability integer DEFAULT 0 NOT NULL,
  scenario_id integer DEFAULT 0 NOT NULL,
  PRIMARY KEY (id)
);

CREATE INDEX ezmv_test_item_object_id ON ezmultivariate_test_item (object_id);
CREATE INDEX ezmv_test_item_scenario_id ON ezmultivariate_test_item (scenario_id);

CREATE SEQUENCE s_multivariate_test_scenario;

CREATE TABLE ezmultivariate_test_scenario (
  id integer NOT NULL,
  node_id integer DEFAULT 0 NOT NULL,
  name VARCHAR2(255),
  is_enabled integer DEFAULT 0 NOT NULL,
  created integer DEFAULT 0 NOT NULL,
  PRIMARY KEY (id)
);

CREATE INDEX ezmv_test_scenario_node_id ON ezmultivariate_test_scenario (node_id);

CREATE OR REPLACE TRIGGER ezmv_test_item_id_tr
BEFORE INSERT ON ezmultivariate_test_item FOR EACH ROW WHEN (new.id IS NULL)
BEGIN
  SELECT s_multivariate_test_item.nextval INTO :new.id FROM dual;
END;
/

CREATE OR REPLACE TRIGGER ezmv_test_scenario_id_tr
BEFORE INSERT ON ezmultivariate_test_scenario FOR EACH ROW WHEN (new.id IS NULL)
BEGIN
  SELECT s_multivariate_test_scenario.nextval INTO :new.id FROM dual;
END;
/
