<?php

/**
 * File containing eZTemplateOperatorArray
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package ezodoscope
 */


$eZTemplateOperatorArray = array();
$eZTemplateOperatorArray[] =
 array( 'script' => 'extension/ezodoscope/autoloads/generateoschtml.php',
        'class' => 'OdoscopeImgHTML',
        'operator_names' => array( 'oscgenerate', 'oscgeneratebymoduleresult', 'oscgeneratebynode' ) );
?>