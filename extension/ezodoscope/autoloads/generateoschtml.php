<?php

/**
 * File containing the OdoscopeImgHTML class for generating the odoscope tracking pixel
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package ezodoscope
 */

/**
 * OdoscopeImgHTML class definition
 */
class OdoscopeImgHTML
{
    /**
     * Construct
     */
    public function __construct()
    {
        $this->Operators = array(
            'oscgenerate',
            'oscgeneratebymoduleresult',
            'oscgeneratebynode'
        );
    }

    function &operatorList()
    {
        return $this->Operators;
    }

    function namedParameterPerOperator()
    {
        return true;
    }

    function namedParameterList()
    {
        return array(
            'oscgenerate' => array(
                'docid' => array(
                    'type' => 'string',
                    'required' => true,
                    'default' => ''
                ),
                'doclabel' => array(
                    'type' => 'string',
                    'required' => true,
                    'default' => ''
                ),
                'path' => array(
                    'type' => 'string',
                    'required' => true,
                    'default' => ''
                ),
                'array' => array(
                    'type' => 'array',
                    'required' => true,
                    'default' => ''
                )
            ),

            'oscgeneratebymoduleresult' => array(
                'modresult' => array(
                    'type' => 'object',
                    'required' => true,
                    'default' => ''
                ),
                'array' => array(
                    'type' => 'array',
                    'required' => true,
                    'default' => ''
                )
            ),

            'oscgeneratebynode' => array(
                'node' => array(
                    'type' => 'object',
                    'required' => true,
                    'default' => ''
                ),
                'array' => array(
                    'type' => 'array',
                    'required' => true,
                    'default' => ''
                )
            )
        );
    }

    function modify( &$tpl, &$operatorName, &$operatorParameters, &$rootNamespace,
                     &$currentNamespace, &$operatorValue, &$namedParameters )
    {
        switch ( $operatorName )
        {
            case 'oscgenerate':
                {
                    $operatorValue = $this->generate( $namedParameters['docid'],
                                                      $namedParameters['doclabel'],
                                                      $namedParameters['path'],
                                                      $namedParameters['array'] );
                }
                break;

            case 'oscgeneratebymoduleresult':
                {
                    $operatorValue = $this->generateByModuleResult( $namedParameters['modresult'],
                                                                    $namedParameters['array'] );

                }
                break;

            case 'oscgeneratebynode':
                {
                    $operatorValue = $this->generateByNode( $namedParameters['node'],
                                                            $namedParameters['array'] );

                }
                break;
        }
    }

    function ownEZDesign( $operatorValue )
    {
        $sys = eZSys::instance();
        $bases = eZTemplateDesignResource::allDesignBases();
        $triedFiles = array();
        $fileInfo = eZTemplateDesignResource::fileMatch( $bases, false, $operatorValue, $triedFiles );

        if ( !$fileInfo )
        {
            $siteDesign = eZTemplateDesignResource::designSetting( 'site' );
            $filePath = "design/$siteDesign/$operatorValue";
        }
        else
        {
            $filePath = $fileInfo['path'];
        }

        $operatorValue = $sys->wwwDir() . '/' . $filePath;
        $operatorValue = htmlspecialchars( $operatorValue );

        return $operatorValue;
    }

    function getHTML( $param )
    {
        if ( isset( $_SERVER['HTTP_DNT'] ) && $_SERVER['HTTP_DNT'] == 1 )
            return;

        if ( isset( $_COOKIE['osc_dnt'] ) && $_COOKIE['osc_dnt'] == 1 )
            return;

        $res = '<div id="odoscope" style="display: none">
                    <noscript>
                        <img src="' . $this->ownEZDesign( 'images/osc.gif' ) . '?' . $param . '" />
                    </noscript>
                    <script src="' . $this->ownEZDesign( 'javascript/odoscope.js' ) . '" type="text/javascript"></script>
                    <script type="text/javascript">
                        var currentUrl = "' . $this->ownEZDesign( 'images/osc.gif' ) . '";osc.img(\'' . $param . '\');
                        ' . $this->addUserDefinedEvents() . '
                    </script>
                </div>';

        return $res;
    }

    function getParamsString( $docid, $doclabel, $path, $array )
    {
        $oscINI = eZINI::instance( 'odoscope.ini' );
        $appendSA = false;
        if ( $oscINI->hasVariable( 'ParamSettings', 'appendSiteAccess' ) )
        {
            $appendSA = $oscINI->variable( 'ParamSettings', 'appendSiteAccess' ) == 1;
        }

        $appendLang = false;
        if ( $oscINI->hasVariable( 'ParamSettings', 'appendLanguage' ) )
        {
            $appendLang = $oscINI->variable( 'ParamSettings', 'appendLanguage' ) == 1;
        }

        $ini = eZINI::instance( 'site.ini' );
        $lang = $ini->variable( 'RegionalSettings', 'Locale' );
        $currentSiteAccess = eZSiteAccess::current();
        $siteAccessName = isset( $currentSiteAccess['name'] ) ? $currentSiteAccess['name'] : '';

        if ( $appendSA )
        {
            $docid .= '_' . $siteAccessName;
            if ( $path == "" )
            {
                $path = $siteAccessName;
            }
            else
            {
                $path = $siteAccessName . "/" . $path;
            }
        }
        if ( $appendLang )
        {
            $docid .= '_' . $lang;
            if ( $path == "" )
            {
                $path = $lang;
            }
            else
            {
                $path = $lang . "/" . $path;
            }
        }

        $param = 'docid=' . $docid . '&amp;doclabel=' . urlencode( $doclabel ) . '&amp;path=' . urlencode( $path );

        if ( $oscINI->variable( 'TrackingSettings', 'TrackSiteAccess' ) == 'enabled' )
            $param .= '&amp;sectionid=' . $siteAccessName;

        foreach ( $array as $key => $value )
        {
            $param .= '&amp;' . urlencode( $key ) . '=' . urlencode( $value );
        }

        return $param;
    }

    function getNodeParams( $node, &$doclabel, &$docid, &$path )
    {
        $docid = $node->attribute( 'node_id' );
        $pathArr = $node->attribute( 'path_array' );
        $path = "";
        if ( count( $pathArr ) == 2 )
        {
            $tmpnode = eZContentObjectTreeNode::fetch( $pathArr[1] );
            $doclabel .= str_replace( '#', '', $tmpnode->attribute( 'name' ) );
        }
        else
        {
            for ( $i = count( $pathArr ) - 1; $i > 1; $i-- )
            {
                $tmpnode = eZContentObjectTreeNode::fetch( $pathArr[$i] );
                if ( $i != count( $pathArr ) - 1 )
                {
                    $doclabel .= ' - ';
                    if ( $path == "" )
                    {
                        $path = str_replace( '#', '', $tmpnode->attribute( 'name' ) );
                    }
                    else
                    {
                        $path = str_replace( '#', '', $tmpnode->attribute( 'name' ) ) . "/" . $path;
                    }
                }
                $doclabel .= str_replace( '#', '', $tmpnode->attribute( 'name' ) );
            }
        }

    }

    function getParams( $modres, &$doclabel, &$docid, &$path )
    {

        $content_info = isset( $modres['content_info'] ) ? $modres['content_info'] : array();
        if ( isset( $content_info['persistent_variable']['doFolderRedirect'] )
                && $content_info['persistent_variable']['doFolderRedirect'] == 1 )
        {
            $docid = $content_info['persistent_variable']['first_folder_child']->attribute( 'node_id' );
        }
        else
        {
            if ( isset( $modres['osc_docid'] ) )
            {
                $docid = $modres['osc_docid'];
            }
            else
            {
                $docid = isset( $modres['node_id'] ) ? $modres['node_id'] : '';
                if ( $docid == '' )
                {
                    $path = $modres['uri'];
                    $docid = $modres['uri'];
                    $doclabel = $modres['uri'];
                }
            }
        }

        $node = eZContentObjectTreeNode::fetch( $docid );

        if ( isset( $content_info['viewmode'] ) && $content_info['viewmode'] == 'sitemap' )
        {
            $doclabel = 'Sitemap';
            $docid = 'sitemap';
        }
        else
        {
            if ( $node )
            {
                $path = '';
                $pathArr = $node->attribute( 'path_array' );

                if ( count( $pathArr ) == 2 )
                {
                    $tmpnode = eZContentObjectTreeNode::fetch( $pathArr[1] );
                    $doclabel .= str_replace( '#', '', $tmpnode->attribute( 'name' ) );
                }
                else
                {
                    $rootNodeDepth = 1;
                    $ini = eZINI::instance( 'site.ini' );
                    if ( $ini->hasVariable( 'SiteSettings', 'RootNodeDepth' ) )
                    {
                        $rootNodeDepth = $ini->variable( 'SiteSettings', 'RootNodeDepth' );
                        $ini = eZINI::instance( 'content.ini' );
                        $rootNodeId = $ini->variable( 'NodeSettings', 'RootNode' );
                        if ( $rootNodeId == $docid )
                        {
                            $doclabel = 'Home';
                        }
                    }

                    for ( $i = count( $pathArr ) - 1; $i > $rootNodeDepth; $i-- )
                    {
                        $tmpnode = eZContentObjectTreeNode::fetch( $pathArr[$i] );
                        if ( $i != count( $pathArr ) - 1 )
                        {
                            $doclabel .= ' - ';

                            if ( $path == "" )
                            {
                                $path = str_replace( '#', '', $tmpnode->attribute( 'name' ) );
                            }
                            else
                            {
                                $path = str_replace( '#', '', $tmpnode->attribute( 'name' ) ) . "/" . $path;
                            }
                        }
                        $doclabel .= str_replace( '#', '', $tmpnode->attribute( 'name' ) );
                    }
                }
            }
            else
            {
                if ( isset( $modres['path'][0]['url'] ) && $modres['path'][0]['url'] )
                {
                    $path = $modres['path'][0]['url'];
                    $path = trim( $path );
                    $pathArr = explode( '/', $path );
                    $newArr = Array();
                    foreach ( $pathArr as $s )
                    {
                        if ( trim( $s ) != '' )
                        {
                            $newArr[] = trim( $s );
                        }
                    }
                    $path = implode( '/', $newArr );
                }
                if ( isset( $modres['path'][0]['osctext'] ) )
                {
                    $doclabel = $modres['path'][0]['osctext'];
                }
                else
                {
                    if ( is_array( $modres['path'] ) )
                    {
                        $doclabel = '';
                        foreach ( $modres['path'] as $key => $value )
                        {
                            if ( $key != 0 )
                            {
                                $doclabel = $value['text'] . ' - ' . $doclabel;
                            }
                            else
                            {
                                $doclabel = $value['text'];
                            }
                        }
                    }
                }

                if ( strpos( $modres['uri'], '/content/search' ) !== false )
                {
                    $doclabel = 'Search';
                    $docid = 'search';
                    $path = '';
                }
                else
                {
                    if ( strpos( $modres['uri'], '/content/advancedsearch' ) !== false )
                    {
                        $doclabel = 'AdvSearch';
                        $docid = 'advsearch';
                        $path = '';
                    }
                }
            }
        }

        if ( $modres['ui_component'] == 'error' )
        {
            $docid .= '_error';
            $doclabel = $modres['uri'];
            $path = 'Error';
        }
    }

    function generate( $docid, $doclabel, $path, $array )
    {
        $param = $this->getParamsString( $docid, $doclabel, $path, $array );
        $res = $this->getHTML( $param );
        return $res;
    }

    public function addMultivariateTestingParams( $modres, &$array )
    {
        $ini = eZINI::instance( 'content.ini' );
        if ( $ini->variable( 'TestingSettings', 'MultivariateTesting' ) == 'disabled' )
            return;

        $node = eZContentObjectTreeNode::fetch( $modres['node_id'] );
        if ( !$node instanceof eZContentObjectTreeNode )
            return;

        $scenario = ezpMultivariateTestScenario::findScenarioByNode( $node );
        if ( !$scenario instanceof ezpMultivariateTestScenario )
            return;

        $array['MultivariateTesting'] = 'Layout_' . $scenario->attribute( 'name' ) . '_' . $node->attribute('name');
    }

    public function addUserDefinedEvents() {
        $code = '';

        $ini = eZINI::instance( 'odoscope.ini' );
        $events = $ini->variable( 'EventSettings', 'AvailableEvents' );
        $paramPrefix = $ini->variable( 'EventSettings', 'EventParamPrefix' );

        foreach( $events as $eventParam => $eventName )
        {
            $code .= 'if ( !osc.hasOwnProperty( \'' . $eventName . '\' ) ) {
                            osc.' . $eventName . ' = function( eventValue ) {
                                this.evt( \'' . $paramPrefix . $eventParam .  '\', eventValue );
                            };
                        };
                        ';
        }

        return $code;
    }

    function generateByModuleResult( $modres, $array )
    {
        $doclabel = null;
        $docid = null;
        $path = null;

        $this->getParams( $modres, $doclabel, $docid, $path );
        $this->addMultivariateTestingParams( $modres, $array );
        $param = $this->getParamsString( $docid, $doclabel, $path, $array );
        $res = $this->getHTML( $param );
        return $res;
    }

    function generateByNode( $node, $array )
    {
        $doclabel = null;
        $docid = null;
        $path = null;

        $this->getNodeParams( $node, $doclabel, $docid, $path );
        $param = $this->getParamsString( $docid, $doclabel, $path, $array );
        $res = $this->getHTML( $param );
        return $res;
    }

    /// privatesection
    var $Operators;
}

?>
