<?php
/**
 * File containing the ezodoscopeInfo class.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license eZ Business Extension License Agreement ("eZ BEL") Version 2.0
 * @version 2.0-0
 * @package ezodoscope
 */

class ezodoscopeInfo
{
    static function info()
    {
        return array( 'Name' => 'eZ odoscope',
                      'Version' => '2.0-0',
                      'Copyright' => 'Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.',
                      'License' => 'eZ Business Extension License Agreement ("eZ BEL") Version 2.0',
                    );
    }
}
?>
