#!/bin/bash -e

# run this script to make .htaccess SSL rewrites work on servers not behind an ELB (such as development servers)

sed -i 's/^RewriteCond %{HTTP:X-Forwarded-Proto}/#RewriteCond %{HTTP:X-Forwarded-Proto}/g' .htaccess 
sed -i 's/^#RewriteCond %{HTTPS}/RewriteCond %{HTTPS}/g' .htaccess

echo '.htaccess has been edited to work without an ELB.'
echo 'DO NOT COMMIT THIS CHANGE. Find "--assume-unchanged" in "man git update-index" for help towards that end.'
