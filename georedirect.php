<?php

$COUNTRY_NAMES = array(
        "Andorra",
		"Albania", "Austria", "Belgium", "Bulgaria",
		"Belarus", "Switzerland",
		"Cyprus", "Czech Republic", "Germany", 
        "Denmark", "Estonia",
		"Spain", "Finland",
		"Faroe Islands",
        "France", "United Kingdom",
        "Gibraltar",
		"Greece",
        "Croatia", "Hungary", "Ireland",
        "Italy", 
        "Liechtenstein", "Lithuania", "Luxembourg",
        "Latvia", "Macedonia",
        "Malta",
        "Netherlands", "Norway",
        "Poland", "Portugal", "Romania",
        "Sweden", "Slovenia", "Switzerland",
        "Slovakia", "San Marino", "Turkey", "Ukraine",
        "Holy See (Vatican City State)", "Serbia", "Montenegro", "Guernsey","Isle of Man","Jersey","Saint Barthelemy","Saint Martin"
        );

if (array_key_exists('test', $_GET) && $_GET['test']) {
	print_r($_COOKIE);
	print_r($_SERVER);
}

header("Cache-Control: private, must-revalidate, max-age=0");
header("Expires: Thu, 01 Jan 1970 00:00:00");

if (!isset($_GET['ip'])) {
    $_COOKIE = array();
}

$out = geolocation((isset($_GET['ip'])) ? $_GET['ip'] : '', $COUNTRY_NAMES);

if (array_key_exists('test', $_GET) && $_GET['test']) {
	die();
}


header("Location: $out");

function geolocation($my_ip_address, $COUNTRY_NAMES){
	if (array_key_exists('cookietest', $_COOKIE)) {

		try { 
		    
            require_once "Net/GeoIP.php";
            $geoip = Net_GeoIP::getInstance("/usr/share/php/data/Net_GeoIP/data/GeoIP.dat", Net_GeoIP::SHARED_MEMORY);
    		$c_name = $geoip->lookupCountryName($my_ip_address);
			$c_code = $geoip->lookupCountryCode($my_ip_address);

			if ($c_code== 'US' || in_array($c_name,$COUNTRY_NAMES)) {
				$addme = $c_code;
			} else {
				$addme = 'ROW';
			}
			$cookie_value = $my_ip_address . '&' . $addme; 
		} catch (Exception $e) {
		    $cookie_value = 'NOIP&US';
		}
		
		if (array_key_exists('test', $_GET) && $_GET['test']) {
			print_r("<br><br>Country = $c_name, Code = $c_code");
			exit();
		}
		
		$dom_r = array_reverse(explode(".", $_SERVER['HTTP_HOST']));
		$domain_for_cookie = ".".$dom_r[1].".".$dom_r[0];
	    $domain_for_cookie = preg_replace("/:.*/", "", $domain_for_cookie);
		//setcookie("UserGeo", $cookie_value, time() + 60*60*24*365, '/', $domain_for_cookie);
		header("Set-Cookie: UserGeo=$cookie_value; path=/; domain=$domain_for_cookie; expires=".gmstrftime("%A, %d-%b-%Y %H:%M:%S GMT",time()+60*60*24*365));
		$out = $_GET['url'];
		if ($_GET['qs']) $out .= "?" . $_GET['qs'];
	} else {
		$out = $_GET['url']."?cookies=disabled";
		if ($_GET['qs']) $out .= "&" . $_GET['qs'];
	}
	return $out;
}


?>

