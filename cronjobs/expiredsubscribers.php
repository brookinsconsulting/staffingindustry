<?php

// http://stackoverflow.com/questions/526556/how-to-flatten-a-multi-dimensional-array-to-simple-one-in-php
function flatten_array($array, $preserve_keys = 0, &$out = array()) {
    # Flatten a multidimensional array to one dimension, optionally preserving keys.
    #
    # $array - the array to flatten
    # $preserve_keys - 0 (default) to not preserve keys, 1 to preserve string keys only, 2 to preserve all keys
    # $out - internal use argument for recursion
    foreach($array as $key => $child)
        if(is_array($child))
            $out = flatten_array($child, $preserve_keys, $out);
        elseif($preserve_keys + is_string($key) > 1)
            $out[$key] = $child;
        else
            $out[] = $child;
    return $out;
}

$NARole = eZRole::fetch(23);
$NARole2 = eZRole::fetch(192);
$NARole3 = eZRole::fetch(197);

$WERole = eZRole::fetch(25);
$WERole2 = eZRole::fetch(195);
$WERole3 = eZRole::fetch(199);

$NAUsers = array_merge($NARole->fetchUserByRole(), $NARole2->fetchUserByRole(), $NARole3->fetchUserByRole());
$WEUsers = array_merge($WERole->fetchUserByRole(), $WERole2->fetchUserByRole(), $WERole3->fetchUserByRole());

$CCWPUsers = eZContentObjectTreeNode::subTreeByNodeID(array('Limitation' => array()), 95845);

$db = eZDB::instance();
$db->begin();

foreach (array_merge($NAUsers, $WEUsers) as $user) {
	$userObject = $user['user_object'];
	$userNode = $userObject->mainNode();
	$userID = $userObject->attribute('id');

	// per david's advice, some users may not be nodes due to a bad import script once run; skip them
	if (!is_a($userNode, 'eZContentObjectTreeNode')) continue;

	// we only want user objects
	if (in_array($userNode->attribute('class_identifier'), array('user', 'client'))) { 
		$subscriptions = $userNode->subTree(array(
			'Limitation' => array(),
			'Depth' => 1,
			'AttributeFilter' => array(
				'or',
				array('subscription/name', 'like', 'North American *'),
				array('subscription/name', 'like', 'Western European *')
			)
		));
		foreach ($subscriptions as $subscription) {
			$subData = $subscription->dataMap();
			$endDate = (int) $subData['end_date']->content()->timestamp();
			
			$isExpired = (time() > ($endDate + 86400));

			$hasNA = in_array($userObject->ID, array_merge(flatten_array($NARole->fetchUserID()), flatten_array($NARole2->fetchUserID()), flatten_array($NARole3->fetchUserID()) ));
			$isNA = (strpos($subscription->attribute('name'), 'North American') !== false);

			$hasWE = in_array($userObject->ID, array_merge(flatten_array($WERole->fetchUserID()), flatten_array($WERole2->fetchUserID()), flatten_array($WERole3->fetchUserID()) ));
			$isWE = (strpos($subscription->attribute('name'), 'Western European') !== false);

			if ($isExpired && $hasNA && $isNA) {
				echo $userNode->attribute('name') . " contains " . $subscription->attribute('name') . " expired " . date('Y-m-d', $endDate) . ", removing " . $NARole->Name . "\n";
				$NARole->removeUserAssignment($userID);
				$NARole2->removeUserAssignment($userID);
				$NARole3->removeUserAssignment($userID);
			} else if ($isExpired && $hasWE && $isWE) {
				echo $userNode->attribute('name') . " contains " . $subscription->attribute('name') . " expired " . date('Y-m-d', $endDate) . ", removing " . $WERole->Name . "\n";
				$WERole->removeUserAssignment($userID);
				$WERole2->removeUserAssignment($userID);
				$WERole3->removeUserAssignment($userID);
			}
		}
	}
}

foreach ($CCWPUsers as $user_node) {
	
	$userObject = $user_node->object();
	$userNode = $userObject->mainNode();
	$userID = $userObject->attribute('id');

	// per david's advice, some users may not be nodes due to a bad import script once run; skip them
	if (!is_a($userNode, 'eZContentObjectTreeNode')) continue;
	
	if (in_array($userNode->attribute('class_identifier'), array('user', 'client'))) { 
		$subscriptions = $userNode->subTree(array(
			'Limitation' => array(),
			'Depth' => 1,
			'ClassFilterType' => 'include',
			'ClassFilterArray' => array('certification_subscription')
		));
		foreach ($subscriptions as $subscription) {
			$subData = $subscription->dataMap();
			$endDate = (int) $subData['end_date']->content()->timestamp();
		
			$isExpired = (time() > ($endDate + 86400));
		
			if ($isExpired) {
				echo $userNode->attribute('name') . " contains " . $subscription->attribute('name') . " expired " . date('Y-m-d', $endDate) . ", removing locatoin" . "\n";
			
				$operationResult = eZOperationHandler::execute( 'content',
	                                                            'removelocation', array( 'node_list' => array($user_node->attribute('node_id')) ),
	                                                            null,
	                                                            true );
			
				//$CCWPRole->removeUserAssignment($userID);
			
			}
		}
	}
	
}

$db->commit();

?>
